(function(window, undefined) {

  var jimLinks = {
    "4858ff74-4122-47c9-ab1c-40d647c6a0ac" : {
      "Button_1" : [
        "625ebf3c-a1f2-449e-87e1-fe261600c091"
      ]
    },
    "d12245cc-1680-458d-89dd-4f0d7fb22724" : {
      "Button_1" : [
        "4858ff74-4122-47c9-ab1c-40d647c6a0ac"
      ]
    },
    "bddac790-31a8-454e-809f-089b9fab475d" : {
      "Button_3" : [
        "d12245cc-1680-458d-89dd-4f0d7fb22724"
      ]
    },
    "471731c7-5bf7-4e14-8c27-b6b93d89d694" : {
      "Button_2" : [
        "58d4745f-40ba-47e3-a9d3-0057e8295c14"
      ],
      "Button_3" : [
        "4858ff74-4122-47c9-ab1c-40d647c6a0ac"
      ]
    },
    "58d4745f-40ba-47e3-a9d3-0057e8295c14" : {
      "Button_2" : [
        "bddac790-31a8-454e-809f-089b9fab475d"
      ],
      "Button_3" : [
        "4858ff74-4122-47c9-ab1c-40d647c6a0ac"
      ]
    },
    "625ebf3c-a1f2-449e-87e1-fe261600c091" : {
      "Button_2" : [
        "471731c7-5bf7-4e14-8c27-b6b93d89d694"
      ],
      "Button_3" : [
        "4858ff74-4122-47c9-ab1c-40d647c6a0ac"
      ]
    }    
  }

  window.jimLinks = jimLinks;
})(window);