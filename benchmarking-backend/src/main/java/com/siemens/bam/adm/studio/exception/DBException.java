/**
 * DBException class
 */
package com.siemens.bam.adm.studio.exception;

public class DBException extends Exception{
	
	private static final long serialVersionUID = 2L;

	//Wrapping the exception and sending it to the top level Exception class
	public DBException(String message, Exception e)
	{
		super(message,e);
	}
	public DBException(String message) 
    {
		super(message);
    }
    
    public DBException()
    {
    	super();
    }

    public DBException(String message, Throwable cause)
    {
    	super(message,cause);
    }
    
    public DBException(Throwable cause)
    {
    	super(cause);
    }
	
}
