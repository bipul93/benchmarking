package com.siemens.bam.adm.studio.utils;

public class DatabaseUtils {

	// Function to get Cassandra Native Key
	public static String[] getCQLKey(String rowKey)
	{
		String[] cqlKey = {rowKey};
		if(rowKey.contains(":"))
			cqlKey = rowKey.split(":");
		return cqlKey;
	}

}
