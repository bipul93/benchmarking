/**
 *
 */
package com.siemens.bam.adm.studio.utils;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.siemens.bam.adm.studio.exception.SchemaException;

/**
 *
 * Class to hold all the properties configured for a client
 *
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class ClientProperties {

	final static Logger logger = LoggerFactory.getLogger(ClientProperties.class.getName());

	public static final Properties prop = new Properties();

	private String operation;
	private String mode;
	private Map<String, List<String>> metricList;
	private String queryFilePath;
	private String command;
	private long startTime;
	private long endTime;


	private static String DEFAULT_COMMAND = "";
	private static String DEFAULT_NUMBER_OF_UNIQUE_KEYS = "10";
	private static String DEFAULT_NUMBER_OF_RECORDS = "-1";
	private static String DEFAULT_BATCH_SIZE = "10";
	private static String DEFAULT_NUMBER_OF_THREADS = "1";
	private static String DEFAULT_METRIC_STEP = "1";
	private static String DEFAULT_METRIC_FUNCTION = "avg";
	private static String DEFAULT_REPORT_SUMMARY_RATE = "10";
	private static String DEFAULT_PAGINATION_LIMIT = "10";

	public ClientProperties(Properties configurationFileName) throws Exception {
		logger.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
		prop.clear();
		prop.putAll(configurationFileName);
		logger.info("Property  is loaded.");
	}

	public ClientProperties(String configurationFileName) throws Exception {

		prop.load(new FileInputStream(configurationFileName));
		if (isMetricsCollectionRequired()) {
			setMetricFilePath(prop.getProperty("MetricFilePath"));
		}
		logger.info("Property file in path " + configurationFileName + " is loaded.");
	}

	public ClientProperties() {
	}

	/*
	 * Client Properties - Common Parameters
	 *
	 */

	/**
	 * Returns the unique value used to identify a client
	 *
	 * @return
	 */
	public String getClientId() {
		if (prop.getProperty("ClientId") == null || prop.getProperty("ClientId").length() == 0) {
			logger.warn("Client Id not set by user. System exiting");
			throw new RuntimeException("Client ID not set by user. Please set the client ID. System exiting");

		}
		return prop.getProperty("ClientId");
	}

	/** get server address **/
	public String getServerAddress(){
		if (prop.getProperty("serverIP") == null || prop.getProperty("serverIP").length() == 0) {
			logger.warn("Server Ip not set by user. System exiting");
			throw new RuntimeException("Server Ip not set by user. Please set the IP. System exiting");

		}
		logger.info(prop.getProperty("serverIP"));
		return prop.getProperty("serverIP");
	}

	/** get server port **/
	public String getServerPort(){
		if (prop.getProperty("serverPort") == null || prop.getProperty("serverPort").length() == 0) {
			logger.warn("Server Port not set by user. System exiting");
			throw new RuntimeException("Server Port not set by user. Please set the port number. System exiting");

		}
		logger.info(prop.getProperty("serverPort"));
		return prop.getProperty("serverPort");
	}
	/**
	 * Returns the operation that the client is configured to execute
	 *
	 * @return
	 */
	public String getOperation() {
		return operation;
	}

	/**
	 * Returns the number of threads to be spawned to execute a workload Default
	 * Value: 10
	 *
	 * @return
	 */
	public int getNumberOfThreads() {

		return Integer.parseInt(prop.getProperty("NumberOfThreads", DEFAULT_NUMBER_OF_THREADS));
	}

	/**
	 * Returns the Data store name
	 *
	 * @return
	 * @throws SchemaException
	 */
	public String getDataStoreName() throws SchemaException {

		return prop.getProperty("DatastoreName");
	}

	/**
	 * Returns the host IPs of the data store cluster
	 *
	 * @return
	 * @throws Exception
	 */
	public String getHostIP() throws Exception {
		return prop.getProperty("Hosts");
	}

	/**
	 * Returns the path to schema file
	 *
	 * @return
	 */
	public String getSchemaPath() {

		return prop.getProperty("SchemaPath");
	}

	/*
	 * In case there is a need to execute operations on different tables then
	 * this method should be modified to select which table the user needs
	 */
	/**
	 * Returns the name of the table on which the benchmarking is done If no
	 * name is provided the code exits
	 *
	 * @return
	 * @throws Exception
	 */
	public String getTableName() throws Exception {
		String tableName = null;
		if (prop.getProperty("TableToBeUsed") == null || prop.getProperty("TableToBeUsed").length() == 0) {
			logger.error(
					"The name of the table on which the operation is to be performed not provided. Please provide the name of the table. Program will exit now.");
			throw new Exception(
					"The name of the table on which the operation is to be performed not provided. Please provide the name of the table. Program will exit now.");
		}
		try {
			tableName = prop.getProperty("TableToBeUsed").split("\\.")[1];
		} catch (Exception e) {
			throw new Exception(
					"In ClientProperties::getTableName Error while parsing string to get tablename. Please check Properties file!");
		}

		return tableName;
	}

	/**
	 * Returns the name of the database on which the benchmarking is done If no
	 * name is provided the code exits
	 *
	 * @return
	 * @throws Exception
	 */
	public String getDatabaseName() throws Exception {
		String dbName = null;
		if (prop.getProperty("TableToBeUsed") == null || prop.getProperty("TableToBeUsed").length() == 0) {
			logger.error(
					"The name of the table on which the operation is to be performed not provided. Please provide the name of the table. Program will exit now.");
			throw new Exception(
					"The name of the table on which the operation is to be performed not provided. Please provide the name of the table. Program will exit now.");
		}
		try {
			dbName = prop.getProperty("TableToBeUsed").split("\\.")[0];
		} catch (Exception e) {
			throw new Exception(
					"In ClientProperties::getDBName Error while parsing string to get database name. Please check Properties file!");
		}

		return dbName;
	}

	/**
	 * Returns true if the data to be inserted/queried from is in the descending
	 * order of time Default Value: false
	 *
	 * @return
	 */
	public static boolean isTableReversedOnTime() {
		if (prop.getProperty("IsTableReversedOnTime").equals("true")) {
			logger.debug("Reverse the timestamp while generating data ");
			return true;
		}
		return false;
	}

	/**
	 * Sets the operation to be performed by the client
	 *
	 * @param operation
	 */
	public void setOperation(String operation) {
		Preconditions.checkArgument(
				(operation.equalsIgnoreCase("Write") || operation.equalsIgnoreCase("Read")
						|| operation.equalsIgnoreCase("CollectMetrics") || operation.equalsIgnoreCase("TestConnection")),
				"Invalid operation specified! Please specify Write or Read or CollectMetrics.");
		this.operation = operation.toUpperCase();
		prop.setProperty("operation", operation);
	}

	/**
	 * validates the common parameters used by Read and Write operations
	 *
	 * @throws SchemaException
	 */
	public void validateCommonParameters() throws SchemaException {

		Preconditions.checkArgument(
				!(prop.getProperty("DatastoreName") == null || prop.getProperty("DatastoreName").length() == 0),
				"Please specify the DatastoreName in the Property file");

		Preconditions.checkArgument(!(prop.getProperty("SchemaPath").length() == 0),
				"Property \"schemaPath\" not provided. Please provide it and re-try");

		Preconditions.checkArgument(!(prop.getProperty("Hosts") == null || prop.getProperty("Hosts").length() == 0),
				"Host IP not provided by user. Please provide the hostIP in Property.conf");

	}

	/**
	 * validates whether the number of threads set is in the right format and
	 * within limits. If not then the default number of threads will be used
	 * Default Value: 1
	 */
	public void validateAndSetNumberOfThreads() {
		if (prop.getProperty("NumberOfThreads") == null) {
			logger.warn("Number of threads not set by user. Default number of threads = " + DEFAULT_NUMBER_OF_THREADS
					+ " will be used.");
			prop.setProperty("NumberOfThreads", DEFAULT_NUMBER_OF_THREADS);
		} else if (prop.getProperty("NumberOfThreads").length() == 0
				|| Integer.parseInt(prop.getProperty("NumberOfThreads")) <= 0) {

			logger.warn("Number of threads set by user is negative. Default number of threads = "
					+ DEFAULT_NUMBER_OF_THREADS + " will be used.");

			prop.setProperty("NumberOfThreads", DEFAULT_NUMBER_OF_THREADS);
		}

	}

	/*
	 * Write parameters
	 *
	 */

	/**
	 * Returns the number of records to be inserted into a datastore Default
	 * Value: 10000
	 *
	 * @return
	 * @throws Exception
	 */
	public int getNumberOfRecords() throws Exception {

		return Integer.parseInt(prop.getProperty("NumberOfRecords", DEFAULT_NUMBER_OF_RECORDS));

	}

	/**
	 * Returns the number of unique values to be generated Default Value: 10
	 *
	 * @return
	 */
	public String getnumberOfKeysToBeGenerated() {
		return prop.getProperty("NumberOfKeysToBeGenerated", DEFAULT_NUMBER_OF_UNIQUE_KEYS);
	}

	/**
	 * Checks if the duration of insert is set Default Value: false
	 *
	 * @return boolean
	 * @throws NumberFormatException
	 * @throws Exception
	 */
	public boolean isInsertDurationSet() throws NumberFormatException, Exception {

		if (getinsertDurationInMins() != null && Integer.parseInt(getinsertDurationInMins()) > 0) {
			return true;
		} else
			return false;
	}

	/**
	 * Returns the duration for which inserts happen into a datastore
	 *
	 * @return String
	 * @throws Exception
	 */
	public String getinsertDurationInMins() throws Exception {
		if (prop.getProperty("InsertDurationInMins") == null || prop.getProperty("InsertDurationInMins").length() == 0
				|| Integer.parseInt(prop.getProperty("InsertDurationInMins")) < 0) {

			return null;

		} else {

			return prop.getProperty("InsertDurationInMins");
		}
	}

	/**
	 * Checks if batch inserts should be done Default Value: false
	 *
	 * @return String
	 */
	public String isBatchInserts() {

		if (prop.getProperty("InBatch") == null || prop.getProperty("InBatch").length() == 0) {
			return "false";
		}

		return prop.getProperty("InBatch", "false");
	}

	/**
	 * Returns the size of each batch of records to be inserted Default Value:
	 * 10
	 *
	 * @return
	 */
	public int getBatchSize() {

		if (prop.getProperty("BatchSize") == null) {
			logger.warn("Batch Size not set by user. Default batch size of " + DEFAULT_BATCH_SIZE + " will be used.");

		} else if (prop.getProperty("BatchSize").length() == 0 || Integer.parseInt(prop.getProperty("BatchSize")) < 0) {
			logger.warn("Batch Size set by user is negative. Default batch size of " + DEFAULT_BATCH_SIZE
					+ " will be used.");
			return Integer.parseInt(DEFAULT_BATCH_SIZE);
		}

		return Integer.parseInt(prop.getProperty("BatchSize", DEFAULT_BATCH_SIZE));
	}

	/**
	 * Checks if throttling of records is required Default Value: false
	 *
	 * @return
	 */
	public boolean isThrottlingRequired() {
		if (getTargetThroughput() != null) {

			return true;
		} else
			return false;
	}

	/**
	 * Returns the target throughput of writes
	 *
	 * @return
	 */
	public String getTargetThroughput() {
		if (prop.getProperty("TargetThroughput") == null || prop.getProperty("TargetThroughput").length() == 0
				|| Double.parseDouble(prop.getProperty("TargetThroughput")) < 0) {

			return null;
		}
		return prop.getProperty("TargetThroughput");
	}

	/**
	 * Set to true if every record in a batch should have the same key Specific
	 * to Write operation
	 *
	 * @return
	 */
	public String isUniqueKeyPerBatch() {

		return prop.getProperty("EnsureUniqueKeyPerBatch", "false");

	}

	/**
	 * Validate if the number of keys to be generated is given in the correct
	 * format
	 */
	public void validateAndSetNumberOfKeysToBeGenerated() {
		if (prop.getProperty("NumberOfKeysToBeGenerated") == null
				|| prop.getProperty("NumberOfKeysToBeGenerated").length() == 0
				|| Integer.parseInt(prop.getProperty("NumberOfKeysToBeGenerated")) < 0) {

			logger.warn(
					"Number Of unique keys to be generated set by user is illegal. The number of unique keys should be positive. "
							+ "The default will be used : " + DEFAULT_NUMBER_OF_UNIQUE_KEYS);

			prop.setProperty("NumberOfKeysToBeGenerated", DEFAULT_NUMBER_OF_UNIQUE_KEYS);
		}

	}

	/**
	 * Validate if the number of records to be inserted is given in the correct
	 * format
	 */
	public void validateAndSetNumberOfRecords() throws Exception {
		if (prop.getProperty("NumberOfRecords") == null || prop.getProperty("NumberOfRecords").length() == 0
				|| Integer.parseInt(prop.getProperty("NumberOfRecords")) < 0) {

			if (!isInsertDurationSet()) {

				throw new Exception(
						"Number of records to be inserted and duration of insert not provided by user. Please provide one");
			}

			prop.setProperty("NumberOfRecords", DEFAULT_NUMBER_OF_RECORDS);
		} else {

			if (isInsertDurationSet()) {
				logger.error(
						"Both number of records to be inserted and duration of insert is provided by user. Please provide ONLY one");
				throw new Exception(
						"Both number of records to be inserted and duration of insert is provided by user. Please provide ONLY one");
			}

		}

	}

	/*
	 * Read parameters
	 *
	 */

	/**
	 * Returns the mode of read operation - Interactive or Batch
	 *
	 * @return
	 */
	public String getMode() {
		return mode;
	}

	/**
	 * Returns the path of file containing queries to be executed (if Read mode
	 * is Batch)
	 *
	 * @return String
	 */
	public String getQueryFilePath() {
		return queryFilePath;
	}

	/**
	 * Return pagination Limit for the query results Default Value: 10
	 *
	 * @return int
	 */
	public static int getPaginationLimit() {
		if (prop.getProperty("PaginationRowLimit").length() == 0) {
			logger.warn("Property \"PaginationRowLimit\" not provided, using the default option of "
					+ DEFAULT_PAGINATION_LIMIT);
		} else if (Integer.parseInt(prop.getProperty("PaginationRowLimit")) < 0) {
			logger.warn("Property \"PaginationRowLimit\" provided is negative, using the default option of"
					+ DEFAULT_PAGINATION_LIMIT);
		}
		return Integer.parseInt(prop.getProperty("PaginationRowLimit", DEFAULT_PAGINATION_LIMIT));

	}

	/**
	 * Returns The file path to where the rowkeys needs to be saved
	 *
	 * @return String
	 */

	public static String getRowkeyFilePath() {
		if (prop.getProperty("RowKeyPath").length() == 0) {
			logger.error("Property \"RowKeyPath\" not provided. Please provide it and retry");
			System.exit(0);
		}
		return prop.getProperty("RowKeyPath");
	}


	//sets Server Address
	public void setServerAddress(String address){
		logger.info(address);
		prop.setProperty("serverIP", address);
	}

	//sets Server Port
	public void setServerPort(String portNumber){
		logger.info(portNumber);
		prop.setProperty("serverPort", portNumber);
	}

	/**
	 * Sets the mode of read operation
	 *
	 * @param mode
	 */
	public void setMode(String mode) {
		Preconditions.checkArgument((mode.equalsIgnoreCase("Interactive") || mode.equalsIgnoreCase("Batch") || mode.equalsIgnoreCase("Web")),
				"Invalid query input mode specified! Please specify Interactive or Batch or Web.");
		this.mode = mode;
		prop.setProperty("mode", mode);
	}

	/**
	 * Sets the path to a file that contains queries to be executed (if Read
	 * mode is Batch)
	 *
	 * @param queryFilePath
	 * @throws Exception
	 */
	public void setQueryFilePath(String queryFilePath) throws Exception {

		this.queryFilePath = queryFilePath;
		prop.setProperty("queryFilePath", queryFilePath);
	}

	/*
	 * Metric Collection parameters
	 *
	 */

	/**
	 * Checks if metric collection from datastore is enabled Default Value:
	 * false
	 *
	 * @return
	 */
	public boolean isMetricsCollectionRequired() {

		return Boolean.parseBoolean(prop.getProperty("EnableMetricsCollection", "false"));
	}

	/**
	 * Returns the types of client metric reporters - console, graphite, CSV
	 *
	 * @return
	 */
	public String getMetricReporter() {
		if (prop.getProperty("MetricReporter") == null || prop.getProperty("MetricReporter").length() == 0) {
			logger.warn("Type of Client Reporter not specified.");
		}
		return prop.getProperty("MetricReporter");
	}

	/** return command to be executed in Web mode */
	public String getCommand() {
		if (prop.getProperty("command") == null || prop.getProperty("Command").length() == 0) {
			logger.warn("The type of  metric function is not set by the user in the Property.conf. "
					+ "Default value : " + DEFAULT_COMMAND + " will be used.");
			return DEFAULT_COMMAND;

		}
		return prop.getProperty("command", DEFAULT_COMMAND);
	}

	/**
	 * Returns the list of metrics to be collected from data store
	 *
	 * @return Map<String, List<String>>
	 */
	public Map<String, List<String>> getMetricList() {
		return metricList;
	}

	/**
	 * Returns the start time for metric collection
	 *
	 * @return long
	 */
	public long getStartTimeForMetricCollection() {
		return startTime;
	}

	/**
	 * Returns the end time for metric collection
	 *
	 * @return long
	 */
	public long getEndTimeForMetricCollection() {
		return endTime;
	}

	/**
	 * Returns the step at which each metric has to be collected from datastore
	 *
	 * @return String
	 */
	public String getMetricStep() {
		if (prop.getProperty("Metric.Step") == null || prop.getProperty("Metric.Step").length() == 0) {
			logger.warn("The MetricStep value is not set by the user in the Property.conf. " + "Default value : "
					+ DEFAULT_METRIC_STEP + " will be used.");

			return DEFAULT_METRIC_STEP;
		}
		return prop.getProperty("Metric.Step", DEFAULT_METRIC_STEP);
	}

	/**
	 * Returns the function to be applied on metrics collected from datastore
	 *
	 * @return String
	 */
	public String getMetricFunction() {
		if (prop.getProperty("Metric.Function") == null || prop.getProperty("Metric.Function").length() == 0) {
			logger.warn("The type of  metric function is not set by the user in the Property.conf. "
					+ "Default value : " + DEFAULT_METRIC_FUNCTION + " will be used.");
			return DEFAULT_METRIC_FUNCTION;

		}
		return prop.getProperty("Metric.Function", DEFAULT_METRIC_FUNCTION);
	}

	/**
	 * Returns the rate at which client metrics should be reported via the
	 * metric reporters Default Value: 10
	 *
	 * @return
	 */
	public String getReportSummaryRate() {

		if (prop.getProperty("ReportSummaryRate") == null) {
			logger.warn("The rate at which metrics need to be collected is not set by the user in the Property.conf. "
					+ "Default value : " + DEFAULT_REPORT_SUMMARY_RATE + " will be used.");

		} else if (prop.getProperty("ReportSummaryRate").length() == 0
				|| Integer.parseInt(prop.getProperty("reportSummaryRate", DEFAULT_REPORT_SUMMARY_RATE)) < 0) {
			logger.warn("The rate at which metrics need to be collected set by the user is negative."
					+ "Default value : " + DEFAULT_REPORT_SUMMARY_RATE + " will be used.");
			return DEFAULT_REPORT_SUMMARY_RATE;
		}

		return prop.getProperty("ReportSummaryRate", DEFAULT_REPORT_SUMMARY_RATE);
	}

	/**
	 * Returns the IP Address of Graphite server Client metrics will not be
	 * reported to Graphite if not provided by user
	 *
	 * @return String
	 */
	public String getGraphiteIP() {

		if (prop.getProperty("GraphiteIP") == null || prop.getProperty("GraphiteIP").length() == 0) {
			logger.warn("Graphite IP not set by user. Metrics cannot be collected! ");
			throw new RuntimeException("Graphite IP not set by user. Metrics cannot be collected! System exiting");

		}
		return prop.getProperty("GraphiteIP");
	}

	/**
	 * Returns the port of Graphite server Client metrics will not be reported
	 * to Graphite if not provided by user
	 *
	 * @return
	 */
	public int getGraphitePort() {
		if (prop.getProperty("GraphitePort") == null || prop.getProperty("GraphitePort").length() == 0) {
			logger.warn("Graphite Port not set by user. Metrics cannot be collected! System exiting");
			throw new RuntimeException("Graphite Port not set by user. Metrics cannot be collected! System exiting");

		}
		return Integer.parseInt(prop.getProperty("GraphitePort"));
	}

	/**
	 * Returns the OpenTSDB Server IP
	 * @return String
	 */
	public String getOpenTSDBIP() {

		if (prop.getProperty("Hbase.OpentsdbIP") == null || prop.getProperty("Hbase.OpentsdbIP").length() == 0) {
			logger.warn("OpenTSDB IP not set by user. Metrics cannot be collected!");

		}
		return prop.getProperty("Hbase.OpentsdbIP");
	}

	/**
	 * Returns the OpenTSDB port
	 * @return String
	 */
	public String getOpenTSDBPort() {

		if (prop.getProperty("Hbase.OpentsdbPort") == null || prop.getProperty("Hbase.OpentsdbPort").length() == 0) {
			logger.warn("OpenTSDB Port not set by user. Metrics cannot be collected!");

		}
		return prop.getProperty("Hbase.OpentsdbPort");
	}

	/**
	 * Sets the start time for collecting metrics
	 *
	 * @param startTime
	 */
	public void setStartTimeForMetricCollection(String startTime) {
		this.startTime = DateFormattingUtils.getMillisFromString(startTime);
	}

	/**
	 * Sets the end time for collecting metrics
	 *
	 * @param startTime
	 */
	public void setEndTimeForMetricCollection(String endTime) {
		this.endTime = DateFormattingUtils.getMillisFromString(endTime);

	}

	/**
	 * Sets the command for Web mode
	 *
	 * @param command
	 * @throws Exception
	 */
	public void setCommand(String commandToBeExec) throws Exception {
		this.command = commandToBeExec;
		prop.setProperty("command",command);
	}

	/**
	 * Sets the file path to list of metrics
	 *
	 * @param metricList
	 * @throws Exception
	 */
	public void setMetricFilePath(String metricList) throws Exception {

		this.metricList = parseMetricList(metricList);
		prop.setProperty("metricList", metricList);
	}

	/*
	 * To parse the list of metrics and store in a Map
	 */
	private static Map<String, List<String>> parseMetricList(String metricList) throws Exception {

		Properties prop = new Properties();

		prop.load(new FileInputStream(metricList));

		Map<String, List<String>> dbMetrics = new HashMap<String, List<String>>();

		Enumeration<?> eProp = prop.propertyNames();

		while (eProp.hasMoreElements()) {
			String keyMetric = (String) eProp.nextElement();

			List<String> metricsList = new ArrayList<String>(Arrays.asList(prop.getProperty(keyMetric).split(",")));
			dbMetrics.put(keyMetric, metricsList);

		}
		return dbMetrics;
	}

	public static Properties getProp() {
		return prop;
	}

	public void setProperty(String propName, String value) {
		prop.setProperty(propName, value);
	}

}
