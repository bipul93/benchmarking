package com.siemens.bam.adm.studio.read.queryModel.queryParameters;

import java.util.ArrayList;

/**
 * Abstract Class that defines the structure of Query Parameters
 * @author Nisha Menon
 */

public abstract class AbstractQueryParams 
{
	private String queryName;
	private ArrayList<String> rowKeys;
	
	public AbstractQueryParams()
	{
		this.queryName = "";
		this.rowKeys = new ArrayList<String>(); // Will be an empty ArrayList for queries without rowkeys
	}
	
	public String getQueryName() {
		return queryName;
	}
	
	public void setQueryName(String queryName) {
		this.queryName = queryName;
	}
	
	public ArrayList<String> getRowKeys() {
		return rowKeys;
	}

	public void setRowKeys(ArrayList<String> rowKeys) {
		this.rowKeys = rowKeys;
	}
}
