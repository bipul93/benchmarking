package com.siemens.bam.adm.studio.schema;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * Enum of supported Data-types
 */
public enum DataTypes {
	DATETIME,
	UUID,
	BOOLEAN,
	TINYINT,
	SMALLINT,
	INT,
	BIGINT,
	NULLINT,
	FLOAT,
	DOUBLE,
	STRING,
	XML,
	DATE,
	BYTES,
	UNKNOWN, INETADDRESS;

	private static final Map<Integer, DataTypes> lookup = new HashMap<Integer, DataTypes>();

	static {
		for (DataTypes dt : EnumSet.allOf(DataTypes.class)) {
			lookup.put(dt.ordinal(), dt);
		}
	}

	public static DataTypes getDatatypeFromOrdinal(int ordinal) {
		return lookup.get(ordinal);
	}
};
