package com.siemens.bam.adm.studio.utils;

import java.util.Random;

public class RandomNumberUtil {
	private static final Random rand = new Random();
	private static final ThreadLocal<Random> rng = new ThreadLocal<Random>();
	
	//http://stackoverflow.com/questions/2546078/java-random-long-number-in-0-x-n-range
	public static long nextLong(long range) {
		 // error checking and 2^x checking removed for simplicity.
		   long bits, val;
		   do {
		      bits = (rand.nextLong() << 1) >>> 1;
		      val = bits % range;
		   } while (bits-val+(range-1) < 0L);
		   return val;
	}

	public static boolean nextBoolean() {
		return rand.nextBoolean();
	}

	public static short nextInt(int i) {
		// TODO Auto-generated method stub
		return (short) rand.nextInt(i);
	}

	public static float nextFloat() {
		
		return rand.nextFloat();
	}

	public static double nextDouble() {
		// TODO Auto-generated method stub
		return rand.nextDouble();
	}
}
