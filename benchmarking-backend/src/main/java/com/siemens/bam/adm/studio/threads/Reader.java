package com.siemens.bam.adm.studio.threads;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

public class Reader<T> implements Callable<T>
{
	private static final Logger logger = Logger.getLogger(Reader.class);
	private Environment env;
	private String rowkey;
	public T resultSet;
	public AbstractQuery query;
	public AbstractDatabaseDriver dbDriver;
	
	public Reader(Environment env, int threadid)
	{
		this.env = env;
		this.rowkey = env.getQueryObject().getQueryParams().getRowKeys().size() == 0 ?
				null : env.getQueryObject().getQueryParams().getRowKeys().get(threadid);
		this.query = env.getQueryObject();
		this.dbDriver = env.getDriver();
	}
	
	@SuppressWarnings("unchecked") 
	public T call() throws Exception 
	{
		logger.debug("Within the call method for the query.");
		logger.debug("Initializing the reads");
		dbDriver.initReads(env);
		logger.debug("Query is of type: " + query.getClass().getSimpleName());
		this.setResultSet((T) query.queryExecute(rowkey, dbDriver));	
		return (T)this.resultSet;	
	}
	
	
	public void setResultSet(T resultSet) 
	{
		this.resultSet = resultSet;
	}
	
}
