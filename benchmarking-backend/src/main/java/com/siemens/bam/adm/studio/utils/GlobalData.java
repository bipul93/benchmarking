
package com.siemens.bam.adm.studio.utils;

public class GlobalData {
  public static boolean webMode = false;
  public static boolean readInProgress = false;
  public static boolean writeInProgress = false;
  public static String webError = "";
  public static String queryResult;
}
