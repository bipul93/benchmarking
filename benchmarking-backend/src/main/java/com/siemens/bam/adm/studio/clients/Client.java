package com.siemens.bam.adm.studio.clients;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.siemens.bam.adm.studio.operation.IDBOperation;
import com.siemens.bam.adm.studio.operation.DBOperationFactory;
import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * Main class
 *
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */

public class Client {

	public static String serverIP = "";
	public static String serverPort = "";

	private static Logger logger = LoggerFactory.getLogger(Client.class.getName());

	public static void main(String[] args) throws Exception {
		/*
		 * Obtain the runtime arguments and set the values in client properties
		 */
		CommandLineParser parser = new BasicParser();
		CommandLine cmd;

		String configurationFileName = null;
		String operation = null;
		String mode = null;
		String queryFilePath = null;
		String startTime = null;
		String endTime = null;
		ClientProperties clientProp = null;
		Boolean testOperation = false;
		Options options = new Options();
		options.addOption("operation", true, "Type of operation to be performed [Read/Write/CollectMetrics] ");
		options.addOption("conf", true, "Path to configuration file ");
		options.addOption("mode", true, "Mode of input for read queries [Interactive/Batch/Web] ");
		options.addOption("command", true, " Command to be executed ");
		options.addOption("queryFilePath", true, "Path to read query input file ");
		options.addOption("startTime", true, "Start time for metric collection [yyyy-MM-dd'T'HH:mm:ss.SSS]");
		options.addOption("endTime", true, "End time for metric collection [yyyy-MM-dd'T'HH:mm:ss.SSS] ");
		options.addOption("serverIP", true, " IP Address of the server ");
		options.addOption("serverPort", true, " server Port ");

		try {
			cmd = parser.parse(options, args);
		} catch (ParseException pe) {
			System.out.println(pe.getMessage());
			HelpFormatter formatter = new HelpFormatter();
			formatter.printHelp("Help", options);
			return;
		}

		/*
		 * Check the type of operation and configuration file path
		 */

		Preconditions.checkArgument(cmd.hasOption("operation"),
				"The type of operation [Read/Write/CollectMetrics] must be specified.");
		operation = cmd.getOptionValue("operation");

		/*
		 * For Read Operation check if mode is specified
		 * If the mode is 'batch' ensure that the file path is specified
		 */
		if (operation.equalsIgnoreCase("read")) {
			Preconditions.checkArgument(cmd.hasOption("conf"), "Please specify the property file location");
			configurationFileName = cmd.getOptionValue("conf");

			try {
				clientProp = new ClientProperties(configurationFileName);
			} catch (Exception e) {
				logger.error(e.getMessage());
				System.exit(1);
			}
			clientProp.setOperation(operation);

			Preconditions.checkArgument(cmd.hasOption("mode"),
					"The mode of input for queries must be specified - [Interactive/Batch/Web].");
			mode = cmd.getOptionValue("mode");

			Preconditions.checkArgument(cmd.hasOption("serverIP") && cmd.hasOption("serverPort"),
			" Please specify the server IP Address and port number.");
			String serverIPAddress =  cmd.getOptionValue("serverIP");
			String serverPortNumber = cmd.getOptionValue("serverPort");

			clientProp.setServerAddress(serverIPAddress);
			clientProp.setServerPort(serverPortNumber);

			if (mode.equalsIgnoreCase("batch")) {
				Preconditions.checkArgument(cmd.hasOption("queryFilePath"),
						"Please provide the path to file with queries.");
				queryFilePath = cmd.getOptionValue("queryFilePath");
				clientProp.setQueryFilePath(queryFilePath);
			}else if(mode.equalsIgnoreCase("Web")){
				Preconditions.checkArgument(cmd.hasOption("command"), "Please provide the command to be executed.");
				String command = cmd.getOptionValue("command");
				clientProp.setCommand(command);
			}

			clientProp.setMode(mode);

		}else if (operation.equalsIgnoreCase("collectMetrics")) {
			Preconditions.checkArgument(cmd.hasOption("conf"), "Please specify the property file location");
			configurationFileName = cmd.getOptionValue("conf");

			try {
				clientProp = new ClientProperties(configurationFileName);
			} catch (Exception e) {
				logger.error(e.getMessage());
				System.exit(1);
			}
			clientProp.setOperation(operation);

			Preconditions.checkArgument(cmd.hasOption("startTime") && cmd.hasOption("endTime"),
					"Please specify the start and end time for collecting metrics");
			startTime = cmd.getOptionValue("startTime");
			endTime = cmd.getOptionValue("endTime");

			clientProp.setStartTimeForMetricCollection(startTime);
			clientProp.setEndTimeForMetricCollection(endTime);

		} else if (operation.equalsIgnoreCase("TestConnection")){
			
			//Preconditions.checkArgument(cmd.hasOption("conf"), "Please specify the property file location");
			//configurationFileName = cmd.getOptionValue("conf");
			Preconditions.checkArgument(cmd.hasOption("serverIP") && cmd.hasOption("serverPort"),
			" Please specify the server IP Address and port number.");
			serverIP =  cmd.getOptionValue("serverIP");
			serverPort = cmd.getOptionValue("serverPort");
			testOperation = true;
			//clientProp.setServerAddress(serverIPAddress);
			//clientProp.setServerPort(serverPortNumber);

		}else{

			Preconditions.checkArgument(cmd.hasOption("conf"), "Please specify the property file location");
			configurationFileName = cmd.getOptionValue("conf");

			try {
				clientProp = new ClientProperties(configurationFileName);
			} catch (Exception e) {
				logger.error(e.getMessage());
				System.exit(1);
			}
			clientProp.setOperation(operation);

			Preconditions.checkArgument(cmd.hasOption("serverIP") && cmd.hasOption("serverPort"),
			" Please specify the server IP Address and port number.");
			String serverIPAddress =  cmd.getOptionValue("serverIP");
			String serverPortNumber = cmd.getOptionValue("serverPort");

			clientProp.setServerAddress(serverIPAddress);
			clientProp.setServerPort(serverPortNumber);

		}

		//logger.info("[Operation] : " + clientProp.getOperation() + " will be performed.");
		/*
		 * Start the clients based on the passed inputs
		 */

		doClientOperation(clientProp,testOperation);

	}

	/**
	 * Method to start the client operation on the data store
	 *
	 * @param clientProp
	 */
	private static void doClientOperation(ClientProperties clientProp,Boolean testStatus) {
		IDBOperation operation;
		if(testStatus){
			try {
				operation = DBOperationFactory.getOperationInstance("testConnection",serverIP,serverPort);
				//operation.buildEnvironment(clientProp);
				operation.start();
			} catch (Exception e) {
				logger.error("System exiting due to error: ", e);
				System.exit(1);
			}
		}else{
			try {
				operation = DBOperationFactory.getOperationInstance(clientProp.getOperation(),null,null);
				operation.buildEnvironment(clientProp);
				operation.start();
			} catch (Exception e) {
				logger.error("System exiting due to error: ", e);
				System.exit(1);
			}
		}
	}
}
