package com.siemens.bam.adm.studio.dbdriver.cassandra;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * 
 * Class to hold all the properties configured to connect to a Cassandra cluster
 * 
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class CassandraProperties {

	private static Properties prop = new Properties();
	final static Logger logger = LoggerFactory.getLogger(CassandraProperties.class.getName());

	public CassandraProperties() {
		
		prop = ClientProperties.getProp();
		
		if (prop.getProperty("Cassandra.NativePort").length() == 0
				|| (prop.getProperty("Cassandra.NativePort") == null)) {
			logger.warn("Property \"Cassandra.NativePort\" not provided, using the default option of "
					+ CassandraConstants.DEFAULT_CASSANDRA_BINARY_PORT);
			prop.setProperty("Cassandra.NativePort", CassandraConstants.DEFAULT_CASSANDRA_BINARY_PORT);
		}

		validateAndSetCassandraConsistencyLevel();

	}

	/**
	 * Returns the port on which Cassandra is running
	 * Default Value: 9042
	 * @return
	 */
	public int getBinaryPort() {
		return Integer
				.parseInt(prop.getProperty("Cassandra.NativePort", CassandraConstants.DEFAULT_CASSANDRA_BINARY_PORT));
	}

	/**
	 * Returns the consistency level defined for an operation
	 * Default Value: ONE
	 * @return
	 */
	public String getCassandraConsistencyLevel() {
		if (prop.getProperty("operation").equalsIgnoreCase("Write")) {
			return prop.getProperty("Cassandra.WriteConsistencyLevel",
					CassandraConstants.DEFAULT_CASSANDRA_WRITE_CONSISTENCY_LEVEL);
		} else if (prop.getProperty("operation").equalsIgnoreCase("Read")) {
			return prop.getProperty("Cassandra.ReadConsistencyLevel",
					CassandraConstants.DEFAULT_CASSANDRA_READ_CONSISTENCY_LEVEL);
		}
		return null;
	}

	/**
	 * Returns the name of Cassandra datacenter
	 * Default Value: all  
	 * @return
	 */
	public String getCassandraDataCenter() {

		return prop.getProperty("Cassandra.Datacenter", "all");
	}

	/**
	 * Returns Opscenter IP
	 * 
	 * @return
	 */
	public String getOpscenterIP() {
		if (prop.getProperty("Cassandra.OpscenterIP") == null
				|| prop.getProperty("Cassandra.OpscenterIP").length() == 0) {

			logger.warn(
					"Cassandra Opscenter IP not provided by user. Metrics will not be collected at the end of the run");

		}
		return prop.getProperty("Cassandra.OpscenterIP");
	}

	/**
	 * Returns the port on which Opscenter is running
	 * Default Value: 8888
	 * @return
	 */
	public String getOpscenterPort() {
		if (prop.getProperty("Cassandra.OpscenterPort") == null
				|| prop.getProperty("Cassandra.OpscenterPort").length() == 0) {

			logger.warn("Cassandra Opscenter port not provided by user. Default port 8888 will be used.");

		}
		return prop.getProperty("Cassandra.OpscenterPort", "8888");
	}

	/**
	 * Returns the cluster id of Cassandra cluster
	 * Default Value: local
	 * @return
	 */
	public String getCassandraClusterId() {
		if (prop.getProperty("Cassandra.ClusterID") == null || prop.getProperty("Cassandra.ClusterID").length() == 0) {

			logger.warn(
					"Cassandra cluster id not provided by user. Default value \"local\" will be used ");

		}
		return prop.getProperty("Cassandra.ClusterID", "local");
	}

	
	/**
	 * Returns the thrift port
	 * @return
	 */
	public int getThriftPort() {
		if (prop.getProperty("Cassandra.ThriftPort").length() == 0) {
			logger.warn("Property \"Cassandra.ThriftPort\" not provided, using the default option of "
					+ CassandraConstants.DEFAULT_CASSANDRA_THRIFT_PORT);
		}
		return Integer
				.parseInt(prop.getProperty("Cassandra.ThriftPort", CassandraConstants.DEFAULT_CASSANDRA_THRIFT_PORT));
	}

	/**
	 * Returns the name of Cassandra cluster
	 * @return String
	 */
	public String getCassandraClusterName() {
		if (prop.getProperty("Cassandra.ClusterName").length() == 0) {
			logger.error("Property \"Cassandra.ClusterName\" not provided. Please provide it and re-try!");
			System.exit(0);
		}
		return prop.getProperty("Cassandra.ClusterName");
	}

	/**
	 * Returns the Cassandra connection pool name
	 * @return
	 */
	public String getCassandraConnectionPool() {
		if (prop.getProperty("Cassandra.ConnectionPool").length() == 0) {
			logger.error("Property \"Cassandra.ConnectionPool\" not provided. Please provide it and re-try!");
			System.exit(0);
		}
		return prop.getProperty("Cassandra.ConnectionPool");
	}

	
	/**
	 * Returns the version of Cassandra installed
	 * Default Value: 2.1
	 * @return
	 */
	public String getTargetCassandraVersion() {
		if (prop.getProperty("Cassandra.TargetCassandraVersion").length() == 0) {
			logger.warn("Property \"Cassandra.TargetCassandraVersion\" not provided, using the default value of"
					+ CassandraConstants.DEFAULT_CASSANDRA_TARGET_VERSION);
		}
		return prop.getProperty("Cassandra.TargetCassandraVersion",
				CassandraConstants.DEFAULT_CASSANDRA_TARGET_VERSION);
	}

	/**
	 * validate and set the consistency level of operations 
	 */
	public void validateAndSetCassandraConsistencyLevel() {
		if (prop.getProperty("operation").equalsIgnoreCase("Write")) {
			if (prop.getProperty("Cassandra.WriteConsistencyLevel") == null
					|| prop.getProperty("Cassandra.WriteConsistencyLevel").length() == 0) {
				logger.warn("The Cassandra write consistency level is not set by the user in the configuration file. "
						+ "Default value : " + CassandraConstants.DEFAULT_CASSANDRA_WRITE_CONSISTENCY_LEVEL
						+ " will be used.");
				prop.setProperty("Cassandra.WriteConsistencyLevel",
						CassandraConstants.DEFAULT_CASSANDRA_WRITE_CONSISTENCY_LEVEL);

			}

		} else if (prop.getProperty("operation").equalsIgnoreCase("Read")) {
			if (prop.getProperty("Cassandra.ReadConsistencyLevel") == null
					|| prop.getProperty("Cassandra.ReadConsistencyLevel").length() == 0) {
				logger.warn("The Cassandra read consistency level is not set by the user in the configuration file. "
						+ "Default value : " + CassandraConstants.DEFAULT_CASSANDRA_READ_CONSISTENCY_LEVEL
						+ " will be used.");
				prop.setProperty("Cassandra.ReadConsistencyLevel",
						CassandraConstants.DEFAULT_CASSANDRA_READ_CONSISTENCY_LEVEL);

			}

		}
	}
	
	/**
	 * Returns the maximum number of connections that can be made to Cassandra thrift
	 * Default Value: 1 
	 * @return String
	 */
	public int getMaxConnPerHostForCassandra() {
		if (prop.getProperty("Cassandra.MaxConnPerHost").length() == 0) {
			logger.warn("Property \"Cassandra.MaxConnPerHost\" not provided, using the default option of "
					+ CassandraConstants.DEFAULT_CASSANDRA_MAX_CONN_PER_HOST);
		}
		return Integer.parseInt(
				prop.getProperty("Cassandra.MaxConnPerHost", CassandraConstants.DEFAULT_CASSANDRA_MAX_CONN_PER_HOST));
	}
}
