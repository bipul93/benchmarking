package com.siemens.bam.adm.studio.dbmetrics;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.cassandra.CassandraProperties;
import com.siemens.bam.adm.studio.metric.Metric;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import net.sf.json.JSONObject;

/**
 * Class to collect metrics from Datastax Opscenter
 * 
 * @author varsha.raveendran@siemens.com
 *
 */
public class CassandraMetrics extends AbstractDataStoreMetrics {

	private final static Logger logger = LoggerFactory.getLogger(CassandraMetrics.class.getName());
	/**
	 * Cluster metrics
	 */
	private static final String CASSANDRA_CF_METRICS = "CassandraCFMetrics";

	/**
	 * Column Family metrics
	 */
	private static final String CASSANDRA_CLUSTER_METRICS = "CassandraClusterMetrics";

	/**
	 * Operating system metrics
	 */
	private static final String CASSANDRA_OS_METRICS = "CassandraOSMetrics";

	private String datacenter;

	private static CassandraProperties cassandraProperties = new CassandraProperties();

	/**
	 * Map<Timestamp, List<Metric>>
	 */
	private Map<String, ArrayList<Metric>> metricsCollected = new LinkedHashMap<String, ArrayList<Metric>>();
	private long startTime;
	private long endTime;

	public CassandraMetrics(Map<String, List<String>> metricList) {
		super(metricList);
	}

	public CassandraMetrics() {
		super();
	}

	@Override
	public void collectMetrics(String fileName, ClientProperties clientProp) throws Exception {

		Map<String, List<String>> dbMetrics = clientProp.getMetricList();
		setMetricsList(dbMetrics);
		datacenter = cassandraProperties.getCassandraDataCenter();

		String url = buildURL(clientProp);

		this.startTime = startTime / 1000;
		this.endTime = endTime / 1000;

		if (dbMetrics.containsKey(CASSANDRA_CLUSTER_METRICS)) {

			retrieveClusterMetrics(startTime, endTime, url, dbMetrics.get(CASSANDRA_CLUSTER_METRICS), clientProp);
		}
		if (dbMetrics.containsKey(CASSANDRA_CF_METRICS)) {

			retrieveCFMetrics(startTime, endTime, url, dbMetrics.get(CASSANDRA_CF_METRICS), clientProp);

		}
		if (dbMetrics.containsKey(CASSANDRA_OS_METRICS)) {

			retrieveClusterMetrics(startTime, endTime, url, dbMetrics.get(CASSANDRA_OS_METRICS), clientProp);

		}

		createLogFiles(fileName);

		saveMetricsToCSV();

	}

	private void saveMetricsToCSV() throws Exception {

		logger.debug("Saving metrics to CSV");

		Output.write("TimeStamp");

		Map.Entry<String, ArrayList<Metric>> entry = metricsCollected.entrySet().iterator().next();

		for (Metric metric : entry.getValue()) {

			Output.write("," + metric.getMetricName());

		}
		Output.write("\n");

		for (Entry<String, ArrayList<Metric>> entryCSV : metricsCollected.entrySet()) {
			Output.write(entryCSV.getKey());

			for (Metric metric : entryCSV.getValue()) {

				Output.write("," + metric.getMetricValue());

			}

			Output.write("\n");

		}

		Output.flush();

	}

	/**
	 * Method to get column family metrics
	 * 
	 * @param startTime
	 * @param endTime
	 * @param url
	 * @param list
	 * @param clientProp
	 * @throws Exception
	 */
	private void retrieveCFMetrics(long startTime, long endTime, String url, List<String> list,
			ClientProperties clientProp) throws Exception {

		String URL = url + "cluster-metrics/" + datacenter + "/" + clientProp.getDatabaseName() + "/"
				+ clientProp.getTableName() + "/";

		for (String metric : list) {
			getMetricsFromOpscenter(metric, URL, clientProp);

		}

	}

	/**
	 * Method to get cluster metrics
	 * 
	 * @param startTime
	 * @param endTime
	 * @param url
	 * @param list
	 * @param clientProp
	 * @throws Exception
	 */
	private void retrieveClusterMetrics(long startTime, long endTime, String url, List<String> list,
			ClientProperties clientProp) throws Exception {

		String URL = url + "cluster-metrics/" + datacenter + "/";

		for (String metric : list) {
			getMetricsFromOpscenter(metric, URL, clientProp);
		}
	}

	/**
	 * Method to concatenate REST URL with additional parameters such as
	 * function to aggregate the metrics and frequency at which the metric
	 * should be collected
	 * 
	 * @param clientProp
	 * @return
	 */
	private String addParametersToURL(ClientProperties clientProp) {

		String url = "&";

		if (clientProp.getMetricStep() != null)
			url = url + "step=" + clientProp.getMetricStep() + "&";

		if (clientProp.getMetricFunction() != null) {
			String function = clientProp.getMetricFunction();
			if (function.equals("avg"))
				function = "average";
			url = url + "function=" + function + "&";

		}

		return url;
	}

	/**
	 * Method to connect to Opscenter to collect the metrics
	 * 
	 * @param metric
	 * @param URL
	 * @param clientProp
	 * @throws Exception
	 */
	private void getMetricsFromOpscenter(String metric, String URL, ClientProperties clientProp) throws Exception {

		String url = URL + metric + "?start=" + startTime + "&end=" + endTime + addParametersToURL(clientProp);
		logger.info(url);

		Map<String, String> metricValues = getResponseFromOpscenter(url);

		if (metricValues != null) {
			for (Map.Entry<String, String> entry : metricValues.entrySet()) {

				String timeStamp = entry.getKey();
				String entryVal = entry.getValue();

				if (metricsCollected.get(timeStamp) == null) {
					metricsCollected.put(timeStamp, new ArrayList<Metric>());
				}
				ArrayList<Metric> list = metricsCollected.get(timeStamp);
				Metric m = new Metric(metric);
				m.setMetricValue(entryVal);
				list.add(m);
				metricsCollected.put(timeStamp, list);

			}
		}

		else {
			logger.debug(metric + " is NULL!");
		}
	}

	private Map<String, String> getResponseFromOpscenter(String opscenterUrl) throws Exception {

		logger.debug("Opscenter URL " + opscenterUrl);
		Map<String, String> metricValues = new LinkedHashMap<String, String>();

		URL url = new URL(opscenterUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		JSONObject output = null;

		output = JSONObject.fromObject(br.readLine().toString());

		Iterator<?> keys = output.keys();
		String key = (String) keys.next();
		JSONObject jsonObj = (JSONObject) output.get(key);
		Iterator<?> jsonKeys = jsonObj.keys();
		while (jsonKeys.hasNext()) {
			String key1 = (String) jsonKeys.next();

			net.sf.json.JSONArray jsonArray = jsonObj.optJSONArray(key1);

			for (int i = 0; i < jsonArray.size(); i++) {
				String val = jsonArray.getString(i);

				String[] subStr = val.substring(1, val.length() - 1).split(",");

				metricValues.put(subStr[0], subStr[1]);
			}
		}
		conn.disconnect();
		return metricValues;
	}

	private String buildURL(ClientProperties clientProp) {

		String opscenterIP = cassandraProperties.getOpscenterIP();
		String opscenterPort = cassandraProperties.getOpscenterPort();
		String clusterID = cassandraProperties.getCassandraClusterId();

		return "http://" + opscenterIP + ":" + opscenterPort + "/" + clusterID + "/";

	}

}
