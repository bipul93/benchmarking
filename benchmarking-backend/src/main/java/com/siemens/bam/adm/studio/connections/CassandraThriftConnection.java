package com.siemens.bam.adm.studio.connections;

import com.netflix.astyanax.AstyanaxContext;
import com.netflix.astyanax.Keyspace;
import com.netflix.astyanax.connectionpool.NodeDiscoveryType;
import com.netflix.astyanax.connectionpool.impl.ConnectionPoolConfigurationImpl;
import com.netflix.astyanax.connectionpool.impl.CountingConnectionPoolMonitor;
import com.netflix.astyanax.impl.AstyanaxConfigurationImpl;
import com.netflix.astyanax.model.ColumnFamily;
import com.netflix.astyanax.model.ConsistencyLevel;
import com.netflix.astyanax.thrift.ThriftFamilyFactory;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.cassandra.CassandraProperties;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.validators.AbstractColumnValidator;
import com.siemens.bam.adm.studio.schema.validators.AbstractKeyValidator;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.Serializer;

/**
 * The Connection Class for Cassandra with Thrift Protocol
 * 
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 * 
 */
public class CassandraThriftConnection implements IConnection {

	final static Logger logger = LoggerFactory.getLogger(CassandraThriftConnection.class.getName());
	private AstyanaxContext<Keyspace> context;
	private Keyspace keyspace;
	private ColumnFamily<Object, Object> columnFamily;
	private static CassandraProperties cassandraProperties = new CassandraProperties();

	public Keyspace getKeyspace() {
		return keyspace;
	}

	public void setKeyspace(Keyspace keyspace) {
		this.keyspace = keyspace;
	}

	public ColumnFamily<Object, Object> getColumnFamily() {
		return columnFamily;
	}

	private static AtomicInteger atomicCounter = new AtomicInteger(0);

	public CassandraThriftConnection(Environment environment) throws Exception {
		logger.debug(Thread.currentThread().getName() + " : Creating a new CassandraThriftConnection instance");
		cassandraProperties.validateAndSetCassandraConsistencyLevel();
		atomicCounter.set(environment.getClientProperties().getNumberOfThreads());

		initializeConnection(environment);
	}

	@Override
	public void initializeConnection(Environment environment) throws DBException, Exception {
		ClientProperties clientProperties = environment.getClientProperties();
		String[] hostIPs = null;
		hostIPs = environment.getClientProperties().getHostIP().split(",");
		StringBuilder sb = new StringBuilder();
		int thriftPort = cassandraProperties.getThriftPort();

		logger.debug("Adding host" + hostIPs[0]);
		sb.append(hostIPs[0]).append(":").append(thriftPort);

		for (int i = 1; i < hostIPs.length; i++) {
			sb.append(",").append(hostIPs[i]).append(":").append(thriftPort);
			logger.debug("Adding host" + hostIPs[i]);
		}

		// The database to connect to
		String db = environment.getDataModel().getDatabaseName();

		// Establishing the connection and initialization of keyspace and column
		// family
		try {
			if (!db.equalsIgnoreCase(null)) {
				AstyanaxConfigurationImpl config = new AstyanaxConfigurationImpl()
						.setTargetCassandraVersion(cassandraProperties.getTargetCassandraVersion());
				if (EnumUtils.isValidEnum(ConsistencyLevel.class,
						"CL_" + cassandraProperties.getCassandraConsistencyLevel())) {

					if (clientProperties.getOperation().equalsIgnoreCase("Write")) {
						config.setDefaultWriteConsistencyLevel(
								ConsistencyLevel.valueOf("CL_" + cassandraProperties.getCassandraConsistencyLevel()));

					} else if (clientProperties.getOperation().equalsIgnoreCase("Read")) {
						config.setDefaultReadConsistencyLevel(
								ConsistencyLevel.valueOf("CL_" + cassandraProperties.getCassandraConsistencyLevel()));
					}
				} else {
					logger.error(
							"No such Consistency Level " + "CL_" + cassandraProperties.getCassandraConsistencyLevel()
									+ " available in Cassandra. Please check the config file. ");
					throw new Exception(
							"No such Consistency Level " + "CL_" + cassandraProperties.getCassandraConsistencyLevel()
									+ " available in Cassandra. Please check the config file. ");
				}

				context = new AstyanaxContext.Builder().forCluster(cassandraProperties.getCassandraClusterName())
						.forKeyspace(db)
						.withAstyanaxConfiguration(
								new AstyanaxConfigurationImpl().setDiscoveryType(NodeDiscoveryType.RING_DESCRIBE))
						.withConnectionPoolConfiguration(
								new ConnectionPoolConfigurationImpl(cassandraProperties.getCassandraConnectionPool())
										.setPort(cassandraProperties.getThriftPort())
										.setMaxConnsPerHost(cassandraProperties.getMaxConnPerHostForCassandra())
										.setSeeds(sb.toString()))
						.withAstyanaxConfiguration(config)
						.withConnectionPoolMonitor(new CountingConnectionPoolMonitor())
						.buildKeyspace(ThriftFamilyFactory.getInstance());

				context.start();
				keyspace = context.getClient();

				Table t = environment.getDataModel().getTable(clientProperties.getTableName());

				this.columnFamily = initializeColumnFamily(t.getTableName(), t.getKeyValidator(),
						t.getColumnValidator());
				logger.debug("Connection to Cassandra Cluster via Thrift completed!");
			} else {
				logger.error("Database name is NULL, Connection cannot be established!");
				System.exit(0);
			}
		} catch (Exception e) {
			logger.error("Cassandra Connection via Thrift failed due to " + e.getMessage());
			throw new DBException("Cassandra Connection via Thrift failed due to ", e);
		}
	}

	@SuppressWarnings("unchecked")
	private ColumnFamily<Object, Object> initializeColumnFamily(String tableName, AbstractKeyValidator keyValidator,
			AbstractColumnValidator columnValidator) {

		ColumnFamily<Object, Object> table;

		table = ColumnFamily.newColumnFamily(tableName, Serializer.getSerializerType(keyValidator.getName()),
				Serializer.getSerializerType(columnValidator.getName()));
		return table;
	}

	@Override
	public void closeConnection() throws DBException {
		try {
			if (atomicCounter.decrementAndGet() == 0) {
				if (context != null) {
					logger.debug(Thread.currentThread().getName() + " : Session closed.");
					context.shutdown();
				}
			}
		} catch (Exception e) {
			throw new DBException("Error while shutting down Cassandra thrift connections : " + e);
		}

	}

}
