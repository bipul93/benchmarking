package com.siemens.bam.adm.studio.read.queryModel.queryTypes;

import java.util.concurrent.CompletionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;
import com.siemens.bam.adm.studio.schema.DataModel;

/**
 *  Abstract class that defines various kinds of queries
 *  @author Nisha Menon
 */

public abstract class AbstractQuery 
{
	private static final Logger logger = LoggerFactory.getLogger(AbstractQuery.class);
	protected DataModel model;
	
	public AbstractQuery(DataModel model)
	{
		this.model = model;
		logger.debug("AbstractQuery object instantiated with model information!");
	}

	/**
	 * Method to get respective query Parameters used by respective queries
	 * @return QueryParams
	 */
	public abstract AbstractQueryParams getQueryParams();
	
	/**
	 * Method to set query parameters for the respective query and return true if all parameters are rightly set 
	 * or false, if there is an error in setting any parameters
	 * @param parameterList
	 * @return boolean
	 */
	public abstract boolean initializeParameters(String[] parameterList);
	
	/**
	 * Method to find the execution time of a query
	 * @return Total time for the query
	 */
	public abstract long findExecutionTime();
	
	/**
	 * Method to execute the query and return the corresponding result Set
	 * @param rowKey
	 * @param dbDriver
	 * @return resultSet according to the corresponding query type
	 */
	public abstract <T> T queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception;
	
	/**
	 * Method to retrieve results of the query execution corresponding to each rowkey
	 * @param numberOfrowkeys
	 * @param CompletionService
	 * @return Final query resultSet combining the rowKey and corresponding resultSet per rowKey
	 */
	public abstract <T> T getResults(int numberOfrowkeys, CompletionService<T> completion_pool) throws Exception; 
	
	/**
	 * Method to print the ResultSet for each query
	 * @param resultSet
	 */
	public abstract <T> void printResults(T resultSet);

}