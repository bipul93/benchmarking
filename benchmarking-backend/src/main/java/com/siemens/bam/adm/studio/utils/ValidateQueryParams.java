package com.siemens.bam.adm.studio.utils;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;

public class ValidateQueryParams 
{
	private static final Logger logger = Logger.getLogger(ValidateQueryParams.class);
	private static final String JODA_PATTERN="yyyy-MM-dd'T'HH:mm:ss.SSS";
	private static final String DATE_PATTERN="yyyy-MM-dd";
	
	/*
	 * Enum for Aggregation type
	 * Used by TagTrendAggregate Query. 
	 */
	public enum AggregationType {
		MAX,
		MIN,
		AVG;
		
		private static final Map<Integer, AggregationType> lookup = new HashMap<Integer, AggregationType>();

		static {
			for (AggregationType dt : EnumSet.allOf(AggregationType.class)) {
				lookup.put(dt.ordinal(), dt);
			}
		}

		public static AggregationType getOperationFromOrdinal(int ordinal) {
			return lookup.get(ordinal);
		}
	};
	
	/*
	 * Validate if the provided query is among the ones that are registered. 
	 */
	public static void validateQueryName(String queryName)
	{

		Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
		if(!(queryNameTypes.contains(queryName)))
		{
			logger.error("QueryName is neccessary for any query. Please provide a valid queryname");
		}

	}
	
	/*
	 * For tag queries, the begin and end provided are validated. 
	 * Begin is always expected to be lesser than end
	 */
	public static boolean validateBeginEndTime(DateTime beginTime, DateTime endTime)
	{
		boolean isValid = false;
		
		if(!(endTime.compareTo(beginTime) < 0))
		{
			isValid = true;
		}	
		else
		{
			logger.error("The end date is smaller than the begin date");
			logger.error("Please enter the dates in the correct order");
		}		
		return isValid;
	}
	
	/*
	 * Validate if the provided timestep is an integer or not. 
	 * If not integer, then convert the double value to either floor or ceil.
	 */
	public static int validateTimeStep(double timestep)
	{
		int timestepApproximated;
		double compareVal = Math.floor(timestep)+0.5;
		
		if(timestep > compareVal)
			timestepApproximated = (int)Math.ceil(timestep);
		else
			timestepApproximated = (int)Math.floor(timestep);
		
		logger.debug("Timestep set to "+ timestepApproximated);
		return timestepApproximated;
	}

	/*
	 * Validate the given aggregation type against the available enums
	 */
	public static boolean validateAggregationType(String aggType)
	{
		boolean isValid = false;
		try
		{
			AggregationType aggT = AggregationType.valueOf(aggType);
			logger.debug("Aggregation Type \""+aggT.toString()+"\" is valid");
			isValid = true;
		}
		catch (IllegalArgumentException  e)
		{
			logger.debug("No Aggregation type defined as: "+ aggType);
			isValid = false;
		}
		return isValid;
	}
	
	/*
	 * Validate a given input against a given data type
	 */
	public static boolean validateDataType(String input, DataTypes type)
	{
		boolean isValid = false;
		switch(type)
		{
				case UUID:try {
								UUID uuid = UUID.fromString(input);
								logger.debug("rowkey <" + uuid + "> in correct format");
								isValid=true;
							}
							catch(Exception e)
							{
								logger.error("Expected format is of UUID type. The provided <" + input + "> is not of this format.");
								logger.error("Please enter in UUID format ********-****-****-****-************");
								isValid=false;
							}
							return isValid;
							
			   case STRING: Scanner scanner = new Scanner(input);  
			   				input = input.trim();
			   				
		    				String validationResult = scanner.findInLine("^[a-zA-Z0-9_]*$"); //[^0-9()+\\-*\\/%]+
		    				
		    				// If the String has any of the above characters, then it is valid. (Numbers are allowed)
		    				if (validationResult != null) 
		    				{
		    					scanner.close();
		    					logger.debug("The input String: " + input + " is valid");
		    					isValid = true;
		    				}
		    				else
		    				{
		    					logger.error("Expected format is of String type. The provided <" + input + "> is not of this format.");
								logger.error("Please enter in String format");
		    					isValid=false;
		    				}
		    				return isValid;
			case DATETIME: try
							{
								DateTime date = null;
								DateTimeFormatter parser = DateTimeFormat.forPattern(JODA_PATTERN);
								date = parser.parseDateTime(input);
								logger.debug("Date <" + date + "> in correct format");
								isValid = true;
							}
							catch(Exception e)
							{
								logger.error("Expected format is of DateTime type. The provided <" + input + "> is not of this format.");
								logger.error("Please enter in yyyy-MM-dd'T'HH:mm:ss.SSS format");
								isValid = false;
							}
							return isValid;
			case DATE: try
							{
								DateTime date = null;
								DateTimeFormatter parser = DateTimeFormat.forPattern(DATE_PATTERN);
								date = parser.parseDateTime(input);
								logger.debug("Date <" + date + "> in correct format");
								isValid = true;
							}
							catch(Exception e)
							{
								logger.error("Expected format is of Date type. The provided <" + input + "> is not of this format.");
								logger.error("Please enter in yyyy-MM-dd format");
								isValid = false;
							}
							return isValid;
				default:	
							return isValid;
					
		}
	}
	
	/*
	 * This is to validate if the specified column is present in the schema
	 * Used by TagTrendTall
	 */
	public static boolean checkColumnInSchema(DataModel model, String colName)
	{		
		List<TableColumn> columnList = model.getTables().get(0).getColumns();
		Iterator<TableColumn> tc = columnList.iterator();
		while(tc.hasNext())
		{
			TableColumn tc1  = tc.next();
			if(tc1.getColName().equals(colName))
			{
				logger.debug("Column " + colName + " present in schema.");
				return true;
			}
		}
		return false;
	}
	
	/*
	 * Validate if the provided row keys in the file is of the same order of that of the schema
	 */
	public static boolean isRowKeyFileDataConsistentWithSchema(String [] rowkeyList, DataModel model)
	{
		boolean isValid = true;
		
		Table table = model.getTables().get(0);
		List<String> inputRowKeys = Arrays.asList(rowkeyList);
		List<DataTypes> rowKeyDataTypes = getRowKeyDataTypes(model);
		
		// Validate if all the row keys in the file provided are of the same order provided in the schema
		for (String rowKey : inputRowKeys)
		{
			System.out.println("here: " + isValid);
			if(!isValid)
				break;
			
			List<String> rowKeyParts = Arrays.asList(rowKey.split("\\"+table.getRowKeySeparator()));
			int i = 0;
			for (String part : rowKeyParts)
			{
				if (!validateDataType(part,rowKeyDataTypes.get(i++)))
				{
					isValid = false;
					logger.info("The given rowkeys are not of same order as the row keys in the schema file");
					break;
				}
			}
		}
		return isValid;
	}
	
	/*
	 * Change the order of row keys as per the schema
	 */
	public static String [] getRowKeysUpdatedAsPerSchema(String [] rowkeyList, DataModel model)
	{
		String [] modifiedRowKeys = new String [rowkeyList.length];
		
		Table table = model.getTables().get(0);
		List<String> inputRowKeys = Arrays.asList(rowkeyList);
		List<DataTypes> rowKeyDataTypes = getRowKeyDataTypes(model);
		
		int rowKeyCount = 0, i = 0, k = 0;
		String tempPostion[] = new String [rowKeyDataTypes.size()];
		
		for (String rowKey : inputRowKeys)
		{
			List<String> rowKeyParts = Arrays.asList(rowKey.split("\\"+table.getRowKeySeparator()));
			
			for (String part : rowKeyParts)
			{
				i = 0;
				while(i < rowKeyDataTypes.size())
				{
					if (validateDataType(part,rowKeyDataTypes.get(i)))
					{
						tempPostion[i] = part;
						//continue;
					}
					i++;
				}
			}
			
			String modifiedRowKey = tempPostion[0];
			k = 1;
			while(k < tempPostion.length)
			{
				modifiedRowKey = modifiedRowKey + table.getRowKeySeparator() + tempPostion[k++];
			}
			modifiedRowKeys[rowKeyCount] = modifiedRowKey;
			rowKeyCount++;
		}
		
		return modifiedRowKeys;
	}
	
	public static List<DataTypes> getRowKeyDataTypes(DataModel model)
	{
		List<DataTypes> rowKeyDataTypes = new LinkedList<DataTypes>();

		Table table = model.getTables().get(0);
		List<String> rowKeys = table.getRowKeys();
		
		List<TableColumn> cols = table.getColumns();		

		// Get the data types of all row keys
		for (String s : rowKeys)
		{
			TableColumn tc = new TableColumn();
			tc.setColName(s);
			int index = cols.indexOf(tc);
			rowKeyDataTypes.add(DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType()));
		}
		
		logger.debug("The rowkey defined in schema is of type: "+rowKeyDataTypes);
		return rowKeyDataTypes;
	}
}
