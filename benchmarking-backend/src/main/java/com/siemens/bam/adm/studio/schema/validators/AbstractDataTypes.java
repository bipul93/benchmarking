package com.siemens.bam.adm.studio.schema.validators;

import java.util.List;

/**
 * Abstract class to define data types
 * @author Nisha Menon
 */

public abstract class AbstractDataTypes {

	List<String> DataTypeSet;

	protected abstract List<String> retrieveValidDataTypes(String type);

}
