package com.siemens.bam.adm.studio.exception;


/**
 * 
 * OperationException class
 * 
 */

public class OperationException extends Exception {

	private static final long serialVersionUID = -4101546130799480942L;

	public OperationException() {
	}

	public OperationException(String message) {
		super(message);
	}

	public OperationException(Throwable cause) {
		super(cause);
	}

	public OperationException(String message, Throwable cause) {
		super(message, cause);
	}

	public OperationException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
