package com.siemens.bam.adm.studio.operation;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.listener.HttpClientListener;
import com.siemens.bam.adm.studio.operation.ReadOperation.PostClass;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.core.Executor;
import com.siemens.bam.adm.studio.threads.Writer;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import com.siemens.bam.adm.studio.utils.GlobalData;

/**
 * Implements the DBOperation interface
 *
 * Methods to execute WRITE workloads and display the execution results at the end of the run
 *
 * @author varsha.raveendran@siemens.com
 *
 */

public class WriteOperation implements IDBOperation {

	private static final Logger logger = Logger.getLogger(WriteOperation.class);
	private static final String NAME = "WRITE";
	String ip = "";
	String port = "";
	Thread t;

	private Environment environment;

	public WriteOperation() {
	}

	/**
	 * Sets the data model, database driver and data generator for WRITE
	 * operations
	 *
	 * @see com.siemens.bam.adm.studio.operation.IDBOperation#buildEnvironment(com.siemens.bam.adm.studio.utils.ClientProperties)
	 */
	@Override
	public void buildEnvironment(ClientProperties clientProp) throws Exception {
		environment = new Environment.EnvironmentBuilder(clientProp).setDataGenerator().build();
		ip = clientProp.getServerAddress();
		port = clientProp.getServerPort();
		validateWriteParameters();
	}

	/**
	 *
	 * @see com.siemens.bam.adm.studio.operation.IDBOperation#start()
	 */
	@Override
	public void start() throws Exception {
		logger.info("starting executor.");
		Executor<Void> executor = new Executor<Void>();
		logger.info("init executor.");
		executor.init(environment);
		//logger.info("running executor." + executor);
		logger.info(environment.getClientProperties().getNumberOfThreads());
		Executor.setNumberOfThreadsRequired(environment.getClientProperties().getNumberOfThreads());
		executor.execute(this);
		logger.info("Completed inserting records.");
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public <T> Callable<T> getInstanceOfOperation(int threadid) throws Exception {
		return new Writer<T>(environment, threadid);
	}

	/**
	 * Calculates and displays the throughput and total number of records that
	 * were inserted
	 *
	 * @see com.siemens.bam.adm.studio.operation.IDBOperation#displayExecutionStats(java.util.concurrent.CompletionService)
	 */
	@Override
	public <T> void displayExecutionStats(CompletionService<T> completionPool) throws Exception {

		Integer numberOfInsertsCompleted = 0;
		//logger.info("executor Threads" + Executor.getNumberOfThreadsRequired());
		for (int threadid = 0; threadid < Executor.getNumberOfThreadsRequired(); threadid++) {
			//logger.info("number" + (Integer) completionPool.take().get());
			numberOfInsertsCompleted += (Integer) completionPool.take().get();
		}
		Executor.getTimer().stopTime();

		logger.info("Number of Records inserted: " + numberOfInsertsCompleted);

		long elapsedTime = Executor.getTimer().getElapsedTime();
		double duration = ((double) elapsedTime) / 1000.0;
		double throughput = numberOfInsertsCompleted / duration;
		logger.info("Time taken to insert records: " + duration + " seconds");
		//DecimalFormat d = new DecimalFormat("#.##");
		logger.info("Average Write Throughput : " + throughput + " operations/sec");

		GlobalData.queryResult = GlobalData.queryResult + "|" + "Number of Records inserted: " + numberOfInsertsCompleted + "|" + "Time taken to insert records: " + duration + " seconds"
		+ "|" + "Average Write Throughput : " + throughput + " operations/sec";
		if (environment.getClientProperties().isMetricsCollectionRequired()) {

			ClientProperties clientProp = environment.getClientProperties();
			clientProp.setStartTimeForMetricCollection(Long.toString(Executor.getTimer().getStartTime()));
			clientProp.setEndTimeForMetricCollection(Long.toString(Executor.getTimer().getStopTime()));
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss");
			Date date = new Date();
			// file to store the metrics
			String filename = clientProp.getTableName() + "." + clientProp.getNumberOfThreads() + "."
					+ clientProp.getNumberOfRecords() + "." + sdf.format(date) + ".csv";

			CollectMetricsOperation metricCollector = new CollectMetricsOperation();
			metricCollector.setMetricFileName(filename);
			metricCollector.start();
		}

		if(GlobalData.webMode){
			/*try{
				PostWriteClass postClass = new PostWriteClass();
				t = new Thread(postClass);
				t.start();
				//t.join();
			}catch (Exception e){
				System.out.println("");
			}*/
			HttpClientListener httpC = new HttpClientListener(ip,port);
			httpC.postTestResult(true);
		}

		GlobalData.writeInProgress = false;
		return;
		//environment.getDriver().close();
	}

	public void validateWriteParameters() throws Exception {
		ClientProperties clientProp = environment.getClientProperties();
		clientProp.validateAndSetNumberOfThreads();
		clientProp.validateAndSetNumberOfKeysToBeGenerated();
		clientProp.validateAndSetNumberOfRecords();
	}

	public class PostWriteClass implements Runnable{
		public void run(){
			try{
				HttpClientListener httpC = new HttpClientListener(ip,port);
				httpC.postTestResult(true);
			}catch(Exception e){

			}
		}
	}
}
