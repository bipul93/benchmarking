package com.siemens.bam.adm.studio.read.userInterface;


import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.lang.Runnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.core.Executor;
import com.siemens.bam.adm.studio.operation.ReadOperation;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;

import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.GlobalData;
import com.siemens.bam.adm.studio.utils.JSONtoPropsParser;
import com.siemens.bam.adm.studio.clients.Client;

/**
 * Class that implements Batch User Interface functionalities
 * @author Nisha Menon
 */
public class BatchUserInterface implements IUserInterface
{
	ClientProperties clientProp = new ClientProperties();
  String serverAddress = clientProp.getServerAddress();
  String serverPort = clientProp.getServerPort();
	private static final Logger logger = LoggerFactory.getLogger(BatchUserInterface.class);
	private String queryFilePath;
	private ReadOperation operation;

	public BatchUserInterface(ReadOperation operation){
		this.operation = operation;

		this.queryFilePath = operation.getEnvironment().getClientProperties().getQueryFilePath();
		logger.trace("BatchUserInterface Object Set");
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void runUserInterface() throws Exception
	{
		logger.debug("Within File User Interface Implementation.");
		BufferedReader br = null;
		try
		{
			String query;
			// Container for all queries listed in the file
			ArrayList<String> queryList = new ArrayList<String>();

			br = new BufferedReader(new FileReader(queryFilePath));
			Executor executor = new Executor();
			int threadsToUse;

			// Read all queries from the file and store it in an ArrayList
			while ((query = br.readLine()) != null)
			{
				logger.debug("Query is "+query);
				queryList.add(query);
			}
			AbstractQuery queryObject = null;
			int queryCount = 0;

			// Parse every provided query in the file.
			for (int i = 0; i < queryList.size(); i++)
			{
				String queryName = "";
				if (!queryList.get(i).equals(null))
				{
					queryCount++;
					logger.debug("Now processing Query No. " +queryCount+ " from file.");

					String[] params = queryList.get(i).split(" ");

					// Validate the queryName
					queryName = params[0];
					ValidateQueryParams.validateQueryName(queryName);

					queryObject=RegisterQueryService.getRegisteredQueries().get(queryName);
					if(queryObject==null)
					{
						logger.warn("Query number " +queryCount+ " is invalid. Moving on to the next query!");
						GlobalData.queryResult = "Query number " +queryCount+ " is invalid. Moving on to the next query!";
						continue;
					}

					if(!queryObject.initializeParameters(params))
					{
						logger.warn("Query number " +queryCount+ " is invalid. Moving on to the next query!");
						GlobalData.queryResult = "Query number " +queryCount+ " is invalid. Moving on to the next query!";
						continue;
					}
				}
				else
				{
					logger.error("Invalid query input!");
					if(!GlobalData.webMode)
						System.exit(0);
				}

				logger.debug("Query parameters set for Query No. " + queryCount);

				operation.getEnvironment().setQueryObject(queryObject);
				executor.init(operation.getEnvironment());

				/*  Number of threads spawned for a query = number of rowkeys in the query
				  If the query has no rowkeys, then one thread would be spawned.*/
					System.out.println("running1");
				threadsToUse = (queryObject.getQueryParams().getRowKeys().size() == 0 ? 1 : queryObject.getQueryParams().getRowKeys().size());
				Executor.setNumberOfThreadsRequired(threadsToUse);
				System.out.println("running2");
				executor.execute(operation);
			}
			br.close();
			logger.info("Exiting the Batch User Interface! " + GlobalData.webMode);
			if(!GlobalData.webMode){
				System.exit(0);
			}else{
				System.out.println("end of command");
				closeConnections();
				/*if (executor.awaitTermination(10, TimeUnit.SECONDS)) {
				  System.out.println("task completed");
				} else {
				  System.out.println("Forcing shutdown...");
				  //executor.shutdownNow();
				}*/
			}

		}
		catch (Exception e)
		{
			throw new Exception ("Batch User Interface Parsing Exception",e);
		}

	}

	public void closeConnections(){
		try {
			//operation.getEnvironment().getDriver().close();
			//System.exit(-1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
