/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag;

import org.joda.time.DateTime;

import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;

/**
 * Class that defines the Query Parameters for TagDataTall Query 
 * @author Nisha Menon
 */
public class TagDataQueryParams extends AbstractQueryParams
{
	private DateTime begin;
	private DateTime end;
	private int limit;
	
	public TagDataQueryParams()
	{
		super();
		this.begin = new DateTime();
		this.end = new DateTime();
		this.limit = 0;
	}

	public DateTime getBegin() {
		return begin;
	}

	public void setBegin(String begin) 
	{
		this.begin = DateFormattingUtils.getJodaDate(begin);
	}

	public DateTime getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = DateFormattingUtils.getJodaDate(end);
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}
