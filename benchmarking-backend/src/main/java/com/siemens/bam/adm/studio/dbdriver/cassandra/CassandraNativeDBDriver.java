package com.siemens.bam.adm.studio.dbdriver.cassandra;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.LocalDate;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.ResultSetFuture;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.exceptions.NoHostAvailableException;
import com.datastax.driver.core.exceptions.QueryExecutionException;
import com.datastax.driver.core.exceptions.QueryValidationException;
import com.siemens.bam.adm.studio.connections.CassandraNativeConnection;
import com.siemens.bam.adm.studio.connections.IConnection;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.SuperColumn;
import com.siemens.bam.adm.studio.schema.SuperColumn.SuperColumnType;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;

/**
 * The CassandraNativeDBDriver class contains methods that perform operations in
 * Cassandra using the binary protocol This includes getting a connection
 * instance and writing to/reading from Cassandra Keyspace
 *
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 * @param <S>
 * @param <T>
 *
 */

public class CassandraNativeDBDriver<S> extends AbstractDatabaseDriver {

	final static Logger logger = LoggerFactory.getLogger(CassandraNativeDBDriver.class.getName());

	private static volatile IConnection connection;
	private static Map<String, BatchStatement> batchStatements = new ConcurrentHashMap<String, BatchStatement>();

	public CassandraNativeDBDriver() {

	}

	/**
	 *
	 * This method gets a CassandraNativeConnection object. Only one instance of
	 * Session object and PreparedStatement should be used while inserting into
	 * Cassandra using the Binary protocol. Hence, the Singleton pattern is used
	 * here.
	 *
	 * (non-Javadoc)
	 *
	 * @throws DBException
	 *
	 * @see com.siemens.write.core.AbstractDatabaseDriver#_init(com.siemens.write.utils
	 *      .ClientProperties)
	 */
	@Override
	public AbstractDatabaseDriver initWrites(Environment environment) throws Exception {

		connection = getConnectionInstance(environment);

		if (environment.getClientProperties().isBatchInserts().equals("true")) {
			BatchStatement batchStmt = new BatchStatement(BatchStatement.Type.UNLOGGED);
			batchStatements.put(Thread.currentThread().getName(), batchStmt);
		}
		return this;
	}


	public AbstractDatabaseDriver initTestConnection(Environment environment) throws Exception {
		connection = getConnectionInstance(environment);
		return this;
	}

	/**
	 * Returns a CassandraNativeConnection object.
	 *
	 * @param environment
	 * @return
	 * @throws Exception
	 */
	private CassandraNativeConnection getConnectionInstance(Environment environment) throws Exception {

		if (null == connection) {
			synchronized (CassandraNativeConnection.class) {
				if (null == connection) {
					connection = new CassandraNativeConnection(environment);
				}
			}
		}
		return (CassandraNativeConnection) connection;
	}

	/**
	 * This method creates a BoundStatement, generates values for every column
	 * and inserts the row .
	 *
	 * @param columns
	 * @param schema
	 * @return
	 * @throws Exception
	 * @throws InterruptedException
	 * @throws ExecutionException
	 */
	@Override
	public boolean insertRecordsIndividually(List<TableColumn> columns, Table table) throws Exception {

		BoundStatement boundStatement = new BoundStatement(((CassandraNativeConnection) connection).getStatement());
		for (TableColumn entry : columns) {

			Object fieldDataValue = table.getTypeOfTable().getValueForField(entry);

			if (entry instanceof SuperColumn) {

				boundStatement = setCollection(boundStatement, entry, ((SuperColumn) entry).getScolType(),
						fieldDataValue);
				continue;
			}
			logger.debug(
					Thread.currentThread().getName() + " --> Data for " + entry.getColName() + " : " + fieldDataValue);
			DataTypes dType = DataTypes.getDatatypeFromOrdinal(entry.getColDataType());

			switch (dType) {

			case UUID:
				boundStatement.setUUID(entry.getColName(), (UUID) fieldDataValue);
				break;

			case DATETIME:
				SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
				timestampFormat.setTimeZone(TimeZone.getDefault());
				Date dDate = new Date(Long.parseLong(fieldDataValue.toString()));
				String strOutput = timestampFormat.format(dDate);
				Date timestamp = null;
				try {
					timestamp = timestampFormat.parse(strOutput);
				} catch (ParseException e) {

					logger.error("Error while parsing date " + timestamp);
					return false;
				}

				Calendar cal = Calendar.getInstance();
				cal.setTime(timestamp);
				cal.setTimeZone(TimeZone.getDefault());

				boundStatement.setTimestamp(entry.getColName(), cal.getTime());
				break;

			case DATE:

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
				Date tDate = new Date(Long.parseLong(fieldDataValue.toString()));
				String strDate = formatter.format(tDate);
				Date date = null;
				try {
					date = formatter.parse(strDate);
				} catch (ParseException e) {

					logger.error("Error while parsing date " + date);
					return false;
				}

				Calendar calendar = Calendar.getInstance();
				calendar.setTime(date);
				calendar.setTimeZone(TimeZone.getDefault());

				// Months returned by the Calendar class in Java are indexed
				// from 0 instead of 1
				boundStatement.setDate(entry.getColName(), LocalDate.fromYearMonthDay(calendar.get(Calendar.YEAR),
						calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)));
				break;

			case BOOLEAN:
				boundStatement.setBool(entry.getColName(), Boolean.parseBoolean(fieldDataValue.toString()));
				break;

			case TINYINT:
			case SMALLINT:
			case INT:
				boundStatement.setInt(entry.getColName(), Integer.parseInt(fieldDataValue.toString()));
				break;

			case BIGINT:
				boundStatement.setLong(entry.getColName(), (Long) fieldDataValue);
				break;

			case NULLINT:
				boundStatement.setInt(entry.getColName(), Integer.parseInt(fieldDataValue.toString()));
				break;

			case FLOAT:
				boundStatement.setFloat(entry.getColName(), (Float) fieldDataValue);
				break;

			case DOUBLE:
				boundStatement.setDouble(entry.getColName(), (Double) fieldDataValue);
				break;

			case STRING:
				boundStatement.setString(entry.getColName(), (String) fieldDataValue);
				break;

			/*
			 * case BYTES:
			 *
			 * ByteBuffer buf =
			 * ByteBuffer.wrap(BytesUtil.toByteArray(fieldDataValue));
			 * boundStatement.setBytes(entry.getColName(), buf);
			 *
			 * break;
			 */

			default:
				try {
					throw new Exception("Unknown field type for fieldname\"" + entry.getColName() + "\"");
				} catch (Exception e) {
					System.exit(0);
				}
			}
		}
		
		ResultSetFuture rsf = ((CassandraNativeConnection) connection).getSession().executeAsync(boundStatement.bind());
		try {
			/*
			 * This is to ensure that there was no exception thrown.
			 */
			rsf.get();
		} catch (Exception e) {
			logger.error("Exception caught in getUninterruptibly" + e);
			return false;
		}
		return true;
	}

	@Override
	public void addStatementsToBatch(int batchSize, List<TableColumn> columns, Table table, Boolean isUniqueKey)
			throws Exception {

		List<TableColumn> newCols = new LinkedList<TableColumn>();
		newCols.addAll(columns);
		Object primaryKeyDataValue = null;
		String primaryKey = null;

		/*
		 * Checks if a unique key per batch is to be created.
		 */
		if (isUniqueKey == true) {

			primaryKey = table.getRowKeys().get(0);

			TableColumn col = new TableColumn();
			col.setColName(primaryKey);
			int index = columns.indexOf(col);
			primaryKeyDataValue = table.getTypeOfTable().getValueForField(columns.get(index));
			logger.debug(Thread.currentThread().getName() + " : Unique Key for Batch = " + primaryKeyDataValue);
		}

		int numberInCurrentBatch = 0;
		while (numberInCurrentBatch < batchSize) {
			BoundStatement boundStatement = new BoundStatement(((CassandraNativeConnection) connection).getStatement());
			for (TableColumn entry : newCols) {
				Object fieldDataValue = null;

				/*
				 * Checks if unique key per batch generated is to be used and if
				 * the column field is the Primary Key field.
				 */

				if (entry.getColName().equals(primaryKey) && primaryKeyDataValue != null){
					// Set the fieldDataValue to the uniqueKey generated
					fieldDataValue = primaryKeyDataValue;
				} else {
					// If not generate a new value for field.
					fieldDataValue = table.getTypeOfTable().getValueForField(entry);
				}
				if (entry instanceof SuperColumn) {

					boundStatement = setCollection(boundStatement, entry, ((SuperColumn) entry).getScolType(),
							fieldDataValue);
				} else {

					boolean flag = bindValueInBoundStatement(entry.getColName(), fieldDataValue, entry.getColDataType(),
							boundStatement);
					if (flag == false)
						continue;
				}
			}
			// Add to the batch statement corresponding to this Thread
			batchStatements.get(Thread.currentThread().getName()).add(boundStatement);
			numberInCurrentBatch++;
		}
	}

	private boolean bindValueInBoundStatement(String colName, Object fieldDataValue, Integer colType,
			BoundStatement boundStatement) {

		DataTypes dType = DataTypes.getDatatypeFromOrdinal(colType);
		switch (dType) {

		case UUID:
			boundStatement.setUUID(colName, (UUID) fieldDataValue);
			break;

		case DATETIME:
			SimpleDateFormat timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			timestampFormat.setTimeZone(TimeZone.getDefault());
			Date dDate = new Date(Long.parseLong(fieldDataValue.toString()));
			String strOutput = timestampFormat.format(dDate);
			Date timestamp = null;
			try {
				timestamp = timestampFormat.parse(strOutput);
			} catch (ParseException e) {

				logger.error("Error while parsing date " + timestamp);
				return false;
			}

			Calendar cal = Calendar.getInstance();
			cal.setTime(timestamp);
			cal.setTimeZone(TimeZone.getDefault());

			boundStatement.setTimestamp(colName, cal.getTime());
			break;

		case DATE:

			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date tDate = new Date(Long.parseLong(fieldDataValue.toString()));
			String strDate = formatter.format(tDate);
			Date date = null;
			try {
				date = formatter.parse(strDate);
			} catch (ParseException e) {

				logger.error("Error while parsing date " + date);
				return false;
			}

			Calendar calendar = Calendar.getInstance();
			calendar.setTime(date);
			calendar.setTimeZone(TimeZone.getDefault());

			// Months returned by the Calendar class in Java are indexed
			// from 0 instead of 1
			boundStatement.setDate(colName, LocalDate.fromYearMonthDay(calendar.get(Calendar.YEAR),
					calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH)));
			break;
		case BOOLEAN:
			boundStatement.setBool(colName, Boolean.parseBoolean(fieldDataValue.toString()));
			break;

		case TINYINT:
		case SMALLINT:
		case INT:
			boundStatement.setInt(colName, Integer.parseInt(fieldDataValue.toString()));
			break;

		case BIGINT:
			boundStatement.setLong(colName, (Long) fieldDataValue);
			break;

		case NULLINT:
			boundStatement.setInt(colName, Integer.parseInt(fieldDataValue.toString()));
			break;

		case FLOAT:
			boundStatement.setFloat(colName, (Float) fieldDataValue);
			break;

		case DOUBLE:
			boundStatement.setDouble(colName, (Double) fieldDataValue);
			break;

		case STRING:
			boundStatement.setString(colName, (String) fieldDataValue);
			break;

		/*
		 * case BYTES:
		 *
		 * ByteBuffer buf = ByteBuffer.wrap(BytesUtil
		 * .toByteArray(fieldDataValue)); boundStatement.setBytes(colName, buf);
		 *
		 * break;
		 */

		default:
			try {
				throw new Exception("Unknown field type for fieldname\"" + colName + "\"");
			} catch (Exception e) {
				System.exit(0);
			}

		}
		return true;
	}

	@Override
	public boolean insertbatch() throws DBException {
		logger.debug("CL  - " + ((CassandraNativeConnection) connection).getCluster().getConfiguration()
				.getQueryOptions().getConsistencyLevel().toString());
		ResultSetFuture rsf = ((CassandraNativeConnection) connection).getSession()
				.executeAsync(batchStatements.get(Thread.currentThread().getName()));

		try {
			rsf.get();
			batchStatements.get(Thread.currentThread().getName()).clear();

		} catch (Exception e) {
			batchStatements.get(Thread.currentThread().getName()).clear();
			logger.error("Exception caught in getUninterruptibly" + e);
			return false;
		}

		return true;
	}

	@SuppressWarnings("unchecked")
	private BoundStatement setCollection(BoundStatement boundStatement, TableColumn entry,
			SuperColumnType superColumnType, Object fieldDataValue) {

		switch (superColumnType) {

		case LIST:
			logger.debug("Setting boundstatement with list");
			return boundStatement.setList(entry.getColName(), (List<Object>) fieldDataValue);

		case SET:
			return boundStatement.setSet(entry.getColName(), (Set<Object>) fieldDataValue);

		case MAP:
			return boundStatement.setMap(entry.getColName(), (Map<Object, Object>) fieldDataValue);

		default:
			logger.error("No such Super Column type implemented! ");
			System.exit(1);
			break;
		}
		return boundStatement;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#_initReads(
	 * com. siemens.bam.adm.studio.core.Environment)
	 */
	@Override
	public AbstractDatabaseDriver initReads(Environment environment) throws Exception {
		connection = getConnectionInstance(environment);
		return this;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForClusteringKeyRange_WideCF(java.lang.String, java.lang.Object,
	 * java.lang.Object, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForClusteringKeyRange_WideCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model) throws Exception {
		logger.debug("In getValuesForClusteringKeyRange_WideCF() method");
		Session session = ((CassandraNativeConnection) connection).getSession();

		NavigableMap<Object, Object> colList = new TreeMap<Object, Object>();

		// Get the Table information from the model object.
		Table table = model.getTables().get(0);
		String tableName = table.getTableName();

		// Get the row-keys and clustering key
		List<TableColumn> cols = table.getColumns();
		List<String> rowKeyList = table.getRowKeys();
		String clusteringKey = table.getClusteringKeys().get(0);

		TableColumn tc = new TableColumn();
		tc.setColName(clusteringKey);
		int index = cols.indexOf(tc);
		DataTypes clusteringKeyDataType = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> rowKeyParts = new ArrayList<String>();
		String rowkeySeperator = table.getRowKeySeparator();

		// Make a list of the separate components of the provided row Key.
		if (rowkeySeperator != null && rowKey.contains(rowkeySeperator)) {
			// The Character is escaped so as to deal with all types of
			// separators.
			rowKeyParts = Arrays.asList(rowKey.split("\\" + table.getRowKeySeparator()));
		} else {
			rowKeyParts.add(rowKey);
		}

		/*
		 * If the clustering key is same as one of the primary keys, then while
		 * forming the query the corresponding primary key condition is
		 * neglected. Only the clustering key condition is added. To achieve
		 * this, the column that is the same for clustering key and primary key,
		 * is removed from the rowKey list.
		 */

		if (rowKeyList.contains(clusteringKey)) {
			rowKeyList.remove(clusteringKey);
		}

		/*
		 * For Date and DateTime type ranges, the provided date is formatted
		 * according to CQL standards.
		 */
		if (startCol instanceof Date || startCol instanceof DateTime) {
			startCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(startCol));
			endCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(endCol));
		}

		// Get the column that is neither a row-key nor a clustering key
		List<TableColumn> tempListofCols = cols;
		int ii = 0;
		String retrievedCol = null;
		while (tempListofCols.size() != 0) {
			if (!(rowKeyList.contains(tempListofCols.get(ii).getColName())
					|| tempListofCols.get(ii).getColName().equalsIgnoreCase(clusteringKey))) {
				retrievedCol = tempListofCols.get(ii).getColName();
				break;
			}
			ii++;
		}
		logger.debug("The column to be retrieved is: " + retrievedCol);

		TableColumn tc1 = new TableColumn();
		tc1.setColName(retrievedCol);
		int index1 = cols.indexOf(tc1);
		DataTypes retrievedColumnDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index1).getColDataType());

		// Start query preparation
		Statement query = null;
		String fullQueryString = "select * from " + tableName + " where ";

		// Add all primary key conditions
		for (int i = 0; i < rowKeyList.size(); i++) {
			TableColumn tc2 = new TableColumn();
			tc2.setColName(rowKeyList.get(i));
			if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.DATETIME)
					|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
							.equals(DataTypes.DATE)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='"
						+ DateFormattingUtils.convertDateToCassandraNativeStringFormat(new DateTime(rowKeyParts.get(i)))
						+ "' and ";
			} else if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.STRING)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='" + rowKeyParts.get(i) + "' and ";
			} else {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "=" + rowKeyParts.get(i) + " and ";
			}
		}

		// Add clustering key condition.
		TableColumn tc3 = new TableColumn();
		tc3.setColName(clusteringKey);
		if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType()).equals(DataTypes.STRING)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATETIME)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATE)) {
			fullQueryString = fullQueryString + clusteringKey + ">='" + startCol + "' and " + clusteringKey + "<='"
					+ endCol + "';";
		} else {
			fullQueryString = fullQueryString + clusteringKey + ">=" + startCol + " and " + clusteringKey + "<="
					+ endCol + ";";
		}

		// Add limit if provided.
		if (limit != 0) {
			fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 1) + " limit "
					+ String.valueOf(limit) + ";";
		}

		logger.debug("The complete query String is: " + fullQueryString);

		query = new SimpleStatement(fullQueryString);
		query.setFetchSize(ClientProperties.getPaginationLimit());
		// Execute the query
		ResultSet results = null;
		try {
			logger.debug("Session is " + session.getState());
			logger.debug("The Consistency Level set for this query is: "
					+ session.getCluster().getConfiguration().getQueryOptions().getConsistencyLevel());
			results = session.execute(query);

		} catch (NoHostAvailableException e1) {
			throw new DBException("NoHostAvailableException occured in getValuesforColumnRange_WideCF()", e1);
		} catch (QueryExecutionException e2) {
			throw new DBException("QueryExecutionException occured in getValuesforColumnRange_WideCF()", e2);
		} catch (QueryValidationException e3) {
			throw new DBException("QueryValidationException occured in getValuesforColumnRange_WideCF()", e3);
		} catch (Exception e) {
			logger.error("Error in query execution: " + e.getMessage());
			throw new Exception("Exception in getValuesforColumnRange_WideCF()", e);
		}

		// Process the results
		try {
			if (results == null) {
				logger.debug("No results for this query!");
			} else {
				for (Row row : results) {
					colList.put(getReturnValueforDatatype(row, clusteringKey, clusteringKeyDataType),
							getReturnValueforDatatype(row, retrievedCol, retrievedColumnDatatype));
				}
			}
		} catch (Exception e) {
			logger.error("Error in getting results!" + e.getMessage());
			throw new Exception("Exception in getting results for getValuesforColumnRange_WideCF()", e);
		}

		return colList;
	}

	/**
	 * Method to return the value corresponding to a row for a particular column
	 *
	 * @param row
	 * @param columnName
	 * @param datatype
	 *            - Column data type
	 * @return value as Object type
	 */
	public Object getReturnValueforDatatype(Row row, String columnName, DataTypes datatype) {

		switch (datatype) {

		case BOOLEAN:
			return row.getBool(columnName);

		case FLOAT:
			return row.getFloat(columnName);
		case DOUBLE:
			return row.getDouble(columnName);
		case BIGINT:
			return row.getLong(columnName); // only getLong works for BIGINT

		case UUID:
			return row.getUUID(columnName);

		case DATE:
			return row.getDate(columnName);
		case DATETIME:
			return row.getTimestamp(columnName);

		case INT:
		case SMALLINT:
		case TINYINT:
			return row.getInt(columnName);

		case STRING:
			return row.getString(columnName);

		case NULLINT:
		case XML:
		case UNKNOWN:
			return null;

		default:
			return null;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getAllRowKeys(
	 * java. lang.String, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public TreeSet<String> getAllRowKeys(String parameter, int value, DataModel model) throws Exception {
		logger.debug("In getAllRowKeys() method.");

		Session session = ((CassandraNativeConnection) connection).getSession();
		TreeSet<String> setOfRowkeys = new TreeSet<String>();

		// Get the Table information from the model object.
		Table table = model.getTables().get(0);
		String tableName = table.getTableName();

		// Get the row-keys and its data-types
		List<TableColumn> cols = table.getColumns();
		List<String> rowKeyList = table.getRowKeys();
		String clusteringKey = table.getClusteringKeys().get(0);

		/*
		 * If one of the primary Keys is same as the clustering key, then only
		 * the other primary key is queried for. To achieve this, the column
		 * that is the same for clustering key and primary key, is removed from
		 * the rowKey list. This is because of the CQL error on selecting
		 * Distinct columns: SELECT DISTINCT queries must only request partition
		 * key columns and/or static columns
		 */

		if (rowKeyList.contains(clusteringKey)) {
			rowKeyList.remove(clusteringKey);
		}

		/*
		 * Create a Map of row Keys and its corresponding data types. This
		 * required for processing the output of the query.
		 */
		Map<String, DataTypes> rowKeyInfo = new HashMap<String, DataTypes>();
		Iterator<TableColumn> tc_iterator = cols.iterator();
		while (tc_iterator.hasNext()) {
			TableColumn tc2 = tc_iterator.next();
			if (rowKeyList.contains(tc2.getColName())) {
				rowKeyList.get(rowKeyList.indexOf(tc2.getColName()));
				DataTypes.getDatatypeFromOrdinal(tc2.getColDataType());
				rowKeyInfo.put(rowKeyList.get(rowKeyList.indexOf(tc2.getColName())),
						DataTypes.getDatatypeFromOrdinal(tc2.getColDataType()));
			}
		}
		logger.debug("The Rowkey Info is as follows: " + rowKeyInfo);

		// Start query preparation
		Statement query = null;
		String fullQueryString = "select distinct ";

		// Add the row key column names that is to be queried
		for (int i = 0; i < rowKeyList.size(); i++) {
			fullQueryString = fullQueryString + rowKeyList.get(i) + ",";
		}

		fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 1) + " from " + tableName + ";";

		// Add limit if provided.
		int limit_value;
		if (parameter.equals("limit")) {
			limit_value = value;
		} else {
			limit_value = Integer.MAX_VALUE;
		}

		fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 1) + " limit " + limit_value + ";";

		logger.debug("The complete query String is: " + fullQueryString);

		query = new SimpleStatement(fullQueryString);
		query.setFetchSize(ClientProperties.getPaginationLimit());

		ResultSet results = null;

		try {
			logger.debug("Session is " + session.getState());
			results = session.execute(query);
		} catch (Exception e) {
			logger.error("Error in query execution: " + e.getMessage());
			throw new Exception("Error in query execution for getAllRowKeys()", e);
		}

		/*
		 * If there is no row key separator, then use " " as the separator. This
		 * is used later for appending the individual row key parts.
		 */
		String rowKeySeperator = table.getRowKeySeparator() != null ? table.getRowKeySeparator() : " ";

		try {
			if (results == null) {
				logger.debug("No results for this query!");
			} else {
				int i = 0;
				String rowkey_value = "";
				Object[] keys = rowKeyInfo.keySet().toArray();
				for (Row row : results) {
					rowkey_value = "";
					i = 0;
					// Stitch together the individual pieces of row keys with
					// the row key separator
					while (i < keys.length) {
						rowkey_value = rowkey_value
								+ getReturnValueforDatatype(row, keys[i].toString(), rowKeyInfo.get(keys[i].toString()))
										.toString()
								+ rowKeySeperator;
						i++;
					}
					// Remove the last rowKeySeperator symbol before adding to
					// the set.
					setOfRowkeys.add(rowkey_value.substring(0, rowkey_value.length() - 1));
				}
			}
		} catch (Exception e) {
			logger.error("Error in getting results!" + e.getMessage());
			throw new Exception("Error in getting results for getAllRowKeys()", e);
		}

		/*
		 * For other parameters, such as 'top' and 'last', the query gets all
		 * results from the database and then processes it to take only required
		 * number of records
		 */

		if (parameter.equals("top") && value != 0) {
			TreeSet<String> rowkeySet = new TreeSet<String>();
			int count = 0;

			for (String value1 : setOfRowkeys) {
				rowkeySet.add(value1);
				count++;
				if (count == value)
					break;
			}
			return rowkeySet;
		}

		if (parameter.equals("last") && value != 0) {
			TreeSet<String> rowkeySet = new TreeSet<String>();
			int count = 0;
			Iterator<String> iterator = setOfRowkeys.descendingIterator();

			while (iterator.hasNext() && count != value) {
				rowkeySet.add((String) iterator.next());
				count++;
			}
			return rowkeySet;
		} else
			return setOfRowkeys;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getLastColumnValue_WideCF(java.lang.String,
	 * com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public Object getLastColumnValue_WideCF(String rowKey, DataModel model) throws Exception {
		logger.debug("In getLastColumnValue_WideCF method of Cassandra Native Driver");

		Session session = ((CassandraNativeConnection) connection).getSession();

		// Get the table information from the model object.
		Table table = model.getTables().get(0);
		String tableName = table.getTableName();

		// Get the row-keys, clustering key, columns and its data-types
		List<String> rowKeyList = table.getRowKeys();
		String clusteringKey = table.getClusteringKeys().get(0);

		List<TableColumn> cols = table.getColumns();

		TableColumn tc = new TableColumn();
		tc.setColName(clusteringKey);
		int index = cols.indexOf(tc);
		DataTypes clusteringKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> rowKeyParts = new ArrayList<String>();
		String rowkeySeperator = table.getRowKeySeparator();

		// Make a list of the separate components of the provided row Key.
		if (rowkeySeperator != null && rowKey.contains(rowkeySeperator)) {
			// The Character is escaped so as to deal with all types of
			// separators.
			rowKeyParts = Arrays.asList(rowKey.split("\\" + table.getRowKeySeparator()));
		} else {
			rowKeyParts.add(rowKey);
		}

		/*
		 * If the clustering Key is same as one of the primary Keys, then while
		 * forming the query the corresponding primary key condition is
		 * neglected. Only the clustering key condition is added. To achieve
		 * this, the column that is the same for clustering key and primary key,
		 * is removed from the rowKey list.
		 */

		if (rowKeyList.contains(clusteringKey)) {
			rowKeyList.remove(clusteringKey);
		}

		Object endValue = null;
		ResultSet results = null;

		// Start query preparation
		Statement query = null;
		String fullQueryString = "select " + clusteringKey + " from " + tableName + " where ";

		// Add all primary key conditions
		for (int i = 0; i < rowKeyList.size(); i++) {
			TableColumn tc2 = new TableColumn();
			tc2.setColName(rowKeyList.get(i));
			if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.DATETIME)
					|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
							.equals(DataTypes.DATE)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='"
						+ DateFormattingUtils.convertDateToCassandraNativeStringFormat(new DateTime(rowKeyParts.get(i)))
						+ "' and ";
			} else if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.STRING)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='" + rowKeyParts.get(i) + "' and ";
			} else {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "=" + rowKeyParts.get(i) + " and ";
			}
		}

		// Add order-by specifications after removing the last 'and' from the
		// previous string
		fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 4) + " order by " + clusteringKey
				+ " desc limit 1 ;";

		query = new SimpleStatement(fullQueryString);

		logger.debug("Query to return the last value for a wide column for a rowKey: " + fullQueryString);
		results = session.execute(query);

		try {
			if (results != null) {
				for (Row row : results) // Loop runs just one time for one row
				{
					endValue = getReturnValueforDatatype(row, clusteringKey, clusteringKeyDatatype);
					logger.debug(endValue.toString());
				}
			}

		} catch (Exception e) {
			logger.debug("Error in getLastWideColumnValue_WideCF(): " + e.getMessage());
			throw new Exception("Error in getLastWideColumnValue_WideCF()", e);
		}

		return endValue;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForClusteringKeyRangePerTimestep_WideCF(java.lang.String,
	 * java.lang.Object, java.lang.Object, int,
	 * com.siemens.bam.adm.studio.schema.DataModel, int, boolean) Used for only
	 * timestamp type wide columns
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForClusteringKeyRangePerTimestep_WideCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model, int timestep, boolean usetimestep_flag) throws Exception {

		logger.debug("In getValuesForClusteringKeyRangePerTimestep_WideCF()");

		/*
		 * Execute queries for every ten seconds and obtain results.
		 */

		if (usetimestep_flag) {
			/*
			 * Assumption: Only timestamp typed wide columns use this method of
			 * executing interval queries.
			 */

			NavigableMap<Object, Object> finalResultSet = new TreeMap<Object, Object>();

			DateTime startDate = new DateTime((Date) startCol);
			DateTime endDate = new DateTime((Date) endCol);

			// Creating the timestep (eg: 10 seconds) interval
			Period p = new Period(timestep * 1000);

			/*
			 * Get results for every timestep interval. For example: If the
			 * provided timestep is 10 seconds, then this loop gets results for
			 * every 10 seconds.
			 */
			for (DateTime begin = startDate; endDate.compareTo(begin) > 0; begin = begin.plus(p)) {
				DateTime endTime = (begin.plus(p).compareTo(endDate) > 0) ? endDate : begin.plus(p);
				NavigableMap<Object, Object> results = getValuesForClusteringKeyRange_WideCF(rowKey, begin, endTime,
						limit, model);
				finalResultSet.putAll(results);
			}
			return finalResultSet;
		}

		// Execute one query and get all results.
		else {
			NavigableMap<Object, Object> results = getValuesForClusteringKeyRange_WideCF(rowKey, startCol, endCol,
					limit, model);
			return results;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForOneColumnWithClusteringKeyRange_TallCF(java.lang.String,
	 * java.lang.Object, java.lang.Object, java.lang.String,
	 * com.siemens.bam.adm.studio.schema.Datatypes, int,
	 * com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForOneColumnWithClusteringKeyRange_TallCF(String rowKey,
			Object startCol, Object endCol, String columnName, DataTypes col_datatype, int limit, DataModel model)
			throws Exception {
		logger.debug("In getValuesForOneColumnWithClusteringKeyRange_TallCF()");
		Session session = ((CassandraNativeConnection) connection).getSession();

		NavigableMap<Object, Object> colList = new TreeMap<Object, Object>();

		// Get the Database and Table information from the model object.
		Table table = model.getTables().get(0);
		String tableName = table.getTableName();

		// Get the row-keys and clustering key
		List<TableColumn> cols = table.getColumns();
		List<String> rowKeyList = table.getRowKeys();
		String clusteringKey = table.getClusteringKeys().get(0);

		TableColumn tc = new TableColumn();
		tc.setColName(clusteringKey);
		int index = cols.indexOf(tc);
		DataTypes clusteringKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> rowKeyParts = new ArrayList<String>();
		String rowkeySeperator = table.getRowKeySeparator();

		// Make a list of the separate components of the provided row Key.
		if (rowkeySeperator != null && rowKey.contains(rowkeySeperator)) {
			rowKeyParts = Arrays.asList(rowKey.split("\\" + table.getRowKeySeparator()));
		} else {
			rowKeyParts.add(rowKey);
		}

		/*
		 * If the clustering Key is same as one of the primary Keys, then while
		 * forming the query the corresponding primary key condition is
		 * neglected. Only the clustering key condition is added. To achieve
		 * this, the column that is the same for clustering key and primary key,
		 * is removed from the rowKey list.
		 */

		if (rowKeyList.contains(clusteringKey)) {
			rowKeyList.remove(clusteringKey);
		}

		/*
		 * For Date and DateTime type ranges, the provided date is formatted
		 * according to CQL standards.
		 */
		if (startCol instanceof Date || startCol instanceof DateTime) {
			startCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(startCol));
			endCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(endCol));
		}

		// Start query preparation
		Statement query = null;
		String fullQueryString = "select " + clusteringKey + "," + columnName + " from " + tableName + " where ";

		// Add all primary key conditions
		for (int i = 0; i < rowKeyList.size(); i++) {
			TableColumn tc2 = new TableColumn();
			tc2.setColName(rowKeyList.get(i));
			if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.DATETIME)
					|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
							.equals(DataTypes.DATE)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='"
						+ DateFormattingUtils.convertDateToCassandraNativeStringFormat(new DateTime(rowKeyParts.get(i)))
						+ "' and ";
			} else if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.STRING)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='" + rowKeyParts.get(i) + "' and ";
			} else {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "=" + rowKeyParts.get(i) + " and ";
			}
		}

		// Add clustering key condition.
		TableColumn tc3 = new TableColumn();
		tc3.setColName(clusteringKey);
		if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType()).equals(DataTypes.STRING)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATETIME)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATE)) {
			fullQueryString = fullQueryString + clusteringKey + ">='" + startCol + "' and " + clusteringKey + "<='"
					+ endCol + "';";
		} else {
			fullQueryString = fullQueryString + clusteringKey + ">=" + startCol + " and " + clusteringKey + "<="
					+ endCol + ";";
		}

		// Add limit if provided.
		if (limit != 0) {
			fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 1) + " limit "
					+ String.valueOf(limit) + ";";
		}

		logger.debug("The complete query String is: " + fullQueryString);

		query = new SimpleStatement(fullQueryString);
		query.setFetchSize(ClientProperties.getPaginationLimit());

		if (connection == null || session == null) {
			logger.error("connection/session is null");
		}

		// Execute the query
		ResultSet results = null;

		try {
			logger.debug("Session is " + session.getState());

			results = session.execute(query);
		} catch (Exception e) {
			logger.error("Error in query execution: " + e.getMessage());
			throw new Exception("Error in query execution: ", e);
		}

		// Process the results
		try {
			if (results == null) {
				logger.debug("No results for this query!");
			} else {
				for (Row row : results) {
					colList.put(getReturnValueforDatatype(row, clusteringKey, clusteringKeyDatatype),
							getReturnValueforDatatype(row, columnName, col_datatype));
				}
			}
		} catch (Exception e) {
			logger.error("Error in getting results." + e.getMessage());
			throw new Exception("Error in getting results. ", e);

		}

		return colList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForAllColumnsWithClusteringKeyRange_TallCF(java.lang.String,
	 * java.lang.Object, java.lang.Object,
	 * com.siemens.bam.adm.studio.schema.DataModel, int)
	 */
	@Override
	public NavigableMap<Object, Map<Object, Object>> getValuesForAllColumnsWithClusteringKeyRange_TallCF(String rowKey,
			Object startCol, Object endCol, DataModel model, int limit) throws Exception {
		logger.debug("In getValuesForAllColumnsWithClusteringKeyRange_TallCF for Cassandra Native Driver");
		Session session = ((CassandraNativeConnection) connection).getSession();

		NavigableMap<Object, Map<Object, Object>> colList = new TreeMap<Object, Map<Object, Object>>();

		// Get the Database and Table information from the model object.
		Table table = model.getTables().get(0);
		String tableName = table.getTableName();

		// Get the row-keys and clustering key
		List<TableColumn> cols = table.getColumns();
		List<String> rowKeyList = table.getRowKeys();
		String clusteringKey = table.getClusteringKeys().get(0);

		TableColumn tc = new TableColumn();
		tc.setColName(clusteringKey);
		int index = cols.indexOf(tc);
		DataTypes clusteringKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> rowKeyParts = new ArrayList<String>();
		String rowkeySeperator = table.getRowKeySeparator();

		// Make a list of the separate components of the provided row Key.
		if (rowkeySeperator != null && rowKey.contains(rowkeySeperator)) {
			rowKeyParts = Arrays.asList(rowKey.split("\\" + table.getRowKeySeparator()));
		} else {
			rowKeyParts.add(rowKey);
		}

		/*
		 * If the clustering Key is same as one of the primary Keys, then while
		 * forming the query the corresponding primary key condition is
		 * neglected. Only the clustering key condition is added. To achieve
		 * this, the column that is the same for clustering key and primary key,
		 * is removed from the rowKey list.
		 */

		if (rowKeyList.contains(clusteringKey)) {
			rowKeyList.remove(clusteringKey);
		}

		/*
		 * For Date and DateTime type ranges, the provided date is formatted
		 * according to CQL standards.
		 */
		if (startCol instanceof Date || startCol instanceof DateTime) {
			startCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(startCol));
			endCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(endCol));
		}

		// Start query preparation
		Statement query = null;
		String fullQueryString = "select * from " + tableName + " where ";

		// Add all primary key conditions
		for (int i = 0; i < rowKeyList.size(); i++) {
			TableColumn tc2 = new TableColumn();
			tc2.setColName(rowKeyList.get(i));
			if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.DATETIME)
					|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
							.equals(DataTypes.DATE)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='"
						+ DateFormattingUtils.convertDateToCassandraNativeStringFormat(new DateTime(rowKeyParts.get(i)))
						+ "' and ";
			} else if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.STRING)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='" + rowKeyParts.get(i) + "' and ";
			} else {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "=" + rowKeyParts.get(i) + " and ";
			}
		}

		// Add clustering key condition.
		TableColumn tc3 = new TableColumn();
		tc3.setColName(clusteringKey);
		if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType()).equals(DataTypes.STRING)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATETIME)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATE)) {
			fullQueryString = fullQueryString + clusteringKey + ">='" + startCol + "' and " + clusteringKey + "<='"
					+ endCol + "';";
		} else {
			fullQueryString = fullQueryString + clusteringKey + ">=" + startCol + " and " + clusteringKey + "<="
					+ endCol + ";";
		}

		// Add limit if provided.
		if (limit != 0) {
			fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 1) + " limit "
					+ String.valueOf(limit) + ";";
		}

		logger.debug("The complete query String is: " + fullQueryString);

		query = new SimpleStatement(fullQueryString);
		query.setFetchSize(ClientProperties.getPaginationLimit());

		if (connection == null || session == null) {
			logger.error("connection/session is null");
		}

		// Execute the query
		ResultSet results = null;
		try {
			logger.debug("Session is " + session.getState());

			results = session.execute(query);
		} catch (Exception e) {
			logger.error("Error in query execution: " + e.getMessage());
			throw new Exception("Error in query execution: ", e);
		}

		// Process the results
		try {
			if (results == null) {
				logger.debug("No results for this query!");
			} else {
				Map<Object, Object> columnValueList = new TreeMap<Object, Object>();
				for (Row row : results) {
					Object key = getReturnValueforDatatype(row, clusteringKey, clusteringKeyDatatype);
					Iterator<TableColumn> tc_iterator = cols.iterator();

					while (tc_iterator.hasNext()) {
						TableColumn tableCol = tc_iterator.next();
						String colName = tableCol.getColName();

						// Check if the column is not Clustering Key column nor
						// RowKey columns
						if (!(colName.equalsIgnoreCase(clusteringKey) || rowKeyList.contains(colName))) {
							DataTypes col_datatype = DataTypes.getDatatypeFromOrdinal(tableCol.getColDataType());
							columnValueList.put(colName, getReturnValueforDatatype(row, colName, col_datatype));
						}
					}
					colList.put(key, columnValueList);
				}
			}
		} catch (Exception e) {
			logger.error("Error in getting results." + e.getMessage());
			throw new Exception("Error in getting results: ", e);
		}

		return colList;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForClusteringKeyRange_SuperColumnCF(java.lang.String,
	 * java.lang.Object, java.lang.Object, int,
	 * com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NavigableMap<Object, S> getValuesForClusteringKeyRange_SuperColumnCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model) throws Exception {
		logger.debug("In getValuesForClusteringKeyRange_SuperColumnCF ()");
		Session session = ((CassandraNativeConnection) connection).getSession();

		NavigableMap<Object, S> colList = new TreeMap<Object, S>();

		// Get the Database and Table information from the model object.
		Table table = model.getTables().get(0);
		String tableName = table.getTableName();

		// Get the row-keys and clustering key
		List<TableColumn> cols = table.getColumns();
		List<String> rowKeyList = table.getRowKeys();
		String clusteringKey = table.getClusteringKeys().get(0);

		TableColumn tc = new TableColumn();
		tc.setColName(clusteringKey);
		int index = cols.indexOf(tc);
		DataTypes clusteringKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> rowKeyParts = new ArrayList<String>();
		String rowkeySeperator = table.getRowKeySeparator();

		// Make a list of the separate components of the provided row Key.
		if (rowkeySeperator != null && rowKey.contains(rowkeySeperator)) {
			rowKeyParts = Arrays.asList(rowKey.split("\\" + table.getRowKeySeparator()));
		} else {
			rowKeyParts.add(rowKey);
		}

		/*
		 * If the clustering Key is same as one of the primary Keys, then while
		 * forming the query the corresponding primary key condition is
		 * neglected. Only the clustering key condition is added. To achieve
		 * this, the column that is the same for clustering key and primary key,
		 * is removed from the rowKey list.
		 */

		if (rowKeyList.contains(clusteringKey)) {
			rowKeyList.remove(clusteringKey);
		}

		/*
		 * For Date and DateTime type ranges, the provided date is formatted
		 * according to CQL standards.
		 */
		if (startCol instanceof Date || startCol instanceof DateTime) {
			startCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(startCol));
			endCol = DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(endCol));
		}

		// Get the super column type
		String superColumnName = "";
		SuperColumnType scolType = null;
		DataTypes superColumnDataType = null;
		Iterator<TableColumn> tc_iterator = cols.iterator();
		while (tc_iterator.hasNext()) {
			TableColumn tc2 = tc_iterator.next();
			if (tc2 instanceof SuperColumn) {
				logger.debug("The supercolumn type Column found!");
				superColumnName = tc2.getColName();
				scolType = ((SuperColumn) tc2).getScolType();
				superColumnDataType = DataTypes.getDatatypeFromOrdinal(tc2.getColDataType());
				break;
			}
		}

		// Start query preparation
		Statement query = null;
		String fullQueryString = "select * from " + tableName + " where ";

		// Add all primary key conditions
		for (int i = 0; i < rowKeyList.size(); i++) {
			TableColumn tc2 = new TableColumn();
			tc2.setColName(rowKeyList.get(i));
			if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.DATETIME)
					|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
							.equals(DataTypes.DATE)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='"
						+ DateFormattingUtils.convertDateToCassandraNativeStringFormat(new DateTime(rowKeyParts.get(i)))
						+ "' and ";
			} else if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc2)).getColDataType())
					.equals(DataTypes.STRING)) {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "='" + rowKeyParts.get(i) + "' and ";
			} else {
				fullQueryString = fullQueryString + rowKeyList.get(i) + "=" + rowKeyParts.get(i) + " and ";
			}
		}

		// Add clustering key condition
		TableColumn tc3 = new TableColumn();
		tc3.setColName(clusteringKey);
		if (DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType()).equals(DataTypes.STRING)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATETIME)
				|| DataTypes.getDatatypeFromOrdinal(cols.get(cols.indexOf(tc3)).getColDataType())
						.equals(DataTypes.DATE)) {
			fullQueryString = fullQueryString + clusteringKey + ">='" + startCol + "' and " + clusteringKey + "<='"
					+ endCol + "';";
		} else {
			fullQueryString = fullQueryString + clusteringKey + ">=" + startCol + " and " + clusteringKey + "<="
					+ endCol + ";";
		}

		// Add limit if provided
		if (limit != 0) {
			fullQueryString = fullQueryString.substring(0, fullQueryString.length() - 1) + " limit "
					+ String.valueOf(limit) + ";";
		}

		logger.debug("The complete query String is: " + fullQueryString);

		query = new SimpleStatement(fullQueryString);
		query.setFetchSize(ClientProperties.getPaginationLimit());

		if (connection == null || session == null) {
			logger.error("connection/session is null");
		}

		ResultSet results = null;

		// Execute the query
		try {
			logger.debug("Session is " + session.getState());

			results = session.execute(query);
		} catch (Exception e) {
			logger.error("Error in query execution: " + e.getMessage());
			throw new Exception("Error in query execution: ", e);

		}

		// Process the results
		try {
			if (results == null) {
				logger.debug("No results for this query!");
			} else {
				for (Row row : results) {
					colList.put(getReturnValueforDatatype(row, clusteringKey, clusteringKeyDatatype),
							(S) getReturnValueforSuperColtype(row, superColumnName, scolType, superColumnDataType));
				}
			}
		} catch (Exception e) {
			logger.error("Error in getting results." + e.getMessage());
			throw new Exception("Error in getting results: ", e);
		}

		return colList;
	}

	@SuppressWarnings("unchecked")
	public S getReturnValueforSuperColtype(Row row, String columnName, SuperColumnType scoltype,
			DataTypes superColumnType) {
		switch (scoltype) {
		case LIST:
			return (S) row.getList(columnName, getReturnTypeForSuperColumn(superColumnType));

		case MAP:
			return (S) row.getMap(columnName, getReturnTypeForSuperColumn(superColumnType),
					getReturnTypeForSuperColumn(superColumnType));

		case SET:
			return (S) row.getSet(columnName, getReturnTypeForSuperColumn(superColumnType));

		default:
			logger.debug("Undefined Super Col Type");
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	public <T> Class<T> getReturnTypeForSuperColumn(DataTypes dataType) {
		switch (dataType) {
		case BOOLEAN:
			return (Class<T>) Boolean.class;
		case FLOAT:
			return (Class<T>) Float.class;
		case DOUBLE:
			return (Class<T>) Double.class;
		case BIGINT:
			return (Class<T>) Long.class;
		case UUID:
			return (Class<T>) UUID.class;
		case DATE:
			return (Class<T>) Date.class;
		case DATETIME:
			return (Class<T>) DateTime.class;
		case INT:
			return (Class<T>) Integer.class;
		case STRING:
			return (Class<T>) String.class;

		case NULLINT:
		case SMALLINT:
		case XML:
		case UNKNOWN:
			return null;
		default:
			return null;

		}

	}

	@Override
	public void close() throws Exception {
		connection.closeConnection();

	}

}
