package com.siemens.bam.adm.studio.dbdriver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.cassandra.CassandraNativeDBDriver;
import com.siemens.bam.adm.studio.dbdriver.cassandra.CassandraThriftDBDriver;
import com.siemens.bam.adm.studio.dbdriver.hbase.HBaseDBDriver;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * A factory to generate the instance of the required Database Driver
 * 
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 * 
 */
public class DatabaseDriverFactory {
	final static Logger logger = LoggerFactory.getLogger(DatabaseDriverFactory.class.getName());

	public static AbstractDatabaseDriver getDataBaseDriver(ClientProperties clientProp) throws Exception {
		String dbType = null;
		
			dbType = clientProp.getDataStoreName();
		
		if (dbType.equalsIgnoreCase("CassandraNative")) {
			logger.debug("Instance of CassandraNativeDBDriver returned.");
			return new CassandraNativeDBDriver<Object>();
			
		} else if(dbType.equalsIgnoreCase("CassandraThrift"))
		{
			logger.debug("Instance of CassandraThriftDBDriver returned.");
			return new CassandraThriftDBDriver();
		}
		else if(dbType.equalsIgnoreCase("HBase")) {
			logger.debug("Instance of HBaseDBDriver returned.");
			return new HBaseDBDriver<Object>();
		}
		
		/*
		 * To add a new datastore add an else-if condition here.
		 */
		else {
			logger.error("No such DB implemented!");

			throw new DBException("No such DB implemented!");

		}

	}
}
