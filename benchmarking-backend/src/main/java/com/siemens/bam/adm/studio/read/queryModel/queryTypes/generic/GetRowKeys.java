/**
 *
 */
package com.siemens.bam.adm.studio.read.queryModel.queryTypes.generic;

import java.io.FileOutputStream;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.metric.ReadMetric;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.generic.GetRowKeysQueryParams;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;

import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import com.siemens.bam.adm.studio.utils.GlobalData;

import org.apache.hadoop.hbase.util.Bytes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class for executing Get Row Keys Query
 * @author Nisha Menon
 */
public class GetRowKeys extends AbstractQuery
{
	private static final Logger logger = LoggerFactory.getLogger(GetRowKeys.class);

	private GetRowKeysQueryParams queryParams;
	public ReadMetric executionStats;

	/**
	 * @param model
	 */
	public GetRowKeys(DataModel model)
	{
		super(model);
		logger.debug("GetRowKeys Query instantiated");
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getQueryParams()
	 */
	@Override
	public GetRowKeysQueryParams getQueryParams()
	{
		return this.queryParams;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#initializeParameters(java.lang.String[])
	 */
	@Override
	public boolean initializeParameters(String[] parameterList)
	{
		// Creating new queryParams object and executionStats Map for every new query.
		this.queryParams = new GetRowKeysQueryParams();
		this.executionStats  = new ReadMetric();

		try
		{
			Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
			String param;
			int count = 0;

			while (count < parameterList.length)
			{
				param = parameterList[count];
				count++;

				if(queryNameTypes.contains(param))
				{
					queryParams.setQueryName(param);
					logger.debug("Query Name set to '"+ queryParams.getQueryName() + "'");
				}
				else if (param.equalsIgnoreCase("limit"))
				{
					String value = parameterList[count];
					count++;
					try
					{
						queryParams.setParamString(param.toLowerCase());
						int paramValue = Integer.parseInt(value);
						queryParams.setParamValue(paramValue);
					}
					catch (NumberFormatException e)
					{
						logger.warn("Could not parse '" + value+ "', defaulting limit to 10");
					}
					logger.debug("Limit set to: " + queryParams.getParamValue());
				}

				else if (param.equalsIgnoreCase("top"))
				{
					String value = parameterList[count];
					count++;
					try
					{
						queryParams.setParamString(param.toLowerCase());
						int paramValue = Integer.parseInt(value);
						queryParams.setParamValue(paramValue);
					}
					catch (NumberFormatException e)
					{
						logger.warn("Could not parse '" + value+ "', defaulting top to 10");
					}
					logger.debug("Top set to: " + queryParams.getParamValue());
				}
				else if (param.equalsIgnoreCase("last"))
				{
					String value = parameterList[count];
					count++;
					try
					{
						queryParams.setParamString(param.toLowerCase());
						int paramValue = Integer.parseInt(value);
						queryParams.setParamValue(paramValue);
					}
					catch (NumberFormatException e)
					{
						logger.warn("Could not parse '" + value+ "', defaulting last to 10");
					}
					logger.debug("Last set to: " + queryParams.getParamValue());
				}

				/**
				 * if random word is given as a command
				 */
				else
				{
					logger.error("'"+param + "' is an unknown command.");
					return false;
				}
			}
			logger.debug("Query Parameters set successfully!");
			return true;
		}
		catch (Exception e)
		{
			logger.error("Unexpected exception has occured -> " + e.toString());
			logger.info("Type 'help' for more information");
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#findExecutionTime()
	 */
	@Override
	public long findExecutionTime()
	{
		return executionStats.getQueryResponseTime();

		/* long maxThreadTime = 0;
		 * for (Map.Entry<String, ReadMetric> entry : executionStats.entrySet())
		{
			logger.info("The time for rowkey <"+entry.getKey()+"> is : " + entry.getValue().getQueryResponseTime());
		    if (entry.getValue().getQueryResponseTime() > maxThreadTime)
		    {
		    	maxThreadTime = entry.getValue().getQueryResponseTime();
		    }
		}

		return maxThreadTime;*/
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#queryExecute(java.lang.String, com.siemens.bam.adm.studio.dbdriver.IDatabaseDriver)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception
	{
		//Object[] result = new Object[2];
		long startThreadTime = System.currentTimeMillis();

		TreeSet<String> results = dbDriver.getAllRowKeys(queryParams.getParamString(),queryParams.getParamValue(), model);
		logger.info("Number of rowkeys is: " + results.size());

		//result[0] = rowKey;
		//result[0] = results;
		long endThreadTime = System.currentTimeMillis();

		executionStats.setEndTime(endThreadTime);
		executionStats.setStartTime(startThreadTime);
		executionStats.setQueryResponseTime(endThreadTime-startThreadTime);

		return results;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getResults(int, java.util.concurrent.CompletionService)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public TreeSet<String> getResults(int numberOfrowkeys, CompletionService completion_pool) throws Exception
	{
		TreeSet<String> QueryResult = new TreeSet<String>();
		try
		{
			Object colList = (Object) completion_pool.take().get();

			QueryResult = (TreeSet<String>)colList;
		}
		catch (InterruptedException ex)
		{
			throw new Exception("InterruptedException thrown in getResults()",ex);
		}
		catch (ExecutionException ex)
		{
			throw new Exception("ExecutionException thrown in getResults()",ex);
		}
		return QueryResult;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#printResults(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void printResults(T resultSet)
	{
		TreeSet<String> rowKeys = (TreeSet<String>) resultSet; //uniqueRowKeys (unique UID+Date) combinations
		if (rowKeys == null)
		{
			logger.info("No rowKeys.");
		}
		else
		{
			try
			{
				FileOutputStream fs = new FileOutputStream(ClientProperties.getRowkeyFilePath());
				GlobalData.queryResult = "";
				for(String rowKey : rowKeys)
				{
					logger.info(rowKey);
					GlobalData.queryResult =  GlobalData.queryResult + "|" + rowKey;
						fs.write(Bytes.toBytes(rowKey));
				    fs.write(System.getProperty("line.separator").getBytes());
				}
				logger.info("Number of unique Rowkeys are: " + rowKeys.size());
				fs.close();
			}
			catch(Exception e)
			{
				logger.error("File Exception"+e.getMessage());
			}
		}

	}

}
