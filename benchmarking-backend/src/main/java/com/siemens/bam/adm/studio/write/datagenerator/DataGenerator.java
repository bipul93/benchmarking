package com.siemens.bam.adm.studio.write.datagenerator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import com.siemens.bam.adm.studio.utils.GlobalData;

/**
 * The DataGenerator is used to generate random data for columns with the data
 * types defined in the schema. This is used for write workloads.
 *
 * Currently the data generator supports the following datatypes:
 *  datetime offset,
 *  date offset
 * 	uuid,
 *  boolean,
 *  tiny int,
 *  small int,
 *  int,
 *  big int,
 *  null int,
 *  float,
 *  double,
 *  varchar and
 *  bytes
 *
 * @author varsha.raveendran@siemens.com
 *
 *
 */
public class DataGenerator {

	final static Logger logger = LoggerFactory.getLogger(DataGenerator.class.getName());

	private boolean isReversedTimestamp = false;

	public void init(DataModel dataModel, ClientProperties clientProp) throws Exception {

		Table table = dataModel.getTable(clientProp.getTableName());
		isReversedTimestamp = table.isTableReversedOnTime();
		table.getTypeOfTable().setDataGenerator(this);
		table.getTypeOfTable().init(table,clientProp);

	}

	public Object generateValueForField(String fieldName, Integer fieldType) throws Exception {

		Record record = new Record();
		logger.debug("DataGenerator::generateValueForField(String fieldName, Integer fieldType): Generate value for [" + fieldName
				+ "] : [" + DataTypes.getDatatypeFromOrdinal(fieldType));

		if (DataTypes.getDatatypeFromOrdinal(fieldType) == DataTypes.DATETIME && isReversedTimestamp) {
			return record.generateReversedTimeStamp();
		}

		return record.generateValuesForFieldName(fieldName, fieldType);
	}

	/**
	 * The method is called if unique values need to be generated for a
	 * particular column at the beginning of a write workload. These values are
	 * stored in a Map. The number of unique values that are generated can be
	 * specified in Property.conf.
	 *
	 * While inserting records individual threads pick a value randomly from
	 * this Map to write a value for the column to the datastore.
	 *
	 * @param table
	 * @param clientProp
	 * @param listOfKeyNames
	 * @return Map<String, List<Object>> A Map with key = ColumnName and Value =
	 *         List of Unique Objects generated for the column
	 * @throws Exception
	 */
	public Map<String, List<Object>> generateUniqueData(Table table, ClientProperties clientProp,
			List<String> listOfKeyNames) throws Exception {

		Record record = new Record();

		Map<String, List<Object>> uniqueDataList = new ConcurrentHashMap<String, List<Object>>();
		GlobalData.queryResult = "";
		List<TableColumn> columns = table.getColumns();
		logger.debug("Table to insert " + table.getTableName());
		int numKeys = Integer.parseInt(clientProp.getnumberOfKeysToBeGenerated());
		logger.debug("Generating {} Unique Keys", numKeys);
		for (int i = 0; i < listOfKeyNames.size(); i++) {

			logger.debug("Generating data for Partition Key <" + listOfKeyNames.get(i) + ">");

			List<Object> tempData = new ArrayList<Object>();

			TableColumn c = new TableColumn();
			c.setColName(listOfKeyNames.get(i));
			int index = columns.indexOf(c);
			if (columns.get(index).getFixedValues() != null
					&& columns.get(index).getFixedValues().size() > 0) {
						GlobalData.queryResult = GlobalData.queryResult + "| Predefined values are set for this column and hence does not need to be generated.";
				logger.debug("Predefined values are set for this column and hence does not need to be generated.");

			}
			else {
				for (int j = 0; j < numKeys; j++) {

					tempData.add(record.generateValuesForFieldName(
					listOfKeyNames.get(i),
					columns.get(index).getColDataType()));
				}
				uniqueDataList.put(listOfKeyNames.get(i), tempData);
				GlobalData.queryResult = GlobalData.queryResult + "|" + "No of Unique keys generated for "
						+ listOfKeyNames.get(i) + " : " + tempData.size();
				logger.debug("No of Unique keys generated for "
						+ listOfKeyNames.get(i) + " : " + tempData.size());
			}
		}
		return uniqueDataList;
	}

	public boolean isReversedTimestamp() {
		return isReversedTimestamp;
	}

}
