package com.siemens.bam.adm.studio.metric;

/**
 *  Class that defines Metrics information to be collected
 */

public class ReadMetric {

	private long startTime;
	private long endTime;
	private long queryResponseTime;
	private int numberOfDatapoints;
	
	public ReadMetric(){
		startTime = 0;
		endTime = 0;
		queryResponseTime = 0;
		numberOfDatapoints = 0;
	}
	public long getQueryResponseTime() {
		return queryResponseTime;
	}

	public void setQueryResponseTime(long queryResponseTime) {
		this.queryResponseTime = queryResponseTime;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public int getNumberOfDatapoints() {
		return numberOfDatapoints;
	}

	public void setNumberOfDatapoints(int numberOfDatapoints) {
		this.numberOfDatapoints = numberOfDatapoints;
	}
	
	
}
