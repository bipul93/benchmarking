package com.siemens.bam.adm.studio.dbdriver.hbase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.EnumUtils;
import org.apache.hadoop.hbase.client.Durability;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.BinaryComparator;
import org.apache.hadoop.hbase.filter.CompareFilter.CompareOp;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.FilterList;
import org.apache.hadoop.hbase.filter.FirstKeyOnlyFilter;
import org.apache.hadoop.hbase.filter.PrefixFilter;
import org.apache.hadoop.hbase.filter.QualifierFilter;
import org.apache.hadoop.hbase.filter.RowFilter;
import org.apache.hadoop.hbase.util.Bytes;
import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.connections.HBaseConnection;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.SuperColumn;
import com.siemens.bam.adm.studio.schema.SuperColumn.SuperColumnType;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * The HBaseDBDriver class contains methods that perform operations in HBase
 * This includes getting a connection instance and writing to/reading from HBase
 * Column Family
 * 
 * @author nisha.menon@siemens.com
 * @author varsha.raveendran@siemens.com
 * @param <S>
 *
 */
public class HBaseDBDriver<S> extends AbstractDatabaseDriver {

	final static Logger logger = LoggerFactory.getLogger(HBaseDBDriver.class.getName());
		
	private static Map<String, LinkedList<Put>> batchStatements = new ConcurrentHashMap<String, LinkedList<Put>>();
	private static HTableInterface table;
	private static String columnFamily;
	private byte columnFamilyBytes[];
	private Durability durability;
	private static String dataBase;

	public HBaseDBDriver() {}

	public HBaseDBDriver(Environment env) throws Exception {
		super();
		try {
			connection = new HBaseConnection(env);
			 

			if (EnumUtils.isValidEnum(Durability.class, HBaseConnection.getHbaseProperties().getHBaseDurability().toUpperCase())) {

				this.durability = Durability.valueOf(HBaseConnection.getHbaseProperties().getHBaseDurability().toUpperCase());
			} else {
				logger.debug("No Durability Level " + HBaseConnection.getHbaseProperties().getHBaseDurability().toUpperCase()
						+ " available in HBase. Please check the Property file");
				throw new Exception("No Durability Level " + HBaseConnection.getHbaseProperties().getHBaseDurability().toUpperCase()
						+ " available in HBase. Please check the Property file");
			}
		} catch (Exception e) {

			throw e;
		}
	}


	/*
	 * Each thread should have its own instances of HBaseDBDriver and
	 * HBaseConnection
	 * 
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#_initWrites(com.
	 * siemens.bam.adm.studio.core.Environment)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public AbstractDatabaseDriver initWrites(Environment environment) throws DBException, Exception {

		HBaseDBDriver hDriver = new HBaseDBDriver(environment);
		try {

			if (environment.getClientProperties().isBatchInserts().equals("true")) {
				LinkedList<Put> batchStmt = new LinkedList<Put>();
				batchStatements.put(Thread.currentThread().getName(), batchStmt);
			}

		} catch (Exception e) {
			logger.error("Exception in HBase init()");
		
			throw e;
		}

		String columnFamily = environment.getClientProperties().getTableName();
		dataBase = environment.getClientProperties().getDatabaseName();
		logger.debug("Database to connect is " + dataBase);

		logger.debug(columnFamily);
		hDriver.columnFamilyBytes = initColumnFamily(columnFamily);
		return hDriver;
	}

	private byte[] initColumnFamily(String columnFamily) throws DBException {
		logger.debug("initColumnFamily - Database to connect is " + dataBase);

		if (columnFamily == null) {
			logger.error("Error, must specify a columnfamily for HBase table");
			throw new DBException("Error, must specify a columnfamily for HBase table");

		}
		byte[] columnFamilyBytes = Bytes.toBytes(columnFamily);
		return columnFamilyBytes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * insertRecordsIndividually(java.util.List,
	 * com.siemens.bam.adm.studio.schema.tableType.ITableType)
	 */
	@Override
	public boolean insertRecordsIndividually(List<TableColumn> columns, Table table) throws DBException, Exception {
		logger.debug("insertRecordsIndividually - Database to connect is " + dataBase);

		HTableInterface hTableInterface = null;
		try {
			hTableInterface = getHTable(dataBase);

		} catch (IOException e) {
			logger.error("Error accessing HBase table: " + e);
			throw new DBException(e);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new DBException(e);
		}

		List<String> primaryKeys = table.getRowKeys();
		List<String> clusteringKeys = table.getClusteringKeys();

		byte[] primaryKey = Bytes.toBytes(getPrimaryKey(table, primaryKeys, columns).toString());

		logger.debug("[Thread-" + Thread.currentThread().getId() + "] Setting up put for key: "
				+ Bytes.toString(primaryKey));

		Put p = new Put(primaryKey);
		p.setDurability(durability);
		for (TableColumn entry : columns) {

			if (!(primaryKeys.contains(entry.getColName()) || clusteringKeys.contains(entry.getColName()))) {

				logger.debug("[Thread-" + Thread.currentThread().getId() + "] Adding field/value " + entry.getColName()
						+ "/" + DataTypes.getDatatypeFromOrdinal(entry.getColDataType()) + " to put request");

				// ColName is built with clustering keys
				String colName = table.getTypeOfTable().buildColumnName(entry.getColName(), table.getClusteringKeys(),
						table.getColumns());

				Object values = table.getTypeOfTable().getValueForField(entry);

				if (entry instanceof SuperColumn) {

					values = setCollection(entry, ((SuperColumn) entry).getScolType(), values);
				}

				p.add(columnFamilyBytes, Bytes.toBytes(colName), Bytes.toBytes(values.toString()));
			}
		}

		try {
			hTableInterface.put(p);
		} catch (IOException e) {

			logger.error("Error doing put: " + e);

			return false;
		} catch (ConcurrentModificationException e) {

			return false;
		} finally {
			putHTable(hTableInterface);
		}

		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#addStatementsToBatch(
	 * int, java.util.List,
	 * com.siemens.bam.adm.studio.schema.tableType.ITableType,
	 * java.lang.Boolean)
	 */
	@Override
	public void addStatementsToBatch(int batchSize, List<TableColumn> columns, Table table, Boolean isUniqueKey)
			throws Exception {

		List<String> primaryKeys = table.getRowKeys();
		List<String> clusteringKeys = table.getClusteringKeys();
		byte[] primaryKeyDataValue = null;

		if (isUniqueKey == true) {
			primaryKeyDataValue = Bytes.toBytes(getPrimaryKey(table, primaryKeys, columns).toString());
			logger.debug("[Thread-" + Thread.currentThread().getId() + "] Setting up put for key: "
					+ Bytes.toString(primaryKeyDataValue));
		}

		int numberInCurrentBatch = 0;
		while (numberInCurrentBatch < batchSize) {
			byte[] pkValue;
			// if it is not unique key inserts then primarykeys, clusteringkeys
			// and primarykey will be set
			if (primaryKeyDataValue == null) {

				pkValue = Bytes.toBytes(getPrimaryKey(table, primaryKeys, columns).toString());
				logger.debug("[Thread-" + Thread.currentThread().getId() + "] Setting up put for key: "
						+ Bytes.toString(primaryKeyDataValue));
			} else {
				pkValue = primaryKeyDataValue;
			}

			Put p = new Put(pkValue);
			p.setDurability(durability);
			for (TableColumn entry : columns) {
				logger.debug("[Thread-" + Thread.currentThread().getId() + "] Adding field/value " + entry.getColName()
						+ "/" + entry.getColDataType() + " to put request");

				if (!(primaryKeys.contains(entry.getColName()) || clusteringKeys.contains(entry.getColName()))) {

					String colName = table.getTypeOfTable().buildColumnName(entry.getColName(),
							table.getClusteringKeys(), table.getColumns());
					Object values = table.getTypeOfTable().getValueForField(entry);
					if (entry instanceof SuperColumn) {

						values = setCollection(entry, ((SuperColumn) entry).getScolType(), values);
					}
					p.add(columnFamilyBytes, Bytes.toBytes(colName), Bytes.toBytes(values.toString()));
				}
			}

			batchStatements.get(Thread.currentThread().getName()).add(p);
			numberInCurrentBatch++;
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#insertbatch()
	 */
	@Override
	public boolean insertbatch() throws DBException {

		HTableInterface hTableInterface = null;
		try {
			hTableInterface = getHTable(dataBase);
		} catch (IOException e) {
			logger.error("Error accessing HBase table: " + e);
			throw new DBException(e);
		}
		try {
			logger.debug("Inserting batch");
			hTableInterface.put(batchStatements.get(Thread.currentThread().getName()));
		} catch (IOException e) {
			logger.error("Error doing put: " + e);
			return false;
		} catch (ConcurrentModificationException e) {
			return false;
		} finally {
			putHTable(hTableInterface);
			batchStatements.get(Thread.currentThread().getName()).clear();
		}
		return true;
	}

	/**
	 * @param <T>
	 * @param entry
	 * @param scolType
	 * @param values
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private <T> Object setCollection(TableColumn entry, SuperColumnType scolType, Object values) {
		switch (scolType) {

		case LIST:

			LinkedList<T> list = new LinkedList<T>();

			list = (LinkedList<T>) values;

			String lists = new String();

			for (T element : list) {
				lists = lists + element + "|";

			}

			return lists;
		case SET:

			logger.error("Set not implemented in HBase inserts");
			return null;
		case MAP:

			logger.error("Map not implemented in HBase inserts");
			return null;

		default:
			logger.error("No such Super Column type implemented! ");
			System.exit(0);

		}
		return null;
	}

	public HTableInterface getHTable(String table) throws IOException, DBException {
		logger.debug("Database to connect is " + table);

		HTableInterface t = null;
		try {

			if (((HBaseConnection) connection).getConnection() == null) {
				logger.error("HConnection is null");
			}
			if (((HBaseConnection) connection).getExecutor() == null) {
				logger.error("Executor is null");
			}
			t = ((HBaseConnection) connection).getConnection().getTable(table,
					((HBaseConnection) connection).getExecutor());

			t.setAutoFlushTo(false);
		} catch (Exception e) {
			e.printStackTrace();
			throw new DBException("Error while getting HTable " + e.getMessage());
		}
		return t;
	}

	private Object getPrimaryKey(Table table, List<String> primaryKeys, List<TableColumn> columns) throws Exception {

		String firstPK = primaryKeys.get(0);
		Object pkValue = null;

		TableColumn col = new TableColumn();
		col.setColName(firstPK);
		int index = columns.indexOf(col);
		pkValue = table.getTypeOfTable().getValueForField(columns.get(index));

		if (primaryKeys.size() > 1) {

			for (String primaryKey : primaryKeys) {

				if (!primaryKey.equalsIgnoreCase(firstPK)) {
					logger.debug("Generating Primary Key with " + primaryKey);
					// + converts Object to String
					col.setColName(primaryKey);
					index = columns.indexOf(col);

					logger.debug("Type of PK added : "
							+ DataTypes.getDatatypeFromOrdinal(columns.get(index).getColDataType()));
					logger.debug("Size of values " + columns.get(index).getFixedValues().size());
					pkValue += table.getRowKeySeparator();
					pkValue += table.getTypeOfTable().getValueForField(columns.get(index)).toString();

				}

			}

		}

		return pkValue;
	}

	private void putHTable(HTableInterface t) throws DBException {

		if (t != null) {
			try {
				t.close();
			} catch (IOException e) {
				logger.error("Error while closing HTable connection!");
				throw new DBException(e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#_initReads(com.
	 * siemens.bam.adm.studio.core.Environment)
	 */
	@Override
	public AbstractDatabaseDriver initReads(Environment environment) throws DBException, Exception {
		connection = new HBaseConnection(environment);
		table = (HTableInterface) ((HBaseConnection) connection).getHTableInterface();
		columnFamily = environment.getClientProperties().getTableName();
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForClusteringKeyRange_WideCF(java.lang.String, java.lang.Object,
	 * java.lang.Object, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForClusteringKeyRange_WideCF(String rowKey, Object startCol, Object endCol,
			int limit, DataModel model) throws Exception
	{
		logger.debug("In getValuesForClusteringKeyRange_WideCF() method"); 

		// Initialize the respective data structures
		NavigableMap<Object, Object> colList = new TreeMap<Object, Object>();
		NavigableMap<byte[], byte[]> colValues = null;

		// Create a filterList that will contain all filters for this query 
		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		
		// Create a Row Filter on the provided row key
		Filter startRow = new RowFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(rowKey)));
		logger.debug("RowFilter is: " + startRow.toString());

		// Create Qualifier Filters
		Filter startRange;
		Filter endRange;		
		
		/*
		 * If the provided clustering key is of Date or DateTime type, then it a further check for reversed time table is done.
		 * Once the check passes, the start and end range filters are built with suitable computation as performed during a reversed table write.
		 */
		if (startCol instanceof Date || startCol instanceof DateTime) 
		{
			if (ClientProperties.isTableReversedOnTime()) 
			{
				logger.info("Reversed Table Read!");

				endRange = new QualifierFilter(CompareOp.LESS_OR_EQUAL, new BinaryComparator(
						Bytes.toBytes(Long.toString(Long.MAX_VALUE - new DateTime((Date) startCol).getMillis()))));
				startRange = new QualifierFilter(CompareOp.GREATER_OR_EQUAL, new BinaryComparator(
						Bytes.toBytes(Long.toString(Long.MAX_VALUE - new DateTime((Date) endCol).getMillis()))));

				logger.debug("Range is from: " + endRange.toString() + " to : " + startRange.toString());
			} 
			else 
			{
				startRange = new QualifierFilter(CompareOp.GREATER_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes(Long.toString(new DateTime((Date) startCol).getMillis()))));
				endRange = new QualifierFilter(CompareOp.LESS_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes(Long.toString(new DateTime((Date) endCol).getMillis()))));

				logger.debug("Range is from: " + startRange.toString() + " to : " + endRange.toString());

			}
		} 
		else 
		{
			startRow = new RowFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(rowKey)));
			startRange = new QualifierFilter(CompareOp.GREATER_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes(startCol.toString())));
			endRange = new QualifierFilter(CompareOp.LESS_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes(endCol.toString())));
		}

		filterList.addFilter(startRange);
		filterList.addFilter(endRange);
		filterList.addFilter(startRow);
		
		// Create a HBase scan object and add all the filters created earlier
		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes(columnFamily));
		scan.setFilter(filterList);
		scan.setCaching(ClientProperties.getPaginationLimit()); // Set the pagination limit

		if (limit != 0)
			scan.setMaxResultsPerColumnFamily(limit); // Limit the results

		// Query Execution and Processing results
		ResultScanner scanner;
		try {
			scanner = table.getScanner(scan);

			logger.debug("Scan completed!");

			if (scanner == null) {
				logger.debug("scanner is null");
			} else {
				logger.debug("Results is not null");
				
				// The results are also processed based on if the clustering key is of Date or DateTime type and if its reversed or not.
				if (startCol instanceof Date || startCol instanceof DateTime) {
					if (ClientProperties.isTableReversedOnTime()) {
						for (Result result : scanner) {
							if (!result.isEmpty()) {
								colValues = result.getFamilyMap(Bytes.toBytes(columnFamily));
								if (!colValues.isEmpty()) {
									for (Map.Entry<byte[], byte[]> entry : colValues.entrySet()) {
										colList.put(
												new Date(Long.MAX_VALUE
														- Long.parseLong(Bytes.toString(entry.getKey()))),
												Bytes.toString(entry.getValue()));
									}
								}
							}
						}
					} else {
						for (Result result : scanner) {
							if (!result.isEmpty()) {
								colValues = result.getFamilyMap(Bytes.toBytes(columnFamily));
								if (!colValues.isEmpty()) {
									for (Map.Entry<byte[], byte[]> entry : colValues.entrySet()) {
										colList.put(new Date(Long.parseLong(Bytes.toString(entry.getKey()))),
												Bytes.toString(entry.getValue()));
									}
								}
							}
						}
					}
				}

				else {
					for (Result result : scanner) {
						if (!result.isEmpty()) {
							colValues = result.getFamilyMap(Bytes.toBytes(columnFamily));
							if (!colValues.isEmpty()) {
								for (Map.Entry<byte[], byte[]> entry : colValues.entrySet()) {
									colList.put(Bytes.toString(entry.getKey()), Bytes.toString(entry.getValue()));
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			logger.debug("Exception in query execution and processing:", e.getMessage());
			throw new Exception("Exception in query execution and processing: ",e);
		}

		return colList;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getAllRowKeys(java.
	 * lang.String, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public TreeSet<String> getAllRowKeys(String parameter, int value, DataModel model) throws Exception 
	{
		logger.debug("In getAllRowKeys() method.");

		// Initialize the respective data structures
		TreeSet<String> setOfRowkeys = new TreeSet<String>();
		
		// Create a HBase Scan object and add the respective conditions
		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes(columnFamily));

		/*
		 * This is done here, so that when the query is fired with 'limit'
		 * condition, the query retrieves only that many records from the
		 * database!
		 */
		int limitValue;
		if (parameter.equals("limit"))
			limitValue = value;
		else
			limitValue = Integer.MAX_VALUE;

		logger.debug("Limit in getAllRowKeys is: " + limitValue);
		scan.setMaxResultSize(limitValue);

		/*
		 *  Filter to retrieve only keys from HBase table. This is faster
		 *  compared to retrieving whole data from the table.
		 */
		scan.setFilter(new FirstKeyOnlyFilter());
		scan.setCaching(ClientProperties.getPaginationLimit());

		// Query Execution and Processing results
		try {
			ResultScanner scanner = table.getScanner(scan);
			for (Result result : scanner) {
				String key = Bytes.toString(result.getRow());
				setOfRowkeys.add(key);
			}
		} catch (IOException e) {
			logger.error("Error in query execution and processing",e);
			throw new Exception ("Error in query execution and processing", e);
		}
		logger.debug("The set size is:" + setOfRowkeys.size());

		/*
		 *  For all other parameters, such as 'top' and 'last', the query gets
		 *  all results from the database and then processes it to take only
		 *  required number of records
		 */
		
		if (parameter.equals("limit") && value != 0) {
			ArrayList<String> rowkeyList = new ArrayList<String>(setOfRowkeys);
			Collections.shuffle(rowkeyList);

			TreeSet<String> rowkeySet = new TreeSet<String>();
			int count = 0;

			for (String value1 : rowkeyList) {
				rowkeySet.add(value1);
				count++;
				if (count == value)
					break;
			}
			return rowkeySet;
		}

		if (parameter.equals("top") && value != 0) {
			TreeSet<String> rowkeySet = new TreeSet<String>();
			int count = 0;

			for (String value1 : setOfRowkeys) {
				rowkeySet.add(value1);
				count++;
				if (count == value)
					break;
			}
			return rowkeySet;
		}

		if (parameter.equals("last") && value != 0) {
			TreeSet<String> rowkeySet = new TreeSet<String>();
			int count = 0;
			Iterator<String> iterator = setOfRowkeys.descendingIterator();

			while (iterator.hasNext() && count != value) {
				rowkeySet.add((String) iterator.next());
				count++;
			}
			return rowkeySet;
		} else
			return setOfRowkeys;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getLastColumnValue_WideCF(java.lang.String, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public Object getLastColumnValue_WideCF(String rowKey, DataModel model) throws Exception
	{
		// Initialize the respective data structures
		NavigableMap<byte[], byte[]> colValues = null;
		Object endValue = null;

		// Get the table information from the model object.
		Table hbaseTable = model.getTables().get(0);

		// Get the clustering key and its data type
		String clusteringKey = hbaseTable.getClusteringKeys().get(0);
		List<TableColumn> cols = hbaseTable.getColumns();
		TableColumn tc = new TableColumn();
		tc.setColName(clusteringKey);
		int index = cols.indexOf(tc);
		DataTypes clusteringKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		// Create a HBase Scan object and add the respective conditions
		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes(columnFamily));
		scan.setFilter(new FirstKeyOnlyFilter());
		
		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		
		// Create a Row Filter on the provided row key
		Filter startRow = new RowFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(rowKey)));
		filterList.addFilter(startRow);
		
		scan.setFilter(filterList);

		logger.debug("Filters are: "+filterList);
		
		// Query Execution and Processing results
		ResultScanner scanner;
		try {
			scanner = table.getScanner(scan);
			logger.debug("Scan completed!");

			if (scanner == null) {
				logger.debug("scanner is null");
			} else {
				logger.debug("Results is not null");

				for (Result result : scanner) {
					if (!result.isEmpty()) 
					{
						// Get the map for one column family
						colValues = result.getFamilyMap(Bytes.toBytes(columnFamily)); 
						if (!colValues.isEmpty()) {				
							for (Map.Entry<byte[], byte[]> entry :colValues.entrySet()) 
							{								
								// How is it certain that the last value is returned? HBase also sorts.. but pick only last value. 
								// Currently all values are put into the map, the last value is the one that is returned. 
								if ((clusteringKeyDatatype.equals(DataTypes.DATE) || clusteringKeyDatatype.equals(DataTypes.DATETIME)))
								{
									if(ClientProperties.isTableReversedOnTime())
									{
										endValue = new Date( Long.MAX_VALUE - Long.parseLong(Bytes.toString(entry.getKey())));
									}
									
									else
									{
										endValue = new Date(Long.parseLong(Bytes.toString(entry.getKey())));
									}
								}
								else
								{
									endValue = Bytes.toString(entry.getKey());
								}
							}
						}
					}	
				}
			}
				
			} catch (IOException e) {
				logger.debug("Error in query execution and processing",e.getMessage());
				throw new Exception("Error in query execution and processing",e);
			}
		
		return endValue;
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#
	 * getValuesForClusteringKeyRangePerTimestep_WideCF(java.lang.String,
	 * java.lang.Object, java.lang.Object, int,
	 * com.siemens.bam.adm.studio.schema.DataModel, int, boolean)
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForClusteringKeyRangePerTimestep_WideCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model, int timestep, boolean usetimestepFlag) throws Exception 
	{
		
		logger.debug("In getValuesForClusteringKeyRangePerTimestep_WideCF ()");

		/*
		 *  Fire queries for every ten seconds and obtain results.
		 */
		if (usetimestepFlag) {
			/*
			 *  Assumption: Only timestamp type wide columns use this method of
			 *  firing interval queries.
			 */

			NavigableMap<Object, Object> Results = new TreeMap<Object, Object>();

			DateTime startDate = new DateTime((Date) startCol);
			DateTime endDate = new DateTime((Date) endCol);
			
			// Creating the time step times interval
			Period p = new Period(timestep * 1000); 

			/*
			 * Get results for every timestep interval. 
			 * For example: If the provided timestep is 10 seconds, then this loop gets results for every 10 seconds.
			 */
			for (DateTime begin = startDate; endDate.compareTo(begin) > 0; begin = begin.plus(p)) {
				DateTime endTime = (begin.plus(p).compareTo(endDate) > 0) ? endDate : begin.plus(p);
				NavigableMap<Object, Object> results = getValuesForClusteringKeyRange_WideCF(rowKey, begin, endTime, limit,
						model);
				Results.putAll(results);
			}
			return Results;
		}

		/*
		 *  Fire one query and get all results.
		 */
		else {
			NavigableMap<Object, Object> results = getValuesForClusteringKeyRange_WideCF(rowKey, startCol, endCol, limit,
					model);
			return results;
		}
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getValuesForOneColumnWithClusteringKeyRange_TallCF(java.lang.String, java.lang.Object, java.lang.Object, java.lang.String, com.siemens.bam.adm.studio.schema.Datatypes, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForOneColumnWithClusteringKeyRange_TallCF(String rowKey, Object startCol,
			Object endCol, String columnName, DataTypes colDataType, int limit, DataModel model) throws Exception
	{
		logger.debug("In getValuesForOneColumnWithClusteringKeyRange_TallCF()");

		// Initialize the respective data structures
		NavigableMap<Object, Object> colList = new TreeMap<Object, Object>();
		NavigableMap<byte[], NavigableMap<byte[], byte[]>> colListValues;
		
		// Get the row-keys and clustering key
		Table hbaseTable = model.getTables().get(0);
		List<TableColumn> cols = hbaseTable.getColumns();		
		String clusKey = hbaseTable.getClusteringKeys().get(0);
		
		TableColumn tc = new TableColumn();
		tc.setColName(clusKey);
		int index = cols.indexOf(tc);
		DataTypes clusKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		// Create a HBase scan object and set the required conditions
		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes(columnFamily));
				
		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		
		// Add the provided row key condition first
		Filter prefixFilter = new PrefixFilter(Bytes.toBytes(rowKey));
		
		// Add the row key filters with the provided clustering key bounds
		RowFilter rowFilterStart,rowFilterEnd;
		
		/*
		 * If the provided clustering key is of Date or DateTime type, then it a further check for reversed time table is done.
		 * Once the check passes, the start and end range filters are built with suitable computation as performed during a reversed table write.
		 */
		if ((startCol instanceof Date || startCol instanceof DateTime)) 
		{
			if(ClientProperties.isTableReversedOnTime())
			{
				logger.info("Reversed Table Read!");
	
				rowFilterEnd = new RowFilter(CompareOp.GREATER_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, Long.MAX_VALUE - new DateTime((Date) startCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
				rowFilterStart = new RowFilter(CompareOp.LESS_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, Long.MAX_VALUE - new DateTime((Date) endCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));			
			} 
			else 
			{
				rowFilterStart = new RowFilter(CompareOp.GREATER_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, new DateTime(startCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
				rowFilterEnd = new RowFilter(CompareOp.LESS_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, new DateTime(endCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
			}
		}
		else 
		{
			rowFilterStart = new RowFilter(CompareOp.GREATER_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, startCol, clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
			rowFilterEnd = new RowFilter(CompareOp.LESS_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, endCol, clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
		}
					
		filterList.addFilter(prefixFilter);
		filterList.addFilter(rowFilterStart);
		filterList.addFilter(rowFilterEnd);
		
		logger.debug(filterList.toString());
		scan.setFilter(filterList);
		scan.addColumn(Bytes.toBytes(columnFamily), Bytes.toBytes(columnName));
		scan.setCaching(ClientProperties.getPaginationLimit());
		
		if(limit!=0)
			scan.setMaxResultsPerColumnFamily(limit);
		
		// Query Execution and Processing
		ResultScanner scanner;
		try 
		{
			scanner = table.getScanner(scan);		
			
			logger.debug("Scan completed!");
			
			if(scanner==null)
			{
				logger.debug("scanner is null");
			}
			else
			{
				logger.debug("Results is not null");
				
				for (Result result : scanner) 
				{
					if (!result.isEmpty()) 
					{
						colListValues = result.getNoVersionMap();
						for (Map.Entry<byte[], NavigableMap<byte[], byte[]>> entryKey : colListValues
								.entrySet()) 
						{
							Map<byte[], byte[]> map = entryKey.getValue();
							for (Map.Entry<byte[], byte[]> entry : map.entrySet()) 
							{
								colList.put(Bytes.toString(entry.getKey()), Bytes.toString(entry.getValue()));
							}
						}
					}
				}
			}
		}
		catch (IOException e) 
		{
			logger.debug("Error in query execution and processing",e.getMessage());
			throw new Exception("Error in query execution and processing",e);
		}
		return colList;
	}

	/*
	 * This method returns the formatted row key as required by HBase
	 */
	public Object getFormattedRowKey(String rowKey, Object columnValue, DataTypes datatype, String rowKeySeperator)
	{
		switch(datatype)
		{
			case BOOLEAN: return rowKey + rowKeySeperator + columnValue;
	
			case FLOAT: return rowKey + rowKeySeperator + Float.toString((float)columnValue);
			case DOUBLE: return rowKey + rowKeySeperator + Double.toString((double)columnValue);
			case BIGINT: return rowKey + rowKeySeperator + Long.toString((long)columnValue);							
			
			case UUID:return rowKey + rowKeySeperator + columnValue;
			
			case DATE: 
			case DATETIME:return rowKey + rowKeySeperator + columnValue;
			
			case SMALLINT:
			case TINYINT: 
			case INT:return rowKey + rowKeySeperator + Integer.toString((int)columnValue);
			
			case NULLINT:
			case XML:
			case UNKNOWN:return null;
			default:return null;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getValuesForAllColumnsWithClusteringKeyRange_TallCF(java.lang.String, java.lang.Object, java.lang.Object, com.siemens.bam.adm.studio.schema.DataModel, int)
	 */
	@Override
	public NavigableMap<Object, Map<Object, Object>> getValuesForAllColumnsWithClusteringKeyRange_TallCF(String rowKey,
			Object startCol, Object endCol, DataModel model, int limit) throws Exception
	{
		logger.debug("In getValuesForAllColumnsWithClusteringKeyRange_TallCF for Cassandra Native Driver");

		// Initialize the respective data structures
		NavigableMap<byte[], NavigableMap<byte[], byte[]>> colValues = null;
		NavigableMap<Object,Map<Object,Object>> colList=new TreeMap<Object,Map<Object,Object>>();

		// Get the row-keys and clustering key
		Table hbaseTable = model.getTables().get(0);
		List<TableColumn> cols = hbaseTable.getColumns();		
		String clusKey = hbaseTable.getClusteringKeys().get(0);
		
		TableColumn tc = new TableColumn();
		tc.setColName(clusKey);
		int index = cols.indexOf(tc);
		DataTypes clusKeyDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		// Create a HBase scan object and set the required condtions
		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes(columnFamily));
		
		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		
		// Add the provided row key condition first
		Filter prefixFilter = new PrefixFilter(Bytes.toBytes(rowKey));
		
		// Add the row key filters with the provided clustering key bounds
		RowFilter rowFilterStart,rowFilterEnd;		
		
		/*
		 * If the provided clustering key is of Date or DateTime type, then it a further check for reversed time table is done.
		 * Once the check passes, the start and end range filters are built with suitable computation as performed during a reversed table write.
		 */
		if ((startCol instanceof Date || startCol instanceof DateTime)) 
		{
			if(ClientProperties.isTableReversedOnTime())
			{
				logger.info("Reversed Table Read!");
	
				rowFilterEnd = new RowFilter(CompareOp.GREATER_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, Long.MAX_VALUE - new DateTime((Date) startCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
				rowFilterStart = new RowFilter(CompareOp.LESS_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, Long.MAX_VALUE - new DateTime((Date) endCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));			
			} 
			else 
			{
				rowFilterStart = new RowFilter(CompareOp.GREATER_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, new DateTime(startCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
				rowFilterEnd = new RowFilter(CompareOp.LESS_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, new DateTime(endCol).getMillis(), clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
			}
		}
		else 
		{
			rowFilterStart = new RowFilter(CompareOp.GREATER_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, startCol, clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
			rowFilterEnd = new RowFilter(CompareOp.LESS_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes((String)getFormattedRowKey(rowKey, endCol, clusKeyDatatype, hbaseTable.getRowKeySeparator()))));
		}
					
		filterList.addFilter(prefixFilter);
		filterList.addFilter(rowFilterStart);
		filterList.addFilter(rowFilterEnd);
		
		logger.debug(filterList.toString());
		scan.setFilter(filterList);
		scan.setCaching(ClientProperties.getPaginationLimit());
		
		if(limit!=0)
			scan.setMaxResultsPerColumnFamily(limit);
		
		// Query Execution and Processing
		ResultScanner scanner;
		try 
		{
			scanner = table.getScanner(scan);		
			
			logger.debug("Scan completed!");
			
			if(scanner==null)
			{
				logger.debug("scanner is null");
			}
			else
			{
				logger.debug("Results is not null");
				Map<Object,Object> columnValueList = new TreeMap<Object,Object>();
	
				for (Result result : scanner) 
				{
					if (!result.isEmpty()) 
					{
						colValues = result.getNoVersionMap();
						if (!colValues.isEmpty()) 
						{
							for (Map.Entry<byte[], NavigableMap<byte[], byte[]>> entry : colValues.entrySet()) 
							{
								NavigableMap<byte[], byte[]> map = entry.getValue();
								
								for (Map.Entry<byte[], byte[]> entryValues : map.entrySet()) 
								{
									columnValueList.put(Bytes.toString(entryValues.getKey()), Bytes.toString(entryValues.getValue()));
								}
								colList.put(startCol, columnValueList);
							}
						}
					}
				}
			}
		}
		catch (IOException e) 
		{
			logger.debug("Error in query execution and processing",e.getMessage());
			throw new Exception("Error in query execution and processing",e);
		}

		return colList;
	}


	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getValuesForColumnRange_SuperColumnCF(java.lang.String, java.lang.Object, java.lang.Object, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NavigableMap<Object, S> getValuesForClusteringKeyRange_SuperColumnCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model) throws Exception
	{
		logger.debug("In getValuesForClusteringKeyRange_SuperColumnCF ()");

		// Initialize the respective data structures
		NavigableMap<Object, S> colList = new TreeMap<Object, S>();
		NavigableMap<byte[], byte[]> colValues = null;
	
		// Get the super column type
		Table hbaseTable = model.getTables().get(0);
		List<TableColumn> cols = hbaseTable.getColumns();
		SuperColumnType scolType = null;
		Iterator<TableColumn> tcIterator = cols.iterator();
		while(tcIterator.hasNext())
		{
			TableColumn tc2 = tcIterator.next();
			if(tc2 instanceof SuperColumn)
			{
				logger.debug("The supercolumn type Column found!");
				scolType = ((SuperColumn) tc2).getScolType();
				break;
			}
		}
				
		// Create a HBase scan object and add the required conditions
		Scan scan = new Scan();
		scan.addFamily(Bytes.toBytes(columnFamily));
	
		FilterList filterList = new FilterList(FilterList.Operator.MUST_PASS_ALL);
		
		// Create a Row Filter on the provided row key
		Filter startRow = new RowFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(rowKey)));
		
		// Create qualifier filters
		Filter startRange, endRange;
		
		/*
		 * If the provided clustering key is of Date or DateTime type, then it a further check for reversed time table is done.
		 * Once the check passes, the start and end range filters are built with suitable computation as performed during a reversed table write.
		 */
		if (startCol instanceof Date || startCol instanceof DateTime) 
		{
			if (ClientProperties.isTableReversedOnTime()) 
			{
				logger.info("Reversed Table Read!");
	
				endRange = new QualifierFilter(CompareOp.LESS_OR_EQUAL, new BinaryComparator(
						Bytes.toBytes(Long.toString(Long.MAX_VALUE - new DateTime((Date) startCol).getMillis()))));
				startRange = new QualifierFilter(CompareOp.GREATER_OR_EQUAL, new BinaryComparator(
						Bytes.toBytes(Long.toString(Long.MAX_VALUE - new DateTime((Date) endCol).getMillis()))));
			} 
			else 
			{
				startRange = new QualifierFilter(CompareOp.GREATER_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes(Long.toString(new DateTime((Date) startCol).getMillis()))));
				endRange = new QualifierFilter(CompareOp.LESS_OR_EQUAL,
						new BinaryComparator(Bytes.toBytes(Long.toString(new DateTime((Date) endCol).getMillis()))));
			}
		} 
		else 
		{
			startRow = new RowFilter(CompareOp.EQUAL, new BinaryComparator(Bytes.toBytes(rowKey)));
			startRange = new QualifierFilter(CompareOp.GREATER_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes(startCol.toString())));
			endRange = new QualifierFilter(CompareOp.LESS_OR_EQUAL,
					new BinaryComparator(Bytes.toBytes(endCol.toString())));
		}
	
		logger.debug("RowFilter is: " + startRow.toString());
		logger.debug("Range is from/to " + startRange.toString() + " to/from : " + endRange.toString());
	
		filterList.addFilter(startRange);
		filterList.addFilter(endRange);
		filterList.addFilter(startRow);
		scan.setFilter(filterList);
		scan.setCaching(ClientProperties.getPaginationLimit());
		
		if (limit != 0)
			scan.setMaxResultsPerColumnFamily(limit);
		
		// Query Processing and Execution
		ResultScanner scanner = null;
		try {
			 scanner = table.getScanner(scan);
			
			logger.debug("Scan completed!");
	
			if (scanner == null) {
				logger.debug("scanner is null");
			} else {
				logger.debug("Results is not null");
				if (startCol instanceof Date || startCol instanceof DateTime) {
					if (ClientProperties.isTableReversedOnTime()) {
						logger.debug("Reversed time data storage");
						for (Result result : scanner) {
							if (!result.isEmpty()) {
								colValues = result.getFamilyMap(Bytes.toBytes(columnFamily));
								if (!colValues.isEmpty()) {
									for (Map.Entry<byte[], byte[]> entry : colValues.entrySet()) {
										colList.put(
												new Date(Long.MAX_VALUE
														- Long.parseLong(Bytes.toString(entry.getKey()))),
												(S)getReturnValueforSuperColtype(entry.getValue(),scolType));
									}
								}
							}
						}
					} else {				
						for (Result result : scanner) {
							if (!result.isEmpty()) {
								colValues = result.getFamilyMap(Bytes.toBytes(columnFamily));
								if (!colValues.isEmpty()) {
									for (Map.Entry<byte[], byte[]> entry : colValues.entrySet()) {
										colList.put(new Date(Long.parseLong(Bytes.toString(entry.getKey()))),(S)getReturnValueforSuperColtype(entry.getValue(),scolType));
									}
								}
							}
						}
					}
				}
	
				else {
					for (Result result : scanner) {
						if (!result.isEmpty()) {
							colValues = result.getFamilyMap(Bytes.toBytes(columnFamily));
							if (!colValues.isEmpty()) {
								for (Map.Entry<byte[], byte[]> entry : colValues.entrySet()) {
									colList.put(Bytes.toString(entry.getKey()), (S) getReturnValueforSuperColtype(entry.getValue(),scolType));
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			logger.debug("Error in query execution and processing",e.getMessage());
			throw new Exception("Error in query execution and processing",e);
		}
	
		return colList;
	}

	@SuppressWarnings("unchecked")
	public S getReturnValueforSuperColtype(byte[] entry, SuperColumnType scoltype)
	{
		switch(scoltype)
		{
		case LIST: return (S) Arrays.asList(Bytes.toString(entry).split("\\|"));
			
		case MAP:
			
		case SET:
			return null;
		default:logger.debug("Undefined Super Col Type");
			return null;
		
		}
	}
	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#close()
	 */
	@Override
	public void close() throws Exception {
		connection.closeConnection();
	}


}
