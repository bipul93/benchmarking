package com.siemens.bam.adm.studio.dbmetrics;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * 
 * Abstract class that defines the methods to collect data store specific
 * metrics and store them in CSV
 * 
 * @author varsha.raveendran@siemens.com
 *
 */
public abstract class AbstractDataStoreMetrics {
	private final static Logger logger = LoggerFactory.getLogger(AbstractDataStoreMetrics.class.getName());

	/**
	 * Map to store the metrics to be collected
	 */
	private Map<String, List<String>> metrics = new HashMap<String, List<String>>();
	public BufferedWriter Output = null;

	/**
	 * Method that is implemented to connect to the data store monitoring tool
	 * and collect metrics for the run
	 * 
	 * @param startTime
	 * @param endTime
	 * @param clientProp
	 * @param map
	 * @throws Exception
	 */
	abstract public void collectMetrics(String fileName, ClientProperties clientProp) throws Exception;

	public AbstractDataStoreMetrics(Map<String, List<String>> metricList) {
		this.setMetricsList(metricList);
	}

	public AbstractDataStoreMetrics() {
	}

	/**
	 * Method to create log directories and files based on operation performed
	 * 
	 * @param clientProp
	 * @throws Exception
	 */
	public void createLogFiles(String fileName) throws Exception {
	
		File file = new File(fileName);
		
		try {
			Output = new BufferedWriter(new FileWriter(file));

		} catch (IOException e) {

			throw new Exception("Cannot write into File " + e);
		}
	}

	public Map<String, List<String>> getMetricsList() {
		return metrics;
	}

	public void setMetricsList(Map<String, List<String>> dbMetrics) {
		this.metrics = dbMetrics;
	}

}
