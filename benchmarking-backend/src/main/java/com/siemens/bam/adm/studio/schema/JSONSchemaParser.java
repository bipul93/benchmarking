package com.siemens.bam.adm.studio.schema;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.exception.SchemaException;
import com.siemens.bam.adm.studio.schema.validators.AbstractColumnValidator;
import com.siemens.bam.adm.studio.schema.validators.AbstractKeyValidator;
import com.siemens.bam.adm.studio.schema.validators.CustomizedColumnValidator;
import com.siemens.bam.adm.studio.schema.validators.CustomizedKeyValidator;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import com.siemens.bam.adm.studio.schema.SuperColumn.SuperColumnType;
import com.siemens.bam.adm.studio.schema.tableType.TableTypeFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * This class is used to parse the schema file which is given in JSON format
 *
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class JSONSchemaParser {

	final static Logger logger = LoggerFactory.getLogger(JSONSchemaParser.class.getName());
	private static JSONObject json;

	/**
	 * Init method to load the schema file from path given by user into a
	 * JSONObject
	 *
	 * @param schemaPath
	 * @throws Exception
	 */
	public static void loadSchema(String schemaPath) throws Exception {
		InputStream is = null;
		is = new FileInputStream(schemaPath);
		String jsonTxt = null;
		jsonTxt = IOUtils.toString(is);
		setJSONObject(jsonTxt);
	}

	public static void setJSONObject(String jsonTxt) throws JSONException {
		json = (JSONObject) JSONSerializer.toJSON(jsonTxt);
	}

	/**
	 * Method to parse the schema file and set the DataModel
	 *
	 * @param clientProp
	 * @return DataModel - This object holds the schema of the database on which
	 *         the operations would be performed
	 * @throws Exception
	 */
	public static DataModel getDataModel(ClientProperties clientProp) throws Exception {

		DataModel dataModel = new DataModel();
		try {

			String dbName = clientProp.getDataStoreName();

			JSONObject database = (JSONObject) json.get("Database");
			String databaseName = (String) database.get("DBname");
			JSONArray jsontables = (JSONArray) database.get("Tables");

			List<Table> tableModel = new ArrayList<Table>(jsontables.size());
			Iterator<?> tableIterator = jsontables.iterator();
			while (tableIterator.hasNext()) {
				JSONObject table = (JSONObject) tableIterator.next();
				Table dbTable = getTableModel(table, dbName);
				dbTable.setTableReversedOnTime(ClientProperties.isTableReversedOnTime());
				tableModel.add(dbTable);
			}
			dataModel.setDatabaseName(databaseName);
			dataModel.setTables(tableModel);

		} catch (Exception e) {
			throw new SchemaException(
					"JSONSchemaParser::getDataModel() - Error while parsing schema from schema file : ", e);
		}

		return dataModel;
	}

	/**
	 * Method to parse the file to get the table model
	 *
	 * @param table
	 * @param dbName
	 * @return Table
	 * @throws Exception
	 */
	private static Table getTableModel(JSONObject table, String dbName) throws Exception {

		Table dbTable = new Table();

		String tableName = (String) table.get("TableName");
		dbTable.setTableName(tableName);

		String typeOfTable = (String) table.get("TypeOfTable");

		dbTable.setTypeOfTable(TableTypeFactory.getTableTypeInstance(typeOfTable));

		JSONArray jsonRowKey = (JSONArray) table.get("RowKey");
		List<String> rowKeys = createListFromJSONArray(jsonRowKey);
		dbTable.setRowKeys(rowKeys);

		String rowKeySeparator = null;
		if (table.get("RowKeySeparator") != null)
			rowKeySeparator = (String) table.get("RowKeySeparator");
		dbTable.setRowKeySeparator(rowKeySeparator);

		JSONArray jsonClusteringKey = (JSONArray) table.get("ClusteringKey");
		List<String> cluskeys = createListFromJSONArray(jsonClusteringKey);
		dbTable.setClusteringKeys(cluskeys);

		String colValidator = (String) table.get("ColumnValidator");
		AbstractColumnValidator columnValidator = getColumnValidator(colValidator, dbName);
		dbTable.setColumnValidator(columnValidator);

		String kValidator = (String) table.get("KeyValidator");
		AbstractKeyValidator keyValidator = getKeyValidator(kValidator, dbName);
		dbTable.setKeyValidator(keyValidator);

		JSONArray jsoncolumns = (JSONArray) table.get("TableColumns");
		List<TableColumn> columns = getTableColumnsForTable(jsoncolumns);

		dbTable.setColumns(columns);
		logger.debug("Created the DataModel for table <" + dbTable.getTableName() + ">");

		return dbTable;
	}

	/**
	 * Method to get the table columns for a table
	 *
	 * @param jsoncolumns
	 * @return List<TableColumn>
	 */
	private static List<TableColumn> getTableColumnsForTable(JSONArray jsoncolumns) {
		List<TableColumn> columns = new LinkedList<TableColumn>();
		List<TableColumn> subColumns = new LinkedList<TableColumn>();

		for (int i = 0; i < jsoncolumns.size(); i++) {

			TableColumn column = null;

			JSONObject jsoncol = (JSONObject) jsoncolumns.get(i);

			String colData = (String) jsoncol.get("Datatype");
			String colDatatype = colData;
			if (colData.contains(":")) {
				column = new SuperColumn();
				colDatatype = colData.split(":")[1];
				((SuperColumn) column).setScolType(SuperColumnType.valueOf(colData.split(":")[0]));

			} else {
				column = new TableColumn();
			}
			String colName = (String) jsoncol.get("ColumnName");
			column.setColName(colName);
			int ordinal = DataTypes.valueOf(DataTypes.class, colDatatype).ordinal();
			column.setColDataType(ordinal);

			List<Object> values = new ArrayList<Object>();

			if (jsoncol.get("Values") != null) {

				logger.debug("This column has fixed values");
				values = getFixedValuesForColumn((JSONArray) (jsoncol.get("Values")));
			}
			column.setFixedValues(values);
			logger.debug("Column: " + colName + " ColType: " + colData);

			if (jsoncol.get("Subcolumns") != null) {

				logger.debug("This is a SuperColumn type");

				subColumns = getSubColumns((JSONArray) jsoncol.get("Subcolumns"));
				((SuperColumn) column).setSubColumns(subColumns);

			}

			if (jsoncol.get("UniqueSet") != null && ((String) jsoncol.get("UniqueSet")).equals("True")) {
				column.setIsUniqueSetOfValuesToBeGenerated(true);
			} else {
				column.setIsUniqueSetOfValuesToBeGenerated(false);
			}

			columns.add(column);

		}
		return columns;
	}

	/**
	 * Method to get the values predefined by the user. Random data will not be
	 * generated if values are defined in the schema file. This is used during
	 * WRITE operation.
	 *
	 * @param colValues
	 * @return List<Object>
	 */
	private static List<Object> getFixedValuesForColumn(JSONArray colValues) {
		List<Object> values = new ArrayList<Object>();
		for (int j = 0; j < colValues.size(); j++) {
			logger.debug("Value :: " + colValues.get(j));

			values.add(colValues.get(j));
		}
		return values;
	}

	private static AbstractKeyValidator getKeyValidator(String kValidator, String dbName) {
		AbstractKeyValidator keyValidator;
		if (kValidator.length() == 0) {
			keyValidator = null;
		} else {
			keyValidator = new CustomizedKeyValidator(kValidator, dbName);
		}
		return keyValidator;
	}

	private static AbstractColumnValidator getColumnValidator(String colValidator, String dbName) {
		AbstractColumnValidator columnValidator;
		if (colValidator.length() == 0) {
			columnValidator = null;
		} else {
			columnValidator = new CustomizedColumnValidator(colValidator, dbName);
		}
		return columnValidator;
	}

	/**
	 * Util method
	 *
	 * @param jsonArray
	 * @return
	 */
	private static List<String> createListFromJSONArray(JSONArray jsonArray) {
		List<String> list = new LinkedList<String>();
		for (int j = 0; j < jsonArray.size(); j++) {
			logger.debug("Add to list: " + (String) jsonArray.get(j));
			list.add((String) jsonArray.get(j));
		}

		return list;
	}

	/**
	 * Method to get the subcolumns' name and datatype within a (super)column
	 *
	 * @param jsonSubCols
	 * @return List<TableColumn>
	 */
	private static List<TableColumn> getSubColumns(JSONArray jsonSubCols) {

		List<TableColumn> subColumns = new LinkedList<TableColumn>();

		for (int i = 0; i < jsonSubCols.size(); i++) {
			TableColumn column = new TableColumn();
			JSONObject jsoncol = (JSONObject) jsonSubCols.get(i);

			int ordinal = DataTypes.valueOf(DataTypes.class, (String) jsoncol.get("Datatype")).ordinal();

			String colName = (String) jsoncol.get("ColumnName");

			column.setColName(colName);
			column.setColDataType(ordinal);
			List<Object> values = new ArrayList<Object>();

			if (jsoncol.get("Values") != null) {

				logger.debug("This column has fixed values");
				values = getFixedValuesForColumn((JSONArray) (jsoncol.get("Values")));
			}
			column.setFixedValues(values);
			if (jsoncol.get("UniqueSet") != null && ((String) jsoncol.get("UniqueSet")).equals("True")) {
				column.setIsUniqueSetOfValuesToBeGenerated(true);
			} else {
				column.setIsUniqueSetOfValuesToBeGenerated(false);
			}
			subColumns.add(column);

			logger.debug("SubColumnName " + (String) jsoncol.get("ColumnName"));
			logger.debug("SubColDatatype" + (String) jsoncol.get("Datatype"));
		}

		return subColumns;
	}

	/**
	 * Method to print the parsed DBModel on console
	 *
	 * @param model
	 */
	public static void printDBModel(DataModel model) throws Exception {

		String dbName = model.getDatabaseName();
		logger.debug("");
		logger.debug("DataBase Name ->  " + dbName);

		Iterator<Table> tblList = model.getTableList(dbName);

		while (tblList.hasNext()) {
			Table table = tblList.next();

			List<String> rowKeys = table.getRowKeys();
			Iterator<String> rowKeysIter = rowKeys.iterator();
			logger.debug(" Row Keys: ");
			while (rowKeysIter.hasNext()) {

				logger.debug(rowKeysIter.next() + " , ");
			}

			List<String> clusKeys = table.getClusteringKeys();
			Iterator<String> clusKeysIter = clusKeys.iterator();
			logger.debug(" Clustering Keys: ");
			while (clusKeysIter.hasNext()) {

				logger.debug(clusKeysIter.next() + " , ");
			}

			String keyValidator = table.getKeyValidator().getName();
			String colValidator = table.getColumnValidator().getName();

			logger.debug("");
			logger.debug("Table Name is " + table.getTableName() + " keyValidator <" + keyValidator + ">"
					+ " columnValidator<" + colValidator + ">");
			logger.debug("");

			List<TableColumn> columnList = table.getColumns();
			Iterator<TableColumn> tc = columnList.iterator();
			while (tc.hasNext()) {
				TableColumn tc1 = tc.next();
				logger.debug(
						" " + tc1.getColName() + "(" + DataTypes.getDatatypeFromOrdinal(tc1.getColDataType()) + ")  ");
			}
		}

		logger.debug("");
	}

}
