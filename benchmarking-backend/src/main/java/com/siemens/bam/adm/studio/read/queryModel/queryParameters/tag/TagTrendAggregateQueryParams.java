/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag;

import org.joda.time.DateTime;

import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;

/**
 * Definition of Query Parameters for TagTrendAggregate Query
 * @author Nisha Menon
 *
 */
public class TagTrendAggregateQueryParams extends AbstractQueryParams 
{
	private DateTime begin;
	private DateTime end;
	private int timeStep;
	private String aggregationType;
	private int limit;
	
	public TagTrendAggregateQueryParams()
	{
		super();
		this.begin = new DateTime();
		this.end = new DateTime();
		this.aggregationType = "MIN";
		this.limit = 0;
		this.timeStep = 10;
	}

	public DateTime getBegin() {
		return begin;
	}

	public void setBegin(String begin) 
	{
		this.begin = DateFormattingUtils.getJodaDate(begin);
	}

	public DateTime getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = DateFormattingUtils.getJodaDate(end);
	}

	public int getTimeStep() {
		return timeStep;
	}

	public void setTimeStep(int timeStep) {
		this.timeStep = timeStep;
	}

	public String getAggregationType() {
		return aggregationType;
	}

	public void setAggregationType(String aggregationType) {
		this.aggregationType = aggregationType;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
