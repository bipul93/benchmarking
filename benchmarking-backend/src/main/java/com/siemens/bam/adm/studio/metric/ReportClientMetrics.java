package com.siemens.bam.adm.studio.metric;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.*;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;
import com.codahale.metrics.graphite.Graphite;
import com.codahale.metrics.graphite.GraphiteReporter;
import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * Class to report client metrics via 3 modes - Console, CSV and Graphite
 *
 * @author varsha.raveendran@siemens.com
 *
 */
public class ReportClientMetrics {
	private final static Logger logger = LoggerFactory.getLogger(ReportClientMetrics.class.getName());
	private static final MetricRegistry metrics = new MetricRegistry();

	private static ConsoleReporter reporter;
	private static CsvReporter csvReporter;
	private static int reportRate;
	private static String fileName;
	private static GraphiteReporter graphiteReporter;

	private static Graphite graphite;
	private static String clientId;
	private static String operation;

	private static Timer request;

	private static Timer.Context ctx = null;

	public ReportClientMetrics(ClientProperties clientProp) throws Exception {

		ReportClientMetrics.clientId = clientProp.getClientId();
		ReportClientMetrics.operation = clientProp.getOperation();
		reportRate = Integer.parseInt(clientProp.getReportSummaryRate());
		logger.debug("Report rate  :  " + reportRate);

		List<String> metricReporters = Arrays.asList(clientProp.getMetricReporter().split(","));

		if (metricReporters.contains("graphite")) {
			graphite = new Graphite(new InetSocketAddress(clientProp.getGraphiteIP(), clientProp.getGraphitePort()));
		}

		if (metricReporters.contains("csv")) {
			String dbName = null;
			try {
				dbName = clientProp.getDatabaseName();
			} catch (Exception e) {
				throw new Exception(
						"Exception thrown in ReportClientMetrics : DB Name not specified in Property.conf!");
			}

			File dir = new File("./output/" + dbName + "/");
			if (!dir.exists()) {
				try {
					dir.mkdir();
				} catch (SecurityException Se) {
					logger.error("Error while creating directory in Java:" + Se);
				}
			}

			fileName = "./output/" + dbName + "/";

			File file = new File(fileName + "_insert-metrics.csv");
			if (file.exists()) {
				file.delete();
			}
		}

		createMetrics();
		createReporters(metricReporters);
	}

	static void createMetrics() {
		request = metrics.timer(operation.toLowerCase() + ".metric");
	}

	/**
	 * Starts the reporters specified in the Property.conf file
	 * The metrics are prefixed with the clientId
	 *
	 * @param metricReporters
	 */
	static void createReporters(List<String> metricReporters) {
		logger.debug("FileName to write the summary into :  " + fileName);
		if (reportRate > 0) {

			if (metricReporters.contains("console")) {
				reporter = ConsoleReporter.forRegistry(metrics).convertRatesTo(TimeUnit.SECONDS)
						.convertDurationsTo(TimeUnit.MILLISECONDS).build();
				reporter.start(reportRate, TimeUnit.SECONDS);
			}

			if (metricReporters.contains("csv")) {
				csvReporter = CsvReporter.forRegistry(metrics).formatFor(Locale.US).convertRatesTo(TimeUnit.SECONDS)
						.convertDurationsTo(TimeUnit.MILLISECONDS).build(new File(fileName));
				csvReporter.start(reportRate, TimeUnit.SECONDS);
			}

			if (metricReporters.contains("graphite")) {
				graphiteReporter = GraphiteReporter.forRegistry(metrics).prefixedWith(clientId)
						.convertRatesTo(TimeUnit.SECONDS).convertDurationsTo(TimeUnit.MILLISECONDS)
						.filter(MetricFilter.ALL).build(graphite);
				graphiteReporter.start(reportRate, TimeUnit.SECONDS);
			}
		}

	}

	public void startTimer() throws InterruptedException {
		ctx = request.time();
	}

	public void stopTimer() throws InterruptedException {
		ctx.stop();
	}

}
