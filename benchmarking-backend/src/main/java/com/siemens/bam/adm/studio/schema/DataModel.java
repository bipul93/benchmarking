package com.siemens.bam.adm.studio.schema;

import java.util.Iterator;

import java.util.List;

import com.google.common.base.Preconditions;

/**
 *  A container for the DataModel specified by the user
 *  The data model is created by parsing the schema file
 *
 */
public class DataModel {

	private String databaseName;
	private List<Table> tables;

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	public Iterator<Table> getTableList(String dbName) {

		return tables.iterator();
	}

	public Table getTable(String tableName) {
		
		Table t = new Table();
		t.setTableName(tableName);
		Preconditions.checkArgument(tables.contains(t), "Table ["
				+ tableName
				+ "] - No such table exists."
				+ " Please check if the table name given in Property.conf and Schema file match!");
		
		return tables.get(tables.indexOf(t));
		
	}

}
