
package com.siemens.bam.adm.studio.utils;

import java.util.Date;

/**
 * Date Range encapsulates a start and end date
 * Used mainly for picking different dates in a given range. 
 * @author Nisha Menon 
 */
public class DateRange 
{
	Date start;
	Date end;

	public Date getEnd() {
		return this.end;
	}

	public Date getStart() {
		return this.start;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public void setStart(Date start) {
		this.start = start;
	}
}