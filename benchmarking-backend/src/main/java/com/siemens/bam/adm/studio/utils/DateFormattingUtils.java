package com.siemens.bam.adm.studio.utils;

import java.util.ArrayList;
import java.util.Date;


import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
/**
 * Class to check all Date formatting 
 * @author Nisha Menon
 */

public class DateFormattingUtils 
{
	private static final Logger logger = LoggerFactory.getLogger(DateFormattingUtils.class);
	private static final DateTimeFormatter JODA_DATE_FORMAT=DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss.SSS");
	private static final DateTimeFormatter CASSANDRA_DATE_FORMAT=DateTimeFormat.forPattern("yyyy-MM-dd");
	private static final DateTimeFormatter FULL_DATE_TIME_FORMAT=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");

	
	//convert into Joda Date format
	public static DateTime getJodaDate(String begindate) 
	{
		DateTime date = JODA_DATE_FORMAT.parseDateTime(begindate);
		return date; 
	}

	//Convert into complete date time
	public static DateTime getFullDateTime(String begindate) 
	{
		DateTime date = FULL_DATE_TIME_FORMAT.parseDateTime(begindate);
		return date; 
	}
	
	/*
	 * Pick all the date ranges in a given range
	 * Example: For a range between Jan 1, 2016 and Jan 5, 2016. 
	 */
	public static ArrayList<DateRange> getDateRanges(DateTime begin, DateTime end) 
	{
		logger.debug("Within getDateRanges()");
		
		ArrayList<DateRange> allDateRange = new ArrayList<DateRange>();
		long diffDays=(Days.daysBetween(begin.toLocalDate(), end.toLocalDate())).getDays();

		if (diffDays == 0) 
		{
			DateRange d = new DateRange();
			d.setStart(convertDateTimeToDate(begin));
			d.setEnd(convertDateTimeToDate(end));
			allDateRange.add(d);
			return allDateRange;
		} 
		else 
		{
			logger.warn("More than one date!");
			DateTime currentDate = begin;
			DateTime currentStart = new DateTime();
			DateTime currentEnd = new DateTime();
			begin.getMillis();
			for (int i = 0; i < (diffDays + 1); i++) 
			{
				if (currentDate.equals(begin)) 
				{
					currentStart = begin;
					currentEnd = defaultEndTimeStamp(begin);
				} 
				else if (areDateTimeEqual(currentDate, end)) 
				{
					currentStart = defaultStartTimeStamp(end);
					currentEnd = end;
				}
				else 
				{
					currentStart = defaultStartTimeStamp(currentDate);
					currentEnd = defaultEndTimeStamp(currentDate);
				}

				currentDate = nextDate(currentDate);

				DateRange d = new DateRange();
				
				d.setStart(convertDateTimeToDate(currentStart));
				d.setEnd(convertDateTimeToDate(currentEnd));
				allDateRange.add(d);
			}
			return allDateRange;
		}
	}

	// Function to get Cassandra Thrift Key - combination of UUID and Date
	public static String getDateFormattedCLIKey(String key, DateTime begin)
	{
		String date_formatted = convertDateToCassandraThriftFormat(begin);
		return key+ ":" + date_formatted.toString();	
	}

	/*
	 * Function to return an end timestamp for a particular Date 
	 * This function returns the endtimestamp as the input date's last time which is 23:59:59
	 * e.g: input: 31st March 12:15:28 IST 2015, output: 31st March 23:59:59 IST 2015
	 */
	public static DateTime defaultEndTimeStamp(DateTime currentDate) 
	{
		int year = currentDate.getYear();
		int month = currentDate.getMonthOfYear();
		int day = currentDate.getDayOfMonth();
		DateTime defaultEndTimestamp = new DateTime(year, month, day, 23, 59, 59, 999);
		return defaultEndTimestamp;
	}
	
	// Returns the start timestamp for a particular date	 
	public static DateTime defaultStartTimeStamp(DateTime currentDate) 
	{
		int year = currentDate.getYear();
		int month = currentDate.getMonthOfYear();
		int day = currentDate.getDayOfMonth();
		DateTime defaultStartTimestamp = new DateTime(year, month, day, 0, 0, 0, 0);
		return defaultStartTimestamp;
	}
	
	// Checks whether two dates are the same
	public static boolean areDateTimeEqual(DateTime currentEnd, DateTime endTimeStamp) 
	{		
		long diffDays = (Days.daysBetween(currentEnd.toLocalDate(), endTimeStamp.toLocalDate())).getDays();

		return diffDays == 0 ? true : false;
	}
		
	// Find the next Date, given a particular date
	public static DateTime nextDate(DateTime startTimeStamp)
	{	
		DateTime date = new DateTime(startTimeStamp.toDate());
		date = date.plusDays(1);
		return date;
	}
	
	public static Date convertDateTimeToDate(DateTime date) 
	{
		Date dt = date.toDate();
		return dt;	
	}
	
	// Function to convert date to cassandra thrift format
	public static String convertDateToCassandraThriftFormat(DateTime date) 
	{
		String timestamp = CASSANDRA_DATE_FORMAT.print(date);
		return timestamp;	
	}
	
	// Function to convert JODA Time to full Date time
	public static String convertJODATimetoFullDateStringFormat(DateTime date) 
	{
		String timestamp = FULL_DATE_TIME_FORMAT.print(date);
		return timestamp;	
	}
	
	// Function to convert date to cassandra native format
	public static String convertDateToCassandraNativeStringFormat(DateTime date) 
	{
		String timestamp = CASSANDRA_DATE_FORMAT.print(date);
		return timestamp;	
	}
	
	public static long getMillisFromString(String beginDate) 
	{
		DateTime date = JODA_DATE_FORMAT.parseDateTime(beginDate);
		return date.getMillis(); 
	}
	
	public static String getDateOnlyFromFullDateTime(String fullDateTime)
	{
		return fullDateTime.substring(0, 10);
	}
}
