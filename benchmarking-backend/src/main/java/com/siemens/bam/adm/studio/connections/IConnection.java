
package com.siemens.bam.adm.studio.connections;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.exception.DBException;

/**
 * Interface to define the methods to initialize and close connections to data stores
 * 
 * When the studio is extended to a new datastore this class should be implemented to make
 * connections to the datastore
 * 
 * @author nisha.menon@siemens.com
 * @author varsha.raveendran@siemens.com
 * 
 */
public interface IConnection {
	/**
	 * Method to establish connection with the respective database
	 * 
	 * @param ClientProperties clientProp
	 * @return IConnections Object
	 * @throws DBException
	 * @throws Exception 
	 */
	public void initializeConnection(Environment environment) throws DBException, Exception;

	/**
	 * Method to close the connection to the database after all queries are
	 * performed
	 * 
	 * @throws DBException
	 */
	public void closeConnection() throws DBException;

	
}