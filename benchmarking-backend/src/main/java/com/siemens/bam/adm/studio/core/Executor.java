package com.siemens.bam.adm.studio.core;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.operation.IDBOperation;
import com.siemens.bam.adm.studio.utils.Timer;

/**
 * The Executor Class is responsible for spawning the specified number of
 * threads and submitting the jobs (based on the operation) to the worker pool
 *
 * @author nisha.menon@siemens.com
 * @author varsha.raveendran@siemens.com
 *
 * @param <T>
 */
public class Executor<T> {

	private static final Logger logger = Logger.getLogger(Executor.class);
	private ExecutorService pool;
	private CompletionService<T> completionPool;

	// The number of threads that will be used to perform the operations
	// For a WRITE operation -> specified in the config file
	// For a READ operation -> specified in the implemented query
	private static int numberOfThreadsRequired;

	private final static Timer timer = new Timer();


	/**
	 * The init method creates the thread pool and completion pool This is
	 * called once at the start of every operation
	 *
	 * @param Environment
	 */
	public void init(Environment env) {
		pool = Executors.newFixedThreadPool(env.getClientProperties().getNumberOfThreads());
		//logger.info(pool);
		completionPool = new ExecutorCompletionService<T>(pool);
		//logger.info(completionPool);
	}

	/**
	 * The execute method is where the core operations are called. After the
	 * environment for the benchmarking is set the execute method is called
	 *
	 * @param operation
	 * @throws Exception
	 */
	public void execute(IDBOperation operation) throws Exception {
		timer.startTime();
		logger.info("Start Time  : " + System.currentTimeMillis());
		// Create each worker thread and submit to pool
		for (int threadid = 0; threadid < numberOfThreadsRequired; threadid++) {
			Callable<T> worker = operation.getInstanceOfOperation(threadid);
			completionPool.submit(worker);
		}
		logger.info(" operation inside execute");
		operation.displayExecutionStats(completionPool);
		// Shutdown the pool and wait for termination
		pool.shutdown();
		if (pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
			logger.info("Executor::execute - All tasks are completed.");
		}
	}
	public static void setNumberOfThreadsRequired(int numberOfThreadsRequired) {
		Executor.numberOfThreadsRequired = numberOfThreadsRequired;
		System.out.println(numberOfThreadsRequired);
	}

	public static int getNumberOfThreadsRequired() {
		return numberOfThreadsRequired;
	}

	public static Timer getTimer() {
		return timer;
	}

}
