package com.siemens.bam.adm.studio.schema;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.siemens.bam.adm.studio.schema.tableType.AbstractTableType;
import com.siemens.bam.adm.studio.schema.validators.AbstractColumnValidator;
import com.siemens.bam.adm.studio.schema.validators.AbstractKeyValidator;

public class Table {

	private String tableName;
	private List<TableColumn> columns = new LinkedList<TableColumn>();
	private List<String> rowKeys;
	private String rowKeySeparator;
	private AbstractTableType typeOfTable;
	private AbstractColumnValidator columnValidator;
	private AbstractKeyValidator keyValidator;
	private List<String> clusteringKeys;
	
	//Applicable for columns that are of DATETIME type and need to be stored in descending order
	private boolean isTableReversedOnTime;

	public boolean isTableReversedOnTime() {
		return isTableReversedOnTime;
	}

	public void setTableReversedOnTime(boolean isTableReversedOnTime) {
		this.isTableReversedOnTime = isTableReversedOnTime;
	}

	public List<String> getClusteringKeys() {
		return clusteringKeys;
	}

	public AbstractColumnValidator getColumnValidator() throws Exception {
		if(columnValidator == null) 
			throw new Exception("Column Validator is null");
		return columnValidator;
	}

	public void setColumnValidator(AbstractColumnValidator columnValidator) {
		this.columnValidator = columnValidator;
	}

	public AbstractKeyValidator getKeyValidator() throws Exception {
		if(keyValidator == null) 
			throw new Exception("Column Validator is null");
		return keyValidator;
	}

	public void setKeyValidator(AbstractKeyValidator keyValidator) {
		this.keyValidator = keyValidator;
	}

	public List<TableColumn> getColumns() {
		return columns;
	}

	public List<String> getRowKeys() {
		return rowKeys;
	}

	public String getRowKeySeparator() {
		return rowKeySeparator;
	}

	public void setRowKeySeparator(String rowKeySeparator) {
		this.rowKeySeparator = rowKeySeparator;
	}

	public String getTableName() {
		return tableName;
	}

	public AbstractTableType getTypeOfTable() {
		return typeOfTable;
	}

	public void setColumns(List<TableColumn> columns) {
		this.columns = columns;
	}

	public void setRowKeys(List<String> rowKeys) {
		this.rowKeys = rowKeys;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public void setTypeOfTable(AbstractTableType typeOfTable) {
		this.typeOfTable = typeOfTable;
	}

	public void setClusteringKeys(List<String> clusteringKeys) {
		this.clusteringKeys = clusteringKeys;

	}
	
	public List<String> getColumnsWithUniqueValues() {
		List<String> list = new ArrayList<String>();
		for(TableColumn c : columns){
			if(c.getIsUniqueSetOfValuesToBeGenerated()){
				list.add(c.getColName());
			}
		}
		return list;
	}

	@Override
	public boolean equals(Object o) {

		if (o instanceof Table) {

			Table mo = (Table) o;
			return mo.tableName.equals(tableName);
		}
		return false;
	}

	@Override
	public int hashCode() {

		return java.util.Objects.hashCode(tableName);
	}

}
