package com.siemens.bam.adm.studio.exception;


/**
 * 
 * SchemaException class
 * 
 */


public class SchemaException extends Exception {

	private static final long serialVersionUID = -4101546130799480942L;

	public SchemaException() {
		// TODO Auto-generated constructor stub
	}

	public SchemaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public SchemaException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public SchemaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public SchemaException(String message, Throwable cause,
			boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
