package com.siemens.bam.adm.studio.dbdriver;

import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeSet;

import com.siemens.bam.adm.studio.connections.IConnection;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;


/**
 * The IDatabaseDriver interface contains all methods to access the drivers. 
 * This interface should be implemented in order to extend to a new datastore.
 * 
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public abstract class AbstractDatabaseDriver {
	
	protected volatile IConnection connection;
	
	/* Methods used to insert records to data store (WRITE operation) */
	/**
	 * This method is called by every thread at the start of inserts.
	 * 
	 * @param environment
	 * @return IDatabaseDriver
	 * @throws DBException
	 * @throws Exception 
	 */
	public abstract AbstractDatabaseDriver initWrites(Environment environment) throws DBException, Exception;

	/**
	 * This method inserts records one at a time.
	 * 
	 * @param columns
	 * @param schema
	 * 
	 * @return boolean
	 * @throws DBException
	 */
	public abstract boolean insertRecordsIndividually(List<TableColumn> columns,
			Table table) throws DBException, Exception;

	/**
	 * 
	 * This method adds statements to a batch.
	 * 
	 * @param batchSize
	 * @param columns
	 * @param schema
	 * @param flag
	 * @throws DBException
	 * @throws Exception 
	 */
	public abstract void addStatementsToBatch(int batchSize, List<TableColumn> columns,
			Table table, Boolean isUniqueKey) throws DBException, Exception;

	/**
	 * 
	 * This method inserts a batch into the Datastore.
	 * 
	 * @return
	 * @throws DBException
	 */
	public abstract boolean insertbatch() throws DBException;
	
	/* Methods used to query records from data store (READ operation) */

	/**
	 * This method is called by every thread of a query to initialize the connection and session object.
	 * 
	 * @param environment
	 * @return IDatabaseDriver driver Object
	 * @throws DBException
	 * @throws Exception 
	 */
	public abstract AbstractDatabaseDriver initReads(Environment environment) throws Exception;
	
	/**
	 * Method to get values for a given {row-key, clustering key range} for a Wide column family 
	 * 
	 * @param rowKey
	 * @param startCol The start column of the range
	 * @param endCol The end column of the range 
	 * @param limit [Optional]
	 * @param model DataModel
	 * @return NavigableMap<Object,Object> Map that has key as clustering key value and value as the returned value for the Wide Column.
	 */
	public abstract NavigableMap<Object, Object> getValuesForClusteringKeyRange_WideCF(String rowKey,
			Object startCol, Object endCol, int limit, DataModel model) throws Exception;
	
	/**
	 * Method to obtain all row-keys of a column family
	 * 
	 * @param parameter - limit,top,last
	 * @param value - value for the parameter (eg: for limit 10, parameter: limit and value: 10)
	 * @param model DataModel
	 * @return Sorted Set of all rowkeys
	 */
	public abstract TreeSet<String> getAllRowKeys(String parameter, int value, DataModel model) throws Exception;
	
	/**
	 * Method to retrieve the last wide column value for a rowKey in a Wide CF (assuming the data is sorted on clustering key)
	 * 
	 * @param rowKey
	 * @param model
	 * @return The last value corresponding to the provided row key
	 */
	public abstract Object getLastColumnValue_WideCF(String rowKey, DataModel model) throws Exception;

	/**
	 * Method to obtain the values per provided time step for a given {row-key, clustering key range} for a Wide column family 
	 * 
	 * @param rowKey
	 * @param startCol
	 * @param endCol
	 * @param limit
	 * @param model
	 * @param timestep
	 * @param usetimestep_flag
	 * @return NavigableMap<Object,Object> Map that has key as clustering key value and value as the returned value for the Wide Column.
	 */
	public abstract NavigableMap<Object, Object> getValuesForClusteringKeyRangePerTimestep_WideCF(String rowKey, Object startCol, Object endCol, int limit, DataModel model, int timestep, boolean usetimestep_flag) throws Exception;

	/**
	 * Method to obtain the values for a given {row-key, clustering key range, column} for a Tall column family
	 * 
	 * @param rowKey
	 * @param startCol
	 * @param endCol
	 * @param columnName
	 * @param col_datatype
	 * @param limit
	 * @param model
	 * @return NavigableMap<Object, Object> Map that has key as clustering key value and value as the returned value for the column queried.
	 */
	public abstract NavigableMap<Object, Object> getValuesForOneColumnWithClusteringKeyRange_TallCF(String rowKey, Object startCol, Object endCol, String columnName, DataTypes col_datatype, int limit, DataModel model) throws Exception;

	/**
	 * Method to obtain the all column values for a given {row-key, clustering key range} for a Tall column family
	 * 
	 * @param rowKey
	 * @param startCol
	 * @param endCol
	 * @param model
	 * @param limit
	 * @return NavigableMap<Object, Map<Object,Object>> Nested Map that has key as clustering key value and value as a map of column names and corresponding values in the Tall Column family.
	 */
	public abstract NavigableMap<Object, Map<Object,Object>> getValuesForAllColumnsWithClusteringKeyRange_TallCF(String rowKey, Object startCol, Object endCol, DataModel model, int limit) throws Exception;

	/**
	 * Method to obtain the super column value for a given {row-key, clustering key range} for a Super column family
	 * 
	 * @param <S> Type of SuperColumn 
	 * @param rowKey
	 * @param startCol
	 * @param endCol
	 * @param limit
	 * @param model
	 * @return NavigableMap<Object, S> Map that has key as clustering key value and value as a super column type value from a Super Column family.
	 */
	public abstract <S> NavigableMap<Object, S> getValuesForClusteringKeyRange_SuperColumnCF(String rowKey, Object startCol, Object endCol, int limit, DataModel model) throws Exception;

	/* Methods used by both Operations */
	
	/**
	 * This method is to close the DB connection and clean up threads. This will
	 * be called by every thread.
	 * 
	 * @throws Exception
	 * 
	 */
	public abstract void close() throws Exception;
	
}
