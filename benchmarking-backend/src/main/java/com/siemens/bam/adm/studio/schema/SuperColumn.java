package com.siemens.bam.adm.studio.schema;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class SuperColumn extends TableColumn {

	public enum SuperColumnType {
		LIST,
		SET,
		MAP;

		private static final Map<Integer, SuperColumnType> lookup = new HashMap<Integer, SuperColumnType>();

		static {
			for (SuperColumnType dt : EnumSet.allOf(SuperColumnType.class)) {
				lookup.put(dt.ordinal(), dt);
			}
		}

		public static SuperColumnType getDatatypeFromOrdinal(int ordinal) {
			return lookup.get(ordinal);
		}
	};

	private List<TableColumn> subColumns = new LinkedList<TableColumn>();
	private SuperColumnType scolType;

	public List<TableColumn> getSubColumns() {
		return subColumns;
	}

	public void setSubColumns(List<TableColumn> subColumns) {
		this.subColumns = subColumns;
	}

	public SuperColumnType getScolType() {
		return scolType;
	}

	public void setScolType(SuperColumnType scolType) {
		this.scolType = scolType;
	}

	public List<TableColumn> getColumns() {
		return subColumns;
	}

	public void setColumns(List<TableColumn> columns) {
		this.subColumns = columns;
	}

}
