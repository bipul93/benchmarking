package com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;

/**
 * Class that defines the Query Parameters for TagTrend Query 
 * @author Nisha Menon
 */

public class TagTrendQueryParams extends AbstractQueryParams
{
	private static final Logger logger = LoggerFactory.getLogger(TagTrendQueryParams.class);

	private DateTime begin;
	private DateTime end;
	private int limit;
	
	public TagTrendQueryParams()
	{
		super();
		this.begin = new DateTime();
		this.end = new DateTime();
		this.limit = 0;
		logger.debug("New QueryParams created");

	}

	public DateTime getBegin() {
		return begin;
	}

	public void setBegin(String begin) 
	{
		this.begin = DateFormattingUtils.getJodaDate(begin);
	}

	public DateTime getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = DateFormattingUtils.getJodaDate(end);
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}
