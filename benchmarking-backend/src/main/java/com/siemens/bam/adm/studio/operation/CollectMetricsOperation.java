package com.siemens.bam.adm.studio.operation;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbmetrics.AbstractDataStoreMetrics;
import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * Implements the DBOperation interface
 *
 * Methods to COLLECTMETRICS for a previously executed workload and store them
 * in a file
 *
 * @author varsha.raveendran@siemens.com
 *
 */
public class CollectMetricsOperation implements IDBOperation {

	private static final Logger logger = Logger.getLogger(CollectMetricsOperation.class);
	private static final String NAME = "COLLECTMETRICS";

	private Environment environment;
	private String fileName = null;

	/**
	 * Sets the data model, database driver for COLLECTMETRICS operations
	 *
	 * @see com.siemens.bam.adm.studio.operation.IDBOperation#buildEnvironment(com.siemens.bam.adm.studio.utils.ClientProperties)
	 */
	@Override
	public void buildEnvironment(ClientProperties clientProp) throws Exception {
		environment = new Environment.EnvironmentBuilder(clientProp).build();
	}

	/**
	 * Method to collect the metrics for a specified duration
	 *
	 * @throws Exception
	 */

	@Override
	public void start() throws Exception {
		ClientProperties clientProp = environment.getClientProperties();

		File dir = new File("./output/" + clientProp.getDataStoreName() + "-Metrics" + clientProp.getOperation());

		if (!dir.exists()) {
			dir.mkdir();
		}

		if (fileName == null) {
			fileName = clientProp.getStartTimeForMetricCollection() + "." + clientProp.getEndTimeForMetricCollection() + "." + ".csv";
		}
		fileName = dir + "\\" + fileName;

		AbstractDataStoreMetrics metricDriver = environment.getDataStoreMetrics();

		if (metricDriver != null) {
			metricDriver.collectMetrics(fileName, clientProp);
			logger.debug("Metric collection completed.");
		}

	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public <T> Callable<T> getInstanceOfOperation(int threadid) {

		logger.debug("The main thread is used to collect metrics");

		return null;
	}

	@Override
	public <T> void displayExecutionStats(CompletionService<T> completionPool) throws Exception {
		logger.info("The CSV file with metrics can be found at location " + fileName);
	}

	/**
	 * Method to set the file to which the metrics is stored
	 * If not set it is stored by default at \n
	 * ./output/<datastore name>.<start time of metric collection>.<end time of metric collection>.csv
	 *
	 * @param filename
	 */
	public void setMetricFileName(String filename) {
		this.fileName = filename;
	}

}
