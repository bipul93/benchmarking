package com.siemens.bam.adm.studio.schema.tableType;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;

public class WideTableType extends AbstractTableType {

	final static Logger logger = LoggerFactory.getLogger(WideTableType.class.getName());

	@Override
	public void init(Table table, ClientProperties clientProp) throws Exception {

		List<String> rowKeys = table.getRowKeys();
		setUniqueSetOfValues(dataGenerator.generateUniqueData(table, clientProp, rowKeys));
	}

	/**
	 * 
	 * The column name is built by concatenating data generated for clustering keys (as defined in JSON schema)
	 * 
	 */
	@Override
	public <T> T buildColumnName(String colName, List<String> names, List<TableColumn> columns) throws Exception {

		int colType;
		Object columnName = null;
		int index = 0;

		TableColumn c = new TableColumn();

		for (String clusterKey : names) {
			c.setColName(clusterKey);

			colType = columns.get(columns.indexOf(c)).getColDataType();

			logger.debug("Cluster Key : " + clusterKey + " type : " + colType + " : "
					+ DataTypes.getDatatypeFromOrdinal(colType));

			columnName = getValueForField(columns.get(columns.indexOf(c)));
			index++;
			if (!(index == names.size()))
				columnName += "+";

		}

		logger.debug("The columnValidator is " + columnName);

		return (T) columnName;
	}

}
