package com.siemens.bam.adm.studio.utils;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.schema.SuperColumn.SuperColumnType;

/**
 * Class that formats the results retrieved for each query and displays it on the console
 * @author Nisha Menon
 *
 * @param <T>
 */
public class ScanQueryResult<T>
{
	private static final Logger logger = Logger.getLogger(ScanQueryResult.class);

	/*
	 * Prints to logger a Map of {Object, Object}
	 */
	public static void scanNavigableMapofObjects(Map<Object, Object> colList)
	{
		if (colList == null)
		{
			logger.info("No results!");
		}
		Iterable<Object> key = colList.keySet();
		Iterator<Object> keyIterator = key.iterator();
		while (keyIterator.hasNext())
		{
			Object col = keyIterator.next();
			Object value = colList.get(col);
			GlobalData.queryResult =  GlobalData.queryResult + "|" + col + ":" + value;
			logger.info(col + " : " + value);
		}
	}

	/*
	 * Prints to logger a Map of {Object, List<Object>}
	 */
	public static void scanNavigableMapofListofObjects(Map<Object, List<Object>> colList)
	{
		if (colList == null)
		{
			logger.info("No results!");
		}
		Iterable<Object> key = colList.keySet();
		Iterator<Object> keyIterator = key.iterator();
		while (keyIterator.hasNext())
		{
			Object col = keyIterator.next();
			logger.info(col);
			List<Object> list = colList.get(col);
			Iterator<Object> i = list.iterator();
			while(i.hasNext())
			{
				Object o = i.next();
				logger.info("\t :" + o);
			}
			System.out.println();
		}
	}

	/*
	 * Prints to logger a Map of {Object, Map<Object,Object>}
	 */
	public static void scanNavigableMapofNestedMapofObjects(Map<Object, Map<Object,Object>> colList)
	{
		if (colList == null)
		{
			logger.info("No results!");
		}
		Iterable<Object> key = colList.keySet();
		Iterator<Object> keyIterator = key.iterator();
		while (keyIterator.hasNext())
		{
			Object col = keyIterator.next();
			logger.info("Key: "+col);
			Map<Object,Object> list = colList.get(col);
			Iterable<Object> nestedMapKey = list.keySet();
			Iterator<Object> nestedMapKeyIterator = nestedMapKey.iterator();
			while(nestedMapKeyIterator.hasNext())
			{
				Object column = nestedMapKeyIterator.next();
				Object nestedValue = list.get(column);
				logger.info(column + " : " + nestedValue);
			}
		}
	}

	/*
	 * Prints to logger a Map of {Object, SuperColumnType T} where T is the type of super column
	 */
	@SuppressWarnings("unchecked")
	public static <T> void scanNavigableMapofSuperColumnObjects(Map<Object, T> colList, SuperColumnType scolType)
	{
		if (colList == null)
		{
			logger.info("No results!");
		}
		else
		{
			switch(scolType)
			{
				case LIST:
					Map<Object, List<Object>> colList1 = (Map<Object, List<Object>>)colList;
					scanNavigableMapofListofObjects(colList1);
					break;
				case MAP:
					Map<Object, Map<Object,Object>> colMap = (Map<Object, Map<Object,Object>>)colList;
					scanNavigableMapofNestedMapofObjects(colMap);;
					break;
				case SET:
					break;
				default:
					break;
			}
		}
	}
}
