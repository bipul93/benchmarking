package com.siemens.bam.adm.studio.operation;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.core.Executor;
import com.siemens.bam.adm.studio.read.userInterface.BatchUserInterface;

import com.siemens.bam.adm.studio.read.userInterface.IUserInterface;
import com.siemens.bam.adm.studio.read.userInterface.TestUserInterface;
import com.siemens.bam.adm.studio.threads.TestDB;
import com.siemens.bam.adm.studio.utils.ClientProperties;

/**
 * Implements the DBOperation interface
 *
 * Methods to execute READ workloads and display the execution results at the end of the run
 *
 * @author varsha.raveendran@siemens.com
 *
 */
public class TestDBOperation implements IDBOperation {

	private static final Logger logger = Logger.getLogger(TestDBOperation.class);
	private Environment environment;
	private static final String NAME = "TestDB";
	public String serverIP = "";
	public String serverPort = "";

	public TestDBOperation(String serverIP, String serverPort) {
		this.serverIP = serverIP;
		this.serverPort = serverPort;
	}

	/**
	 * Sets the data model, database driver and registers queries at the query factory for READ
	 * operations
	 *
	 * @see com.siemens.bam.adm.studio.operation.IDBOperation#buildEnvironment(com.siemens.bam.adm.studio.utils.ClientProperties)
	 */
	@Override
	public void buildEnvironment(ClientProperties clientProp) throws Exception {
		environment = new Environment.EnvironmentBuilder(clientProp).setQueryService().build();
	}

	@Override
	public void start() throws Exception {

		IUserInterface uiType;
		//String mode = environment.getClientProperties().getMode();
		//logger.info(environment.getClientProperties() + "...");

		//if (mode.equalsIgnoreCase("Interactive")) {

			//logger.info("Starting Interactive user interface");

			uiType = new TestUserInterface(this);
			uiType.runUserInterface();

		//}

	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public <T> Callable<T> getInstanceOfOperation(int threadid) {
		return new TestDB<T>(environment, threadid);
	}

	@Override
	public <T> void displayExecutionStats(CompletionService<T> completionPool) throws Exception {
		logger.info("Fetching Results");
		T Results = (T) environment.getQueryObject().getResults(Executor.getNumberOfThreadsRequired(), completionPool);

		logger.info("Displaying Results -> ");
		environment.getQueryObject().printResults(Results);

		long time = environment.getQueryObject().findExecutionTime();
		logger.info("The total time for the query (max time of all spawned threads) is " + time + " ms");

	}

	public Environment getEnvironment() {
		return environment;
	}

	public String getServerIP(){
		return this.serverIP;
	}

	public String getServerPort(){
		return this.serverPort;
	}

}
