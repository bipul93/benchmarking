package com.siemens.bam.adm.studio.read.userInterface;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.core.Executor;
import com.siemens.bam.adm.studio.operation.ReadOperation;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;

/**
 * Class that implements Interactive User Interface functionalities
 * @author Nisha Menon
 */
public class InteractiveUserInterface implements IUserInterface
{
	private static final Logger logger = LoggerFactory.getLogger(InteractiveUserInterface.class);
	private ReadOperation operation;
	private boolean webFlag = false;
	public InteractiveUserInterface(ReadOperation operation)
	{
		this.operation = operation;
		logger.trace("Command Line User Interface Object Set.");
	}

	@Override
	public void runUserInterface() throws Exception
	{
		
		//if web flag is true

		if(webFlag==false){
			//
		}else{
			//
		}

		logger.debug("Within Interactive User Interface Implementation.");
		System.out.println();
		System.out.println("Read Benchmark Tool > ");
		System.out.println("/**	-------------------------------------------------------------------------------------"
						+ "\n"
						+ " *	NOTE: Query parameters are specified as a series of key-value pairs."
						+ "\n"
						+ " *	Multiple parameters can be specified in a single line."
						+ "\n"
						+ " *"
						+ "\n"
						+ " *	EXAMPLE:"
						+ "\n"
						+ " *	<QueryName> rowKey [<rowkey1>,<rowkey2>,...,<rowkeyN>]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS>"
						+ "\n"
						+ " *	end <yyyy-MM-dd>T<HH:mm:ss.SSS> limit <N> timeStep <T in seconds> aggregationType <AVG|MAX|MIN>"
						+ "\n"
						+ " *  	Avoid extra space between key-values."
						+ "\n"
						+ " */	-------------------------------------------------------------------------------------");
		System.out.println("	Type 'help' for more information" + "\n" + "	Type 'querylist' for getting the list of queries");
		System.out.print(">> ");
		try
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String nextLine;
			int threadsToUse;
			while (true)
			{
				nextLine = br.readLine();
				if (nextLine.equalsIgnoreCase("exit"))
				{
					logger.info("Exiting the Interactive User Interface!");
					System.exit(0);
				}
				/**
				 * Help request
				 */
				else if (nextLine.equalsIgnoreCase("help"))
				{
					System.out.println();
					System.out.println("/**	-------------------------------------------------------------------------------------"
									+ "\n"
									+ " *	EXAMPLE:"
									+ "\n"
									+ " *	<QueryName> rowKey [<rowkey1>,<rowkey2>,...,<rowkeyN>]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS>"
									+ "\n"
									+ " *	end <yyyy-MM-dd>T<HH:mm:ss.SSS> limit <N> timestep <T in seconds> aggregationType <AVG|MAX|MIN>"
									+ "\n"
									+ " */	-------------------------------------------------------------------------------------");
					System.out.println();
					System.out.println("List of accepted commands: " + "\n"
							+ "querylist " + "\n"
							+ "rowkey [<rowkey1>,<rowkey2>,...,<rowkeyN>]\n"
							+ "rowkey [file] (this command picks up rowkeys from the file provided by property \"RowKeyPath\" in the configuration file) \n"
							+ "begin <yyyy-MM-dd>T<HH:mm:ss.SSS>" + "\n"
							+ "end <yyyy-MM-dd>T<HH:mm:ss.SSS>" + "\n"
							+ "column <column_name>" + "\n"
							+ "time <T in seconds> [for tagtrendlatest query]" + "\n"
							+ "timestep <T in seconds> [for tagtrendaggregate query]" + "\n" + "limit <N>" + "\n"
							+ "aggregationType <MAX|MIN|AVG> [for tagtrendaggregate query]" + "\n" + "exit " + "\n"
							+ "");
					System.out.println("Type help <queryname> for more details!");
				}

				else if (nextLine.contains("help "))
				{
					String[] params = nextLine.split(" +");
					helpMessages(params[1]);
				}

				else if (nextLine.equalsIgnoreCase("querylist"))
				{
					Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
					System.out.println("The list of queries available: " +queryNameTypes);
				}
				// Receive each query and process it accordingly.
				else
				{
					String[] params = nextLine.split(" +");

					//validate the queryName
					String queryName = params[0];
					ValidateQueryParams.validateQueryName(queryName);

					AbstractQuery queryObject = RegisterQueryService.getRegisteredQueries().get(queryName);

					if(queryObject==null)
					{
						logger.error("Invalid Query! Query object could not be created.");
					}
					else
					{
						if(queryObject.initializeParameters(params))
						{
							logger.debug("Valid Query! Proceeding to execute the query.");
							operation.getEnvironment().setQueryObject(queryObject);

							Executor<Void> executor = new Executor<Void>();
							executor.init(operation.getEnvironment());

							/*  Number of threads spawned for a query = number of rowkeys in the query
							  If the query has no rowkeys, then one thread would be spawned.*/

							threadsToUse = (queryObject.getQueryParams().getRowKeys().size() == 0 ? 1 : queryObject.getQueryParams().getRowKeys().size());
							Executor.setNumberOfThreadsRequired(threadsToUse);
							executor.execute(operation);
						}
					}
				}

				System.out.print(">> ");
			}
		}
		catch (Exception e)
		{
			throw new Exception ("Interactive User Interface Parsing Exception",e);
		}
	}
	public void helpMessages(String queryName)
	{
		switch(queryName)
		{
		case "tagtrend":
			System.out.println("tagtrend rowkey [<rowkey1>,<rowkey2>,..]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS> end <yyyy-MM-dd>T<HH:mm:ss.SSS>");
			System.out.println("An Optional limit parameter can also be specified with the limit <value> clause");
			break;

		case "tagtrendaggregate":
			System.out.println("tagtrendaggregate rowkey [<rowkey1>,<rowkey2>,..]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS> end <yyyy-MM-dd>T<HH:mm:ss.SSS> timestep <value> aggregationType <MAX|MIN|AVG>");
			break;

		case "tagtrendlatest":
			System.out.println("tagtrendlatest rowkey [<rowkey1>,<rowkey2>,..]|[file] timerange <value in seconds> ");
			break;

		case "tagdatatall":
			System.out.println("tagdatatall rowkey [<rowkey1>,<rowkey2>,..]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS> end <yyyy-MM-dd>T<HH:mm:ss.SSS>");
			System.out.println("An Optional limit parameter can also be specified with the limit <value> clause");
			break;

		case "tagtrendtall":
			System.out.println("tagtrendtall rowkey [<rowkey1>,<rowkey2>,..]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS> end <yyyy-MM-dd>T<HH:mm:ss.SSS> column <column_name>");
			System.out.println("An Optional limit parameter can also be specified with the limit <value> clause");
			break;

		case "tagdatasupercolumn":
			System.out.println("tagdatasupercolumn rowkey [<rowkey1>,<rowkey2>,..]|[file] begin <yyyy-MM-dd>T<HH:mm:ss.SSS> end <yyyy-MM-dd>T<HH:mm:ss.SSS> ");
			System.out.println("An Optional limit parameter can also be specified with the limit <value> clause");
			break;

		case "getrowkeys":
			System.out.println("getrowkeys ");
			System.out.println("Optional limit|top|last parameter could also be specified as follows:");
			System.out.println("getrowkeys limit <value> ");
			System.out.println("getrowkeys top <value> ");
			System.out.println("getrowkeys last <value> ");
			break;

		case "getstudentmarkswide":
			System.out.println("getstudentmarkswide rowkey [<rowkey1>,<rowkey2>,..] begin <startRange> end <endRange>");
			System.out.println("An Optional limit parameter can also be specified with the limit <value> clause");
			break;

		default:System.out.println("Query not defined!");
			break;
		}
	}

}
