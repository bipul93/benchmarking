package com.siemens.bam.adm.studio.schema.validators;

import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * Class to create a customized Column validator
 * @author Nisha Menon
 */

public class CustomizedColumnValidator extends AbstractColumnValidator 
{
	private static final Logger logger = Logger.getLogger(CustomizedColumnValidator.class);
	private String modelType;

	public CustomizedColumnValidator() 
	{
		this.ColumnValidator = null;
	}

	public CustomizedColumnValidator(String key, String type) 
	{
		this.ColumnValidator = key;
		this.modelType = type;
		this.validateComparator(key, this.modelType);
	}

	private void validateComparator(String cmp, String modelType) 
	{
		CustomizedDataTypes cus = new CustomizedDataTypes();
		ArrayList<String> validTypes = cus.retrieveValidDataTypes(modelType);
		if (validTypes.contains(cmp)) {
			logger.debug("Column Validator : Valid Key " + cmp);
		} else {
			logger.debug("Column Validator : Invalid Key " + cmp );
		}
	}

}
