package com.siemens.bam.adm.studio.connections;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.MasterNotRunningException;
import org.apache.hadoop.hbase.ZooKeeperConnectionException;
import org.apache.hadoop.hbase.client.HBaseAdmin;
import org.apache.hadoop.hbase.client.HConnection;
import org.apache.hadoop.hbase.client.HConnectionManager;
import org.apache.hadoop.hbase.client.HTableInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.hadoop.hbase.HConstants;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.hbase.HBaseProperties;
import com.siemens.bam.adm.studio.exception.DBException;

/**
 * The Connection Class for HBase
 * 
 * @author nisha.menon@siemens.com
 * @author varsha.raveendran@siemens.com
 * 
 */
public class HBaseConnection implements IConnection {
	private static final Logger logger = LoggerFactory.getLogger(HBaseConnection.class.getName());
	private Configuration config;
	private static HBaseProperties hbaseProperties = new HBaseProperties();

	public static HBaseProperties getHbaseProperties() {
		return hbaseProperties;
	}

	public HConnection getConnection() {
		return connection;
	}

	public ThreadPoolExecutor getExecutor() {
		return executor;
	}

	private HConnection connection;
	private HTableInterface table;
	private final ThreadPoolExecutor executor;

	// The value of the final data member executor must be set in the
	// constructor
	public HBaseConnection(Environment environment) throws DBException, Exception {

		config = HBaseConfiguration.create();
		config.setBoolean("hbase.ipc.client.tcpnodelay", true);

		logger.debug("Hbase Root Dir " + config.get("hbase.rootdir"));
		config.set("hbase.client.retries.numbers", hbaseProperties.getHBaseConnectionRetriesNumber());

		connection = HConnectionManager.createConnection(config);

		int coreThreads = Runtime.getRuntime().availableProcessors();
		int maxThreads = coreThreads * 2;
		this.executor = new ThreadPoolExecutor(coreThreads, maxThreads,
				// Time out threads after 60 seconds of inactivity
				60L, TimeUnit.SECONDS,
				// Queue up to N tasks per worker threads (N=100 by default)
				new LinkedBlockingQueue<Runnable>(maxThreads * config.getInt(HConstants.HBASE_CLIENT_MAX_TOTAL_TASKS,
						HConstants.DEFAULT_HBASE_CLIENT_MAX_TOTAL_TASKS)),
				// Create daemon threads
				new ThreadFactory() {
					public Thread newThread(Runnable r) {
						Thread t = new Thread(r);
						t.setDaemon(true);
						return t;
					}
				});
		// Allow for the core thread pool to shrink with inactivity
		this.executor.allowCoreThreadTimeOut(true);

		this.executor.prestartAllCoreThreads();

		initializeConnection(environment);
		logger.debug("HBase Connection Object initialised!");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.siemens.bam.adm.studio.connections.IConnections#getConnection(com.
	 * siemens.bam.adm.studio.core.Environment)
	 */
	public void initializeConnection(Environment environment) throws DBException, Exception {
		String db = environment.getClientProperties().getDatabaseName();

		try {

			// in HBase, keyword "table" is analogous to "database" in SQL
			table = connection.getTable(db);
			logger.info("Connection to HBase Cluster successful!");

		} catch (MasterNotRunningException e) {
			logger.error("Error in HBase Master Connection" + e.getMessage());
		} catch (ZooKeeperConnectionException e) {
			logger.error("Error in HBase Zookeeper Connection" + e.getMessage());
		} catch (IOException e) {
			logger.error("Error in HBase Connection" + e.getMessage());
		}

		checkifTableExists(db, environment.getClientProperties().getTableName());
	}

	private void checkifTableExists(String tableName, String columnFamily) throws DBException, IOException {
		logger.debug("Checking if HBase table exists");

		HBaseAdmin admin = null;
		try {
			admin = new HBaseAdmin(connection);
		} catch (MasterNotRunningException e) {
			logger.error("HBase Master Not running exception!");
			throw new DBException(e);
		} catch (ZooKeeperConnectionException e) {
			logger.error("HBase Master Not running exception!");
			throw new DBException(e);
		} finally {
			if (admin != null) {
				admin.close();
			}
		}

		try {
			if (!admin.tableExists(tableName)) {

				logger.error("Table " + tableName + " does not exist in HBase!");

				throw new DBException("Table does not exist in HBase!");
			}
		} catch (IOException e) {

			logger.error(e.getMessage());
			throw new DBException("Error while checking if table exists!");
		} finally {
			admin.close();
		}
		admin.close();
	}

	public HTableInterface getHTableInterface() {
		return table;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.connections.IConnections#close()
	 */
	public void closeConnection() throws DBException {
		try {
			connection.close();
		} catch (IOException e) {
			logger.error("Error in HBase Connection Close" + e.getMessage());
		}
	}

}
