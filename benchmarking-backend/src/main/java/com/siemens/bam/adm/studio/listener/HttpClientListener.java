package com.siemens.bam.adm.studio.listener;

import java.io.IOException;
import java.io.*;
import java.net.*;
import java.lang.Object.*;
import java.util.*;
import java.util.concurrent.*;
import java.net.*;
import java.lang.Thread;

import org.apache.commons.httpclient.*;
import org.apache.commons.httpclient.methods.*;
import org.apache.http.conn.scheme.*;
import org.apache.http.params.HttpParams;
import org.json.simple.parser.JSONParser;
import org.json.simple.JSONObject;
import org.apache.http.params.BasicHttpParams;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;
import org.apache.commons.httpclient.methods.StringRequestEntity;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.apache.http.client.*;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.HttpResponse;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import com.siemens.bam.adm.studio.connections.CassandraNativeConnection;
import com.siemens.bam.adm.studio.operation.IDBOperation;
import com.siemens.bam.adm.studio.operation.DBOperationFactory;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.GlobalData;
import com.siemens.bam.adm.studio.utils.JSONtoPropsParser;
import com.siemens.bam.adm.studio.clients.Client;

public class HttpClientListener {
  ClientProperties clientProp;
  String serverAddress = "";
  String serverPort = "";
  String ipValue = "";
  boolean connectionActive = true;

  String configFilePath = "config/Property.conf";
  final static Logger logger = LoggerFactory.getLogger(HttpClientListener.class.getName());

  public HttpClientListener(String ip, String port){
      serverAddress = ip;
      serverPort = port;
  }

  public void connect(){

  }

  /*private static DefaultHttpClient client;

    public synchronized static DefaultHttpClient getThreadSafeClient() {

        if (client != null)
            return client;

        client = new DefaultHttpClient();

        ClientConnectionManager mgr = client.getConnectionManager();

        HttpParams params = client.getParams();
        client = new DefaultHttpClient(
        new ThreadSafeClientConnManager(params,
            mgr.getSchemeRegistry()), params);

        return client;
    }*/

    //REGISTER CLIENT

    public void registerClient(String ipAddress) throws Exception{

      MultiThreadedHttpConnectionManager connectionManager =
        		new MultiThreadedHttpConnectionManager();

        	HttpClient client = new HttpClient(connectionManager);
          String url = "http://" + serverAddress + ":" + serverPort + "/benchmarkingServer/activateMe";
          //System.out.println(url);
          GetMethod post = new GetMethod(url);
          //
          NameValuePair [] parameters = new NameValuePair[1];
          //is test result send ip else send query result
          String escapedQResult = escapeHtml(GlobalData.queryResult);
          parameters[0] = new NameValuePair("clientIP",ipAddress);

          post.setQueryString(parameters);

          try {
                int status = client.executeMethod(post);
                String val = post.getResponseBodyAsString();
                //System.out.println(status);
                //System.out.println(val);
          } catch(Exception e){
            e.printStackTrace();
          } finally {
              // be sure the connection is released back to the connection
              // manager
              //logger.info("RELEASING CONNECTION HERE!!!");
              post.releaseConnection();
          }
    }

    public void postTestResult(boolean value) throws Exception{
      //
      MultiThreadedHttpConnectionManager connectionManager =
        		new MultiThreadedHttpConnectionManager();

      	  HttpClient client = new HttpClient(connectionManager);
          String url = "http://" + serverAddress + ":"+ serverPort +"/benchmarkingServer/postTestResult";
          PostMethod post = new PostMethod(url);
          logger.info(serverAddress + ":"+serverPort);
          String escapedQResult = escapeHtml(GlobalData.queryResult);
          try {

                //String[] values = val.split("~");
                System.out.println("------------------------------------");
                //System.out.println(val);
                String jsonString = "";
                if(value){
	                if(escapedQResult != null && escapedQResult != ""){
	                  jsonString = "{\"queryResult\":\""+escapedQResult+"\",\"clientIP\":\""+ipValue+"\"}";
	                  logger.info(jsonString);
	                }else{
	                  jsonString = "{\"testResult\":\""+String.valueOf(value)+"\",\"clientIP\":\""+ipValue+"\",\"result\":\"\"}";
	                }
	                post.addParameter("jsondata",jsonString);
                }else{
                	jsonString = "{\"queryResult\":\"The client is busy\",\"clientIP\":\""+ipValue+"\"}";
                	post.addParameter("jsondata",jsonString);
                }
                logger.info(jsonString);
                int status = client.executeMethod(post);
                String val = post.getResponseBodyAsString();
                System.out.println(status);
                logger.info(val);
          } catch(Exception e){
            logger.info("exception ");
            e.printStackTrace();
          } finally {
              post.releaseConnection();
          }
    }

  public void keepAliveConnect() throws Exception {
      //System.out.println(System.getProperty("os.name"));
    ipValue = InetAddress.getLocalHost().getHostAddress();
    //in case InetAddress fails
    if(ipValue.equalsIgnoreCase("127.0.0.1") || ipValue.equalsIgnoreCase("127.0.1.1")){
      Enumeration e2 = NetworkInterface.getNetworkInterfaces();
      while(e2.hasMoreElements()){
        NetworkInterface n = (NetworkInterface) e2.nextElement();
        Enumeration ee = n.getInetAddresses();
        while (ee.hasMoreElements()){
            InetAddress i = (InetAddress) ee.nextElement();
            if(i.getHostAddress().contains(":") || i.getHostAddress().equalsIgnoreCase("127.0.0.1") || i.getHostAddress().equalsIgnoreCase("127.0.1.1")){
              // ignoring ipv6 and local addresses
              continue;
            }
            ipValue = i.getHostAddress();
        }
      }
    }
    logger.info(ipValue);
    registerClient(ipValue);
    //postTestResult(true);
    //InputStream inputStream = null;
    MultiThreadedHttpConnectionManager connectionManager =
      		new MultiThreadedHttpConnectionManager();
        String url = "http://" + serverAddress + ":"+ serverPort +"/benchmarkingServer/getInfo";
        //System.out.println(url);
        HttpClient client = new HttpClient(connectionManager);
        GetMethod get = new GetMethod(url);
        PostMethod post = new PostMethod(url);
        get.addRequestHeader("accept", "application/json");

        try {
          //logger.info("Sending");
            while(connectionActive){
              int status = client.executeMethod(get);
              String responseMessage = get.getResponseBodyAsString();
                GlobalData.webMode = true;
                //logger.info(responseMessage);
                    if(responseMessage.indexOf("~") != -1){
                        final String data[] = responseMessage.split("~");
                        //System.out.println(data.length);
                        if(data.length > 3){
                            //logger.info(data[1] + data[2] + data[3]);
                        }else{
                            //logger.info(data[1] + data[2]);
                        }
                        final String dataSets[] = responseMessage.split("~");
                        if(data[1].equalsIgnoreCase("write")){
                        	if(!GlobalData.readInProgress && !GlobalData.writeInProgress){
                        		try{
	                                    //saveSchemaFile(data[3]);
	                                    logger.info("writing");
	                                    GlobalData.writeInProgress = true;
	                                    performWriteOperation(data[4]);
	                              }catch(NullPointerException e){
	                                  logger.info("writing error , please try again!");
	                                  continue;
	                              }
                        	}else{
                        		postTestResult(false);
                        	}
                      }else if(data[2].equalsIgnoreCase("run command") || data[2].equalsIgnoreCase("Test Connection")){
                            //saveSchemaFile(data[3]);
                    	  if(!GlobalData.readInProgress && !GlobalData.writeInProgress){
                    		  GlobalData.readInProgress = true;
                            readBatch(data[4],data[1]);
                    	  }else{
                    		  postTestResult(false);
                    	  }
                        }else if(data[1].equalsIgnoreCase("Test Connection")){
                        	logger.info(ipValue);
                        	logger.info(data[0]);
                        	if(data[0].equals(ipValue)){
                        		testIfConnected(data[2]);
                        	}
                        }
                    }
              //}
              Thread.sleep(1000);
            }

        } catch(Exception e){
          postTestResult(false);
          GlobalData.webMode = false;
          GlobalData.webError = e.getMessage();
          e.printStackTrace();

        } finally {
            get.releaseConnection();
        }
    }

  public void testIfConnected(String dataProps) throws Exception {
      //logger.info(dataProps);
	 Properties confFilePath = saveConfFile(dataProps);
     System.out.println(confFilePath);
     //logger.info(confFilePath);
     //java -Dlog4j.configuration=file:src/main/resources/log4j-config.xml com.siemens.bam.adm.studio.clients.Client -conf config/Property.conf -operation TestConnection -mode interactive
     clientProp = new ClientProperties(confFilePath);
     logger.info(clientProp.getOperation());
     doClientOperation(clientProp,true);
     postTestResult(true);
  }

  public void performWriteOperation(String dataProps) throws Exception{
	//String confFilePath = saveConfFile();
    Properties confFilePath = saveConfFile(dataProps);
    clientProp = new ClientProperties(confFilePath);
    clientProp.setOperation("write");
    clientProp.setServerAddress(serverAddress);
    clientProp.setServerPort(serverPort);
    doClientOperation(clientProp,false);

  }

  public void performReadInteractive(String dataProps) throws Exception {
    //java -Dlog4j.configuration=file:src/main/resources/log4j-config.xml com.siemens.bam.adm.studio.clients.Client -conf config/Property.conf -operation TestConnection -mode interactive
	//String confFilePath = saveConfFile("");
  }

  public void readBatch(String dataProps, String command) throws Exception{
	  logger.info("reading");
      String commandFilePath = saveFile(command);
      //save command to a file
	  Properties confFilePath = saveConfFile(dataProps);
      System.out.println(confFilePath);
      clientProp = new ClientProperties(confFilePath);
      clientProp.setOperation("read");
      clientProp.setMode("batch");
      clientProp.setQueryFilePath(commandFilePath);
      clientProp.setServerAddress(serverAddress);
      clientProp.setServerPort(serverPort);
      doClientOperation(clientProp,false);
  }

  public void doClientOperation(ClientProperties clientProp,Boolean testStatus) {
      IDBOperation operation;
      if(testStatus){
          try {
              operation = DBOperationFactory.getOperationInstance("testConnection",serverAddress,serverPort);
              operation.buildEnvironment(clientProp);
              operation.start();
          } catch (Exception e) {
              logger.error("System exiting due to error: ", e);
              System.exit(1);
          }
      }else{
          try {
              operation = DBOperationFactory.getOperationInstance(clientProp.getOperation(),null,null);
              operation.buildEnvironment(clientProp);
              operation.start();
          } catch (Exception e) {
              logger.error("System exiting due to error: ", e);
              System.exit(1);
          }
      }
  }

  public String saveFile(String commandCotent) throws Exception{
    String filePath = "cmdToRun.txt";
    //getProjectPath
    File cmdFile = new File(System.getProperty("."), filePath);
		FileWriter fw = new FileWriter(cmdFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
    bw.write(commandCotent);
    bw.close();

    return cmdFile.getAbsolutePath();
  }

  public String saveSchemaFile(String schemaContent) throws Exception{
    Calendar cal = Calendar.getInstance();
    String timeStamp = cal.getTime().toString();
    String fileName = "TestSchema_".concat(timeStamp.replaceAll(" ","_"));
    String filePath = fileName.concat(".json");
    logger.info(filePath);
    //newConfig.setProperty("SchemaPath",cmdFile.getAbsolutePath());

    if(System.getProperty("os.name").toLowerCase() == "linux"){
        File cmdFile = new File(System.getProperty("./schema"), filePath);
    		FileWriter fw = new FileWriter(cmdFile.getAbsoluteFile());
    		BufferedWriter bw = new BufferedWriter(fw);
        bw.write(schemaContent);
        bw.close();
        return cmdFile.getAbsolutePath();
    }else{
        File cmdFile = new File(System.getProperty(".\\schema"), filePath);
    		FileWriter fw = new FileWriter(cmdFile.getAbsoluteFile());
    		BufferedWriter bw = new BufferedWriter(fw);
        bw.write(schemaContent);
        bw.close();
        return cmdFile.getAbsolutePath();
    }
  }

  public String saveMetricsProperties(String metricProperties) throws Exception{

    String filePath = "ListOfMetrics.properties";
    logger.info(filePath);
    //newConfig.setProperty("SchemaPath",cmdFile.getAbsolutePath());

    if(System.getProperty("os.name").toLowerCase() == "linux"){
        File cmdFile = new File(System.getProperty("./input"), filePath);
    		FileWriter fw = new FileWriter(cmdFile.getAbsoluteFile());
    		BufferedWriter bw = new BufferedWriter(fw);
        bw.write(metricProperties);
        bw.close();
        return cmdFile.getAbsolutePath();
    }else{
        File cmdFile = new File(System.getProperty(".\\input"), filePath);
    		FileWriter fw = new FileWriter(cmdFile.getAbsoluteFile());
    		BufferedWriter bw = new BufferedWriter(fw);
        bw.write(metricProperties);
        bw.close();
        return cmdFile.getAbsolutePath();
    }
  }



  public Properties saveConfFile(String jsonStringData){
        Properties props = new Properties();
	    String filePath = "property.conf";
	    JSONParser parser = new JSONParser();
	    //logger.info(jsonStringData);
        //String jsondata = jsonStringData.replaceAll("\"","\'");
        logger.info(jsonStringData);
        try{
            JSONObject jsonObj = (JSONObject) parser.parse(jsonStringData);

            props = jsonTOProperties(jsonObj,"");

    	    System.out.println(jsonObj.toString());
        }catch(Exception e){
            e.printStackTrace();

        }
        /*if(System.getProperty("os.name").toLowerCase() == "linux"){
    	    File confFile = new File(System.getProperty("./config/"), filePath);
    			FileWriter fw = new FileWriter(confFile.getAbsoluteFile());
    			BufferedWriter bw = new BufferedWriter(fw);

    	    bw.write(jsonStringData);
    	    bw.close();
            return confFile.getAbsolutePath();
        }else{

    	    File confFile = new File(System.getProperty(".\\config\\"), filePath);
    			FileWriter fw = new FileWriter(confFile.getAbsoluteFile());
    			BufferedWriter bw = new BufferedWriter(fw);

    	    bw.write(jsonStringData);
    	    bw.close();
            return confFile.getAbsolutePath();
        }*/
        return props;
	  }

    public Properties jsonTOProperties(JSONObject jsonObject,String appendString){
        Properties props = new Properties();
        Iterator<String> iterator = jsonObject.keySet().iterator();
        while(iterator.hasNext()){
            String key = (String)iterator.next();
            Object value;
            if(jsonObject.get(key) instanceof String){
                value = (String) jsonObject.get(key);
                String keyName = appendString.concat(key);
                props.setProperty(keyName,String.valueOf(value));
            }else if(jsonObject.get(key) instanceof Long){
                value = (Long) jsonObject.get(key);
                String keyName = appendString.concat(key);
                props.setProperty(keyName,String.valueOf(value));
            }else if(jsonObject.get(key) instanceof Integer){
                value = (Integer) jsonObject.get(key);
                String keyName = appendString.concat(key);
                props.setProperty(keyName,String.valueOf(value));
            }else if(jsonObject.get(key) instanceof Boolean){
                value = (Boolean) jsonObject.get(key);
                String keyName = appendString.concat(key);
                props.setProperty(keyName,String.valueOf(value));
            }else{

                value = (String)jsonObject.get(key).toString();
                JSONParser parser = new JSONParser();
                try{
                    JSONObject newObj = (JSONObject) parser.parse(String.valueOf(value));
                    String newKey = String.valueOf(key).concat(".");
                    Properties newProps = jsonTOProperties(newObj,newKey);
                    props.putAll(newProps);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
        return props;
    }
}
