package com.siemens.bam.adm.studio.dbdriver.hbase;

/**
 * Class that defines constants used for HBase
 * 
 * @author nisha.menon@siemens.com
 * @author varsha.raveendran@siemens.com
 * 
 */
public class HBaseConstants {
	
	public static final String DEFAULT_HBASE_CONNECTION_RETRIES = "3";
	
	/*
	 * ASYNC_WAL - Write the Mutation to the WAL asynchronously 
	 * FSYNC_WAL - Write the Mutation to the WAL synchronously and force the entries to disk. 
	 * SKIP_WAL - Do not write the Mutation to the WAL 
	 * SYNC_WAL - Write the Mutation to the WAL synchronously. 
	 * USE_DEFAULT - If this is for tables durability, use HBase's global default value (SYNC_WAL).
	 */
	 
	public static final String DEFAULT_HBASE_DURABILITY = "SYNC_WAL";
	
	public static final String DEFAULT_HBASE_CLIENT_RETRIES = "10";
	
	

}
