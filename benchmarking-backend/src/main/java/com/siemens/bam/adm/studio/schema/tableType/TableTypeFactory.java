package com.siemens.bam.adm.studio.schema.tableType;

public class TableTypeFactory {

	public static AbstractTableType getTableTypeInstance(String typeOfTable)
			throws Exception {
		if (typeOfTable.equalsIgnoreCase("Wide")) {
			return new WideTableType();
		}
		else if (typeOfTable.equalsIgnoreCase("SuperColumn")) {
			return new SuperColumnTableType();
		}
		else if (typeOfTable.equalsIgnoreCase("Tall")) {
			return new TallTableType();
		}
		else {
			throw new Exception("No schema for " + typeOfTable
					+ " table is implemented in TableTypeFactory!");
		}
	}

}
