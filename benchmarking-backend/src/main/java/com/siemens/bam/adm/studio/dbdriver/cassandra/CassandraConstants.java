package com.siemens.bam.adm.studio.dbdriver.cassandra;

public class CassandraConstants {

	public static final String DEFAULT_CASSANDRA_CLUSTER_NAME = "Test Cluster";

	public static final String DEFAULT_CASSANDRA_BINARY_PORT = "9042";

/*
 * Cassandra Native driver : Valid values are: ANY, ONE, TWO, THREE, QUORUM, ALL, LOCAL_QUORUM, EACH_QUORUM, SERIAL and LOCAL_SERIAL
 * Cassandra Thrift driver : Valid values are: CL_ALL, CL_ANY, CL_EACH_QUORUM, CL_LOCAL_QUORUM, CL_ONE,CL_QUORUM and CL_THREE,CL_TWO           	
 */
	public static final String DEFAULT_CASSANDRA_WRITE_CONSISTENCY_LEVEL = "ONE";

	public static final String DEFAULT_CASSANDRA_READ_CONSISTENCY_LEVEL = "ONE";

	public static final String DEFAULT_CASSANDRA_THRIFT_PORT = "9160";

	public static final String DEFAULT_CASSANDRA_TARGET_VERSION = "2.1";

	public static final String DEFAULT_CASSANDRA_MAX_CONN_PER_HOST = "1";

}
