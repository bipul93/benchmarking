package com.siemens.bam.adm.studio.schema.validators;

/**
 * Abstract class to create a key validator
 * @author Nisha Menon
 */

public abstract class AbstractKeyValidator {

	protected String keyValidator;

	public String getName() {

		return this.keyValidator;
	}
}
