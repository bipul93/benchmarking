/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.metric.ReadMetric;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag.TagTrendAggregateQueryParams;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;
import com.siemens.bam.adm.studio.utils.DateRange;
import com.siemens.bam.adm.studio.utils.ScanQueryResult;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;

/**
 * Query Class implementation for TagTrend Aggregate
 * @author Nisha Menon
 *
 */
public class TagTrendAggregate extends AbstractQuery
{
	private static final Logger logger = LoggerFactory.getLogger(TagTrendAggregate.class);
	protected TagTrendAggregateQueryParams queryParams;
	public Map<String,ReadMetric> executionStats; 
	private Table table;

	/**
	 * @param model
	 */
	public TagTrendAggregate(DataModel model) {
		super(model);
		logger.debug("TagTrendAggregate Query instantiated");
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getQueryParams()
	 */
	@Override
	public TagTrendAggregateQueryParams getQueryParams() {
		return this.queryParams;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#initializeParameters(java.lang.String[])
	 */
	@Override
	public boolean initializeParameters(String[] parameterList) 
	{
		// Creating new queryParams object and executionStats Map for every new query.
		this.queryParams = new TagTrendAggregateQueryParams();
		this.executionStats  = new ConcurrentHashMap<String,ReadMetric>();
		
		table = model.getTables().get(0);
		String pk  = table.getRowKeys().get(0);
		List<TableColumn> cols = table.getColumns();
		
		TableColumn tc = new TableColumn();
		tc.setColName(pk);
		int index = cols.indexOf(tc);
		DataTypes datatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());
	
		List<String> wordList = Arrays.asList(parameterList);
		if(wordList.contains("rowkey") && wordList.contains("begin") && wordList.contains("end"))
		{
			try 
			{
				Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
				String param;
				int count = 0;
				
				while (count < parameterList.length) 
				{
					param = parameterList[count];
					count++;
					
					if(queryNameTypes.contains(param))
					{
						queryParams.setQueryName(param);
						logger.debug("Query Name set to '"+ queryParams.getQueryName() + "'");
					}				
					else if (param.equalsIgnoreCase("rowkey")) 
					{
						try
						{
							String rowkey = parameterList[count];
							count++;
							ArrayList<String> arrList = new ArrayList<String>();
							
							if (rowkey.startsWith("[") & rowkey.endsWith("]")) 
							{
								rowkey = rowkey.substring(1, rowkey.length() - 1);
								String[] rowKeyList;
								
								if(rowkey.compareToIgnoreCase("file")==0)
								{
									Path filePath = new File(ClientProperties.getRowkeyFilePath()).toPath();
									Charset charset = Charset.defaultCharset();        
									List<String> stringList = Files.readAllLines(filePath, charset);
									rowKeyList = stringList.toArray(new String[]{});
									
									/*
									 *  Validate the format of row keys in the provided file.
									 *  If not consistent, then change according to the schema
									 */
									if (!ValidateQueryParams.isRowKeyFileDataConsistentWithSchema(rowKeyList,model))
									{
										// Change the order as per the schema
										rowKeyList = ValidateQueryParams.getRowKeysUpdatedAsPerSchema(rowKeyList,model);
									}
									
									logger.debug("All row-keys validated and added in a format as described by schema");
									arrList.addAll(Arrays.asList(rowKeyList));
									queryParams.setRowKeys(arrList);
								}
								else
								{
									rowKeyList = rowkey.split(",");	
								
									for (int i = 0; i < rowKeyList.length; i++) 
									{
										String s = rowKeyList[i];
										if (ValidateQueryParams.validateDataType(s,datatype)) 
										{
											arrList.add(s);
										}
										else
										{
											logger.warn("Format issue for rowkey <"+s+"> .. PLease enter in correct format!");
											return false;
										}
									}
									logger.debug("All row-keys validated and added in "+datatype+" format");
									queryParams.setRowKeys(arrList);
								}
							} 			
							else
							{
								logger.error("Please specify rowkeys in square brackets seperated by a comma eg: [,,]!");
								return false;
							}
							logger.debug("Complete list of rowkeys are ->");
							for (int i = 0; i < queryParams.getRowKeys().size(); i++) 
							{
								logger.debug(queryParams.getRowKeys().get(i));
							}
						}
						catch(Exception e)
						{
							logger.error("Rowkey missing! PLease specify rowkeys in square brackets seperated by a comma eg: [,,]!");
							return false;
						}
					}
	
					else if (param.equalsIgnoreCase("begin")) 
					{
						try
						{
							String beginString = parameterList[count];
							count++;
							if (ValidateQueryParams.validateDataType(beginString, DataTypes.DATETIME)) 
							{
								queryParams.setBegin(beginString);
								logger.debug("begin set to: " + beginString);
							} 
							else 
							{
								logger.error("Error: Please enter begin date in yyyy-MM-dd'T'HH:mm:ss.SSS format.");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.error("Error: Begin Date missing. Please enter a valid begin date in <dd-mm-yyyy> <HH:mm:ss.SSS> format.");
							return false;
						}
					}
	
					else if (param.equalsIgnoreCase("end")) 
					{
						try
						{
							String endString = parameterList[count];
							count++;
							if (ValidateQueryParams.validateDataType(endString, DataTypes.DATETIME)) 
							{
								queryParams.setEnd(endString);
								logger.debug("end set to: " + endString);
							} 
							else 
							{
								logger.error("Error: Please enter end date in yyyy-MM-dd'T'HH:mm:ss.SSS format.");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.error("Error: End Date missing. Please enter a valid end date in <dd-mm-yyyy> <HH:mm:ss.SSS> format.");
							return false;
						}
					}
					else if (param.equalsIgnoreCase("aggregationtype")) 
					{
						try
						{
							String aggregationType = parameterList[count];
							count++;
							if (ValidateQueryParams.validateAggregationType(aggregationType)) 
							{
								queryParams.setAggregationType(aggregationType);
								logger.debug("Aggregation type set to: " + aggregationType);
							} 
							else 
							{
								logger.error("Error: Please select aggregation types from <MIN,MAX,AVG> only!");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.warn("Aggregation type missing! Assuming default aggregation type as \"MIN\" and proceeding!");
						}
					}
					else if (param.equalsIgnoreCase("timestep")) 
					{
						try
						{
							String timestepValue = parameterList[count];
							count++;
							int timeStep = ValidateQueryParams.validateTimeStep(Double.parseDouble(timestepValue));
							queryParams.setTimeStep(timeStep);
							logger.debug("Time step set to: " + timeStep);							
						}
						catch(NumberFormatException e)
						{
							logger.warn("Time step missing! Assuming default timestep as \"10\" and proceeding!");
						}
					}
					/**
					 * if random word is given as a command
					 */
					else 
					{
						logger.error("'" +param+ "' is an unknown command.");
						return false;
					}
				}
				if(ValidateQueryParams.validateBeginEndTime(queryParams.getBegin(),queryParams.getEnd()))
				{
					logger.debug("Query Parameters set successfully!");
					return true;
				}
				else
				{
					logger.error("Begin Date greater than end date!");
					return false;
				}
			} 
			catch (Exception e) 
			{
				logger.error("Unexpected exception has occured -> " + e.toString());
				logger.info("Type 'help' for more information");
				return false;
			}
		}
		else
		{
			logger.error("Arguments rowkey, begin and end are mandatory for \"tagtrendAggregate\" query! PLease provide them and re-try!");
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#findExecutionTime()
	 */
	@Override
	public long findExecutionTime() 
	{
		long maxThreadTime = 0;
		for (Map.Entry<String, ReadMetric> entry : executionStats.entrySet())
		{
			logger.info("The time for rowkey <"+entry.getKey()+"> is " + entry.getValue().getQueryResponseTime() + " ms");
		    if (entry.getValue().getQueryResponseTime() > maxThreadTime)
		    {	
		    	maxThreadTime = entry.getValue().getQueryResponseTime();
		    }
		}
		
		return maxThreadTime;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#queryExecute(java.lang.String, com.siemens.bam.adm.studio.dbdriver.IDatabaseDriver)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception
	{
		logger.debug("Executing query for TagTrendAggregate!");
		NavigableMap<Object, Object> colFinalList = new TreeMap<Object, Object>();
		NavigableMap<Object, Object> finalResults=new TreeMap<Object, Object>();
		Object[] result = new Object[2];
		
		try 
		{
			Iterable<DateRange> allRange = DateFormattingUtils.getDateRanges(queryParams.getBegin(),queryParams.getEnd());
			Iterator<DateRange> dateIterator = null;
			try 
			{
				dateIterator = allRange.iterator();
			} 
			catch (Exception e) 
			{
				logger.warn("DateRange is NULL");
			}
			int timestep = queryParams.getTimeStep();
			boolean usetimestep_flag=false;
			
			String rowkeySeperator = table.getRowKeySeparator();

			long startThreadTime = System.currentTimeMillis();
			logger.debug("Start time for Thread ID: " + Thread.currentThread().getId()+ " is: " + startThreadTime);

			
			// execute for each Date Range object
			while (dateIterator.hasNext()) 
			{
				DateRange dRange = dateIterator.next();
				
				// Append the row key as required. (UUID : Date)
				if (rowkeySeperator!=null && !rowKey.contains(table.getRowKeySeparator()))
				{
					rowKey = rowKey + table.getRowKeySeparator() + 
							DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(dRange.getStart())).substring(0, 10); 
				}
				NavigableMap<Object, Object> cols = 
						dbDriver.getValuesForClusteringKeyRangePerTimestep_WideCF(rowKey, 
								dRange.getStart(), 
								dRange.getEnd(), 
								queryParams.getLimit(), 
								model, timestep, usetimestep_flag);
				colFinalList.putAll(cols); 
			}
			
			finalResults=performAggregation(colFinalList);
			
			long endThreadTime = System.currentTimeMillis();
			logger.debug("End time for Thread ID: " + Thread.currentThread().getId() + " is: " + endThreadTime);
			
			int numberOfDatapointsPerRowkey = finalResults.size();				
			logger.debug("Number of Datapoints after aggregation for Row key <"+rowKey+"> = " + numberOfDatapointsPerRowkey);
			
			result[0] = rowKey;
			result[1] = finalResults;
			
			ReadMetric m = new ReadMetric();
			m.setEndTime(endThreadTime);
			m.setStartTime(startThreadTime);
			m.setQueryResponseTime(endThreadTime - startThreadTime);
			m.setNumberOfDatapoints(numberOfDatapointsPerRowkey);
			
			logger.info("Adding time for rowkey <"+rowKey+"> as "+(endThreadTime - startThreadTime));
			executionStats.put(rowKey, m);
			
		} 
		catch (Exception e) 
		{
			throw new Exception ("Exception in TagTrendAggregate::queryExecute()",e);
		}
		logger.debug("Total number of Datapoints for rowkey <"+ rowKey +"> ="+ colFinalList.size());
		
		return result;
	}

	public NavigableMap<Object,Object> performAggregation(NavigableMap<Object, Object> col) throws Exception
	{		
		try
		{
			DateTime beginDate = queryParams.getBegin();
			Period period = new Period((int)queryParams.getTimeStep() * 1000);
			DateTime endDate = beginDate.plus(period);

			ArrayList<Double> valueList=new ArrayList<Double>();
			NavigableMap<Object,Object> AggregatedResults = new TreeMap<Object,Object>();

			boolean currentDateEnteredtheSlot = false;
			double value = 0.0;
			
			for(Map.Entry<Object, Object>entry:col.entrySet())
			{			  			
				Date current_date = (Date) entry.getKey();
				if(current_date.compareTo(endDate.toDate()) <= 0)
				{
					valueList.add(Double.parseDouble(entry.getValue().toString()));
				}
		 
				if(current_date.compareTo(endDate.toDate()) >= 0)
				{
					if(valueList.size() != 0)
					{
						value = getAggregatedResult(queryParams.getAggregationType(),valueList);
						AggregatedResults.put(beginDate.toDate(),value);
					}
					else
					{
						AggregatedResults.put(beginDate.toDate(),Double.POSITIVE_INFINITY);
					}
					beginDate = endDate;
					endDate = endDate.plus(period);
					valueList.clear();
					
					currentDateEnteredtheSlot=false;
					while(currentDateEnteredtheSlot==false)
					{
						if(current_date.compareTo(endDate.toDate()) <= 0)
						{
							valueList.add(Double.parseDouble(entry.getValue().toString()));
							currentDateEnteredtheSlot=true;
						}
						else
						{
							AggregatedResults.put(beginDate.toDate(),Double.POSITIVE_INFINITY);
							beginDate = endDate;
							endDate = endDate.plus(period);
						}
					}
				}
			}
			// The last list left out is aggregated and added
			if(valueList.size() != 0)
			{
				value = getAggregatedResult(queryParams.getAggregationType(),valueList);
				AggregatedResults.put(beginDate.toDate(),value);
			}
			return AggregatedResults;
		}
		catch(Exception e)
		{
			throw new Exception("Exception in performAggregation()",e);
		}
	}

	public double getAggregatedResult(String aggregationType, ArrayList<Double> values) 
	{
		if(aggregationType.compareTo("MIN") == 0)
			return Collections.min(values);
		else if (aggregationType.compareTo("MAX") == 0)
			return Collections.max(values);
		else if(aggregationType.compareTo("AVG") == 0)
		{
			int size = values.size();
			int i = 0;
			double sum = 0.0;
			while(size != 0)
			{	
				sum = sum + values.get(i);
				i++;
				size--;
			}
			return sum/values.size();
		}
		else
			return 0;
	}
	
	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getResults(int, java.util.concurrent.CompletionService)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, NavigableMap<Object, Object>> getResults(int numberOfrowkeys, CompletionService completion_pool) throws Exception
	{
		Map<String, NavigableMap<Object, Object>> QueryResult = new ConcurrentHashMap<String, NavigableMap<Object, Object>>();
		for(int i = 0; i < numberOfrowkeys; i++)
		{
			try 
			{
				Object[] colList = (Object[]) completion_pool.take().get();
			
				String key = (String) colList[0];
				
				QueryResult.put(key, (NavigableMap<Object, Object>)colList[1]);
			}
			catch (InterruptedException ex) 
			{
				throw new Exception("InterruptedException thrown in getResults()",ex);
			} 
			catch (ExecutionException ex) 
			{
				throw new Exception("ExecutionException thrown in getResults()",ex);
			}
		}
		return QueryResult;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#printResults(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void printResults(T resultSet) 
	{
		Map<String, NavigableMap<Object, Object>> results = (Map<String, NavigableMap<Object, Object>>)resultSet;
		Iterable<String> keys = results.keySet();
		Iterator<String> keyString = keys.iterator();
		while (keyString.hasNext()) 
		{
			String k = keyString.next();
			logger.info(" ------------------ RowKey : " + k
					+ " ------------------- ");
			NavigableMap<Object, Object> c = results.get(k);
			ScanQueryResult.scanNavigableMapofObjects(c);
		}
	}

}
