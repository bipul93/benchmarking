package com.siemens.bam.adm.studio.schema.tableType;

import java.security.SecureRandom;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.write.datagenerator.DataGenerator;

/**
 * Interface that defines methods to write into different types of tables
 * 
 * It is extended by TallTableType, WideTableType and SuperColumnTableType
 * 
 * @author varsha.raveendran@siemens.com
 *
 */
public abstract class AbstractTableType {

	final static Logger logger = LoggerFactory.getLogger(AbstractTableType.class.getName());

	protected DataGenerator dataGenerator;

	// This map stores values that are generated before the start of insert
	// operations for columns(Key). During
	// the insert operations values will be picked from this set randomly
	// instead of generating data just before inserting into the datastore
	protected Map<String, List<Object>> uniqueSetOfValues = new ConcurrentHashMap<String, List<Object>>();

	/**
	 * See TallTableType, WideTableType and SuperColumnTableType
	 * 
	 * @param table
	 * @param clientProp
	 * @throws Exception
	 */
	public abstract void init(Table table, ClientProperties clientProp) throws Exception;

	/**
	 * Returns a value for the field specified by its name and data type 
	 * 
	 * If the user has specified that the values for this field must be from an
	 * unique set of values generated at the start of the execution then a value
	 * is randomly picked from that set else a new value is generated and
	 * returned.
	 * 
	 * Unique set can be defined by setting key "UniqueSet" in schema file to
	 * "true"
	 * 
	 * @param fieldName
	 * @param fieldType
	 * @return Object
	 * @throws Exception
	 */
	public Object getValueForField(String fieldName, Integer fieldType) throws Exception {

		if (uniqueSetOfValues.containsKey(fieldName)) {
			logger.debug("Get Primary Key data");
			if (dataGenerator.isReversedTimestamp() && 
					DataTypes.getDatatypeFromOrdinal(fieldType) == DataTypes.DATETIME) {
				return dataGenerator.generateValueForField(fieldName, fieldType);
			}
			int randomIndex = (new SecureRandom()).nextInt(uniqueSetOfValues.get(fieldName).size());
			return uniqueSetOfValues.get(fieldName).get(randomIndex);

		} else {
			return dataGenerator.generateValueForField(fieldName, fieldType);
		}
	}

	/**
	 * 
	 * Returns a value for the TableColumn 
	 * 
	 * If the user has specified a fixed set of values for this field in the schema file then a value
	 * is randomly picked from that set else a new value is generated and
	 * returned.
	 * 
	 * Fixed values can be defined by setting key "Values" in schema file to a list of values
	 * 
	 * @param entry
	 * @return Object
	 * @throws Exception
	 */
	public Object getValueForField(TableColumn entry) throws Exception {

		logger.debug("Getting value for field: " + entry.getColName());

		if (entry.getFixedValues().size() > 0) {
			logger.debug("Selecting one from a predefined value from a list of size: " + 
																entry.getFixedValues().size());
			
			return selectValueForField(entry);
		}
		return getValueForField(entry.getColName(), entry.getColDataType());
	}


	private Object selectValueForField(TableColumn entry) {
		Random rand = new Random();
		Object val = entry.getFixedValues().get(rand.nextInt(entry.getFixedValues().size()));
		logger.debug(val + " selected");
		return val;
	}

	public abstract <T> T buildColumnName(String colName, List<String> names, List<TableColumn> columns)
			throws Exception;

	public void setUniqueSetOfValues(Map<String, List<Object>> uniqueSetOfValues) {
		this.uniqueSetOfValues = uniqueSetOfValues;
	}

	public void setDataGenerator(DataGenerator dataGenerator) {
		this.dataGenerator = dataGenerator;
	}

}
