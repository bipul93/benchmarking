package com.siemens.bam.adm.studio.schema.validators;

import java.util.ArrayList;

import org.apache.log4j.Logger;

/**
 * Class to create customized key validator
 * @author Nisha Menon
 */

public class CustomizedKeyValidator extends AbstractKeyValidator 
{
	private static final Logger logger = Logger.getLogger(CustomizedKeyValidator.class);
	private String modelType;

	public CustomizedKeyValidator() 
	{
		this.keyValidator = null;
	}

	public CustomizedKeyValidator(String key, String type) 
	{
		this.keyValidator = key;
		this.modelType = type;
		this.validateComparator(key, this.modelType);
	}

	private void validateComparator(String cmp, String modelType) 
	{
		CustomizedDataTypes cus = new CustomizedDataTypes();
		ArrayList<String> validTypes = cus.retrieveValidDataTypes(modelType);
		if (validTypes.contains(cmp)) 
		{
			logger.debug("Key Validator : Valid Key");
		} 
		else 
		{
			logger.debug("Key Validator : Invalid Key");
		}
	}


}
