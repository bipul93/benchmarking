package com.siemens.bam.adm.studio.read.queryModel.queryParameters.generic;

import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;

/**
 * @author Nisha Menon
 * Query Parameters for Get Row Keys Query
 */
public class GetRowKeysQueryParams extends AbstractQueryParams
{
	private String paramString;
	private int paramValue;
	
	public GetRowKeysQueryParams()
	{
		super();
		this.paramString = "";
		this.paramValue = 10;
	}

	public int getParamValue() {
		return paramValue;
	}

	public void setParamValue(int value) {
		this.paramValue = value;
	}

	public String getParamString() {
		return paramString;
	}

	public void setParamString(String parameter) {
		this.paramString = parameter;
	}
}

