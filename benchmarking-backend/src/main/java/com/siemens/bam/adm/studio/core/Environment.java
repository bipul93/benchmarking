
package com.siemens.bam.adm.studio.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.DatabaseDriverFactory;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.dbmetrics.DataStoreMetricsFactory;
import com.siemens.bam.adm.studio.dbmetrics.AbstractDataStoreMetrics;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.JSONSchemaParser;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.write.datagenerator.DataGenerator;

/**
 * The Environment class builds/sets the client environment to perform the
 * read/write operation. This includes setting the data model, database driver
 * instance, data generator(for a WRITE operation) and query factory (for a READ
 * operation)
 * 
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class Environment {

	final static Logger logger = LoggerFactory.getLogger(Environment.class.getName());

	private ClientProperties clientProp;
	private DataModel dataModel;
	private AbstractQuery queryObject;
	private AbstractDatabaseDriver driver;
	private AbstractDataStoreMetrics dataStoreMetrics;

	private DataGenerator dataGenerator;

	private RegisterQueryService queryService;

	public RegisterQueryService getQueryFactory() {
		return queryService;
	}

	public static class EnvironmentBuilder {

		private ClientProperties clientProp;
		private DataModel dataModel;
		private AbstractDatabaseDriver driver;

		private AbstractDataStoreMetrics dataStoreMetrics = null;

		private DataGenerator dataGenerator = null;

		private RegisterQueryService queryService = null;

		/**
		 * Constructor sets the data model, database driver and database metrics
		 * driver
		 * 
		 * @param clientProp
		 * @throws Exception
		 */
		public EnvironmentBuilder(ClientProperties clientProp) throws Exception {
			this.clientProp = clientProp;
			this.clientProp.validateCommonParameters();

			JSONSchemaParser.loadSchema(clientProp.getSchemaPath());
			this.dataModel = JSONSchemaParser.getDataModel(clientProp);
			this.driver = DatabaseDriverFactory.getDataBaseDriver(clientProp);

			if (clientProp.isMetricsCollectionRequired()) {
				this.dataStoreMetrics = DataStoreMetricsFactory.getDBMetricInstance(clientProp);
			}
		}

		/**
		 * 
		 * Method is called to set the data generator for a WRITE operation
		 * 
		 * @return EnvironmentBuilder
		 * @throws Exception
		 */
		public EnvironmentBuilder setDataGenerator() throws Exception {

			this.dataGenerator = new DataGenerator();
			dataGenerator.init(dataModel, clientProp);
			return this;
		}


		/**
		 * Method is called to register available queries for a READ operation
		 * 
		 * @return EnvironmentBuilder
		 */
		public EnvironmentBuilder setQueryService() {
			logger.debug("Set the QueryService");
			this.queryService = new RegisterQueryService(dataModel);
			return this;
		}
		
		/**
		 * Builds the environment
		 * 
		 * @return Environment
		 */
		public Environment build() {
			return new Environment(this);
		}

	}

	private Environment(EnvironmentBuilder environmentBuilder) {
		clientProp = environmentBuilder.clientProp;
		dataModel = environmentBuilder.dataModel;
		dataGenerator = environmentBuilder.dataGenerator;
		driver = environmentBuilder.driver;
		dataStoreMetrics = environmentBuilder.dataStoreMetrics;
		queryService = environmentBuilder.queryService;
		logger.info("Environment is set for the client to start executing workloads.");
	}

	public AbstractDatabaseDriver getDriver() {
		return driver;
	}

	public ClientProperties getClientProperties() {
		return clientProp;
	}

	public DataModel getDataModel() {
		return dataModel;
	}

	public DataGenerator getDataGenerator() {
		return dataGenerator;
	}

	public void setQueryObject(AbstractQuery queryObject) {
		this.queryObject = queryObject;
	}

	public AbstractQuery getQueryObject() {
		return queryObject;
	}

	public AbstractDataStoreMetrics getDataStoreMetrics() {
		return dataStoreMetrics;
	}


}
