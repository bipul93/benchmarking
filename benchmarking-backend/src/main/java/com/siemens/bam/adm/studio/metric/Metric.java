package com.siemens.bam.adm.studio.metric;

/**
 * Model class for Metric 
 * 
 * @author varsha.raveendran@siemens.com
 *
 */
public class Metric {

	// Name of metric
	private String metricName;
	
	//Value of metric stored
	private String metricValue;

	public Metric(String name) {
		this.metricName = name;
		this.metricValue = new String();
	}

	/**
	 * Return the name of metric
	 * @return
	 */
	public String getMetricName() {
		return metricName;
	}

	/**
	 * Set the name of metric
	 * @param metricName
	 */
	public void setMetricName(String metricName) {
		this.metricName = metricName;
	}

	/**
	 * Return the value of metric
	 * @return
	 */
	public String getMetricValue() {
		return metricValue;
	}

	/**
	 * Set the value of metric
	 * @param metricValue
	 */
	public void setMetricValue(String metricValue) {
		this.metricValue = metricValue;
	}

	@Override
	public boolean equals(Object o) {

		if (o instanceof Metric) {

			Metric mo = (Metric) o;
			return mo.getMetricName().equals(metricName);
		}
		return false;
	}

	@Override
	public int hashCode() {

		return java.util.Objects.hashCode(metricName);
	}
}
