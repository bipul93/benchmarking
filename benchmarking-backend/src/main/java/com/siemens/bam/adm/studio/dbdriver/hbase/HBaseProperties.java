package com.siemens.bam.adm.studio.dbdriver.hbase;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.utils.ClientProperties;

public class HBaseProperties {

	private static Properties prop = new Properties();
	final static Logger logger = LoggerFactory.getLogger(HBaseProperties.class.getName());

	public HBaseProperties() {

		prop = ClientProperties.getProp();

		if (prop.getProperty("Hbase.Client.Retries.Number") == null) {
			prop.setProperty("Hbase.Client.Retries.Number", HBaseConstants.DEFAULT_HBASE_CONNECTION_RETRIES);
		} else if (prop.getProperty("Hbase.Client.Retries.Number").length() == 0) {
			logger.warn("Property \"Hbase.Client.Retries.Number\" not provided, using the default option of "
					+ HBaseConstants.DEFAULT_HBASE_CONNECTION_RETRIES);

		} else if (Integer.parseInt(prop.getProperty("Hbase.Client.Retries.Number")) < 0) {
			logger.warn("Property \"Hbase.Client.Retries.Number\" provided is negative, using the default option of"
					+ HBaseConstants.DEFAULT_HBASE_CONNECTION_RETRIES);
		}
		
		if (prop.getProperty("Hbase.Durability") == null || prop.getProperty("Hbase.Durability").length() == 0) {
			logger.warn("HBase Durability not set by user. Default : " + HBaseConstants.DEFAULT_HBASE_DURABILITY
					+ " will be used.");
			 prop.setProperty("Hbase.Durability" ,HBaseConstants.DEFAULT_HBASE_DURABILITY);
		}

	}

	/**
	 * Returns number of times the client tries to connect to HBase till a successful connection is made
	 * @return String
	 */
	public String getHBaseConnectionRetriesNumber() {

		return prop.getProperty("Hbase.Client.Retries.Number", HBaseConstants.DEFAULT_HBASE_CONNECTION_RETRIES);
	}

	/**
	 * Returns the durability set in HBase
	 * @return String
	 */
	public String getHBaseDurability() {

		
		return prop.getProperty("Hbase.Durability", HBaseConstants.DEFAULT_HBASE_DURABILITY);
	}



}
