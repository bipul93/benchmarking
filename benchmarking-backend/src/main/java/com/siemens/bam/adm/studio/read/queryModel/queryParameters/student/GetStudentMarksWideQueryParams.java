/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryParameters.student;

import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;

/**
 * Query Params Class for GetStudentMarksWideQuery 
 * @author Nisha Menon
 *
 */
public class GetStudentMarksWideQueryParams extends AbstractQueryParams {
	
	private String begin;
	private String end;
	private int limit;
	
	public GetStudentMarksWideQueryParams()
	{
		super();
		this.begin = null;
		this.begin = null;
		this.limit = 0;
	}
	
	public String getBegin() {
		return begin;
	}

	public void setBegin(String begin) {
		this.begin = begin;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

}
