package com.siemens.bam.adm.studio.read.queryModel.queryTypes;

import java.util.Map;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.generic.GetRowKeys;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.student.GetStudentMarksWide;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagDataSuperColumn;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagDataTall;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagTrend;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagTrendAggregate;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagTrendAggregateWithTimestep;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagTrendLatest;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag.TagTrendTall;
import com.siemens.bam.adm.studio.schema.DataModel;

/**
 * This class registers all queries available in the Studio.
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class RegisterQueryService {

	/**
	 * Static Map of all Registered Queries
	 */
	private static final Map<String, AbstractQuery> mQueries = new TreeMap<String, AbstractQuery>(String.CASE_INSENSITIVE_ORDER);
	private static final Logger logger = LoggerFactory.getLogger(RegisterQueryService.class);


	public RegisterQueryService(DataModel model) {
		logger.debug("In constructor of RegisterQueryServices");
		registerQueries(model);
	}

	public static <Q> void registerQueries(DataModel model) {
		logger.debug("Registering available queries...");
		mQueries.put("TagTrend" , new TagTrend(model));
		mQueries.put("TagTrendAggregate" , new TagTrendAggregate(model));
		mQueries.put("TagTrendAggregateWithTimestep" , new TagTrendAggregateWithTimestep(model));
		mQueries.put("TagTrendLatest", new TagTrendLatest(model));
		mQueries.put("TagTrendTall", new TagTrendTall(model));
		mQueries.put("TagDataTall", new TagDataTall(model));
		mQueries.put("TagDataSuperColumn", new TagDataSuperColumn<Q>(model));
		mQueries.put("GetRowKeys", new GetRowKeys(model));
		mQueries.put("GetStudentMarksWide", new GetStudentMarksWide(model));
		mQueries.put("dummyCommand", null);
		//TODO : Add a new entry to mQueries map (<New Query Name>,<New Query Object>) when a new Query is added
	}

	public static Map<String, AbstractQuery> getRegisteredQueries() {
		return mQueries;
	}

}
