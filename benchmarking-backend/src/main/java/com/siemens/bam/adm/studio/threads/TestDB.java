package com.siemens.bam.adm.studio.threads;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

public class TestDB<T> implements Callable<T>
{
	private static final Logger logger = Logger.getLogger(TestDB.class);
	private Environment env;
	private String rowkey;
	public T resultSet;
	public AbstractQuery query;
	public AbstractDatabaseDriver dbDriver;

	public TestDB(Environment env, int threadid)
	{
		this.env = env;
		this.rowkey = env.getQueryObject().getQueryParams().getRowKeys().size() == 0 ?
				null : env.getQueryObject().getQueryParams().getRowKeys().get(threadid);
		this.query = env.getQueryObject();
		this.dbDriver = env.getDriver();
	}

	@SuppressWarnings("unchecked")
	public T call() throws Exception
	{
    boolean dbStatus = false;
		logger.debug("Within the call method for the query.");
		logger.debug("Initializing the reads");
		/*if(dbDriver.initTestConnection(env) != null){
      //dbStatus = true;
      this.setResultSet((T) query.queryExecute(rowkey, dbDriver));
  		return (T)this.resultSet;
    }*/
		logger.debug("Query is of type: " + query.getClass().getSimpleName());
		//this.setResultSet((T) query.queryExecute(rowkey, dbDriver));
		//return (T)this.resultSet;
    return null;
	}


	public void setResultSet(T resultSet)
	{
		this.resultSet = resultSet;
	}

}
