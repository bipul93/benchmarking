package com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.metric.ReadMetric;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag.TagTrendQueryParams;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;
import com.siemens.bam.adm.studio.utils.DateRange;
import com.siemens.bam.adm.studio.utils.ScanQueryResult;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;

import com.siemens.bam.adm.studio.utils.GlobalData;

/**
 * Class for executing Tag Trend Query for a Wide Column Family
 * @author Nisha Menon
 */

public class TagTrend extends AbstractQuery
{
	private static final Logger logger = LoggerFactory.getLogger(TagTrend.class);
	private TagTrendQueryParams queryParams;
	public Map<String,ReadMetric> executionStats;
	private Table table;

	/**
	 * @param model
	 */
	public TagTrend(DataModel model)
	{
		super(model);
		logger.debug("TagTrend Query instantiated");
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#initializeParameters(java.lang.String[])
	 */

	@Override
	public TagTrendQueryParams getQueryParams()
	{
		return this.queryParams;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#initializeParameters(java.lang.String[])
	 */
	@Override
	public boolean initializeParameters(String[] paramsList)
	{
		// Creating new queryParams object and executionStats Map for every new query.
		this.queryParams = new TagTrendQueryParams();
		this.executionStats  = new ConcurrentHashMap<String,ReadMetric>();

		// Get the data type of primary key column
		table = model.getTables().get(0);
		String pk  = table.getRowKeys().get(0);
		List<TableColumn> cols = table.getColumns();

		TableColumn tc = new TableColumn();
		tc.setColName(pk);
		int index = cols.indexOf(tc);
		DataTypes pkDatatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> wordList = Arrays.asList(paramsList);
		if(wordList.contains("rowkey") && wordList.contains("begin") && wordList.contains("end"))
		{
			try
			{
				// Obtain the list of registered queries
				Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
				String param;
				int count = 0;

				// Parse all the parameters provided
				while (count < paramsList.length)
				{
					param = paramsList[count];
					count++;
					if(queryNameTypes.contains(param))
					{
						queryParams.setQueryName(param);
						logger.debug("Query Name set to '"+ queryParams.getQueryName() + "'");
					}
					else if (param.equalsIgnoreCase("rowkey"))
					{
						try
						{
							String rowkey = paramsList[count];
							count++;
							ArrayList<String> arrList = new ArrayList<String>();

							if (rowkey.startsWith("[") & rowkey.endsWith("]"))
							{
								rowkey = rowkey.substring(1, rowkey.length() - 1);
								String[] rowKeyList;

								if(rowkey.compareToIgnoreCase("file")==0)
								{
									Path filePath = new File(ClientProperties.getRowkeyFilePath()).toPath();
									Charset charset = Charset.defaultCharset();
									List<String> stringList = Files.readAllLines(filePath, charset);
									rowKeyList = stringList.toArray(new String[]{});

									/*
									 *  Validate the format of row keys in the provided file.
									 *  If not consistent, then change according to the schema
									 */
									if (!ValidateQueryParams.isRowKeyFileDataConsistentWithSchema(rowKeyList,model))
									{
										// Change the order as per the schema
										rowKeyList = ValidateQueryParams.getRowKeysUpdatedAsPerSchema(rowKeyList,model);

									}

									logger.debug("All row-keys validated and added in a format as described by schema");
									arrList.addAll(Arrays.asList(rowKeyList));
									queryParams.setRowKeys(arrList);
								}
								else
								{
									rowKeyList = rowkey.split(",");

									for (int i = 0; i < rowKeyList.length; i++)
									{
										String rk = rowKeyList[i];
										if (ValidateQueryParams.validateDataType(rk, pkDatatype))
										{
											arrList.add(rk);
										}
										else
										{
											logger.warn("Format issue for rowkey <"+rk+"> .. PLease enter in correct format!");
											return false;
										}
									}
									logger.debug("All row-keys validated and added in "+ pkDatatype +" format");
									queryParams.setRowKeys(arrList);
								}
							}

							else
							{
								logger.error("Please specify rowkeys in square brackets seperated by a comma eg: [,,]!");
								return false;
							}
							logger.debug("Complete list of rowkeys are ->");
							for (int i = 0; i < queryParams.getRowKeys().size(); i++)
							{
								logger.debug(queryParams.getRowKeys().get(i));
							}
						}
						catch(Exception e)
						{
							logger.error("Rowkey missing! PLease specify rowkeys in square brackets seperated by a comma eg: [,,]!");
							return false;
						}
					}

					else if (param.equalsIgnoreCase("begin"))
					{
						try
						{
							String beginString = paramsList[count];
							count++;

							if (ValidateQueryParams.validateDataType(beginString,DataTypes.DATETIME))
							{
								queryParams.setBegin(beginString);
								logger.debug("begin set to: " + beginString);
							}
							else
							{
								logger.error("Error: Please enter begin date in yyyy-MM-dd'T'HH:mm:ss.SSS format.");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.error("Error: Begin Date missing. Please enter a valid begin date in <dd-mm-yyyy> <HH:mm:ss.SSS> format.");
							return false;
						}
					}

					else if (param.equalsIgnoreCase("end"))
					{
						try
						{
							String endString = paramsList[count];
							count++;
							if (ValidateQueryParams.validateDataType(endString,DataTypes.DATETIME))
							{
								queryParams.setEnd(endString);
								logger.debug("end set to: " + endString);
							}
							else
							{
								logger.error("Error: Please enter end date in yyyy-MM-dd'T'HH:mm:ss.SSS format.");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.error("Error: End Date missing. Please enter a valid end date in <dd-mm-yyyy> <HH:mm:ss.SSS> format.");
							return false;
						}
					}
					else if (param.equalsIgnoreCase("limit"))
					{
						String limitString = paramsList[count];
						count++;
						try
						{
							int limit = Integer.parseInt(limitString);
							queryParams.setLimit(limit);
						}
						catch (NumberFormatException e)
						{
							logger.warn("Could not parse '" + limitString+ "', defaulting limit to Max Integer Value");
						}
						logger.debug("Limit set to: " + queryParams.getLimit());
					}
					/**
					 * if random word is given as a command
					 */
					else
					{
						logger.error("'" +param+ "' is an unknown command.");
						return false;
					}
				}

				logger.info("The parameters set for this query are:\n");
				logger.info("RowKey: \t" + queryParams.getRowKeys() + "\n");
				logger.info("Begin: \t" + queryParams.getBegin() + "\n");
				logger.info("End: \t" + queryParams.getEnd() + "\n");
				logger.info("Limit: \t" + queryParams.getLimit() + "\n");

				//Validate begin and end for time ranges
				if(ValidateQueryParams.validateBeginEndTime(queryParams.getBegin(), queryParams.getEnd()))
				{
					logger.debug("Query Parameters set successfully!");
					return true;
				}
				else
				{
					logger.error("Begin Date greater than end date!");
					return false;
				}
			}
			catch (Exception e)
			{
				logger.error("Unexpected exception has occured -> " + e.toString());
				logger.info("Type 'help' for more information");
				return false;
			}
		}
		else
		{
			logger.error("Arguments rowkey, begin and end are mandatory for \"tagtrend\" query! PLease provide them and re-try!");
			return false;
		}
	}

	@Override
	public long findExecutionTime()
	{
		long maxThreadTime=0;
		for (Map.Entry<String, ReadMetric> entry : executionStats.entrySet())
		{
			logger.info("The time for rowkey <"+entry.getKey()+"> is " + entry.getValue().getQueryResponseTime() + " ms");
		    if (entry.getValue().getQueryResponseTime() > maxThreadTime)
		    {
		    	maxThreadTime = entry.getValue().getQueryResponseTime();
		    }
		}

		return maxThreadTime;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#queryExecute(java.lang.String, com.siemens.bam.adm.studio.read.queryModel.queryTypes.IDataBaseDriver)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception
	{
		logger.debug("Executing query for TagTrend!");
		NavigableMap<Object, Object> colFinalList = new TreeMap<Object, Object>();
		Object[] result = new Object[2];

		int numberOfDatapointsPerRowkey = 0;

		try
		{
			// Pick all the dates in the provided Date range
			Iterable<DateRange> allRange = DateFormattingUtils.getDateRanges(queryParams.getBegin(), queryParams.getEnd());

			Iterator<DateRange> dateIterator = null;
			try
			{
				dateIterator = allRange.iterator();
			}
			catch (Exception e)
			{
				logger.warn("DateRange is NULL");
			}

			String rowkeySeperator = table.getRowKeySeparator();

			long startThreadTime = System.currentTimeMillis();

			// Run the query for each date picked
			while (dateIterator.hasNext())
			{
				DateRange d = dateIterator.next();

				// Append the row key as required. (UUID : Date)
				if (rowkeySeperator!=null && !rowKey.contains(table.getRowKeySeparator()))
				{
					rowKey = rowKey + table.getRowKeySeparator() + DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(d.getStart())).substring(0, 10);
				}

				NavigableMap<Object, Object> cols1 = dbDriver.getValuesForClusteringKeyRange_WideCF(rowKey, d.getStart(), d.getEnd(), queryParams.getLimit(), model);

				colFinalList.putAll(cols1);
			}

			long endThreadTime = System.currentTimeMillis();
			logger.debug("The total time for " + Thread.currentThread().getId() + " is: " + (endThreadTime-startThreadTime) );

			numberOfDatapointsPerRowkey = colFinalList.size();
			logger.debug("Number of Datapoints retrieved for Row key <"+rowKey+"> = " + numberOfDatapointsPerRowkey);

			result[0] = rowKey;
			result[1] = colFinalList;

			ReadMetric m = new ReadMetric();
			m.setEndTime(endThreadTime);
			m.setStartTime(startThreadTime);
			m.setQueryResponseTime(endThreadTime-startThreadTime);
			m.setNumberOfDatapoints(numberOfDatapointsPerRowkey);

			logger.info("Adding time for rowkey <"+rowKey+"> as "+(endThreadTime-startThreadTime));
			executionStats.put(rowKey, m);
		}
		catch (Exception e)
		{
			logger.error("Error in queryExecute()"+e.getMessage());
			throw new Exception ("Exception in TagTrend::queryExecute()",e);
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getResults(java.lang.String[], java.util.concurrent.CompletionService)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, NavigableMap<Object, Object>> getResults(int numberOfrowkeys, CompletionService pool) throws Exception
	{
		Map<String, NavigableMap<Object, Object>> QueryResult = new ConcurrentHashMap<String, NavigableMap<Object, Object>>();

		// Get the result set for each row key and add it to the resultMap
		for(int i =0 ;i<numberOfrowkeys;i++)
		{
			try
			{
				Object[] colList = (Object[]) pool.take().get();

				String key = (String) colList[0];

				QueryResult.put(key, (NavigableMap<Object, Object>)colList[1]);
			}
			catch (InterruptedException ex)
			{
				throw new Exception("InterruptedException thrown in getResults()",ex);
			}
			catch (ExecutionException ex)
			{
				throw new Exception("ExecutionException thrown in getResults()",ex);
			}
		}

		return QueryResult;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#printResults(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void printResults(T tagTrendResult)
	{
		Map<String, NavigableMap<Object, Object>> results = (Map<String, NavigableMap<Object, Object>>)tagTrendResult;
		Iterable<String> keys = results.keySet();
		Iterator<String> keyString = keys.iterator();
		GlobalData.queryResult = "";
		// Print results for each rowkey
		while (keyString.hasNext())
		{
			String k = keyString.next();
			GlobalData.queryResult = GlobalData.queryResult + "|" + k;
			logger.info(" ------------------ RowKey : " + k
					+ " ------------------- ");
			NavigableMap<Object, Object> c = results.get(k);
			ScanQueryResult.scanNavigableMapofObjects(c);
		}
	}
}
