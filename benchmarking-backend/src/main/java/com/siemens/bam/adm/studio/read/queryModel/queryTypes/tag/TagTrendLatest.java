/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.joda.time.DateTime;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.metric.ReadMetric;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag.TagTrendLatestQueryParams;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;

import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.DateFormattingUtils;
import com.siemens.bam.adm.studio.utils.DateRange;
import com.siemens.bam.adm.studio.utils.ScanQueryResult;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;

/**
 * Query Class for TagTrendLatest Query
 * @author Nisha Menon
 *
 */
public class TagTrendLatest extends AbstractQuery
{
	private static final Logger logger = LoggerFactory.getLogger(TagTrendLatest.class);
	private TagTrendLatestQueryParams queryParams;
	public Map<String,ReadMetric> executionStats;
	private Table table;

	/**
	 * @param model
	 */
	public TagTrendLatest(DataModel model) {
		super(model);
		logger.debug("TagTrendLatest Query instantiated");
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getQueryParams()
	 */
	@Override
	public TagTrendLatestQueryParams getQueryParams() {
		return this.queryParams;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#initializeParameters(java.lang.String[])
	 */
	@Override
	public boolean initializeParameters(String[] parameterList) 
	{
		// Creating new queryParams object and executionStats Map for every new query.
		this.queryParams = new TagTrendLatestQueryParams();
		this.executionStats  = new ConcurrentHashMap<String,ReadMetric>();
		
		table = model.getTables().get(0);
		String pk  = table.getRowKeys().get(0);
		List<TableColumn> cols = table.getColumns();
		
		TableColumn tc = new TableColumn();
		tc.setColName(pk);
		int index = cols.indexOf(tc);
		DataTypes datatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());
		
		List<String> wordList = Arrays.asList(parameterList);
		if(wordList.contains("rowkey") && wordList.contains("time"))
		{
			try 
			{
				Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
				String param;
				int count = 0;
				
				while (count < parameterList.length) 
				{
					param = parameterList[count];
					count++;
					
					if(queryNameTypes.contains(param))
					{
						queryParams.setQueryName(param);
						logger.debug("Query Name set to '"+ queryParams.getQueryName() + "'");
					}				
					
					else if (param.equalsIgnoreCase("rowkey")) 
					{
						try
						{
							String rowkey = parameterList[count];
							count++;
							ArrayList<String> arrList = new ArrayList<String>();
							
							if (rowkey.startsWith("[") & rowkey.endsWith("]")) 
							{
								rowkey = rowkey.substring(1, rowkey.length() - 1);
								String[] rowKeyList;
								
								if(rowkey.compareToIgnoreCase("file")==0)
								{
									Path filePath = new File(ClientProperties.getRowkeyFilePath()).toPath();
									Charset charset = Charset.defaultCharset();        
									List<String> stringList = Files.readAllLines(filePath, charset);
									rowKeyList = stringList.toArray(new String[]{});
									
									/*
									 *  Validate the format of row keys in the provided file.
									 *  If not consistent, then change according to the schema
									 */
									if (!ValidateQueryParams.isRowKeyFileDataConsistentWithSchema(rowKeyList,model))
									{
										// Change the order as per the schema
										rowKeyList = ValidateQueryParams.getRowKeysUpdatedAsPerSchema(rowKeyList,model);
										
									}
									
									logger.debug("All row-keys validated and added in a format as described by schema");
									arrList.addAll(Arrays.asList(rowKeyList));
									queryParams.setRowKeys(arrList);
								}
								else
								{
									rowKeyList = rowkey.split(",");	
								
									for (int i = 0; i < rowKeyList.length; i++) 
									{
										String s = rowKeyList[i];
										if (ValidateQueryParams.validateDataType(s,datatype)) 
										{
											arrList.add(s);
										}
										else
										{
											logger.warn("Format issue for rowkey <"+s+"> .. PLease enter in correct format!");
											return false;
										}
									}
									logger.debug("All row-keys validated and added in "+ datatype +" format");
									queryParams.setRowKeys(arrList);
								}
							} 			
							else
							{
								logger.error("Please specify rowkeys in square brackets seperated by a comma eg: [,,]!");
								return false;
							}
							logger.debug("Complete list of rowkeys are ->");
							for (int i = 0; i < queryParams.getRowKeys().size(); i++) 
							{
								logger.debug(queryParams.getRowKeys().get(i));
							}
						}
						catch(Exception e)
						{
							logger.error("Rowkey missing! PLease specify rowkeys in square brackets seperated by a comma eg: [,,]!");
							return false;
						}
					}
	
					else if (param.equalsIgnoreCase("time")) 
					{
						try
						{
							String time = parameterList[count];
							count++;
							queryParams.setTime(time);
							logger.debug("Time is set to : " + time + " seconds" );
						}
						catch(Exception e)
						{
							logger.error("Error: Time missing. Please enter a valid time value in seconds.");
							return false;
						}
					}	
					
					else if (param.equalsIgnoreCase("limit")) 
					{
						String limitString = parameterList[count];
						count++;
						try 
						{
							int limit = Integer.parseInt(limitString);
							queryParams.setLimit(limit);
						}
						catch (NumberFormatException e) 
						{
							logger.warn("Could not parse '" + limitString + "', defaulting limit to Max Integer Value");
						}
						logger.debug("Limit set to: " + queryParams.getLimit());
					}
					/**
					 * if random word is given as a command
					 */
					else 
					{
						logger.error("'" + param + "' is an unknown command.");
						return false;
					}
				}
				logger.debug("Query Parameters set successfully!");
				return true;			
			} 
			catch (Exception e) 
			{
				logger.error("Unexpected exception has occured -> " + e.toString());
				logger.info("Type 'help' for more information");
				return false;
			}
		}
		else
		{
			logger.error("Argument rowkey and time is mandatory for \"tagtrendlatest\" query! PLease provide it and re-try!");
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#findExecutionTime()
	 */
	@Override
	public long findExecutionTime() 
	{
		long maxThreadTime = 0;
		for (Map.Entry<String, ReadMetric> entry : executionStats.entrySet())
		{
			logger.info("The time for rowkey <"+entry.getKey()+"> is " + entry.getValue().getQueryResponseTime() + " ms");
		    if (entry.getValue().getQueryResponseTime() > maxThreadTime)
		    {	
		    	maxThreadTime = entry.getValue().getQueryResponseTime();
		    }
		}
		
		return maxThreadTime;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#queryExecute(java.lang.String, com.siemens.bam.adm.studio.dbdriver.IDatabaseDriver)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public  Object[]  queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception
	{
		logger.debug("Executing query for TagTrendLatest!");
		NavigableMap<Object, Object> colFinalList = new TreeMap<Object, Object>();
		Object[] result = new Object[2];
		int numberOfDatapointsPerRowkey = 0;
		String rowkeySeperator = table.getRowKeySeparator();
		DateTime endValue, beginValue;

		long startThreadTime = System.currentTimeMillis();
		logger.debug("Start time for Thread ID: " + Thread.currentThread().getId()+ " is: " + startThreadTime);

		// Get the starting and ending time for a provided rowKey and provided time range
		DateTime currentDate = new DateTime();
		String computedRowKey = "";
		Object value;
		while(true)
		{
			// Append the row key as required. (UUID : Date)
			if (rowkeySeperator != null && !rowKey.contains(table.getRowKeySeparator())) 
			{
				computedRowKey = rowKey + table.getRowKeySeparator() + DateFormattingUtils.convertJODATimetoFullDateStringFormat(currentDate).substring(0, 10); 
			}
			else
			{
				computedRowKey = rowKey;
			}
			value = dbDriver.getLastColumnValue_WideCF(computedRowKey,model);
			if (value!=null)
			{
				endValue = new DateTime ((Date)value);
			  	beginValue = endValue.minus(new Period(queryParams.getTime() * 1000));
			  	break;
			}		
			computedRowKey = "";
			currentDate = currentDate.minusDays(1);
		}
			
		try 
		{
			Iterable<DateRange> allRange = DateFormattingUtils.getDateRanges(beginValue,endValue);
		
			Iterator<DateRange> dateIterator = null;
			try 
			{
				dateIterator = allRange.iterator();
			} 
			catch (Exception e) 
			{
				logger.error("DateRange is NULL");
			}
						
			// execute for each Date Range object
			while (dateIterator.hasNext()) 
			{
				DateRange d = dateIterator.next();
				
				// Append the row key as required. (UUID : Date)
				if (rowkeySeperator!=null && !rowKey.contains(table.getRowKeySeparator()))
				{
					rowKey = rowKey + table.getRowKeySeparator() + DateFormattingUtils.convertJODATimetoFullDateStringFormat(new DateTime(d.getStart())).substring(0, 10); 
				}
				
				NavigableMap<Object, Object> cols1 = dbDriver.getValuesForClusteringKeyRange_WideCF(rowKey, d.getStart(), d.getEnd(), queryParams.getLimit(), model);
				
				colFinalList.putAll(cols1); 
			}
			
			long endThreadTime = System.currentTimeMillis();
			logger.debug("End time for Thread ID: " + Thread.currentThread().getId() + " is: " + endThreadTime);

			numberOfDatapointsPerRowkey = colFinalList.size();				
			logger.debug("Number of Datapoints retrieved for Row key <"+rowKey+"> = " + numberOfDatapointsPerRowkey);
			
			result[0] = rowKey;
			result[1] = colFinalList;
			
			ReadMetric m = new ReadMetric();
			m.setEndTime(endThreadTime);
			m.setStartTime(startThreadTime);
			m.setQueryResponseTime(endThreadTime - startThreadTime);
			m.setNumberOfDatapoints(numberOfDatapointsPerRowkey);
			
			logger.info("Adding time for rowkey <"+rowKey+"> as "+(endThreadTime - startThreadTime));
			executionStats.put(rowKey, m);
		} 
		catch (Exception e) 
		{
			logger.error(e.toString());
			throw new Exception ("Exception in TagTrendLatest::queryExecute()",e);
		}
		logger.debug("Total number of Datapoints for rowkey <"+ rowKey +"> ="+ colFinalList.size());
		return result;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getResults(int, java.util.concurrent.CompletionService)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, NavigableMap<Object, Object>> getResults(int numberOfrowkeys, CompletionService completion_pool) throws Exception 
	{
		Map<String, NavigableMap<Object, Object>> QueryResult = new ConcurrentHashMap<String, NavigableMap<Object, Object>>();
		
		for(int i = 0; i < numberOfrowkeys; i++)
		{
			try 
			{
				Object[] colList = (Object[]) completion_pool.take().get();
			
				String key = (String) colList[0];
				
				QueryResult.put(key, (NavigableMap<Object, Object>)colList[1]);
			}
			catch (InterruptedException ex) 
			{
				throw new Exception("InterruptedException thrown in getResults()",ex);
			} 
			catch (ExecutionException ex) 
			{
				throw new Exception("ExecutionException thrown in getResults()",ex);
			}
		}
		return QueryResult;

	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#printResults(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void printResults(T tagTrendLatestResult) 
	{
		Map<String, NavigableMap<Object, Object>> results = (Map<String, NavigableMap<Object, Object>>)tagTrendLatestResult;
		Iterable<String> keys = results.keySet();
		Iterator<String> keyString = keys.iterator();
		while (keyString.hasNext()) 
		{
			String k = keyString.next();
			logger.info(" ------------------ RowKey : " + k
					+ " ------------------- ");
			NavigableMap<Object, Object> c = results.get(k);
			ScanQueryResult.scanNavigableMapofObjects(c);
		}
	}

}
