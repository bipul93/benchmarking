package com.siemens.bam.adm.studio.threads;

import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.util.concurrent.RateLimiter;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.metric.ReportClientMetrics;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.google.common.base.Preconditions;

/**
 * The Writer class implements Callable interface *
 *
 * @author varsha.raveendran@siemens.com
 * @param <T>
 *
 */
public class Writer<T> implements Callable<T> {

	private final static Logger logger = LoggerFactory.getLogger(Writer.class.getName());
	private Environment env;
	private AbstractDatabaseDriver dbDriver;
	private int threadid;
	private static volatile int threadcount;
	private int insertsperthread;
	private static volatile int remaininginserts;
	private static volatile ClientProperties clientProp;
	private static volatile List<TableColumn> columns;

	private static volatile RateLimiter rateLimiter = null;

	private static volatile double targetthroughput = 0.0;
	private static volatile int throttlingDurationInMins = 0;
	private static final long start = System.currentTimeMillis();
	private static volatile boolean isBatch = false;
	private int batchSize = 0;
	private Integer numberofInsertsCompleted;
	private static volatile Table table;
	private static volatile boolean setFlag = false;
	private static ReportClientMetrics reportClientMetrics;

	/**
	 * Required parameters to insert records into a datastore is set in the
	 * constructor
	 *
	 * Static members are set in a synchronized block by only one thread
	 *
	 * @param Environment
	 * @param int
	 * @throws Exception
	 */
	public Writer(Environment env, int threadid) throws Exception {

		this.threadid = threadid;
		this.env = env;
		Writer.clientProp = env.getClientProperties();
		int totalNumberOfRecords = clientProp.getNumberOfRecords();
		threadcount = clientProp.getNumberOfThreads();
		insertsperthread = (totalNumberOfRecords / threadcount);
		remaininginserts = totalNumberOfRecords % threadcount;
		isBatch = false;
		batchSize = 0;
		if (clientProp.isBatchInserts().equalsIgnoreCase("true")) {
			isBatch = true;

			batchSize = clientProp.getBatchSize();

			if(!clientProp.isInsertDurationSet())
				batchSize = (clientProp.getBatchSize() >= insertsperthread) ? insertsperthread : clientProp.getBatchSize();

			if(batchSize <= 0){
				throw new Exception("Batch Size is zero. Please modify in conf file");
			}
			logger.debug("Batch Insertion will be done with batchSize= " + batchSize);

		}

		dbDriver = env.getDriver();
		Preconditions.checkNotNull(dbDriver, "DB instance is null in WriterThread()");

		if (threadid == threadcount - 1) {
			insertsperthread += remaininginserts;
		}

		if (!setFlag) {
			synchronized (Writer.class) {
				if (!setFlag) {
					logger.debug("Set parameters for Writer Thread");
					setParameters(env);
					setFlag = true;
				}
			}
		}
		logger.debug("Thread [" + threadid + "] will insert [" + insertsperthread + "] records");

	}

	/**
	 *
	 * Called by only one thread to set the static members of the WriterThread
	 * class
	 *
	 * @param env
	 * @throws Exception
	 */
	private void setParameters(Environment env) throws Exception {
		String tableName = null;
		tableName = clientProp.getTableName();
		table = env.getDataModel().getTable(tableName);
		columns = env.getDataModel().getTable(tableName).getColumns();
		boolean isThrottling = clientProp.isThrottlingRequired();

		// Setting duration of inserts
		if (clientProp.isInsertDurationSet()) {
			Writer.throttlingDurationInMins = Integer.parseInt(clientProp.getinsertDurationInMins());
			logger.debug("Throttle inserts for [" + throttlingDurationInMins + "] minutes");
		}

		// Throttling the workload by 'throughput'
		if (isThrottling) {
			Writer.targetthroughput = Double.parseDouble(clientProp.getTargetThroughput());
			//logger.info("target" + targetthroughput);

			if(clientProp.isBatchInserts().equalsIgnoreCase("true")){
				// Throttling for batch inserts would be done at targetthroughput/batchSize
				targetthroughput = targetthroughput/batchSize;
			}

			rateLimiter = RateLimiter.create(targetthroughput);
		}
		Writer.reportClientMetrics = new ReportClientMetrics(clientProp);
	}

	@SuppressWarnings("unchecked")
	@Override
	public T call() throws Exception {
		logger.debug("In call method of WriterThread");
		AbstractDatabaseDriver newDBInstance = null;

		Long start = System.currentTimeMillis();

		try {
			newDBInstance = dbDriver.initWrites(env);
		} catch (DBException e) {
			logger.error("Exception thrown by " + Thread.currentThread().getName() + "  " + e.getMessage());
			System.exit(1);
		}

		if (isBatch) {
			numberofInsertsCompleted = (targetthroughput > 0) ? doBatchInsertWithThrottling(newDBInstance)
					: doBatchInsert(newDBInstance);

		} else {
			numberofInsertsCompleted = (targetthroughput > 0) ? doSingleInsertsWithThrottling(newDBInstance)
					: doSingleInserts(newDBInstance);
		}

		try {
			logger.debug("Close DB connection called by " + Thread.currentThread().getName());
			newDBInstance.close();
		} catch (Exception e) {
			logger.error("Exception thrown by " + Thread.currentThread().getName() + "  " + e.getMessage());
			System.exit(1);
		}
		Long stop = System.currentTimeMillis();

		logger.debug(Thread.currentThread().getName() + " time: " + (stop-start)/1000);

		return (T) numberofInsertsCompleted;
	}

	private Integer doSingleInsertsWithThrottling(AbstractDatabaseDriver newDBInstance) throws Exception {
		int numberofInserts = 0;
		while (throttlingDurationInMins > 0 || numberofInserts < insertsperthread) {
			logger.debug("Thread " + Thread.currentThread().getId() + " Inserts completed " + numberofInserts);

			// A rate limiter distributes permits at a configurable rate -
			// http://docs.guava-libraries.googlecode.com/git/javadoc/com/google/common/util/concurrent/RateLimiter.html

			if (isDurationOfInsertReached()) {
				logger.debug("The time is [" + ((System.currentTimeMillis() - start))
						+ "] - Stop thread as the time for throttling " + "has reached.");
				break;
			}

			reportClientMetrics.startTimer();

			// Call method of driver to insert records individually
			try {
				rateLimiter.acquire();
				numberofInserts += newDBInstance.insertRecordsIndividually(columns, table) ? 1 : 0;

			} catch (DBException e) {
				logger.error("Error while inserting statements individually " + e);
				System.exit(0);
			}
			reportClientMetrics.stopTimer();
		}
		return numberofInserts;
	}

	private int doBatchInsertWithThrottling(AbstractDatabaseDriver newDBInstance) throws Exception {
		int batchInserts = 0;
		Boolean isUniqueKey = Boolean.valueOf(clientProp.isUniqueKeyPerBatch());
		while (throttlingDurationInMins > 0 || batchInserts < insertsperthread) {

			if (isDurationOfInsertReached()) {
				logger.debug("The time is [" + ((System.currentTimeMillis() - start))
						+ "] - Stop thread as the time for throttling " + "has reached.");
				break;
			}

			// This is done to handle the last set of records to be inserted by
			// the thread. If the batchSize is more than the actual number to be
			// inserted then batchSize is set to the remaining number of records
			if (!clientProp.isInsertDurationSet() && (batchInserts + batchSize) >= insertsperthread) {
				batchSize = insertsperthread - batchInserts;
			}
			logger.debug("Thread " + Thread.currentThread().getId() + " batchSize " + batchSize);
			rateLimiter.acquire();
			// Call method of driver to insert records into a batch of size
			// defined by batchSize
			try {
				newDBInstance.addStatementsToBatch(batchSize, columns, table, isUniqueKey);
			} catch (Exception e) {
				logger.error("WriterThread::doBatchInsert - Error while adding statements to batch" + e);
				System.exit(0);
			}

			reportClientMetrics.startTimer();

			try {
				batchInserts += (newDBInstance.insertbatch() ? batchSize : 0);
				logger.debug("Thread " + Thread.currentThread().getId() + " Inserts completed " + batchInserts);

			} catch (DBException e1) {
				logger.error("Error while inserting statements to batch" + e1);
				System.exit(0);
			}
			reportClientMetrics.stopTimer();
		}
		logger.debug("Thread with ID '{}'  inserted {} records", Thread.currentThread().getName(), batchInserts);
		return batchInserts;
	}

	/**
	 * Method to insert records individually
	 *
	 * @param newDBInstance
	 * @return
	 * @throws Exception
	 */
	private Integer doSingleInserts(AbstractDatabaseDriver newDBInstance) throws Exception {
		int numberofInserts = 0;
		while (throttlingDurationInMins > 0 || numberofInserts < insertsperthread) {
			logger.debug("Thread " + Thread.currentThread().getId() + " Inserts completed " + numberofInserts);

			if (isDurationOfInsertReached()) {
				logger.debug("The time is [" + ((System.currentTimeMillis() - start))
						+ "] - Stop thread as the time for throttling " + "has reached.");
				break;
			}

			reportClientMetrics.startTimer();

			// Call method of driver to insert records individually
			try {
				numberofInserts += newDBInstance.insertRecordsIndividually(columns, table) ? 1 : 0;

			} catch (DBException e) {
				logger.error("Error while inserting statements individually " + e);
				System.exit(0);
			}
			reportClientMetrics.stopTimer();
		}
		return numberofInserts;
	}

	/**
	 * Method to insert records in batches
	 *
	 * @param newDBInstance
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	private int doBatchInsert(AbstractDatabaseDriver newDBInstance) throws NumberFormatException, Exception {
		int batchInserts = 0;

		while (throttlingDurationInMins > 0 || batchInserts < insertsperthread) {

			Boolean isUniqueKey = Boolean.valueOf(clientProp.isUniqueKeyPerBatch());

			if (isDurationOfInsertReached()) {

				logger.debug("The time is [" + ((System.currentTimeMillis() - start))
						+ "] - Stop thread as the time for throttling " + "has reached.");
				break;
			}

			// This is done to handle the last set of records to be inserted by
			// the thread. If the batchSize is more than the actual number to be
			// inserted then batchSize is set to the remaining number of records
			if (insertsperthread > 1 && (batchInserts + batchSize) >= insertsperthread) {

				batchSize = insertsperthread - batchInserts;
			}
			logger.debug("Thread " + Thread.currentThread().getId() + " batchSize " + batchSize);

			// Call method of driver to insert records into a batch of size
			// defined by batchSize
			try {
				newDBInstance.addStatementsToBatch(batchSize, columns, table, isUniqueKey);
			} catch (Exception e) {
				logger.error("WriterThread::doBatchInsert - Error while adding statements to batch" + e);
				System.exit(0);
			}

			reportClientMetrics.startTimer();

			try {
				//	numberofInserts += newDBInstance.insertRecordsIndividually(columns, table) ? 1 : 0;
				batchInserts += newDBInstance.insertbatch() ? batchSize : 0;
				logger.debug("Thread " + Thread.currentThread().getId() + " Inserts completed " + batchInserts);

			} catch (DBException e1) {
				logger.error("Error while inserting statements to batch" + e1);
				System.exit(0);
			}
			reportClientMetrics.stopTimer();
		}
		logger.debug("Thread with ID '{}'  inserted {} records", Thread.currentThread().getName(), batchInserts);
		return batchInserts;
	}

	/**
	 * Checks if the duration specified by the user for which records are
	 * inserted into the data store is complete
	 *
	 * @return
	 * @throws Exception
	 * @throws NumberFormatException
	 */
	private boolean isDurationOfInsertReached() throws NumberFormatException, Exception {
		logger.debug("Duration: " + (System.currentTimeMillis() - start) + " throttlingDuration: " + (throttlingDurationInMins * 60 * 1000));
		return (clientProp.isInsertDurationSet()
				&& ((System.currentTimeMillis() - start) > (throttlingDurationInMins * 60 * 1000)));
	}

}
