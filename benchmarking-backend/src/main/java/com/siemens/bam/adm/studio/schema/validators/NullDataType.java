package com.siemens.bam.adm.studio.schema.validators;

import java.util.List;

/**
 * Class for null data types.
 * @author Nisha Menon
 */


public class NullDataType extends AbstractDataTypes {

	@Override
	protected List<String> retrieveValidDataTypes(String type) 
	{
		return null;
	}

}