package com.siemens.bam.adm.studio.dbmetrics;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbmetrics.AbstractDataStoreMetrics;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.utils.ClientProperties;

public class DataStoreMetricsFactory {
	final static Logger logger = LoggerFactory.getLogger(DataStoreMetricsFactory.class.getName());

	public static AbstractDataStoreMetrics getDBMetricInstance(ClientProperties clientProp) throws Exception {

		String dbType = clientProp.getDataStoreName();

		if (dbType.contains("Cassandra")) {
			logger.debug("Instance of CassandraNativeDBDriver returned.");
			return new CassandraMetrics();
		} else if (dbType.contains("HBase")) {
			return new HBaseMetrics();
		} else {
			logger.error("No such DB implemented!");

			throw new DBException("No such DB implemented!");

		}
	}
}
