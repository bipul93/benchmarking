package com.siemens.bam.adm.studio.utils;

import java.io.*;
import java.util.Properties;
import java.util.Iterator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.json.JSONArray;
import net.sf.json.JSONSerializer;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

public class JSONtoPropsParser {

  JSONObject object = null;
  Properties props;

  public void setJSONObject(JSONObject obj){
    this.object = obj;
  }

  public void makePropsFile() throws Exception {
    //
    props = new Properties();
    convertToProps();
    savePropsFile();
  }

  public void convertToProps(){
    Iterator<?> keys = object.keys();

    while( keys.hasNext() ){
      String key = (String)keys.next();
      Object keyValue = object.get(key);

      addKeyValuePair(key,keyValue);
    }
    System.out.println(props);
  }


  public void addKeyValuePair(String key, Object value){
    if(value instanceof String || value instanceof Integer || value instanceof Boolean){
        // add long or other possible non-json types
        //props.setProperty(key,keyValue);
        String propValue = value.toString();
        props.setProperty(key,propValue);
        //System.out.println(" " + key + " => " + propValue);
      } else {
        String stringifiedObject = value.toString();
        JSONObject subObj = (JSONObject) JSONSerializer.toJSON(stringifiedObject);
        Iterator<?> subKeys = subObj.keys();
        while( subKeys.hasNext() ){
          String subKey = (String)subKeys.next();
          Object subKeyValue = subObj.get(subKey);
          String keyWithADot = key.concat(".");
          String newKey = keyWithADot.concat(subKey);
          addKeyValuePair(newKey,subKeyValue);
        }
      }
  }

  public void savePropsFile() throws Exception{
    File myFile = new File(System.getProperty("user.home"), "example.conf");
		props.store(new FileWriter(myFile),null);
  }
}
