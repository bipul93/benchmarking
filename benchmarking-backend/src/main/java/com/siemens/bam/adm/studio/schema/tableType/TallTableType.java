package com.siemens.bam.adm.studio.schema.tableType;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;

public class TallTableType extends AbstractTableType {

	final static Logger logger = LoggerFactory.getLogger(TallTableType.class.getName());

	@Override
	public void init(Table table, ClientProperties clientProp) throws Exception {

		List<String> rowKeys = table.getColumnsWithUniqueValues();

		setUniqueSetOfValues(dataGenerator.generateUniqueData(table, clientProp, rowKeys));

	}

	/**
	 * The column name is returned for Tall tables
	 * 
	 */
	@Override
	public <T> T buildColumnName(String colName, List<String> names, List<TableColumn> columns) throws Exception {
		return (T) colName;
	}

}
