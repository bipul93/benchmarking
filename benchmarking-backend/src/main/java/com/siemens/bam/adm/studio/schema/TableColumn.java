package com.siemens.bam.adm.studio.schema;

import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TableColumn {

	final static Logger logger = LoggerFactory
			.getLogger(TableColumn.class
					.getName());
	private String colName;
	private Integer colDataType;
	private List<Object> fixedValues;
	private Boolean isUniqueSetOfValuesToBeGenerated;
	
	public Boolean getIsUniqueSetOfValuesToBeGenerated() {
		return isUniqueSetOfValuesToBeGenerated;
	}

	public void setIsUniqueSetOfValuesToBeGenerated(Boolean isUniqueSetOfValuesToBeGenerated) {
		this.isUniqueSetOfValuesToBeGenerated = isUniqueSetOfValuesToBeGenerated;
	}

	public List<Object> getFixedValues() {
		return fixedValues;
	}

	public void setFixedValues(List<Object> values) {
		this.fixedValues = values;
	}

	public String getColName() {
		return colName;
	}

	public void setColName(String colName) {
		this.colName = colName;
	}

	public Integer getColDataType() {
		return colDataType;
	}

	public void setColDataType(Integer colDataType) {
		this.colDataType = colDataType;
	}

	@Override
	public boolean equals(Object o) {

		if (o instanceof TableColumn) {

			TableColumn mo = (TableColumn) o;
			return mo.colName.equals(colName);
		}
		return false;
	}

	@Override
	public int hashCode() {
		logger.debug("In equals method of TableColumn with ");

		return java.util.Objects.hashCode(colName);
	}
}
