package com.siemens.bam.adm.studio.read.userInterface;
/**
 * Interface that defines common methods for input parameter parsing for CommandLine and File user interfaces
 * @author Nisha Menon
 */

public interface IUserInterface 
{
	// Method that is to be implemented by the respective user interfaces extending this class
	public abstract void runUserInterface() throws Exception;

}
