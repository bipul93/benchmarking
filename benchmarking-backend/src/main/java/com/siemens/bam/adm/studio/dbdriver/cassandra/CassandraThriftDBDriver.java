package com.siemens.bam.adm.studio.dbdriver.cassandra;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeSet;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.astyanax.MutationBatch;
import com.netflix.astyanax.connectionpool.OperationResult;
import com.netflix.astyanax.connectionpool.exceptions.ConnectionException;
import com.siemens.bam.adm.studio.connections.CassandraThriftConnection;
import com.siemens.bam.adm.studio.connections.IConnection;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.Serializer;

/**
 * The CassandraThriftDBDriver class contains methods that perform operations in
 * Cassandra using the thrift protocol This includes getting a connection
 * instance and writing to/reading from Cassandra Keyspace
 * 
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class CassandraThriftDBDriver extends AbstractDatabaseDriver {

	final static Logger logger = LoggerFactory.getLogger(CassandraThriftDBDriver.class.getName());
	private static volatile IConnection connection;
	private static Map<String, MutationBatch> batchStatements = new ConcurrentHashMap<String, MutationBatch>();
	

	public CassandraThriftDBDriver() {	}

	@Override
	public AbstractDatabaseDriver initWrites(Environment environment) throws DBException, Exception {
		getConnectionInstance(environment);
		if (environment.getClientProperties().isBatchInserts().equals("true")) {
			MutationBatch batchStmt = ((CassandraThriftConnection) connection).getKeyspace().prepareMutationBatch();
			batchStatements.put(Thread.currentThread().getName(), batchStmt);
		}
		return this;
	}

	// A singleton object of CassandraThriftConnection is created as the
	// keyspace object is thread safe and can be used across threads.
	private void getConnectionInstance(Environment environment) throws Exception {
		if (null == connection) {
			synchronized (CassandraThriftConnection.class) {
				if (null == connection) {
					connection = new CassandraThriftConnection(environment);
				}
			}
		}
	}

	@Override
	public boolean insertRecordsIndividually(List<TableColumn> columns, Table table) throws DBException, Exception {
		MutationBatch mutation = ((CassandraThriftConnection) connection).getKeyspace().prepareMutationBatch();

		List<String> primaryKeys = table.getRowKeys();
		List<String> clusteringKeys = table.getClusteringKeys();

		Object wrappedKey = buildRowKey(table, primaryKeys, columns);

		for (TableColumn entry : columns) {

			if (!(primaryKeys.contains(entry.getColName()) || clusteringKeys.contains(entry.getColName()))) {

				// Colname = Clustering Key that is defined in the schema JSON
				// file
				Object colName = Serializer.convertToBytes(table.getTypeOfTable().buildColumnName(
								entry.getColName(), 
								table.getClusteringKeys(), 
								table.getColumns()).toString(),
								table.getColumnValidator().getName());

				logger.debug("[Thread-" + Thread.currentThread().getId() + "] Adding field/value " + entry.getColName()
						+ "/" + DataTypes.getDatatypeFromOrdinal(entry.getColDataType()) + " to put request");

				Object fieldDataValue = table.getTypeOfTable().getValueForField(entry);
				logger.debug(Thread.currentThread().getName() + " --> Data for " + entry.getColName() + " : "
						+ fieldDataValue);
				DataTypes dType = DataTypes.getDatatypeFromOrdinal(entry.getColDataType());

				switch (dType) {
				case DATETIME:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							new Date(Long.parseLong(fieldDataValue.toString())));
					break;

				case UUID:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(UUID) fieldDataValue);
					break;

				case DATE:
					SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
					Date date = null;
					try {
						date = formatter.parse(fieldDataValue.toString());
					} catch (ParseException e) {

						logger.error("Error while parsing date " + date);
						return false;
					}
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName, date);
					break;
				case BOOLEAN:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(Boolean) fieldDataValue);
					break;

				case INT:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							Integer.parseInt(fieldDataValue.toString()));
					break;
				case NULLINT:
				case SMALLINT:
				case TINYINT:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(short) fieldDataValue);
					break;
				case BIGINT:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(Long) fieldDataValue);
					break;
				case FLOAT:
					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(Float) fieldDataValue);
					break;

				case DOUBLE:

					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(Double) fieldDataValue);
					break;

				case STRING:

					mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
							(String) fieldDataValue);
					break;
				default:
					throw new Exception("Unknown field type for fieldname\"" + entry.getColName() + "\"");
				}
			}
		}
		try {
			OperationResult<Void> result = mutation.execute();
			logger.debug("Thread " + Thread.currentThread().getId() + " executed the write operation");
			return true;
		} catch (ConnectionException e) {
			logger.debug("ConnectionException in CassandraThriftDBDriver::insertRecordsIndividually - " + e.getMessage());
			return false;
		} catch(Exception e){
			logger.debug("Error while inserting in CassandraThriftDBDriver::insertRecordsIndividually - " + e.getMessage());
			return false;
		}
	}

	@Override
	public void addStatementsToBatch(int batchSize, List<TableColumn> columns, Table table, Boolean isUniqueKey)
			throws DBException, Exception {
		MutationBatch mutation = batchStatements.get(Thread.currentThread().getName());

		List<String> primaryKeys = table.getRowKeys();
		List<String> clusteringKeys = table.getClusteringKeys();

		Object primaryKeyDataValue = null;

		if (isUniqueKey == true) {
			primaryKeyDataValue = buildRowKey(table, primaryKeys, columns);

		}

		int numberInCurrentBatch = 0;
		while (numberInCurrentBatch < batchSize) {
			Object wrappedKey;
			if (primaryKeyDataValue == null) {
				wrappedKey = buildRowKey(table, primaryKeys, columns);
			} else {
				wrappedKey = primaryKeyDataValue;
			}
			for (TableColumn entry : columns) {

				if (!(primaryKeys.contains(entry.getColName()) || clusteringKeys.contains(entry.getColName()))) {

					logger.debug("[Thread-" + Thread.currentThread().getId() + "] Adding field/value "
							+ entry.getColName() + "/" + DataTypes.getDatatypeFromOrdinal(entry.getColDataType())
							+ " to put request");

					Object colName = Serializer
							.convertToBytes(
									table.getTypeOfTable()
											.buildColumnName(entry.getColName(), table.getClusteringKeys(),
													table.getColumns())
											.toString(),
									table.getColumnValidator().getName());

					Object fieldDataValue = table.getTypeOfTable().getValueForField(entry);

					logger.debug(Thread.currentThread().getName() + " --> Data for " + entry.getColName() + " : "
							+ fieldDataValue);
					DataTypes dType = DataTypes.getDatatypeFromOrdinal(entry.getColDataType());

					switch (dType) {
					case DATETIME:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								new Date(Long.parseLong(fieldDataValue.toString())));
						break;

					case UUID:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(UUID) fieldDataValue);
						break;

					case DATE:
						SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
						Date date = null;
						try {
							date = formatter.parse(fieldDataValue.toString());
						} catch (ParseException e) {

							logger.error("Error while parsing date " + date);

						}
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								date);
						break;
					case BOOLEAN:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(Boolean) fieldDataValue);
						break;

					case INT:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								Integer.parseInt(fieldDataValue.toString()));
						break;
					case NULLINT:
					case SMALLINT:
					case TINYINT:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(short) fieldDataValue);
						break;
					case BIGINT:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(Long) fieldDataValue);
						break;
					case FLOAT:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(Float) fieldDataValue);
						break;

					case DOUBLE:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(Double) fieldDataValue);
						break;

					case STRING:
						mutation.withRow(((CassandraThriftConnection) connection).getColumnFamily(), wrappedKey).putColumn(colName,
								(String) fieldDataValue);
						break;
					default:
						throw new Exception("Unknown field type for fieldname\"" + entry.getColName() + "\"");
					}
				}
			}
			numberInCurrentBatch++;
			batchStatements.put(Thread.currentThread().getName(), mutation);
		}
	}

	@Override
	public boolean insertbatch() throws DBException {
		try {
			OperationResult<Void> result = batchStatements.get(Thread.currentThread().getName()).execute();
			logger.debug("Thread " + Thread.currentThread().getId() + " executed the write operation");
			batchStatements.get(Thread.currentThread().getName()).discardMutations();
			return true;
		} catch (ConnectionException e) {
			logger.debug("Error while inserting " + e.getMessage());
			return false;
		}
	}

	private Object buildRowKey(Table table, List<String> primaryKeys, List<TableColumn> columns) throws Exception {

		String firstPK = primaryKeys.get(0);
		Object pkValue = null;

		TableColumn col = new TableColumn();
		col.setColName(firstPK);
		int index = columns.indexOf(col);
		pkValue = table.getTypeOfTable().getValueForField(columns.get(index));

		if (primaryKeys.size() > 1) {

			for (String primaryKey : primaryKeys) {

				if (!primaryKey.equalsIgnoreCase(firstPK)) {
					logger.debug("Building Primary Key with " + primaryKey);
					// + converts Object to String
					col.setColName(primaryKey);
					index = columns.indexOf(col);

					try {
						logger.debug("Type of PK added : "
								+ DataTypes.getDatatypeFromOrdinal(columns.get(index).getColDataType()));
					} catch (Exception e) {
						throw new DBException("Error while getting data type for column " + primaryKey);
					}
					logger.debug("Size of values " + columns.get(index).getFixedValues().size());
					pkValue += table.getRowKeySeparator();
					pkValue += table.getTypeOfTable().getValueForField(columns.get(index)).toString();

				}

			}
			return pkValue;
		} else {
			return pkValue.toString();
		}

	}

	@Override
	public AbstractDatabaseDriver initReads(Environment environment) throws DBException, Exception {
		getConnectionInstance(environment);
		return this;
	}

	@Override
	public NavigableMap<Object, Object> getValuesForClusteringKeyRange_WideCF(String rowKey, Object startCol, Object endCol,
			int limit, DataModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TreeSet<String> getAllRowKeys(String parameter, int value, DataModel model) {
		// TODO Auto-generated method stub
		return null;
	}	

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getLastWideColumnValue_WideCF(java.lang.String, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public Object getLastColumnValue_WideCF(String rowKey, DataModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NavigableMap<Object, Object> getValuesForClusteringKeyRangePerTimestep_WideCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model, int timestep, boolean usetimestep_flag) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void close() throws Exception {
		connection.closeConnection();

	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getValuesForOneColumnWithColumnRange_TallCF(java.lang.String, java.lang.Object, java.lang.Object, java.lang.String, com.siemens.bam.adm.studio.schema.Datatypes, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public NavigableMap<Object, Object> getValuesForOneColumnWithClusteringKeyRange_TallCF(String rowKey, Object startCol,
			Object endCol, String columnName, DataTypes col_datatype, int limit, DataModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver#getValuesForColumnRange_SuperColumnCF(java.lang.String, java.lang.Object, java.lang.Object, int, com.siemens.bam.adm.studio.schema.DataModel)
	 */
	@Override
	public <S> NavigableMap<Object, S> getValuesForClusteringKeyRange_SuperColumnCF(String rowKey, Object startCol,
			Object endCol, int limit, DataModel model) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public NavigableMap<Object, Map<Object, Object>> getValuesForAllColumnsWithClusteringKeyRange_TallCF(String rowKey,
			Object startCol, Object endCol, DataModel model, int limit) {
		// TODO Auto-generated method stub
		return null;
	}


}
