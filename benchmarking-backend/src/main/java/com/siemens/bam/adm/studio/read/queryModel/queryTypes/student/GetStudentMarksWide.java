/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryTypes.student;

import java.io.File;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.metric.ReadMetric;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.student.GetStudentMarksWideQueryParams;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;

import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;

import com.siemens.bam.adm.studio.schema.DataModel;
import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;
import com.siemens.bam.adm.studio.utils.ScanQueryResult;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;

/**
 * Query Class that gets data from Student Schema
 * 
 * @author ic019063
 *
 */
public class GetStudentMarksWide extends AbstractQuery {
	private static final Logger logger = LoggerFactory.getLogger(GetStudentMarksWide.class);
	private GetStudentMarksWideQueryParams queryParams;
	public Map<String, ReadMetric> executionStats;

	/**
	 * @param model
	 */
	public GetStudentMarksWide(DataModel model) {
		super(model);
		logger.debug("GetStudentMarksWide Query instantiated");

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#
	 * getQueryParams()
	 */
	@Override
	public AbstractQueryParams getQueryParams() {
		return this.queryParams;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#
	 * initializeParameters(java.lang.String[])
	 */
	@Override
	public boolean initializeParameters(String[] parameterList)
	{	
		// Creating new queryParams object and executionStats Map for every new query.
		this.queryParams = new GetStudentMarksWideQueryParams();
		this.executionStats = new ConcurrentHashMap<String, ReadMetric>();
		
		Table table = model.getTables().get(0);
		List<TableColumn> cols = table.getColumns();
		List<String> rowkeys = table.getRowKeys();

		TableColumn tc = new TableColumn();
		DataTypes[] rowKeyDataTypes = new DataTypes[10];
		int rkCount = 0;

		for (String rk : rowkeys) {
			tc.setColName(rk);
			int index = cols.indexOf(tc);
			DataTypes datatype = DataTypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());
			rowKeyDataTypes[rkCount++] = datatype;
		}

		List<String> wordList = Arrays.asList(parameterList);
		if (wordList.contains("rowkey") && wordList.contains("begin") && wordList.contains("end")) {
			try {
				Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
				String param;
				int count = 0;

				while (count < parameterList.length) {
					param = parameterList[count];
					count++;

					if (queryNameTypes.contains(param)) {
						queryParams.setQueryName(param);
						logger.debug("Query Name set to '" + queryParams.getQueryName() + "'");
					} else if (param.equalsIgnoreCase("rowkey")) {
						try {
							String rowkey = parameterList[count];
							count++;
							ArrayList<String> arrList = new ArrayList<String>();

							if (rowkey.startsWith("[") & rowkey.endsWith("]")) {
								rowkey = rowkey.substring(1, rowkey.length() - 1);
								String[] rowKeyList;

								if (rowkey.compareToIgnoreCase("file") == 0) {
									Path filePath = new File(ClientProperties.getRowkeyFilePath()).toPath();
									Charset charset = Charset.defaultCharset();
									List<String> stringList = Files.readAllLines(filePath, charset);
									rowKeyList = stringList.toArray(new String[] {});
								} else
									rowKeyList = rowkey.split(",");

								logger.debug("The first rowKey in the list is: " + rowKeyList[0]);

								String[] rowKeyParts;
								for (int i = 0; i < rowKeyList.length; i++) {
									String rk = rowKeyList[i];
									if (table.getRowKeySeparator()!=null && rk.contains(table.getRowKeySeparator())) {
										rowKeyParts = rk.split("\\"+table.getRowKeySeparator());
										for (int k = 0; k < rowKeyParts.length; k++) {
											if (ValidateQueryParams.validateDataType(rowKeyParts[k],
													rowKeyDataTypes[k])) {
												continue;
											} else {
												logger.warn("Format issue for input <" + rowKeyParts[k]
														+ "> .. Expected <" + rowKeyDataTypes[k] + "> format!");
												return false;
											}
										}
										arrList.add(rk);
									} else {
										if (ValidateQueryParams.validateDataType(rk, rowKeyDataTypes[0])) {
											arrList.add(rk);
										} else {
											logger.warn("Format issue for rowkey <" + rk
													+ "> .. PLease enter in correct format!");
											return false;
										}
									}
								}
								logger.debug("All row-keys validated and added");
								queryParams.setRowKeys(arrList);
							}

							else {
								logger.error(
										"Please specify rowkeys in square brackets seperated by a comma eg: [,,]!");
								return false;
							}
							logger.debug("Complete list of rowkeys are ->");
							for (int i = 0; i < queryParams.getRowKeys().size(); i++) {
								logger.debug(queryParams.getRowKeys().get(i));
							}
						} catch (Exception e) {
							logger.error(
									"Rowkey missing! PLease specify rowkeys in square brackets seperated by a comma eg [,,]!");
							
							return false;
						}
					}

					else if (param.equalsIgnoreCase("begin")) {
						try {
							String beginString = parameterList[count];
							count++;
							if (ValidateQueryParams.validateDataType(beginString, DataTypes.STRING)) {
								queryParams.setBegin(beginString);
								logger.debug("begin set to: " + beginString);
							} else {
								logger.error("Error: Please enter begin term in String format.");
								return false;
							}
						} catch (Exception e) {
							logger.error(
									"Error: Begin Term missing. Please enter a valid begin term in String format.");
							return false;
						}
					}

					else if (param.equalsIgnoreCase("end")) {
						try {
							String endString = parameterList[count];
							count++;
							if (ValidateQueryParams.validateDataType(endString, DataTypes.STRING)) {
								queryParams.setEnd(endString);
								logger.debug("end set to: " + endString);
							} else {
								logger.error("Error: Please enter end term in String format.");
								return false;
							}
						} catch (Exception e) {
							logger.error("Error: End Date missing. Please enter a valid begin term in String format.");
							return false;
						}
					} else if (param.equalsIgnoreCase("limit")) {
						String limitString = parameterList[count];
						count++;
						try {
							int limit = Integer.parseInt(limitString);
							queryParams.setLimit(limit);
						} catch (NumberFormatException e) {
							logger.warn("Could not parse '" + limitString + "', defaulting limit to Max Integer Value");
						}
						logger.debug("Limit set to: " + queryParams.getLimit());
					}
					/**
					 * if random word is given as a command
					 */
					else {
						logger.error("'" + param + "' is an unknown command.");
						return false;
					}
				}
				return true;
			} catch (Exception e) {
				logger.error("Unexpected exception has occured -> " + e.getMessage());
				logger.info("Type 'help' for more information");
				return false;
			}
		} else {
			logger.error(
					"Arguments rowkey, begin and end are mandatory for \"getstudentmarkswide\" query! PLease provide them and re-try!");
			return false;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#
	 * findExecutionTime()
	 */
	@Override
	public long findExecutionTime() {
		long maxThreadTime = 0;
		for (Map.Entry<String, ReadMetric> entry : executionStats.entrySet()) {
			logger.info("The time for rowkey <" + entry.getKey() + "> is " + entry.getValue().getQueryResponseTime()+" ms");
			if (entry.getValue().getQueryResponseTime() > maxThreadTime) {
				maxThreadTime = entry.getValue().getQueryResponseTime();
			}
		}

		return maxThreadTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#
	 * queryExecute(java.lang.String,
	 * com.siemens.bam.adm.studio.dbdriver.IDatabaseDriver)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public Object[] queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception {
		logger.debug("Executing query for GetStudentMarkswide");

		NavigableMap<Object, Object> colFinalList = new TreeMap<Object, Object>();
		Object[] result = new Object[2];

		int numberOfDatapointsPerRowkey = 0;

		try {
			long startThreadTime = System.currentTimeMillis();
			colFinalList = dbDriver.getValuesForClusteringKeyRange_WideCF(rowKey, queryParams.getBegin(), queryParams.getEnd(),
					queryParams.getLimit(), model);

			long endThreadTime = System.currentTimeMillis();
			logger.debug("The total time for " + Thread.currentThread().getId() + " is: "
					+ (endThreadTime - startThreadTime));

			numberOfDatapointsPerRowkey = colFinalList.size();
			logger.debug(
					"Number of Datapoints retrieved for Row key <" + rowKey + "> = " + numberOfDatapointsPerRowkey);

			result[0] = rowKey;
			result[1] = colFinalList;

			ReadMetric m = new ReadMetric();
			m.setEndTime(endThreadTime);
			m.setStartTime(startThreadTime);
			m.setQueryResponseTime(endThreadTime - startThreadTime);
			m.setNumberOfDatapoints(numberOfDatapointsPerRowkey);

			logger.info("Adding time for rowkey <" + rowKey + "> as " + (endThreadTime - startThreadTime));
			executionStats.put(rowKey, m);

		} catch (Exception e) {
			throw new Exception ("Exception in GetStudentMarkswide::queryExecute()",e);
		}

		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#
	 * getResults(int, java.util.concurrent.CompletionService)
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Map<String, NavigableMap<Object, Object>> getResults(int numberOfrowkeys,
			CompletionService completion_pool) throws Exception
	{
		Map<String, NavigableMap<Object, Object>> QueryResult = new ConcurrentHashMap<String, NavigableMap<Object, Object>>();

		for (int i = 0; i < numberOfrowkeys; i++) 
		{
			try 
			{
				Object[] colList = (Object[]) completion_pool.take().get();
				String key = (String) colList[0];
				QueryResult.put(key, (NavigableMap<Object, Object>) colList[1]);
			} 
			catch (InterruptedException ex) 
			{
				throw new Exception("InterruptedException thrown in getResults()",ex);
			} 
			catch (ExecutionException ex) 
			{
				throw new Exception("ExecutionException thrown in getResults()",ex);
			}
		}

		return QueryResult;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#
	 * printResults(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void printResults(T resultSet) {
		Map<String, NavigableMap<Object, Object>> results = (Map<String, NavigableMap<Object, Object>>) resultSet;
		Iterable<String> keys = results.keySet();
		Iterator<String> keyString = keys.iterator();
		while (keyString.hasNext()) {
			String k = keyString.next();
			logger.info(" ------------------ RowKey : " + k + " ------------------- ");
			NavigableMap<Object, Object> c = results.get(k);
			ScanQueryResult.scanNavigableMapofObjects(c);
		}

	}

}
