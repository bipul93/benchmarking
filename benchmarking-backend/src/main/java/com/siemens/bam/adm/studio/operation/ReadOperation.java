package com.siemens.bam.adm.studio.operation;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;

import org.apache.log4j.Logger;
import java.lang.Runnable;
import java.lang.Thread;

import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.core.Executor;
import com.siemens.bam.adm.studio.read.userInterface.BatchUserInterface;

import com.siemens.bam.adm.studio.listener.HttpClientListener;

import com.siemens.bam.adm.studio.read.userInterface.IUserInterface;
import com.siemens.bam.adm.studio.read.userInterface.InteractiveUserInterface;
import com.siemens.bam.adm.studio.threads.Reader;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import com.siemens.bam.adm.studio.utils.GlobalData;

/**
 * Implements the DBOperation interface
 *
 * Methods to execute READ workloads and display the execution results at the end of the run
 *
 * @author varsha.raveendran@siemens.com
 *
 */
public class ReadOperation implements IDBOperation {

	private static final Logger logger = Logger.getLogger(ReadOperation.class);
	private Environment environment;
	private static final String NAME = "READ";
	String ip = "";
	String port = "";

	public ReadOperation() {}

	/**
	 * Sets the data model, database driver and registers queries at the query factory for READ
	 * operations
	 *
	 * @see com.siemens.bam.adm.studio.operation.IDBOperation#buildEnvironment(com.siemens.bam.adm.studio.utils.ClientProperties)
	 */
	@Override
	public void buildEnvironment(ClientProperties clientProp) throws Exception {
		environment = new Environment.EnvironmentBuilder(clientProp).setQueryService().build();
		ip = clientProp.getServerAddress();
		port = clientProp.getServerPort();
	}

	@Override
	public void start() throws Exception {

		IUserInterface uiType;
		String mode = environment.getClientProperties().getMode();

		if (mode.equalsIgnoreCase("Interactive")) {

			logger.info("Starting Interactive user interface");
			uiType = new InteractiveUserInterface(this);
			uiType.runUserInterface();

		} else if (mode.equalsIgnoreCase("Batch")) {

			logger.info("Starting Batch/File user interface");
			uiType = new BatchUserInterface(this);
			uiType.runUserInterface();

		}else if(mode.equalsIgnoreCase("Web")){
			logger.info("Starting Web user interface");
			//run one command and get result
		}
	}

	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public <T> Callable<T> getInstanceOfOperation(int threadid) {
		return new Reader<T>(environment, threadid);
	}

	@Override
	public <T> void displayExecutionStats(CompletionService<T> completionPool) throws Exception {
		logger.info("Fetching Results");
		T Results = (T) environment.getQueryObject().getResults(Executor.getNumberOfThreadsRequired(), completionPool);

		logger.info("Displaying Results -> ");
		environment.getQueryObject().printResults(Results);
		//logger.info(GlobalData.queryResult);

		if(GlobalData.webMode){
			//Thread t = new Thread();
			try{
				PostClass postClass = new PostClass();
				Thread t = new Thread(postClass);
				t.start();
			}catch (Exception e){
				System.out.println("");
			}

			/*HttpClientListener httpC = new HttpClientListener();
			httpC.postTestResult(true);
			System.out.println("post completed");*/
			GlobalData.readInProgress = false;
		}

		long time = environment.getQueryObject().findExecutionTime();
		logger.info("The total time for the query (max time of all spawned threads) is " + time + " ms");
	}

	public Environment getEnvironment() {
		return environment;
	}
	public class PostClass implements Runnable{

		public void run(){
			try{
				System.out.println("posting results ############################################################");
				HttpClientListener httpC = new HttpClientListener(ip,port);
				httpC.postTestResult(true);
			}catch(Exception e){

			}

		}

	}
}
