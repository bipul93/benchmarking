package com.siemens.bam.adm.studio.operation;

import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;

import com.siemens.bam.adm.studio.utils.ClientProperties;

public interface IDBOperation {
		
	/**
	 * Returns the name of the DB operation
	 * @return String
	 */
	abstract public String getName();
	
	/**
	 * Build the environment for the DB workload to execute
	 * 
	 * @param clientProp
	 * @throws Exception
	 */
	abstract public void buildEnvironment(ClientProperties clientProp) throws Exception;
	
	/**
	 * Returns the instance of the DB operation
	 * @param threadid
	 * @return Callable<T>
	 * @throws Exception
	 */
	abstract public <T> Callable<T> getInstanceOfOperation(int threadid) throws Exception;
	
	
	/**
	 * Method to start the workload
	 * @throws Exception
	 */
	abstract public void start() throws Exception;
	
	/**
	 * Display the execution results for a DB operation
	 * @param completionPool
	 * @throws Exception
	 */
	public abstract <T> void displayExecutionStats(CompletionService<T> completionPool) throws Exception;
}
