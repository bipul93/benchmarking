package com.siemens.bam.adm.studio.operation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.exception.OperationException;

/**
 * Factory pattern that returns an instance of DBOperationType
 *
 * @author varsha.raveendran@siemens.com
 *
 */
public class DBOperationFactory {
	private static Logger logger = LoggerFactory.getLogger(DBOperationFactory.class.getName());

	public static IDBOperation getOperationInstance(String operation, String ip, String port) throws OperationException {

		logger.debug("DBOperationFactory returns an instance of " + operation);

		if(operation.equalsIgnoreCase("Write")) {

			return new WriteOperation();

		} else if(operation.equalsIgnoreCase("Read")) {

			return new ReadOperation();

		} else if(operation.equalsIgnoreCase("CollectMetrics")) {

			return new CollectMetricsOperation();

		}else if(operation.equalsIgnoreCase("TestConnection")){
			return new TestDBOperation(ip,port);
		} else {
			logger.error("Unsupported Operation - Valid operations are WRITE, READ and CollectMetrics.");
			throw new OperationException("Unsupported Operation - Valid operations are WRITE, READ and CollectMetrics.");
		}

		//TODO: Insert an Else condition if a new OperationType is added
	}
}
