/** not now
 *
 */
package com.siemens.bam.adm.studio.read.queryModel.queryTypes.tag;

import java.util.Map;
import java.util.NavigableMap;
import java.util.concurrent.CompletionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.dbdriver.AbstractDatabaseDriver;
import com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag.TagTrendAggregateQueryParams;
import com.siemens.bam.adm.studio.schema.DataModel;

/**
 * Query Class implementation for TagTrend Aggregate with Time Step
 * @author Nisha Menon
 *
 */
public class TagTrendAggregateWithTimestep extends TagTrendAggregate
{
	private static final Logger logger = LoggerFactory.getLogger(TagTrendAggregateWithTimestep.class);

	/**
	 * @param model
	 */
	public TagTrendAggregateWithTimestep(DataModel model) {
		super(model);
		logger.debug("TagTrendAggregateWithTimestep Query instantiated");

	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.TagTrendAggregateWithTimestep#getQueryParams()
	 */
	@Override
	public TagTrendAggregateQueryParams getQueryParams() {
		return super.getQueryParams();
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#initializeParameters(java.lang.String[])
	 */
	@Override
	public boolean initializeParameters(String[] parameterList)
	{
		/*Table table = model.getTables().get(0);
		String pk  = table.getRowKeys().get(0);
		List<TableColumn> cols = table.getColumns();

		TableColumn tc = new TableColumn();
		tc.setColName(pk);
		int index = cols.indexOf(tc);
		Datatypes datatype = Datatypes.getDatatypeFromOrdinal(cols.get(index).getColDataType());

		List<String> wordList = Arrays.asList(parameterList);
		if(wordList.contains("rowkey") && wordList.contains("begin") && wordList.contains("end") && wordList.contains("timestep"))
		{
			try
			{
				Set<String> queryNameTypes = RegisterQueryService.getRegisteredQueries().keySet();
				String param;
				int count = 0;

				while (count < parameterList.length)
				{
					param = parameterList[count];
					count++;

					if(queryNameTypes.contains(param))
					{
						queryParams.setQueryName(param);
						logger.debug("Query Name set to '"+ queryParams.getQueryName() + "'");
					}
					else if (param.equalsIgnoreCase("rowkey"))
					{
						try
						{
							String rowkey = parameterList[count];
							count++;
							ArrayList<String> arrList = new ArrayList<String>();

							if (rowkey.startsWith("[") & rowkey.endsWith("]"))
							{
								rowkey = rowkey.substring(1, rowkey.length() - 1);
								String[] rowKeyList;

								if(rowkey.compareToIgnoreCase("file")==0)
								{
									Path filePath = new File(ClientProperties.getRowkeyFilePath()).toPath();
									Charset charset = Charset.defaultCharset();
									List<String> stringList = Files.readAllLines(filePath, charset);
									rowKeyList = stringList.toArray(new String[]{});
								}
								else
								rowKeyList = rowkey.split(",");

								for (int i = 0; i < rowKeyList.length; i++)
								{
									String s = rowKeyList[i];
									if (ValidateQueryParams.validateDataType(s,datatype))
									{
										arrList.add(s);
									}
									else
									{
										logger.warn("Format issue for rowkey <"+s+"> .. PLease enter in correct format!");
										return false;
									}
								}
								logger.debug("All row-keys validated and added in "+datatype+" format");
								queryParams.setRowKeys(arrList);
							}
							else
							{
								logger.error("Please specify rowkeys in square brackets seperated by a comma eg: [,,]!");
								return false;
							}
							logger.debug("Complete list of rowkeys are ->");
							for (int i = 0; i < queryParams.getRowKeys().size(); i++)
							{
								logger.debug(queryParams.getRowKeys().get(i));
							}
						}
						catch(Exception e)
						{
							logger.error("Rowkey missing! PLease specify rowkeys in square brackets seperated by a comma eg: [,,]!");
							return false;
						}
					}

					else if (param.equalsIgnoreCase("begin"))
					{
						try
						{
							String beginString = parameterList[count];
							count++;
							if (ValidateQueryParams.validateDataType(beginString,Datatypes.DATETIME))
							{
								queryParams.setBegin(beginString);
								logger.debug("begin set to: " + beginString);
							}
							else
							{
								logger.error("Error: Please enter begin date in yyyy-MM-dd'T'HH:mm:ss.SSS format.");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.error("Error: Begin Date missing. Please enter a valid begin date in <dd-mm-yyyy> <HH:mm:ss.SSS> format.");
							return false;
						}
					}

					else if (param.equalsIgnoreCase("end"))
					{
						try
						{
							String endString = parameterList[count];
							count++;
							if (ValidateQueryParams.validateDataType(endString,Datatypes.DATETIME))
							{
								queryParams.setEnd(endString);
								logger.debug("end set to: " + endString);
							}
							else
							{
								logger.error("Error: Please enter end date in yyyy-MM-dd'T'HH:mm:ss.SSS format.");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.error("Error: End Date missing. Please enter a valid end date in <dd-mm-yyyy> <HH:mm:ss.SSS> format.");
							return false;
						}
					}
					else if (param.equalsIgnoreCase("aggregationtype"))
					{
						try
						{
							String aggregationType = parameterList[count];
							count++;
							if (ValidateQueryParams.validateAggregationType(aggregationType))
							{
								queryParams.setAggregationType(aggregationType);
								logger.debug("Aggregation type set to: " + aggregationType);
							}
							else
							{
								logger.error("Error: Please select aggregation types from <MIN,MAX,AVG> only!");
								return false;
							}
						}
						catch(Exception e)
						{
							logger.warn("Aggregation type missing! Assuming default aggregation type as \"MIN\" and proceeding!");
						}
					}
					else if (param.equalsIgnoreCase("timestep"))
					{
						try
						{
							String timestepValue = parameterList[count];
							count++;
							int timeStep = ValidateQueryParams.validateTimeStep(Double.parseDouble(timestepValue));
							queryParams.setTimeStep(timeStep);
							logger.debug("Time step set to: " + timeStep);
						}
						catch(NumberFormatException e)
						{
							logger.warn("Time step missing! Assuming default timestep as \"10\" and proceeding!");
						}
					}
					*//**
					 * if random word is given as a command
					 *//*
					else
					{
						logger.error("'"+param + "' is an unknown command.");
						return false;
					}
				}
				if(ValidateQueryParams.validateBeginEnd(queryParams.getBegin(),queryParams.getEnd()))
				{
					logger.debug("Query Parameters set successfully!");
					return true;
				}
				else
				{
					logger.error("Begin Date greater than end date!");
					return false;
				}
			}
			catch (Exception e)
			{
				logger.error("Unexpected exception has occured -> " + e.toString());
				logger.info("Type 'help' for more information");
				return false;
			}
		}
		else
		{
			logger.error("Arguments rowkey, begin, end and timestep are mandatory for \"tagtrendAggregateWithTimestep\" query! PLease provide them and re-try!");
			return false;
		}*/

		return super.initializeParameters(parameterList);
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#findExecutionTime()
	 */
	@Override
	public long findExecutionTime()
	{
		/*long maxThreadTime=0;
		for (Map.Entry<String, ReadMetric> entry : executionStats.entrySet())
		{
			logger.info("The time for rowkey <"+entry.getKey()+"> is : " + entry.getValue().getQueryResponseTime());
		    if (entry.getValue().getQueryResponseTime() > maxThreadTime)
		    {
		    	maxThreadTime = entry.getValue().getQueryResponseTime();
		    }
		}

		return maxThreadTime; */
		return super.findExecutionTime();
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#queryExecute(java.lang.String, com.siemens.bam.adm.studio.dbdriver.IDatabaseDriver)
	 */
	@Override
	public Object[] queryExecute(String rowKey, AbstractDatabaseDriver dbDriver) throws Exception
	{
		/*logger.debug("Executing query for TagAggregate!");
		NavigableMap<Object, Object> colFinalList = new TreeMap<Object, Object>();
		NavigableMap<Object, Object> finalResults=new TreeMap<Object, Object>();
		Object[] result = new Object[2];

		try
		{
			Iterable<DateRange> allRange = DateFormattingUtils.getDateRangesWithKey(queryParams.getBegin(),queryParams.getEnd(),rowKey);
			Iterator<DateRange> dateIterator = null;
			try
			{
				dateIterator = allRange.iterator();
			}
			catch (Exception e)
			{
				logger.warn("DateRange is NULL");
			}
			int timestep = queryParams.getTimeStep();
			boolean usetimestep_flag=Boolean.parseBoolean(ClientProperties.getUseTimeStepFlag());


			long startThreadTime = System.currentTimeMillis();
			logger.debug("Start time for Thread ID: " + Thread.currentThread().getId()+ " is: " + startThreadTime);


			// execute for each Date Range object
			while (dateIterator.hasNext())
			{
				DateRange d = dateIterator.next();
				NavigableMap<Object, Object> cols = dbDriver.getValuesForColumnRangePerTimestep_WideCF(rowKey, d.getStart(), d.getEnd(), queryParams.getLimit(), model,timestep,usetimestep_flag);
				colFinalList.putAll(cols);
			}

			finalResults=performAggregation(colFinalList);

			long endThreadTime = System.currentTimeMillis();
			logger.debug("End time for Thread ID: " + Thread.currentThread().getId() + " is: " + endThreadTime);

			int numberOfDatapointsPerRowkey = finalResults.size();
			logger.debug("Number of Datapoints after aggregation for Row key <"+rowKey+"> = " + numberOfDatapointsPerRowkey);

			result[0]= rowKey;
			result[1]= finalResults;

			ReadMetric m = new ReadMetric();
			m.setEndTime(endThreadTime);
			m.setStartTime(startThreadTime);
			m.setQueryResponseTime(endThreadTime-startThreadTime);
			m.setNumberOfDatapoints(numberOfDatapointsPerRowkey);

			logger.info("Adding time for rowkey <"+rowKey+"> as "+(endThreadTime-startThreadTime));
			executionStats.put(rowKey, m);

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		logger.debug("Total number of Datapoints for rowkey <"+ rowKey +"> ="+ colFinalList.size());

		return result;*/

		return super.queryExecute(rowKey, dbDriver);
	}

	public NavigableMap<Object,Object> performAggregation(NavigableMap<Object, Object> col) throws Exception
	{
		/*try
		{
			DateTime begindt=queryParams.getBegin();
			Period p = new Period((int)queryParams.getTimeStep() * 1000);
			DateTime enddt=begindt.plus(p);

			ArrayList<Double> valueList=new ArrayList<Double>();
			NavigableMap<Object,Object> AggregatedResults = new TreeMap<Object,Object>();

			boolean currentDateEnteredtheSlot = false;
			double value = 0.0;

			for(Map.Entry<Object, Object>entry:col.entrySet())
			{
				Date current_date = (Date) entry.getKey();
				if(current_date.compareTo(enddt.toDate())<=0)
				{
					valueList.add(Double.parseDouble(entry.getValue().toString()));
				}

				if(current_date.compareTo(enddt.toDate())>=0)
				{
					if(valueList.size()!=0)
					{
						value = getAggregatedResult(queryParams.getAggregationType(),valueList);
						AggregatedResults.put(begindt.toDate(),value);
					}
					else
					{
						AggregatedResults.put(begindt.toDate(),Double.POSITIVE_INFINITY);
					}
					begindt=enddt;
					enddt=enddt.plus(p);
					valueList.clear();

					currentDateEnteredtheSlot=false;
					while(currentDateEnteredtheSlot==false)
					{
						if(current_date.compareTo(enddt.toDate())<=0)
						{
							valueList.add(Double.parseDouble(entry.getValue().toString()));
							currentDateEnteredtheSlot=true;
						}
						else
						{
							AggregatedResults.put(begindt.toDate(),Double.POSITIVE_INFINITY);
							begindt=enddt;
							enddt=enddt.plus(p);
						}
					}
				}
			}
			//the last list left out is aggregated and added!
			if(valueList.size()!=0)
			{
				value = getAggregatedResult(queryParams.getAggregationType(),valueList);
				AggregatedResults.put(begindt.toDate(),value);
			}
			return AggregatedResults;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return null;
		}*/

		return super.performAggregation(col);
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#getResults(int, java.util.concurrent.CompletionService)
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Map<String, NavigableMap<Object, Object>> getResults(int numberOfrowkeys, CompletionService completion_pool) throws Exception
	{
		/*Map<String, NavigableMap<Object, Object>> QueryResult = new ConcurrentHashMap<String, NavigableMap<Object, Object>>();
		for(int i =0 ;i<numberOfrowkeys;i++)
		{
			try
			{
				Object[] colList = (Object[]) completion_pool.take().get();

				String key = (String) colList[0];

				QueryResult.put(key, (NavigableMap<Object, Object>)colList[1]);
			}
			catch (InterruptedException ex)
			{
				ex.printStackTrace();
			}
			catch (ExecutionException ex)
			{
				ex.printStackTrace();
			}
		}
		return QueryResult;*/

		return super.getResults(numberOfrowkeys, completion_pool);
	}

	/* (non-Javadoc)
	 * @see com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery#printResults(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> void printResults(T resultSet)
	{
		/*Map<String, NavigableMap<Object, Object>> results = (Map<String, NavigableMap<Object, Object>>)resultSet;
		Iterable<String> keys = results.keySet();
		Iterator<String> keyString = keys.iterator();
		while (keyString.hasNext())
		{
			String k = keyString.next();
			logger.info(" ------------------ RowKey : " + k
					+ " ------------------- ");
			NavigableMap<Object, Object> c = results.get(k);
			ScanQueryResult.scanNavigableMapofObjects1(c);
		}*/

		super.printResults(resultSet);
	}

}
