package com.siemens.bam.adm.studio.utils;

/*
 * http://stackoverflow.com/questions/12392516/to-calculate-the-total-time-elapsed
 * -using-threads-in-java
 */

public class Timer {
	private long startTime = 0L;
	private long stopTime = 0L;
	public boolean running = false;
	public double elapsed = 0.0;

	public void startTime() {
		this.setStartTime(System.currentTimeMillis());
		this.running = true;
	}

	public void stopTime() {
		this.setStopTime(System.currentTimeMillis());
		this.running = false;
	}

	// Elapsed time in seconds
	public long getElapsedTime() {
		return ((this.getStopTime() - this.getStartTime()));
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getStopTime() {
		return stopTime;
	}

	public void setStopTime(long stopTime) {
		this.stopTime = stopTime;
	}
}
