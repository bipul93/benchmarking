package com.siemens.bam.adm.studio.dbmetrics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.metric.Metric;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import net.sf.json.JSONObject;

/**
 * Class to collect and store metrics from OpenTSDB for HBase
 * 
 * @author varsha.raveendran@siemens.com
 *
 */
public class HBaseMetrics extends AbstractDataStoreMetrics {

	private final static Logger logger = LoggerFactory.getLogger(HBaseMetrics.class.getName());

	private static final String HBASE_MASTER_METRICS = "HBaseMasterMetrics";
	private static final String HBASE_REGIONSERVER_METRICS = "HBaseRegionServerMetrics";

	private Map<Long, LinkedList<Metric>> metricsCollected = new TreeMap<Long, LinkedList<Metric>>();

	private Set<String> metricNames = new LinkedHashSet<String>();

	public HBaseMetrics(Map<String, List<String>> metricList) {
		super(metricList);

	}

	public HBaseMetrics() {
		super();
	}

	@Override
	public void collectMetrics(String fileName, ClientProperties clientProp) throws Exception {

		Map<String, List<String>> dbMetrics = clientProp.getMetricList();

		setMetricsList(dbMetrics);

		DateFormat formatter = new SimpleDateFormat("yyyy/MM/dd-HH:mm:ss");

		Date startDate = new Date(clientProp.getStartTimeForMetricCollection());
		Date endDate = new Date(clientProp.getEndTimeForMetricCollection());
		String startT = formatter.format(startDate);
		String endT = formatter.format(endDate);

		String url = buildURL(clientProp);
		logger.debug(" Formatted Start time : " + startT);
		logger.debug(" Formatted End time : " + endT);

		logger.debug("Built  " + url);

		if (dbMetrics.containsKey(HBASE_MASTER_METRICS)) {

			retrieveMasterMetrics(startT, endT, url, dbMetrics.get(HBASE_MASTER_METRICS), clientProp);
		}
		if (dbMetrics.containsKey(HBASE_REGIONSERVER_METRICS)) {

			retrieveRegionServerMetrics(startT, endT, url, dbMetrics.get(HBASE_REGIONSERVER_METRICS), clientProp);

		}

		createLogFiles(fileName);

		saveMetricsToCSV();

	}

	private void saveMetricsToCSV() throws Exception {

		logger.debug("Saving metrics to CSV..");
		try {
			Output.write("TimeStamp");

			for (String metricName : metricNames) {
				logger.debug(metricName);
				Output.write("," + metricName);

			}
			Output.write("\n");

			for (Entry<Long, LinkedList<Metric>> entryCSV : metricsCollected.entrySet()) {
				Output.write(String.valueOf(entryCSV.getKey()));
				logger.debug(String.valueOf(entryCSV.getKey()));
				Metric m = null;
				for (String s : metricNames) {
					m = new Metric(s);
					logger.debug(s + " index : " + entryCSV.getValue().indexOf(m));

					if (entryCSV.getValue().indexOf(m) != -1) {
						String val = (entryCSV.getValue().get(entryCSV.getValue().indexOf(m))).getMetricValue();
						logger.debug("Value " + val);

						Output.write("," + val);
					} else {
						logger.debug("Writing null");
						Output.write("," + null);

					}
				}

				Output.write("\n");

			}
			Output.flush();

		} catch (IOException e) {

			logger.error("Error while writing metrics into File ");
			throw new Exception(e);
		}

	}

	/**
	 * Method to collect HBase master metrics
	 * 
	 * @param startT
	 * @param endTime
	 * @param url
	 * @param list
	 * @param clientProp
	 * @throws Exception
	 */
	private void retrieveMasterMetrics(String startT, String endTime, String url, List<String> list,
			ClientProperties clientProp) throws Exception {

		String URL = url + "start=" + startT + "&end=" + endTime + "&m=" + clientProp.getMetricFunction() + ":";

		Thread.sleep(120000);
		for (String metric : list) {
			getMetricsFromOpenTSDB(null, metric, URL, clientProp);

		}

	}

	/**
	 * Method to collect HBase Region Server metrics
	 * 
	 * @param startT
	 * @param endT
	 * @param url
	 * @param list
	 * @param clientProp
	 * @throws Exception
	 */
	private void retrieveRegionServerMetrics(String startT, String endT, String url, List<String> list,
			ClientProperties clientProp) throws Exception {

		String[] regionservers = clientProp.getHostIP().split(",");
		String URL = url + "start=" + startT + "&end=" + endT + "&m=" + clientProp.getMetricFunction() + ":";
		Thread.sleep(120000);
		for (String metric : list) {
			getMetricsFromOpenTSDB(null, metric, URL, clientProp);

		}

		for (String rs : regionservers) {
			URL = url + "start=" + startT + "&end=" + endT + "&m=" + clientProp.getMetricFunction() + ":";

			for (String metric : list) {
				getMetricsFromOpenTSDB(rs, metric, URL, clientProp);

			}
		}

	}

	private String addParametersToURL(ClientProperties clientProp) {

		String url = "&";

		if (clientProp.getMetricStep() != null)
			url = url + "autorelated=" + clientProp.getMetricStep() + "&";

		return url;
	}

	/**
	 * Method to connect to OpenTSDB to collect the metrics
	 * 
	 * @param host
	 * @param metric
	 * @param URL
	 * @param clientProp
	 * @throws Exception
	 */
	private void getMetricsFromOpenTSDB(String host, String metric, String URL, ClientProperties clientProp)
			throws Exception {

		String url = URL + metric;

		if (host != null) {
			url = url + "{host=" + host + "}";
		}

		url = url + addParametersToURL(clientProp);

		Map<String, String> metricValues = getResponseFromOpenTSDB(url);

		if (metricValues != null) {
			for (Map.Entry<String, String> entry : metricValues.entrySet()) {

				String timeStamp = entry.getKey();
				String entryVal = entry.getValue();

				if (metricsCollected.get(Long.parseLong(timeStamp)) == null) {
					metricsCollected.put(Long.parseLong(timeStamp), new LinkedList<Metric>());
				}
				LinkedList<Metric> list = metricsCollected.get(Long.parseLong(timeStamp));
				if (host == null) {
					host = "Metric";
				}
				Metric m = new Metric(host + "_" + metric);
				metricNames.add(host + "_" + metric);
				m.setMetricValue(entryVal);
				list.add(m);
				metricsCollected.put(Long.parseLong(timeStamp), list);

			}
		}

	}

	private Map<String, String> getResponseFromOpenTSDB(String opentsdbUrl) throws Exception {

		logger.debug("Open TSDB URL " + opentsdbUrl);
		Map<String, String> metricValues = new LinkedHashMap<String, String>();

		URL url = new URL(opentsdbUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		JSONObject output = null;

		String jsonResult = br.readLine().toString();
		output = JSONObject.fromObject(jsonResult.substring(1, jsonResult.length() - 1));

		JSONObject jsonKeyValue = (JSONObject) output.getJSONObject("dps");
		logger.debug(jsonKeyValue.toString());
		Iterator<?> jsonKeys = jsonKeyValue.keys();

		while (jsonKeys.hasNext()) {
			String key = (String) jsonKeys.next();
			logger.debug(key + ": " + jsonKeyValue.get(key));
			metricValues.put(key, String.valueOf(jsonKeyValue.get(key).toString()));

		}

		conn.disconnect();

		return metricValues;
	}

	private String buildURL(ClientProperties clientProp) {

		String opentsdbIP = clientProp.getOpenTSDBIP();
		String opentsdbPort = clientProp.getOpenTSDBPort();

		logger.debug(opentsdbIP + " : " + opentsdbPort);
		return "http://" + opentsdbIP + ":" + opentsdbPort + "/api/query?";

	}

}
