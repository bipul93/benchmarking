/**
 * 
 */
package com.siemens.bam.adm.studio.read.queryModel.queryParameters.tag;

import com.siemens.bam.adm.studio.read.queryModel.queryParameters.AbstractQueryParams;

/**
 * QueryParams for TagTrendLatest
 * @author Nisha Menon
 *
 */
public class TagTrendLatestQueryParams extends AbstractQueryParams
{
	private int limit;
	private int time;
	
	public TagTrendLatestQueryParams()
	{
		super();
		this.limit = 0;
		this.time = 1;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public void setTime(String begin) 
	{
		this.time = Integer.parseInt(begin);
	}
	
	public int getTime()
	{		
		return time;		
	}

}
