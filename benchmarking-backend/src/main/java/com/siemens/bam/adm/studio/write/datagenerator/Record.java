package com.siemens.bam.adm.studio.write.datagenerator;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.utils.RandomNumberUtil;

public class Record {

	final static Logger logger = LoggerFactory.getLogger(Record.class.getName());

	public Object generateValuesForFieldName(String fieldname, Integer fieldtype) throws Exception {

		DataTypes dType = DataTypes.getDatatypeFromOrdinal(fieldtype);

		switch (dType) {

		case DATETIME:
			return getCurrentDateTimeOffsetInLong();

		case UUID:
			return getUUID();

		case BOOLEAN:
			return generateRandomBooleanValue();

		case TINYINT:
			return generateRandomTinyIntValue();

		case SMALLINT:
			return generateRandomSmallIntValue();

		case INT:
			return generateRandomIntValue();

		case BIGINT:
			return generateRandomBigIntValue(Long.MAX_VALUE);

		case NULLINT:
			return generateNullIntValue();

		case FLOAT:
			return generateRandomFloatValue();

		case DOUBLE:
			return generateRandomDoubleValue();

		case STRING:
			return generateVarcharData();

		case DATE:
			return generateCurrentDateInLong();

		case BYTES:
			return generateRandomBytes();
			
		case INETADDRESS:
			return generateInetAddress();

		default:
			try {
				throw new Exception("Unknown field type for fieldname\"" + fieldname + "\"");
			} catch (Exception e) {
				System.exit(0);
			}
		}
		return null;
	}

	private String generateInetAddress() {
		Random r = new Random();
		return r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256) + "." + r.nextInt(256);
	}

	public long generateReversedTimeStamp() {
		Date date = new Date();
		Long offset = generateRandomBigIntValue(999999L);
		long lDate = date.getTime() + offset;
		return (Long.MAX_VALUE - lDate);
	}

	public static Long generateRandomBigIntValue(long range) {
		Long rndLong = RandomNumberUtil.nextLong(range);
		return rndLong;
	}

	public static String getCurrentDateTimeOffsetInLong() {
		Date date = new Date();
		Long offset = generateRandomBigIntValue(999999L);
		Long lDate = date.getTime() + offset;

		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		formatter.setTimeZone(TimeZone.getDefault());

		return lDate.toString();
	}

	public static UUID getUUID() {
		return UUID.randomUUID();
	}

	public static boolean generateRandomBooleanValue() {
		boolean rndBool = RandomNumberUtil.nextBoolean();
		return rndBool;
	}

	public static short generateRandomTinyIntValue() {
		short rndSmallInt = (short) RandomNumberUtil.nextInt(127);
		return rndSmallInt;
	}

	public static short generateRandomSmallIntValue() {
		short rndSmallInt = (short) RandomNumberUtil.nextInt(Short.MAX_VALUE + 1);
		return rndSmallInt;
	}

	public static int generateRandomIntValue() {
		int rndInt = RandomNumberUtil.nextInt(Integer.MAX_VALUE);
		int minVal = 0;
		return rndInt - minVal;
	}

	public static short generateNullIntValue() {
		return 0;
	}

	private static Float generateRandomFloatValue() {
		float rndFloat = RandomNumberUtil.nextFloat();
		byte b1 = (byte) RandomNumberUtil.nextInt(Byte.MAX_VALUE + 1);
		byte b2 = 0;
		return rndFloat + b1 - b2;
	}

	public static Double generateRandomDoubleValue() {
		double rndDouble = RandomNumberUtil.nextDouble();
		byte b1 = (byte) RandomNumberUtil.nextInt(Byte.MAX_VALUE + 1);
		byte b2 = 0;
		return rndDouble + b1 - b2;
	}

	public static String generateVarcharData() {
		return RandomStringUtils.randomAlphanumeric(10);
	}

	public static String generateCurrentDateInLong() throws Exception {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dateWithoutTime = null;
		try {
			dateWithoutTime = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			throw e;
		}
		Long date = dateWithoutTime.getTime();
		return date.toString();
	}

	private byte[] generateRandomBytes() {
		byte[] b = new byte[60];
		new Random().nextBytes(b);
		return b;
	}

}
