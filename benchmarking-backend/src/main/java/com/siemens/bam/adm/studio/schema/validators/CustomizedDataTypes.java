package com.siemens.bam.adm.studio.schema.validators;

import java.util.ArrayList;

/**
 * Class to create a customized data types
 * @author Nisha Menon
 */

public class CustomizedDataTypes extends AbstractDataTypes {

	private ArrayList<String> DataTypeSet = new ArrayList<String>();

	@Override
	public ArrayList<String> retrieveValidDataTypes(String type) 
	{
		if (type.contains("Cassandra")) 
		{
			this.DataTypeSet.add("BytesType");
			this.DataTypeSet.add("AsciiType");
			this.DataTypeSet.add("UTF8Type");
			this.DataTypeSet.add("IntegerType");
			this.DataTypeSet.add("Int32Type");
			this.DataTypeSet.add("InetAddressType");
			this.DataTypeSet.add("LongType");
			this.DataTypeSet.add("UUIDType");
			this.DataTypeSet.add("TimeUUIDType");
			this.DataTypeSet.add("DateType");
			this.DataTypeSet.add("BooleanType");
			this.DataTypeSet.add("FloatType");
			this.DataTypeSet.add("DoubleType");
			this.DataTypeSet.add("DecimalType");
			this.DataTypeSet.add("CounterColumnType");
		}
		return this.DataTypeSet;
	}

	public ArrayList<String> getDataTypeSet() {

		return this.DataTypeSet;
	}

	public void setDataTypeSet(ArrayList<String> dataTypeSet) {

		this.DataTypeSet = dataTypeSet;
	}

}
