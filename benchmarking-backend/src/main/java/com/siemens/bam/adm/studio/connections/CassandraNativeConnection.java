package com.siemens.bam.adm.studio.connections;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.lang3.EnumUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.siemens.bam.adm.studio.core.Environment;
import com.siemens.bam.adm.studio.dbdriver.cassandra.CassandraProperties;
import com.siemens.bam.adm.studio.exception.DBException;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;

/**
 * The Connection Class for Cassandra with Binary Protocol
 *
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class CassandraNativeConnection implements IConnection {

	final static Logger logger = LoggerFactory.getLogger(CassandraNativeConnection.class.getName());

	private static AtomicInteger atomicCounter = new AtomicInteger(0);
	private Session session;
	private Cluster cluster;
	private PreparedStatement statement;
	private static CassandraProperties cassandraProperties = new CassandraProperties();

	// The atomic counter variable is used to ensure that the connection is
	// closed for all threads
	public CassandraNativeConnection(Environment environment) throws Exception {
		logger.debug(Thread.currentThread().getName() + " : Creating a new CassandraNativeConnection instance");
		cassandraProperties.validateAndSetCassandraConsistencyLevel();
		atomicCounter.set(environment.getClientProperties().getNumberOfThreads());

		initializeConnection(environment);
		if (environment.getClientProperties().getOperation().equalsIgnoreCase("Write")) {
			logger.debug("Create Insert Statement");
			createInsertStatement(environment);
		}

	}

	@Override
	public void initializeConnection(Environment environment) throws Exception {

		String[] hostIPs = environment.getClientProperties().getHostIP().split(",");

		Builder builder = new Cluster.Builder().addContactPoint(hostIPs[0]);

		if (hostIPs.length > 1) {
			logger.debug(Thread.currentThread().getName() + " Adding additional Cassandra contact points =>");
			for (int i = 1; i < hostIPs.length; i++) {
				logger.debug(hostIPs[i]);
				builder.addContactPoint(hostIPs[i]);
			}
		}

		try {

			if (!EnumUtils.isValidEnum(ConsistencyLevel.class, cassandraProperties.getCassandraConsistencyLevel())) {

				logger.error("No such Consistency Level " + cassandraProperties.getCassandraConsistencyLevel()
						+ " available in Cassandra. Please check the config file. ");
				throw new Exception("No such Consistency Level " + cassandraProperties.getCassandraConsistencyLevel()
						+ " available in Cassandra. Please check the config file. ");
			}

			cluster = builder.withPort(cassandraProperties.getBinaryPort())
					.withQueryOptions(new QueryOptions().setConsistencyLevel(
							ConsistencyLevel.valueOf(cassandraProperties.getCassandraConsistencyLevel())))
					.withLoadBalancingPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build())).build();

			Metadata metadata = cluster.getMetadata();
			for (Host host : metadata.getAllHosts()) {
				logger.debug("Datacenter: {}", host.getDatacenter());
				logger.debug(" Host: {}", host.getAddress().toString());
				logger.debug(" Rack: {}",

						host.getRack());
			}

			logger.debug("Connected to Cluster [" + metadata.getClusterName() + "]");
		} catch (Exception e) {
			throw new DBException(
					"Exception in CassandraNativeConnection::getConnection - Error in connecting to Host " + e);
		}
		try {
			session = cluster.connect(environment.getDataModel().getDatabaseName());
		} catch (Exception e) {
			throw new DBException(
					"Exception in CassandraNativeConnection::getConnection - Error while connecting to cluster " + e);
		}

	}

	public void createInsertStatement(Environment environment) throws Exception {

		String keyspace = environment.getClientProperties().getDatabaseName();
		String colFamily = environment.getClientProperties().getTableName();

		Table table = environment.getDataModel().getTable(colFamily);

		if (!checkIfTableExists(keyspace, colFamily)) {

			logger.error("Keyspace/ColumnFamily does not exist! ");
			throw new DBException("Keyspace/ColumnFamily does not exist! ");
		}

		Insert is = QueryBuilder.insertInto(keyspace, colFamily);

		try {
			List<TableColumn> columns = table.getColumns();

			for (TableColumn col : columns) {

				is.value(col.getColName(), QueryBuilder.bindMarker());
			}

		} catch (Exception e) {
			throw new DBException("Error while creating insert statement! " + e);
		}
		logger.debug(is.toString());

		try {
			statement = session.prepare(is);

		} catch (Exception e) {
			throw new DBException("Error while preparing  statement! " + e);
		}

	}

	private boolean checkIfTableExists(String keyspace, String colFamily) {

		try {
			Statement statement = new SimpleStatement("SELECT * FROM system_schema.keyspaces " + "WHERE keyspace_name = '"
					+ keyspace.toLowerCase() + "';");

		 statement.setConsistencyLevel(ConsistencyLevel.ONE);
		 logger.debug("CL for statement - " + statement.getConsistencyLevel().toString());
			ResultSet results = session.execute(statement);

			if (results.all().size() == 0) {

				logger.debug("Keyspace " + keyspace + "does not exist. " + results.all().size());

				return false;
			} else {
				 statement = new SimpleStatement("SELECT table_name FROM system_schema.tables WHERE keyspace_name='"
							+ keyspace.toLowerCase() + "';");

				 statement.setConsistencyLevel(ConsistencyLevel.ONE);
				 logger.debug("CL for statement - " + statement.getConsistencyLevel().toString());
				results = session.execute(statement);

				boolean isTableExists = false;

				for (Row row : results) {
					if (row.getString("table_name").equalsIgnoreCase(colFamily.toLowerCase())) {
						isTableExists = true;
					}
				}

				if (!isTableExists) {

					logger.debug("CF " + colFamily + " does not exist. " + results.all().size());
					return false;
				} else
					return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error while executing CQL statement: " + e.getMessage());
			System.exit(1);
		}
		return false;

	}

	public boolean testConnection(Environment environment) throws Exception{
			initializeConnection(environment);

			if(session != null){

				closeConnection();
				logger.info("true");
				return true;
			}
			logger.info("false");
			return false;
	}

	@Override
	public void closeConnection() throws DBException {

		try {
			
			if (atomicCounter.decrementAndGet() == 0) {
				if (session != null && cluster != null) {
					logger.debug(Thread.currentThread().getName() + " : Session closed.");
					session.close();
					cluster.close();

				}
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
	}

	public Session getSession() throws DBException {
		if(session == null) {
			throw new DBException("Exception thrown : Session is NULL. ");
		}
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Cluster getCluster() {
		return cluster;
	}

	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}

	public PreparedStatement getStatement() {
		return statement;
	}

	public void setStatement(PreparedStatement statement) {
		this.statement = statement;
	}
}
