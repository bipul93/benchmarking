package com.siemens.bam.adm.studio.read.userInterface;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.core.Executor;
import com.siemens.bam.adm.studio.operation.TestDBOperation;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.RegisterQueryService;
import com.siemens.bam.adm.studio.read.queryModel.queryTypes.AbstractQuery;
import com.siemens.bam.adm.studio.utils.ValidateQueryParams;
import com.siemens.bam.adm.studio.listener.HttpClientListener;

public class TestUserInterface implements IUserInterface{
  private static final Logger logger = LoggerFactory.getLogger(InteractiveUserInterface.class);
	public TestDBOperation operation;
	public TestUserInterface(TestDBOperation operation){
        //logger.info(operation);
		this.operation = operation;
		logger.trace("Command Line User Interface Object Set.");
	}

  @Override
	public void runUserInterface() throws Exception{
		logger.debug("Within Interactive User Interface Implementation.");

    //System.out.println();

    //connection successfully established post to server

    /*  send updates to client
      */
      Thread t = new Thread(new Runnable() {
        public void run(){
            try{
              HttpClientListener httpClientListener = new HttpClientListener(operation.getServerIP(),operation.getServerPort());
              httpClientListener.keepAliveConnect();

            }catch(Exception e){
              System.out.println(e.getMessage());
            }
        }
      });
      t.start();

	}
}
