package com.siemens.bam.adm.studio.utils;


import java.util.Date;
import java.util.UUID;

import org.apache.hadoop.hbase.util.Bytes;

import com.netflix.astyanax.serializers.AbstractSerializer;
import com.netflix.astyanax.serializers.AsciiSerializer;
import com.netflix.astyanax.serializers.BooleanSerializer;
import com.netflix.astyanax.serializers.ByteSerializer;
import com.netflix.astyanax.serializers.DateSerializer;
import com.netflix.astyanax.serializers.DoubleSerializer;
import com.netflix.astyanax.serializers.FloatSerializer;
import com.netflix.astyanax.serializers.Int32Serializer;
import com.netflix.astyanax.serializers.IntegerSerializer;
import com.netflix.astyanax.serializers.LongSerializer;
import com.netflix.astyanax.serializers.StringSerializer;
import com.netflix.astyanax.serializers.TimeUUIDSerializer;
import com.netflix.astyanax.serializers.UUIDSerializer;

/*
 * This class has a method that returns Astyanax serializers depending of the type of data that needs to be serialized 
 */
public class Serializer {

	@SuppressWarnings({ "rawtypes" })
	public static AbstractSerializer getSerializerType(String instance) {


		switch (instance) {
		
		case "DateType":
			return DateSerializer.get();

		case "UUIDType":
			return UUIDSerializer.get();

		case "TimeUUIDType":
			return TimeUUIDSerializer.get();
		
		case "BooleanType":
			return BooleanSerializer.get();

		case "IntegerType":
			return IntegerSerializer.get();

		case "LongType":
			return LongSerializer.get();

		case "FloatType":
			return FloatSerializer.get();

		case "DoubleType":
			return DoubleSerializer.get();

		case "UTF8Type":
			return StringSerializer.get();

		case "AsciiType":
			return AsciiSerializer.get();

		case "BytesType":
			return ByteSerializer.get();

		case "Int32Type":
			return Int32Serializer.get();

		// case "InetAddressType":

		default:
			try {
				throw new Exception("Unknown serializer type: \"" + instance + "\"");
			} catch (Exception e) {
				System.exit(0);
			}
			return null;
		}

	}
	
	public static Object convertToBytes(String value, String serializerType) {


		switch (serializerType) {
		case "DateType":
			return new Date(Long.parseLong(value));

		case "UUIDType":
			return UUID.fromString(value);

		case "TimeUUIDType":
			return UUID.fromString(value);
		
		case "BooleanType":
			return Boolean.valueOf(value);

		case "IntegerType":
			return Integer.parseInt(value);

		case "LongType":
			return Long.parseLong(value);

		case "FloatType":
			return Float.parseFloat(value);

		case "DoubleType":
			return Double.parseDouble(value);

		case "UTF8Type":
			return value;

		case "AsciiType":
			return value;

		case "BytesType":
			return Bytes.toBytes(value);

		case "Int32Type":
			return Integer.parseInt(value);
		
		default:
			try {
				throw new Exception("Unknown serializer type: \"" + serializerType + "\"");
			} catch (Exception e) {
				System.exit(0);
			}
			return null;
		}

	}

}
