package com.siemens.bam.adm.studio.schema.tableType;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.siemens.bam.adm.studio.schema.DataTypes;
import com.siemens.bam.adm.studio.schema.SuperColumn;
import com.siemens.bam.adm.studio.schema.SuperColumn.SuperColumnType;
import com.siemens.bam.adm.studio.schema.Table;
import com.siemens.bam.adm.studio.schema.TableColumn;
import com.siemens.bam.adm.studio.utils.ClientProperties;

public class SuperColumnTableType extends AbstractTableType {
	final static Logger logger = LoggerFactory.getLogger(SuperColumnTableType.class.getName());

	@Override
	public Object getValueForField(TableColumn entry) throws Exception {

		if (entry instanceof SuperColumn) {
			logger.debug("This column is a SuperColumn");

			SuperColumn col = (SuperColumn) entry;
			SuperColumnType type = col.getScolType();
			List<TableColumn> subColumns = col.getSubColumns();
			Integer datatype = col.getColDataType();
			return generateCollectionValues(type, subColumns, datatype);
		} else {
			return super.getValueForField(entry);
		}
	}

	private Object generateCollectionValues(SuperColumnType type, List<TableColumn> subColumns, Integer datatype)
			throws Exception {

		switch (type) {
		case LIST:
			return generateList(subColumns, datatype);
		case SET:
			return generateSet(subColumns, datatype);
		case MAP:
			return generateMap(subColumns, datatype);
		default:
			throw new Exception("No such Super Column type implemented! ");
		}
	}

	private Object generateMap(List<TableColumn> subColumns, Integer datatype) throws Exception {

		Map<String, Object> values = new LinkedHashMap<String, Object>();
		for (TableColumn col : subColumns) {
			logger.debug("Adding to Map the value for " + col.getColName());
			values.put(col.getColName(), convertToDatatype(super.getValueForField(col), datatype));

		}
		return values;
	}

	private Object generateSet(List<TableColumn> subColumns, Integer datatype) throws Exception {

		Set<Object> values = new LinkedHashSet<Object>();
		for (TableColumn col : subColumns) {
			logger.debug("Adding to Set the value for " + col.getColName());
			values.add(convertToDatatype(super.getValueForField(col), datatype));
		}
		return values;
	}

	private Object generateList(List<TableColumn> subColumns, Integer datatype) throws Exception {

		List<Object> values = new LinkedList<Object>();

		for (TableColumn col : subColumns) {
			logger.debug("Adding to list the value for " + col.getColName());
			values.add(convertToDatatype(super.getValueForField(col), datatype));
		}
		return values;
	}

	@Override
	public void init(Table table, ClientProperties clientProp) throws Exception {
		List<String> rowKeys = table.getRowKeys();

		setUniqueSetOfValues(dataGenerator.generateUniqueData(table, clientProp, rowKeys));

	}

	private Object convertToDatatype(Object val, Integer type) throws Exception {
		DataTypes dType = DataTypes.getDatatypeFromOrdinal(type);
		switch (dType) {
		case DATETIME:
			return Long.valueOf(val.toString());

		case UUID:
			return UUID.fromString(val.toString());

		case DATE:
			SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
			Date date = null;
			try {
				date = formatter.parse(val.toString());
			} catch (ParseException e) {

				logger.error("Error while parsing date " + date);
				return false;
			}
			return date;
		case BOOLEAN:
			return Boolean.valueOf(val.toString());

		case TINYINT:
		case SMALLINT:
		case INT:
		case NULLINT:
			return Integer.valueOf(val.toString());
		case BIGINT:
			return Long.valueOf(val.toString());

		case FLOAT:
			return Float.valueOf(val.toString());

		case DOUBLE:
			return Double.valueOf(val.toString());

		case STRING:
			return val.toString();
		default:
			logger.error("Not yet implemented in convertToDatatype");
			throw new Exception("Not yet implemented in SuperColumnTableType::convertToDatatype");

		}

	}

	/**
	 * 
	 * The column name is built by concatenating data generated for clustering
	 * keys (as defined in JSON schema)
	 * 
	 */
	@Override
	public <T> T buildColumnName(String colName, List<String> names, List<TableColumn> columns) throws Exception {
		int colType;
		Object columnName = null;
		int index = 0;
		TableColumn c = new TableColumn();
		for (String clusterKey : names) {
			c.setColName(clusterKey);

			colType = columns.get(columns.indexOf(c)).getColDataType();

			logger.debug("Cluster Key : " + clusterKey + " type : " + colType + " : "
					+ DataTypes.getDatatypeFromOrdinal(colType));

			columnName = getValueForField(columns.get(columns.indexOf(c)));
			index++;
			if (!(index == names.size()))
				columnName += "+";

		}
		logger.debug("The columnValidator is " + columnName);
		return (T) columnName;
	}

}
