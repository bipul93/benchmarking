package test.com.siemens.bam.adm.studio.clients;

import org.testng.annotations.Test;

import com.siemens.bam.adm.studio.clients.Client;

public class TestClient {
	
  @Test(expectedExceptions =  java.lang.IllegalArgumentException.class)
  public void testPropertyFileLocationException() throws Exception {
	  
	 
		Client.main(new String[] {"arg1", "arg2", "arg3"});
	
  }
	
	@Test(expectedExceptions =  java.lang.IllegalArgumentException.class)
	  public void testReadParameters() throws Exception {
		  
		
			Client.main(new String[] {"-conf", "config\\Property.conf ", "-operation" , "Read"});
		
	  }
}


