package test.com.siemens.bam.adm.studio.schema;

import org.testng.annotations.Test;

import com.siemens.bam.adm.studio.exception.SchemaException;
import com.siemens.bam.adm.studio.schema.JSONSchemaParser;
import com.siemens.bam.adm.studio.utils.ClientProperties;

import net.sf.json.JSONException;

public class TestSchemaParser {
	
  @Test(expectedExceptions =  JSONException.class)
  public void TestWrongJSONFormat() throws JSONException {
	  String jsonTxt = "";
  
	  try {
		JSONSchemaParser.setJSONObject(jsonTxt);
	} catch (JSONException e) {
		throw e;
		}
  }
  
 @Test(expectedExceptions =  { Exception.class, SchemaException.class})
  public void TestWrongDatatypeForAColumn() throws Exception, SchemaException {
	  String jsonTxt =  "{"
				+ " \"Database\" : {"
				+ " \"DBname\" : \"TestDatabase\" ,"
				+ "	\"Tables\" : ["
				+ "		{"
				+ "			\"TableName\" : \"TestTable\","
				+ "			\"RowKey\" : [ \"TestID\",\"TestDate\"],"
				+ "			\"ClusteringKey\" : [\"\"],"
				+ "			\"KeyValidator\" : \"\","
				+ "			\"ColumnValidator\" : \"\","
				+ "			\"TypeOfTable\" : \"Wide\","
				+ "			\"TableColumns\" : ["
				+ "				{ "
				+ "					\"ColumnName\" : \"TestID\", "
				+ "					\"Datatype\" : \"UUID\", "
				+ "				}, "
				+ "				{ "
				+ "					\"ColumnName\" : \"TestDate\", "
				+ "					\"Datatype\" : \"DATETIME\" "
				+ "				}, "
				+ "				{ "
				+ "					\"ColumnName\" : \"TestTime\", "
				+ "					\"Datatype\" : \"DATETIME\" "
				+ "				},"
				+ "				{"
				+ "					\"ColumnName\" : \"TestValue\","
				+ "					\"Datatype\" : \"XYZ\" "    
				+ "				}"
				+ "			] "
				+ "		} "
				+ " 	] "
				+ "   }"
			    + "}";
	  
	  
	  JSONSchemaParser.setJSONObject(jsonTxt);
	  ClientProperties clientProp = new ClientProperties();
	  clientProp.setProperty("isTableReversedOnTime", "false");
	  clientProp.setProperty("datastoreName", "Cassandra");
	  JSONSchemaParser.getDataModel(clientProp );
  }
}
