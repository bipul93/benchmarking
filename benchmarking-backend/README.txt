Benchmarking Studio

An integrated studio that has the ability to generate data for a dynamic schema and configurable workload patterns, thus enabling users to perform
business use-case specific evaluation of datastores while measuring the key performance indicators.


Getting Started: 
----------------------------------------------------------------------------------------------------------------

1. Build and compile the code : 
		a. Download the code.
		b. Do a clean Maven build [mvn clean install]
		
2. Setup the database and table on the datastore to be benchmarked
	
3. Schema File:
	Create a JSON file (in the format given below) that shows the schema for the database and table that was setup in Step (2). Examples of schema files are available in the input folder.
	 
	 DBName - Name of Database that was setup in Step (2)
	 TableName - Name of Table that was setup in Step (2)
	 RowKey - A JSON List of row keys (primary keys)
	 RowKeySeparator - Specify the type of separator(if required) for building Row Keys 
	 ClusteringKey - A JSON List of clustering keys (on which the records are clustered in the datastore) The name is derived from the concept of clustering keys in C* CQL
	 In C* Thrift version this refers to the comparator(~colName) and in HBase it refers to the column name
	 KeyValidator - Datatype of key [DateType, UUIDType, TimeUUIDType, BooleanType, IntegerType, LongType, FloatType, DoubleType, UTF8Type, AsciiType, BytesType, Int32Type]
	 
	 ColumnValidator - Datatype of column name [DateType, UUIDType, TimeUUIDType, BooleanType, IntegerType, LongType, FloatType, DoubleType, UTF8Type, AsciiType, BytesType, Int32Type]
	 
	 TypeOfTable - Tall, Wide or SuperColumn
	 
	 TableColumns - JSONArray of Table columns
	 		ColumnName - Name of column in table
	 		Datatype - The datatype of the column [DATETIME, UUID,BOOLEAN,TINYINT,SMALLINT,INT,	BIGINT,NULLINT,FLOAT,DOUBLE,STRING,	XML,DATE,BYTES,UNKNOWN]
	 		Lists, Sets and Maps can be inserted with Datatype set to LIST:<Datatype>, SET:<Datatype>, MAP:<Datatype>
	 		
	 		UniqueSet - Optional. Set to True if there is a need to insert values from a set of values. These values will be randomly generated and stored at the start of the run and during the insert operations values will be picked from this set randomly.
	 		
	 		Values  - Optional. A JSON List with s set of fixed values. Only these values will be inserted for this column during the run. 
	 		Example:
	 		 		"Values"   : ["TERM_1", "TERM_2","TERM_3","TERM_4"]
	 		
	 		Subcolumns - This is required for a supercolumn type of table. JSON Array of table columns similar to Table Columns
	 		

An example schema is shown below:
	 
	 {
	"Database" : {
		"DBname" : "TLG_V_4" ,
		"Tables" : [
			{
				"TableName" : "TagsWideCF",
				"RowKey" : [ "TagUID","TagDate"],
				"ClusteringKey" : ["TagTimeStamp"],
				"KeyValidator" : "",
				"ColumnValidator" : "",
				"TypeOfTable" : "Wide",
				"TableColumns" : [
					{
						"ColumnName" : "TagUID",
						"Datatype" : "UUID",
						"UniqueSet" : "True"
					}
					,
					{
						"ColumnName" : "TagDate",
						"Datatype" : "DATETIME" 
					},
				
					{
						"ColumnName" : "TagTimeStamp",
						"Datatype" : "DATETIME" 
					},
					{
						"ColumnName" : "ValueFloat",
						"Datatype" : "DOUBLE" 
					}
					
				]
			} 
		]
	}
}
	 
4. Configure the run parameters in Property.conf file located in conf folder

5. To run the tool:
	cd BenchmarkingStudio
	bin\runClient.bat <path to Property.conf file> <operation to be performed> <options>
	
	Operation to be performed:
		Read - to read from the data store
		Write - to insert records into the data store
		CollectMetrics - to collect and store metrics in a CSV for a specified duration of time. This is mostly applicable for previously run workloads.
		
	Options:
	For a read operation the following options are mandatory:
		mode - Interactive (to run queries on the console)
			 - Batch (to run queries from a file)
	   
	For a CollectMetrics operation:
		startTime - Start time for metric collection [yyyy-MM-dd'T'HH:mm:ss.SSS]
		endTime - End time for metric collection [yyyy-MM-dd'T'HH:mm:ss.SSS]
		
		
eg: bin\runClient.bat BenchmarkingStudio\config\Property.conf read interactive
	bin\runClient.bat BenchmarkingStudio\config\Property.conf read batch BenchmarkingStudio\FileInput.txt


