$(document).ready(function(){
    var localAddress = getLocalIPAddress();
    //sendIPAddressToServer(localAddress);
    /*var serverAddress = localStorage.getItem("SERVER_IP");
    console.log(serverAddress);*/

    $("#editServerAddressButton").on('click',function(){
    	//console.log($("#editServerAddress").is(':visible'));
    	if($("#editServerAddress").is(':visible')){
    		$("#editServerAddress").css('display','none');
    		$("#editServerAddressButton").html("Edit Server IP");
    	}else{
    		$("#editServerAddress").css('display','inline-block');
    		$("#editServerAddressButton").html("Save Server IP");
    	}
    });

    if(localStorage.getItem("SERVER_IP") == null){
    	$("#serverIPTextBox").val(serverIPAddress);
    }else{
    	serverIPAddress = localStorage.getItem("SERVER_IP");
    	$("#serverIPTextBox").val(localStorage.getItem("SERVER_IP"));
    }
    $("#goToClientScreen").on('click',function(){
    	serverIPAddress = $("#serverIPTextBox").val();
    	localStorage.setItem("SERVER_IP",serverIPAddress);
    });

    function getLocalIPAddress(){
        var ipAddress = '';
            window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;   //compatibility for firefox and chrome
        var pc = new RTCPeerConnection({iceServers:[]}), noop = function(){};
        pc.createDataChannel("");    //create a bogus data channel
        pc.createOffer(pc.setLocalDescription.bind(pc), noop);    // create offer and set local description
        pc.onicecandidate = function(ice){  //listen for candidate events
            if(!ice || !ice.candidate || !ice.candidate.candidate)  return;
            var myIP = /([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/.exec(ice.candidate.candidate)[1];
            console.log('IP = ', myIP);
            ipAddress = myIP;
            pc.onicecandidate = noop;
        }

        if(ipAddress == ''){
            return null;
        }else{
            return ipAddress;
        }
    }

    function sendIPAddressToServer(data){
        $.ajax({
            type:'GET',
            dataType: 'jsonp',
            data: data,
            url:'http://'+serverIPAddress+'/activateMe',//tomcat server address
            success: function(data){
                console.log(data);

            },
            failure:function(error){
                console.log(error);
            }
        });
    }
});
