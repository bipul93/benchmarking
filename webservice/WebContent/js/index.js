var dragonElement;
$(document).ready(function(){
	var breadCrumbsCount = 0;
	serverIPAddress = localStorage.getItem("SERVER_IP");
//    $(".loaderContainer").css('display','block');
//    setTimeout(function(){
//        if($(".loaderContainer").is(":visible")){
//            $(".loaderContainer").css('display','none');
//            alert("Connection to Server Failed!");
//        }
//    },2000);

	$("#projectTestConnection").css('display','none');
    var dragon = dragula([
            document.getElementById('box'),
            document.getElementById('read-dropzone'),
            document.getElementById('write-dropzone')
        ],{
        revertOnSpill: true,
        removeOnSpill: false,
        mirrorContainer: document.body,
        copySortSource: true,
        ignoreInputTextSelection: true

      }).on('drag', function (el) {
        el.className = el.className.replace('ex-moved', '');
        //console.log($(el).attr('id'));
        //testConnection($(el).attr('id'));
      }).on('drop', function (el, target, source) {
        el.className += ' ex-moved';
        //console.log($(el).attr('class'));
        /*if($(el).attr('class').indexOf('clientNotActive') != -1){
        	dragon.cancel('revert');
        }*/
        //var moveDirection = $(target).attr('id') !== "box";
        var moveDirection = $(source).attr('id') === "box" //added by bipul
        if(testFlag && moveDirection){
        	testFlag = false;
            dragonElement = el;
        	testConnection($(el).attr('id'));
        }
        if(!checkValues()){ //added by bipul
            $('#firstModal .nextModalScreen').prop('disabled', true);
        }
        //on test fail revert drag n drop
      }).on('over', function (el, container) {
        container.className += ' ex-over';
      }).on('out', function (el, container) {
        container.className = container.className.replace('ex-over', '');
        //console.log($(el).attr('id'));
      });

    getJSONValues();
    getDevices();
    getProject();

    //added by bipul,
    $('[data-toggle="tooltip"]').tooltip();
    //added by bipul, no special chracter, just assign noSpecialChar class to input box
    $('input[type="text"].noSpecialChar').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });
    //added by bipul
    function ValidateIPaddress(ipaddress) {
        if (/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/.test(ipaddress)) {
            return (true);
        }else if(ipaddress === ""){ return (true); }
        swal("Invalid IP address", "Please enter ip address in correct format. For multiple ips, use comma.","error")
        //swal.showInputError("Invalid email!");
        return (false);
    }
    $('.ipAdddressInput').blur(function(e){
        var ele = $(this);
        var addresses = $(ele).val();
        addresses = addresses.split(',');
        $.each(addresses, function( index, value) {
            if(!ValidateIPaddress(value)){
                $(ele).css("border-color", "#dc5654");
                return;
            }else{
                $(ele).css("border-color", "initial");
            }

        });
    });

    $('#readWriteTabs a').click(function (e) {
        //$(".rwMetricsDisplaySpan").html(" ");
        //$(this).tab('show');
    });

	$("#projectTestConnection").on("click",function(){
		testConnection();
	});

    $("#metricCollectionEnable").change(function(e){

    	if($(this).is(":checked")){
    		//$("#metricsCollectionsEdit").css("display","block");
    	}else{
    		//$("#metricsCollectionsEdit").css("display","none");
    	}
    });

    $('.testConnection').click(function(e){
        testConnection();
    });

    $('.dbType').change(function(){
        switch(this.value){
            case 'cassandra':
                displayCassandraProperties();
                break;
            case 'hbase':
                displayHbaseProperties();
                break;
            default:
        }
    });

	$('#newProjectModal').on('shown.bs.modal', function (e) {
		breadCrumbsCount = 1;
		displayCassandraProperties();
		//$('.breadcrumb').html('');
		//$('.breadcrumb').append("<li class='projectCrumb' ><a href='#'>Project</a></li>");
		crumbsNavigation();
		if(projectModal){
			//$("#projectTestConnection").css('display','inline-block');
            swal({
                title: "Testing connection...",
                text: "",
                html: true,
                imageUrl:'./images/loader.gif',
                imageSize: "80x80",
                showConfirmButton: false,
                allowEscapeKey: false,
            });
			$('.nextModalScreen').prop('disabled', true);
            $('#read-dropzone .client').each(function( index ) {
                dragonElement = $(this);
        	    testConnection($(dragonElement).attr('id'));
            });
            $('#write-dropzone .client').each(function( index ) {
                dragonElement = $(this);
        	    testConnection($(dragonElement).attr('id'));
            });

		}else{
			$("#projectTestConnection").css('display','none');
			$('.nextModalScreen').prop('disabled', true);
		}
		//
			/* The dragging code for '.draggable' from the demo above
		 * applies to this demo as well so it doesn't have to be repeated. */

		/*var dragon = dragula([
				document.getElementById('box'),
				document.getElementById('read-dropzone'),
				document.getElementById('write-dropzone')
		],{
			revertOnSpill: true,
  			mirrorContainer: document.body,
            copySortSource: true,
  			ignoreInputTextSelection: true

		  }).on('drag', function (el) {
			   el.className = el.className.replace('ex-moved', '');
		  }).on('drop', function (el) {
			   el.className += ' ex-moved';
		  }).on('over', function (el, container) {
			   container.className += ' ex-over';
		  }).on('out', function (el, container) {
			   container.className = container.className.replace('ex-over', '');
		  });*/

	});

	/*
	 *
	 *
	 */



    $('#newProjectModal').on('hide.bs.modal',function(e){
        $('#read-dropzone').html('');
    	$('#write-dropzone').html('');
    	getJSONValues();
    	refreshClientDevicesData();
    	goToFirstScreen();
    	projectModal = false;
    });

    $('#dbFileUploadBtn').on('click',function(e){
        $('#dbFileName').trigger('click');
    });

	/*
	*/

    function onReaderLoad(event){
		var append = "";
        try{
	        var obj = JSON.parse(event.target.result);
	        //console.log(obj.Database.DBname);
	
			append += "<div class='border-solid-2' style='margin:2px;padding:5px;'><table class='uploadSchemaInput' style='width:100%;'>"+
			          "<tr><td><label for='dbName' >Database Name :</label></td><td><input id='dbName' type='text' value="+obj.Database.DBname+" disabled /></td></tr>"+
			          "<tr><td></td><td><a style='text-decoration:underline;float:right;' onClick='addTableToForm()'>add table</a></td></tr>"+
			          "</table>";
			
	        var tablesArray = obj.Database.Tables;
	        for(var i=0;i<tablesArray.length;i++){
				var table = tablesArray[i];
				append +="<div id="+i+" ><table class='tables_table'>"+
							"<tr><td><label >Table Name :</label></td><td><input class='tblName' type='text' value="+table.TableName+" /></td></tr>"+
						    "<tr><td><label >Row Key :</label></td><td>" +
						    "<select class='rowKey commandInput' type='text' multiple='multiple' size='2' >";
				
				if(table.RowKey.length == 0){
			        append += "<option value='TagUID' selected='' >TagUID</option>"+
			        		"<option value='TagDate' selected='' >TagDate</option>";
				}else if(table.RowKey.length == 1){
					if(table.RowKey.conains('TAGUID')){
						append += "<option value='TagUID' selected='selected' >TagUID</option>"+
		        		"<option value='TagDate' selected='' >TagDate</option>";
					}else{
						append += "<option value='TagUID' selected='' >TagUID</option>"+
		        		"<option value='TagDate' selected='selected' >TagDate</option>";
					}
				}else{
					append += "<option value='TagUID' selected='selected' >TagUID</option>"+
	        		"<option value='TagDate' selected='selected' >TagDate</option>";
				}
				var clusteringKeys = "";
				for(var k=0;k<table.ClusteringKey.length;k++){
					clusteringKeys += table.ClusteringKey[k];
					if(k != (table.ClusteringKey.length - 1)){
						clusteringKeys += ",";
					}
				}
				
				append += "</select>"+
						    "</td></tr>"+
						    "<tr><td><label >ClusteringKey :</label></td><td><input class='clusteringKey ' type='text' value="+clusteringKeys+" /></td></tr>"+
						    "<tr><td><label >KeyValidator :</label></td><td><input class='keyValidator' type='text' value="+JSON.stringify(table.KeyValidator)+" /></td></tr>"+
						    "<tr><td><label >ColumnValidator :</label></td><td><input class='columnValidator' type='text' value="+JSON.stringify(table.ColumnValidator)+" /></td></tr>"+
						    "<tr><td><label >Type of Table :</label></td><td><input class='typeOfTable' type='text' value="+JSON.stringify(table.TypeOfTable)+" /></td></tr>"+
						    "<tr><td></td><td><a style='text-decoration:underline;float:right;' onClick='addColumnToForm("+i+")'>add column</a></td></tr></table>";
				append += "<table class='column_table'>"; 
	        	var columnsArray = table.TableColumns;
	        	for(var j=0; j<columnsArray.length;j++){
	        		var column = columnsArray[j];
	        		//
	        		console.log(column);
					append += "<tr><td><label>Column Name :</label></td><td><input class='ColName' value="+column.ColumnName+" type='text' /></td>"+
					          "<td><label>Data Type :</label></td><td>" +
					          "<select id='"+i+"_column_"+j+"_datatype' class='dataType commandInput' type='text' >"+
					          "<option value='BIGINT' >BIGINT</option>"+
					          "<option value='BOOLEAN' >BOOLEAN</option>"+
					          "<option value='BYTES' >BYTES</option>"+
					          "<option value='DATE' >DATE</option>"+
					          "<option value='DATETIME' >DATETIME</option>"+
					          "<option value='DOUBLE' >DOUBLE</option>"+
					          "<option value='FLOAT' >FLOAT</option>"+
					          "<option value='INETADDRESS' >INETADDRESS</option>"+
					          "<option value='INT' >INT</option>"+
					          "<option value='NULLINT' >NULLINT</option>"+
					          "<option value='SMALLINT' >SMALLINT</option>"+
					          "<option value='STRING' >STRING</option>"+
					          "<option value='TINYINT' >TINYINT</option>"+
					          "<option value='UUID' >UUID</option>"+
					          "</select>"+
					          "</td></tr>";
	        	}
				append += "</table></div>";
	        }
	        append +="</div>";
	        $("#uploadedSchemaInfo").html(append);
	        for(var i=0;i<tablesArray.length;i++){
	        	var columnsArray = tablesArray[i].TableColumns;
	        	for(var j=0; j<columnsArray.length;j++){
	        		var column = columnsArray[j];
	        		var selectSelector = '#' + i + '_column_' + j + '_datatype';
	        		$(selectSelector).val(column.Datatype);
	        	}
	        }
		}catch(ex){
			swal({
                title: "Error parsing JSON file",
                text: "The file is either not a JSON or the JSON file is not in proper format",
                type: "error"
            });
		}
        
    }

    $('#dbFileName').on('change',function(e){
    	var reader = new FileReader();
        reader.onload = onReaderLoad;
        reader.readAsText(event.target.files[0]);
    });

	function crumbsNavigation(){
		$('.breadcrumb li').on('click',function(){
	      //console.log($(this).attr('class'));
            //$('.breadcrumb li').removeClass('active');
	      if($(this).attr('class').indexOf('active') == -1){
	        //console.log($(this).attr('class'));

	        if($(this).attr('class').indexOf('project') != -1){
                goToFirstScreen();
                //$('.breadcrumb li.projectCrumb').addClass('active');
	        }else if($(this).attr('class').indexOf('configure') != -1){
                goToSecondScreen();
                //$('.breadcrumb li.configureCrumb').addClass('active');
	        }else if($(this).attr('class').indexOf('database') != -1){
                goToThirdScreen();
                //$('.breadcrumb li.databaseCrumb').addClass('active');
	        }else if($(this).attr('class').indexOf('operation') != -1){
                goToFourthScreen();
                //$('.breadcrumb li.operationCrumb').addClass('active');
	        }else if($(this).attr('class').indexOf('metrics') != -1){
                goToFifthScreen();
                //$('.breadcrumb li.metricsCrumb').addClass('active');
	        }
	       }
		});
	}

    $('.prevModalScreen').on('click',function(){
        //console.log("" + $(this).parent().parent().attr('id'));

        if($(this).parent().parent().attr('id') == "fifthModal"){
            goToFourthScreen();
        }else if($(this).parent().parent().attr('id') == "fourthModal"){
            goToThirdScreen();
        }else if($(this).parent().parent().attr('id') == "thirdModal") {
            goToSecondScreenBack();
        }else if($(this).parent().parent().attr('id') == "secondModal"){
            goToFirstScreen();
        }else{

        }
    });

    $(".goToSummary").on('click',function(){
    	goToSummary();
    });

    $('.nextModalScreen').on('click',function(){
        //console.log("" + $(this).parent().parent().attr('id'));
        //change first second etc to resp modal names

        //$('.breadcrumb li').removeClass('active');
        if($(this).parent().parent().attr('id') == "firstModal"){

            goToSecondScreen();
            if(breadCrumbsCount == 1){
            	breadCrumbsCount++;
	            //$('.breadcrumb').append("<li class='configureCrumb' ><a href='#'>Configure</a></li>");
	        	crumbsNavigation();
            }
            //('.breadcrumb li.configureCrumb').addClass('active');
        }else if($(this).parent().parent().attr('id') == "secondModal"){
        	//getDatabaseConfigValues();
        	saveCassandraProperties();
            goToThirdScreen();
            if(breadCrumbsCount == 2){
            	breadCrumbsCount++;
	            //$('.breadcrumb').append("<li class='databaseCrumb' ><a href='#'>Database</a></li>");
	        	crumbsNavigation();
            }
            //$('.breadcrumb li.databaseCrumb').addClass('active');
        }else if($(this).parent().parent().attr('id') == "thirdModal") {
            goToFourthScreen();
            if(breadCrumbsCount == 3){
            	breadCrumbsCount++;
	            //$('.breadcrumb').append("<li class='operationCrumb' ><a href='#'>Operation</a></li>");
	        	crumbsNavigation();
            }
            //$('.breadcrumb li.operationCrumb').addClass('active');
        }else if($(this).parent().parent().attr('id') == "fourthModal"){
            goToFifthScreen();
            if(breadCrumbsCount == 4){
	            breadCrumbsCount++;
	            //$('.breadcrumb').append("<li class='metricsCrumb' ><a href='#'>Metrics</a></li>");
	        	crumbsNavigation();
            }
            //$('.breadcrumb li.metricsCrumb').addClass('active');
        }else{
            goToSummary();
        }
    });

    /*$('#hostIPVal').change(function(e){
        globalData.Hosts = $('#hostIPVal').val();
        setJSONValues(globalData);
    });

    $('#dbProp1Val').change(function(e){
       if($('#cassandraRadioButton').is(':checked')){

       }else{

       }
    });*/

    function getDatabaseConfigValues(){

    }

    function testConnection(deviceIP){
    	console.log(deviceIP);
        var databaseType = $('.dbType:checked').val();
        $(".loaderContainer").css("display","block");
        $(".loaderContainer .message").text("Connecting to "+deviceIP);
        var postData = {
            "deviceIP": deviceIP,
            "dbType":databaseType,
            "selectedReadClients": JSON.stringify(globalRC),
            "selectedWriteClients": JSON.stringify(globalWC),
            "data":JSON.stringify(globalData)
        };
        if(!projectModal){
            swal({
                title: "Connecting to "+deviceIP,
                html: true,
                imageUrl:'./images/loader.gif',
                imageSize: "80x80",
                showConfirmButton: false,
                allowEscapeKey: false,
            });
        }else{
            $('.sweet-alert p').append('<div data-address="'+deviceIP+'">'+deviceIP+'</div>');
        }
        $.ajax({
            type: 'POST',
            dataType: 'jsonp',
            url: 'http://'+serverIPAddress+':8080/benchmarkingServer/testConnection',
            data: postData,
            success: function(data) {
                getTestResults(deviceIP);
            },
            error: function (error) {
                if(!projectModal){
                    swal({title: "Connection failed",
                      text: "Connection to the server failed",
                      type: "error"
                    });
                }
                if(dragonElement !== undefined){
                    //dragon.start(dragonElement);
                    //dragon.remove();
                    $(dragonElement).detach().appendTo('#box');
                    dragonElement = undefined;
                }
            }
        });



    }

    function getTestResults(deviceIP){
    	setTimeout(function(){
    		$.ajax({
                type: 'GET',
                dataType: 'jsonp',
                url: 'http://'+serverIPAddress+':8080/benchmarkingServer/getTestConnectionResult',
                success: function(data) {
                	console.log(data);
                	testFlag = true;
                	if(data == undefined){
                		//dragon.cancel('revert');
                		//dragon.remove();
                        if(!projectModal && dragonElement !== undefined){
                            //dragon.start(dragonElement);
                            //dragon.remove();
                            $(dragonElement).detach().appendTo('#box');
                            dragonElement = undefined;

                            swal({title: "Connection failed",
                              text: "Client not connected",
                              type: "error"
                            });
                        }else{
                            $('.sweet-alert p').find('div[data-address="'+deviceIP+'"]').html('Connection failed for: '+deviceIP);
                            swal({title: "Connection failed",
                              text: "Client not connected",
                              type: "error"
                            });
                        }

                		$(".loaderContainer").css("display","none");
                        //swal.close();

                		$('.nextModalScreen').prop('disabled', true);
                		$('#testWhileDragResult').css('display','none');
                	}else{
                		$(".loaderContainer").css("display","none");
                        if(!projectModal){
                            swal.close();
                        }else{
                            $('.sweet-alert p').find('div[data-address="'+deviceIP+'"]').html('Connection success to: '+deviceIP);
                            swal.close();
                        }
                		$('#testWhileDragResult').css('display','inline-block');
                		$('.nextModalScreen').prop('disabled', false);

                		setTimeout(function(){
                			$('#testWhileDragResult').css('display','none');
                		},3000);

                	}
                  //console.log(data.result.value);
                	/*if(data.result.value == ""){
                		$(".loaderContainer").css("display","none");
                        //$("#clientsTestBlock div i").css('display','none');
                        alert("connection failed");
                	}else{
                      //
                      var clientInfo = data.result.value.split('&')[1].split('=')[1];

                      $("#clientsTestBlock").children().each(function(){
                          //console.log($(this).attr('id').split('-')[0]);
                          var clientIP = $(this).attr('id').split('-')[0];

                          //console.log(clientInfo);
                          //console.log(clientIP);
                          console.log($(this).children());
                            if(clientInfo == clientIP){
                              $(".loaderContainer").css("display","none");
                              //var localID = ""+$(this).attr('id')+" i";
                              //console.log(localID);
                              //$("#"+localID).css('display','inline-block');
                              $(this).append("<i class='fa fa-check ' aria-hidden='true' style='padding-left: 10px;color:#93DC69;display:inline-block;' ></i>");
                            }else{

                              //$(this).children('i').css('display','none');
                              console.log("not connected");
                            }
                      });
                  }*/
                },
                error: function (error) {
                	$(".loaderContainer").css("display","none");
                    swal({title: "Connection failed",
                          text: "Connection to the server failed",
                          type: "error"
                    });
                    if(dragonElement !== undefined){
                        //dragon.start(dragonElement);
                        //dragon.remove();
                        $(dragonElement).detach().appendTo('#box');
                        dragonElement = undefined;
                    }
                }
             });
    	},3000);
    }

    function saveCassandraProperties(){
    	globalData.Hosts = $('#hostIPVal').val();
    	globalData.Cassandra.ClusterName = $('#dbProp1Val').val();
        globalData.Cassandra.ConnectionPool = $('#dbProp2Val').val();
        globalData.Cassandra.Connectionretries = $('#dbProp3Val').val();
        globalData.Cassandra.Operationretries = $('#dbProp4Val').val();
        globalData.Cassandra.ThriftPort = $('#dbProp5Val').val();
        globalData.Cassandra.NativePort = $('#dbProp6Val').val();
        globalData.Cassandra.MaxConnPerHost = $('#dbProp7Val').val();
        globalData.Cassandra.TargetCassandraVersion = $('#dbProp8Val').val();
        globalData.Cassandra.ReadConsistencyLevel = $('#dbProp9Val').val();
        globalData.Cassandra.WriteConsistencyLevel = $('#dbProp10Val').val();
        globalData.Cassandra.OpscenterIP = $('#dbProp11Val').val();
        globalData.Cassandra.OpscenterPort = $('#dbProp12Val').val();
        globalData.Cassandra.Datacenter = $('#dbProp13Val').val();
        globalData.Cassandra.ClusterID = $('#dbProp14Val').val();
        console.log(globalData);
    }

    function displayCassandraProperties() {

        $('#cassandraRadioButton').attr('checked', true);

        for(var i=3;i<15;i++){
            var labelTag = '#dbProp'+i+'Label';
            var inputTag = '#dbProp'+i+'Val';
            $(labelTag).css('visibility','visible');
            $(inputTag).css('visibility','visible');
        }

        $('#dbProp1Label').text(' Cluster Name ');
        $('#dbProp2Label').text(' Connection Pool ');

        $('#hostIPVal').val(globalData.Hosts);
        $('#dbProp1Val').val(globalData.Cassandra.ClusterName);
        $('#dbProp2Val').val(globalData.Cassandra.ConnectionPool);
        $('#dbProp3Val').val(globalData.Cassandra.Connectionretries);
        $('#dbProp4Val').val(globalData.Cassandra.Operationretries);
        $('#dbProp5Val').val(globalData.Cassandra.ThriftPort);
        $('#dbProp6Val').val(globalData.Cassandra.NativePort);
        $('#dbProp7Val').val(globalData.Cassandra.MaxConnPerHost);
        $('#dbProp8Val').val(globalData.Cassandra.TargetCassandraVersion);
        $('#dbProp9Val').val(globalData.Cassandra.ReadConsistencyLevel);
        $('#dbProp10Val').val(globalData.Cassandra.WriteConsistencyLevel);
        $('#dbProp11Val').val(globalData.Cassandra.OpscenterIP);
        $('#dbProp12Val').val(globalData.Cassandra.OpscenterPort);
        $('#dbProp13Val').val(globalData.Cassandra.Datacenter);
        $('#dbProp14Val').val(globalData.Cassandra.ClusterID);
    }

    function displayHbaseProperties() {
        for(var i=3;i<15;i++){
            var labelTag = '#dbProp'+i+'Label';
            var inputTag = '#dbProp'+i+'Val';
            $(labelTag).css('visibility','hidden');
            $(inputTag).css('visibility','hidden');
        }

        $('#dbProp1Label').text(' Hbase.Durability ');
        $('#dbProp2Label').text(' Hbase.Client.Retries.Number ');

        $('#hostIPVal').val(globalData.Hosts);
        $('#dbProp1Val').val(globalData.Hbase.Durability);
        $('#dbProp2Val').val(globalData.Hbase.Client.Retries.Number);

    }

    function goToSecondScreen() {
        //toggles the modal screen to second screen

        if(checkValues()){
            var projectName = $("#projectNameInput").val();
            if(projectName !== ""){
                selectedValues();
                $('#firstModal').toggle(false);
                $('#thirdModal').toggle(false);
                $('#fifthModal').toggle(false);
                $('#fourthModal').toggle(false);
                $('#secondModal').toggle(true);
            }else{
                swal({title: "Missing field",
                      text: "Please provide project's name",
                      type: "info"
                });
            }
        }else{
            swal({title: "Clients missing",
                  text: "Please select read/write clients",
                  type: "error"
            });
        }
    }


    function goToSecondScreenBack() {
        //toggles the modal screen to second screen

            $('#firstModal').toggle(false);
            $('#thirdModal').toggle(false);
            $('#fifthModal').toggle(false);
            $('#fourthModal').toggle(false);
            $('#secondModal').toggle(true);

    }

    function goToFirstScreen() {
        //toggles the modal screen to first screen

    	globalRC = [];
    	globalWC= [];

        $('#secondModal').toggle(false);
        $('#thirdModal').toggle(false);
        $('#fifthModal').toggle(false);
        $('#fourthModal').toggle(false);
        $('#firstModal').toggle(true);
    }

    function goToThirdScreen(){

        //toggles the modal screen to third screen
        $('#firstModal').toggle(false);
        $('#secondModal').toggle(false);
        $('#fifthModal').toggle(false);
        $('#fourthModal').toggle(false);
        $('#thirdModal').toggle(true);


    }


    function goToFourthScreen(){
        //toggles the modal screen to third screen

        $('#firstModal').toggle(false);
        $('#secondModal').toggle(false);
        $('#thirdModal').toggle(false);
        $('#fifthModal').toggle(false);
        $('#fourthModal').toggle(true);
    }

    function goToFifthScreen(){
        //toggles the modal screen to third screen

        $('#firstModal').toggle(false);
        $('#secondModal').toggle(false);
        $('#thirdModal').toggle(false);
        $('#fourthModal').toggle(false);
        $('#fifthModal').toggle(true);
    }


    function goToSummary(){
        //save project and then open summary page
        saveProject();
    }

    function saveProject(){
        var projectName = $("#projectNameInput").val();
        var rMetrics = [];
        var wMetrics = [];
        if(globalRC.length == 0 && globalWC.length == 0){
        	swal({title: "Incomplete data",
                  type: "error"
                });
        }else{
        	if(globalRC.length == 0){
        		globalRC.push[''];
        	}else if(globalWC.length == 0){
        		globalRC.push[''];
        	}

        	var databaseName = $("#dbName").val();
        	var tables = $(".tblName").val();
        	var columns = $('.ColName').val();

        	if(databaseName != ''){
        		rMetrics.push(databaseName);
        		wMetrics.push(databaseName);
        		rMetrics.push(tables);
        		wMetrics.push(tables);
        		rMetrics.push(columns);
        		wMetrics.push(columns);
        	}

	        var data = {
	            "name":projectName,
	            "selectedReadClients": globalRC,
	            "selectedWriteClients": globalWC,
	            "dataSet": JSON.stringify(globalData),
	            "readMetrics":rMetrics,
	            "writeMetrics":wMetrics,
	            "schema":getSchemaValues()
	        };
	        localStorage.setItem('cassandraProperties',JSON.stringify(globalData));
	        $.ajax({
	           type: 'POST',
	           dataType: 'jsonp',
	           url: 'http://'+serverIPAddress+':8080/benchmarkingServer/saveProject',
	           data: data,
	           success: function(data) {
	               console.log("save project status " + data);
	               //on success go to summary page
	               window.location = "summary.html";
	           },
	           error: function (error) {
	               console.log(error);
	           }
	        });
        }
    }

    function getProject(){
        $.ajax({

           type: 'GET',
           dataType: 'jsonp',
           url: 'http://'+serverIPAddress+':8080/benchmarkingServer/getProject',
           success: function(data) {
               console.log(data);
               globalProjects = data;
               buildProjects(data);
            },
           failure: function (error) {
               console.log(error);
           }
        });
    }

    function buildProjects(projects){
        var str = "";
        var allStr = "";
        var count = 0;
        globalProjects = projects;
        for(var key in projects){
        	count++;
        	if(count == 8) break;
            str += "<div class='projectLoader client' id='"+key+"' data-toggle='modal' data-target='#newProjectModal'  > <i class='fa fa-folder-open-o' aria-hidden='true'></i><div>"+key+"</div> </div>";
        }

        for(var key in projects){
        	allStr += "<div class='projectLoader client' id='"+key+"' data-toggle='modal' data-target='#newProjectModal'  > <i class='fa fa-folder-open-o' aria-hidden='true'></i><span class='fa fa-trash deleteProject '></span><div>"+key+"</div></div>";
        }

        /*$('.u_link').on('click',function(){

        });*/

        $("#allProjectsDisplay").html(allStr);
        $("#clientProjects").html(str);
        projectOnClickListener(count, projects);

        $(".projectLoader span.deleteProject").on('click',function(event){
            event.stopPropagation();
            event.preventDefault();
            deleteProject($(this).parent().attr('id'));
        });

    }

    function deleteProject(clientName){
    	//console.log(clientName);

    	var postData = { "clientName": clientName };
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this project!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        },
        function(){
            $.ajax({
                type: 'POST',
                dataType: 'jsonp',
                url: 'http://'+serverIPAddress+':8080/benchmarkingServer/deleteProject',
                data: postData,
                success: function(data) {
                    //alert(data.result);
                    location.reload();
                    //swal("Deleted!", "Your project has been deleted.", "success");
                },
                error: function (error) {
                   swal("Failed!", "Project deletion failed", "error");
                }
             });

        });

    }

    function projectOnClickListener(id,data){
        //

        $(".projectLoader").on('click',function(e){
        	e.preventDefault();
        	projectModal = true;
			//$("#projectTestConnection").css('display','inline-block');
        	console.log($(this).attr('id'));
        	for(var key in globalProjects){
        		if(key == $(this).attr('id')){
        			var currentProject = globalProjects[key];
        			globalData = currentProject.dataSet;
        		}
        	}
        	$('#projectNameInput').val($(this).attr('id'));
        	var appendToReadBox = '';
        	var appendToWriteBox = '';
        	//open modal with pre loaded data
        	$('.nextModalScreen').prop('disabled', false);
        	$('.box').html('');
        	for(key in data){
        		if(key.trim() == $(this).attr('id')){
        			//console.log(data[key]);
        			var innerData = data[key];
        			for(innerKey in innerData){
        				//console.log(innerData[innerKey]);
        				if(innerKey == 'readClients' ){

        					var values = innerData[innerKey].split('-');
        					//console.log(values + " readclients ");
        					//console.log(values.split('-'));
        					for(var i=0;i<values.length;i++){
        						if(values[i] != "" && values[i] != "null" && values[i] != null){
        							var stringValue = values[i].toString();
        							appendToReadBox += "<div class='client clientActive' name='"+stringValue+"' id='"+stringValue+"'><i class='fa fa-laptop' aria-hidden='true'></i><span>"+stringValue+"</div>";
        							break;
        						}
        					}
        					break;
        				}
        			}
        			for(innerKey in innerData){
        				//console.log(innerData[innerKey]);
        				if(innerKey == 'writeClients' ){
        					var values = innerData[innerKey].split('-');
        					//console.log(values.split('-'));
        					for(var i=0;i<values.length;i++){
        						if(values[i] != "" && values[i] != "null" && values[i] != null){
        							//console.log(values[i]);
        							var stringValue = values[i].toString();
        							appendToWriteBox += "<div class='client clientActive' name='"+stringValue+"' id='"+stringValue+"'><i class='fa fa-laptop' aria-hidden='true'></i><span>"+stringValue+"</div>";
        							break;
        						}
        					}
        					break;
        				}
        			}
        			//appendToReadBox += "<div class='client clientActive' name='"+key+"' id='"+data[key].readClients+"'><i class='fa fa-laptop' aria-hidden='true'></i><span>"+key+"</div>";
        			break;
        		}
        	}
        	$('#read-dropzone').html(appendToReadBox);
        	$('#write-dropzone').html(appendToWriteBox);
        	console.log($('.nextModalScreen').prop('disabled'));
        	$('.nextModalScreen').prop('disabled', false);
        	//$('#testWhileDragResult').css('display','inline-block');
        	//rebuild
        });
    }

    function checkValues(){
        //check if read/write has elements
        var readClientCount = $('#read-dropzone .client').length;
        var writeClientCount = $('#write-dropzone .client').length;
        if(readClientCount > 0 || writeClientCount > 0) {
            return true;
        }else{
            return false;
        }
    }

    function selectedValues(){
    	console.log(projectModal);
        var readClients = '';
        var writeClients = '';
        var allClients = '';
        var readClientsLength = $("#read-dropzone").children().length;
        var writeClientsLength = $("#write-dropzone").children().length;
        console.log(readClientsLength+" -> "+writeClientsLength);
        if(readClientsLength == 0 && writeClientsLength > 0){
            $('#readTab').removeAttr("data-toggle");
            $('#writeTab').attr("data-toggle", "tab");
            $('#writeTab').trigger('click');
        }else if(writeClientsLength == 0 && readClientsLength > 0){
            $('#writeTab').removeAttr("data-toggle");
            $('#readTab').attr("data-toggle", "tab");
            $('#readTab').trigger('click');
        }else{
            $('#readTab').attr("data-toggle", "tab");
            $('#writeTab').attr("data-toggle", "tab");
            $('#readTab').trigger('click');
        }

        $("#read-dropzone").children().each(function(){
            globalRC.push($(this).attr('id'));
            readClients = readClients + "<div id='"+$(this).attr('id')+"-readClients' class='readMetricsOfSelectedClients client' style='z-index:100;margin:5px;display:inline-block;' ><i class='fa fa-laptop' aria-hidden='true' style='display:block;' ></i><span>"+$(this).attr('name')+"</span></div>";
            allClients = allClients + "<div id='"+$(this).attr('id')+"-allClients' style='margin-top:10px;margin-bottom:10px;' ><span>"+ $(this).attr('name') +"</span></div>";
        });

        $("#write-dropzone").children().each(function(){
            globalWC.push($(this).attr('id'));
            writeClients = writeClients + "<div id='"+$(this).attr('id')+"-writeClients' class='writeMetricsOfSelectedClients client' style='z-index:100;margin:5px;display:inline-block;' ><i class='fa fa-laptop' aria-hidden='true' style='display:block;' ></i><span>"+$(this).attr('name')+"</span></div>";
            allClients = allClients + "<div id='"+$(this).attr('id')+"-allClients' style='margin-top:10px;margin-bottom:10px;' ><span>"+ $(this).attr('name') +"</span></div>";
        });

        $("#readClientsBox3").html(readClients);
        $("#writeClientsBox3").html(writeClients);
        //if(allClients == ""){
        	//$("#clientsTestBlock").html("<span>No Clients Selected!</span>");
        //}else{
        	//$("#clientsTestBlock").html(allClients);
        	//$("#clientsTestBlock div i").css('display','none');
        //}
        localStorage.setItem("readClients",JSON.stringify(globalRC));
        localStorage.setItem("writeClients",JSON.stringify(globalWC));
        showMetricsOnClick();
    }

    function showMetricsOnClick(){
        $(".rwMetricsDisplaySpan").empty();
        $(".wrMetricsDisplaySpan").empty();
        $(".readMetricsOfSelectedClients").click(function(){
            var metricsString = "";
            var divIDMerged = $(this).attr('id').split("-");
            metricsString = getDeviceProps(divIDMerged[0],true);

            var append = "<div class='offset-30' style='margin-top: 20px;'>"+
                             "<div style='margin-right: 20px;'>"+divIDMerged[0]+" Properties </div>"+
                             "<select class='selectCmd commandInput' style='margin: 10px 0;'>"+
                                 "<option value='getRowKeys'>getRowKeys</option>"+
                                 "<option value='tagTrends'>tagTrends</option>"+
                                 "<option value='tagTrendAggregate'>tagTrendAggregate</option>"+
                             "</select>"+
                            "<div class='parametersBox' style='display:none;'>"+
                                 "<Input class='inputParamsRowKey' placeholder='Row Key' type='text' />"+
                                 "<Input class='inputParamsBegin' placeholder='Begin' type='text' />"+
                                 "<Input class='inputParamsEnd' placeholder='End' type='text' />"+
                                 "<Input class='inputParamsLimit' placeholder='Limit (optional)' type='text' />"+
                                 "<br/><a href='#' class='addQueries' style='float:right;' >Add Query</a>"+
                             "</div><br/>"+
        				 "</div>";

           $(".rwMetricsDisplaySpan").html(append);
            console.log(metricsString);

            $('.selectCmd').on('click',function(e){
            	console.log($(this).val());
            	var value = $(this).val();

            	if(value == 'getRowKeys'){
            		$(this).parent().find('.parametersBox').css('display','none');
            	}else{
            		$(this).parent().find('.parametersBox').css('display','inline-block');
            	}
            	var elements = "";
            	$(".rwMetricsDisplaySpan").append(elements);
            });

            $('.addQueries').on('click',function(){
            	var appendToParameterBox = "<br/><Input class='inputParamsRowKey' placeholder='Row Key' type='text' />"+
				 "<Input class='inputParamsBegin' placeholder='Begin' type='text' />"+
				 "<Input class='inputParamsEnd' placeholder='End' type='text' />"+
				 "<Input class='inputParamsLimit' placeholder='Limit (optional)' type='text' />"+
				 "<br/>";
            	$(this).parent().find('parametersBox').append(appendToParameterBox);
            });

        });


        $(".writeMetricsOfSelectedClients").click(function(){
            var metricsString = "";
            var divIDMerged = $(this).attr('id').split("-");
            metricsString = getDeviceProps(divIDMerged[0],false);

            var append = "<div class='offset-30'>" +
            		"<div>"+ divIDMerged[1] +"Properties</div>"+
            		"<table class='configurationPropertiesTable'>"+
                        "<tr>"+
                            "<td>Table To Be Used</td>"+
                            "<td><select class='tableToBeUsed commandInput'>" +
                            "<option value='TagsWide' selected='selected' >TagsWide</option>"+
                            "<option value='TagsTall' >TagsTall</option>"+
                            "<option value='TagsSuperColumn' >TagsSuperColumn</option>"+
                            "</select></td>"+
                        "</tr>"+
                        "<tr>"+
                        "<td>Number of Records</td>"+
	                        "<td><input class='numberOfRecords' value='10000' type='text'/></td>"+
	                    "</tr>"+
                        "<td>Insert Duration in Minutes</td>"+
	                        "<td><input class='insertDurationInMins' value='1' type='text'/></td>"+
	                    "</tr>"+
                        "<tr>"+
                        	"<td>Write In Batch</td>"+
	                        "<td><select class='inBatch commandInput' ><option value='false' selected='selected' >false</option><option value='true'>true</option></select></td>"+
	                    "</tr>"+
	                    "<tr class='showIfBatchIsTrue' >"+
	                        "<td>Batch Size</td>"+
	                        "<td><input class='batchSize' value='10' type='text'/></td>"+
	                    "</tr>"+
                        "<tr>"+
                            "<td>Ensure Unique Key Per Batch</td>"+
                            "<td><select class='ensureUniqueKeyPerBatch commandInput' ><option value='false'>false</option><option value='true'>true</option></select></td>"+
                        "</tr>"+
                        "<tr>"+
                            "<td>Target Throughput</td>"+
                            "<td><input class='targetThroughPut' type='text'/></td>"+
                        "</tr>"+
                        "<tr>"+
                            "<td>Number Of Keys To Be Generated</td>"+
                            "<td><input class='keysToBeGenerated' value='10' type='text'/></td>"+
                        "</tr>"+
                    "</table>"+
            		"</div>";
            $(".wrMetricsDisplaySpan").html(append);
            $('.showIfBatchIsTrue').css('display','none');
            $(".inBatch").on('change',function(){
            	if($(this).val() == 'true'){
            		$('.showIfBatchIsTrue').css('display','');
            	}else{
            		$('.showIfBatchIsTrue').css('display','none');
            	}
            });
            //console.log(metricsString);
        });
    }

    function getSchemaValues(){
    	var object = {};
    	if($("#dbName").val() == ""){
    		return "";
    	}else{
	    	object.DBname = $("#dbName").val();
	    	object.tables = [];
	    	$(".tables_table").each(function() {
	    		var table = {};
	    	    table.TableName = $(this).find(".tableName").val();
	    	    table.RowKey = $(this).find(".rowKey").val();
	    	    table.ColumnValidator = $(this).find(".columnValidator").val();
	    	    if(typeof($(this).find(".rowKeySeparator").val()) != 'undefined'){
		    	    if($(this).find(".rowKeySeparator").val().trim() != ""){
		    	    	table.RowKeySeperator = $(this).find(".rowKeySeparator").val();
		    	    }
	    	    }
	    	    table.KeyValidator = $(this).find(".keyValidator").val();
	    	    table.TypeOfTable = $(this).find(".typeOfTable").val();
	    	    table.TableColumns = [];
	    	    $(this).find(".column_table td").each(function(){
	    	    	var columnObject = {};
	    	    	columnObject.ColumnName = $(this).find(".ColName").val();
	    	    	columnObject.ColumnName = $(this).find(".dataType").val();
	    	    	table.TableColumns.push(columnObject);
	    	    });
	    	    object.tables.push(table);
	    	});
	    	return JSON.stringify(object);
    	}
    }


    function getProps(){
        //creating copies of json for each client
        var cnt = 0;
        var propValues = {};
        var tempId = globalData.ClientId;
        for(var key in allDevices){
            if(allDevices.hasOwnProperty(key)){
                cnt++;
                propValues[key] = {};
                for(var outerKey in globalData){
                    if(globalData.hasOwnProperty(outerKey)){
                        if(outerKey == "ClientId"){
                            propValues[key][outerKey] = key;
                        }else{
                            propValues[key][outerKey] = globalData[outerKey];
                        }
                    }
                }
            }
        }
        allConfigs = propValues;
    }

    function getDeviceProps(clientID, readProps){
        var returnString = " ";
        //
        if(readProps){
            for(var key in allDevices){
                if(allDevices.hasOwnProperty(key)){
                    if(clientID == key){
                        returnString += "Client IP : " + allDevices[key].ip + "</br>";
                        for(var innerKey in allConfigs[key]){
                            if(allConfigs[key].hasOwnProperty(innerKey)){
                                switch(innerKey){
                                	case "RowKeyPath":
                                    case "PaginationRowLimit":
                                        returnString += " " + innerKey + " : " + allConfigs[key][innerKey] + "</br>";
                                        break;
                                    case "NumberOfThreads":
                                    case "EnablePagination":
                                    	returnString += " " + innerKey + " : <input type='text' value='" + allConfigs[key][innerKey] + "'/></br>";
                                    	break;

                                    default:
                                        break;
                                }
                            }
                        }

                    }
                }
            }
        }else{
            for(var key in allDevices){
                if(allDevices.hasOwnProperty(key)){
                    if(clientID == key){
                        returnString += "Client IP : " + allDevices[key].ip + "</br>";
                        for(var innerKey in allConfigs[key]){
                            if(allConfigs[key].hasOwnProperty(innerKey)){

                                switch(innerKey){

                                    case "TableToBeUsed":
                                    case "NumberOfRecords":
                                    case "NumberOfKeysToBeGenerated":

                                        returnString += " " + innerKey + " : " + allConfigs[key][innerKey] + "</br>";
                                        break;
                                    default:
                                        break;

                                if(innerKey == "Metric"){
                                    for(var metricKey in allConfigs[key][innerKey]){
                                        returnString += innerKey + "." + metricKey + " : " + allConfigs[key][innerKey][metricKey] + "</br>";
                                    }
                                }else if(innerKey == "Hbase"){

                                }else if(innerKey == "Cassandra"){

                                }else{

                                    returnString += " " + innerKey + " : " + allConfigs[key][innerKey] + "</br>";
                                }
                            }
                        }

                    }
                }
            }
        }
      }
        return returnString;
    }

    function getJSONValues(){
        $.getJSON( "ppts.json", function( data ) {
            globalData = data;
            fillValuesInSettingsPage();
            //displayCassandraProperties();
        });
    }


    function fillValuesInSettingsPage(){

      var dataText = "";

      $.each(globalData, function(key, value){
           if(key == "Cassandra"){

           }else if(key == "Hbase"){

           }else if(key == "Metric"){

           }else{
        	   dataText = dataText + "<span style='margin-left:20px;' > "+key+" : " + value + "</span><br>";
           }
      });

      //$('#uploadSchemaFileContent').html();
    }

    function setJSONValues( values ){
        $.ajax({
           type: 'POST',
           dataType: 'json',
           url: 'ppts.json',
           data: data,
           success: function(data) {
                swal({title: "",
                    text: data.message,
                    type: "info"
                });
           },
           error: function (data) {
               swal('Please try again');
           }
        });
    }


    function refreshClientDevicesData(){
    	$('#projectNameInput').val('');
        $.ajax({
            type:'GET',
            dataType: 'jsonp',
            url:'http://'+serverIPAddress+':8080/benchmarkingServer/RefreshDevices',//tomcat server address
            success: function(data){
                console.log(data);
                allDevices = data;
                getProps();
                buildClientIcons(data);
            },
            error:function(error){
                $(".loaderContainer").css('display','none');
                swal({title: "Refresh failed",
                  type: "error"
                });
            }
        });
    }

    function getDevices(){
        $.ajax({
            type:'GET',
            dataType: 'jsonp',
            url:'http://'+serverIPAddress+':8080/benchmarkingServer/IPAddressesServlet',//tomcat server address
            success: function(data){
                console.log(data);
                allDevices = data;
                getProps();
                buildClientIcons(data);
            },
            error:function(error){
                $(".loaderContainer").css('display','none');
                alert("error: " + error);
            },
            timeout: 3000
        });
    }

    function clearScreens(){

    }

    function buildClientIcons(data){

        $(".loaderContainer").css('display','none');
        var appendIcons = "<div class='offset-30 pt-12'>";
        var appendToModal = "<a id='refreshActiveClientList' style='float:right;margin-right:2px;' ><i class='fa fa-refresh' aria-hidden='true'></i></a>";

        for(var key in data){
            if(data[key].active == "false"){
                appendIcons += "<div class='client clientNotActive' name='"+key+"' id='client_"+data[key].ip+"' style='margin: 0 auto;width:120px;'><i class='fa fa-laptop' aria-hidden='true'></i><span>"+key+" - "+data[key].ip+"</div>";
                appendToModal += "<div class='client clientNotActive' name='"+key+"' id='"+data[key].ip+"'><i class='fa fa-laptop' aria-hidden='true'></i><span>"+key+"</div>";
            }else if(data[key].active == "true"){
                appendIcons += "<div class='client clientActive' name='"+key+"' id='client_"+data[key].ip+"' style='margin: 0 auto;width:120px;' ><i class='fa fa-laptop' aria-hidden='true'></i><span>"+key+" - "+data[key].ip+"</div>";
                appendToModal += "<div class='client clientActive' name='"+key+"' id='"+data[key].ip+"'><i class='fa fa-laptop' aria-hidden='true'></i><span> "+key+"</div>";
            }else{

            }
        }
        $(".registered_clients").html(appendIcons);
        $("#box").html(appendToModal);

        $("#refreshActiveClientList").on('click',function(){
            refreshClientDevicesData();
			$("#read-dropzone").html("");
			$("#write-dropzone").html("");
        });
    }
});
