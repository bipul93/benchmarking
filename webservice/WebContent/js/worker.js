/*
This file forks out a web worker to continuously fetch the data from the UDP Multicast
*/

//console.log("RefreshTimer Value = " + refreshTimerValue);
self.addEventListener('message', function(e){
	setInterval(function(){ self.postMessage(e.data); }, 10000);       
}, false);