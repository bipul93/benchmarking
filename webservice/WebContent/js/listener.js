var worker;

//Start the Thread
function startSocketWorker(){
 //worker thread to call service
 try{
   if(typeof(worker) == "undefined") {
    //worker = new Worker('js/worker.js');
    worker = new Worker('js/worker.js');
    worker.addEventListener('message', function(e){
		updateData();
   }, false);
   worker.postMessage('Data Updating');
  }
 }catch(error){
	 alert(error);
 }
 //end of worker thread
}

//Stop the Thread
function stopSocketWorker() {
 if(typeof(worker) != "undefined") {
  worker.terminate();
  worker = undefined;
 }else{ return; }
}

function resetSocketWorker(){
    stopSocketWorker();
    startSocketWorker();
}

function updateData() {
	getResult();
}

function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);
        }, 0);
    }
}

function getResult(){
	$.ajax({
	       type: 'GET',
	       dataType: 'jsonp',
	       url: 'http://'+serverIPAddress+':8080/benchmarkingServer/getQueryResult',

	       success: function(data) {
	    	   if(data.value == ""){
		         //console.log(JSON.stringify(data));
	        	 console.log("fetching");
	    	   }else {
	           var queries = decodeURI(data.value.queryResult);
	           console.log(queries);
	           if(queries != ""){
	        	   if(queries == "Queue Empty"){
	        		   console.log("Queue Empty");
	        		   stopSocketWorker();
	        	   }else{
		        	   var queryArray = queries.split("|");
			           var appendedString = "";
                       var loopLimit = 10;
                       if(queryArray.length <= 10){
                           loopLimit = queryArray.length;
                       }
			           for(var i=1; i<loopLimit;i++){
			             appendedString = appendedString + "<div class='qryResRow'>" + decodeURI(queryArray[i].replace("%3",":")) + "</div>";
			           }
                       appendedString = appendedString +"<button class='btn' id='downloadResults'>Download Results</button>";
			           if($("#readSum").is(":visible")){
			        	   $("#qryRes").html(appendedString);
			        	   $(".showCommandStatus a").css('display','inline-block');
			        	   $(".queryProgress").css('display','none');
			           }else{
			        	   $("#writeResults").html(appendedString);
			        	   $("#writeQuery").css('display','inline-block');
			        	   $(".queryProgress").css('display','none');
			           }

                       $("#downloadResults").on('click',function(){
                           console.log("");
                           download(queryArray,"results","TEXT");
                       });
			           stopSocketWorker();
	        	   }
	           }else{
	        	   $(".showCommandStatus a").css('display','inline-block');
		    	   $(".queryProgress").css('display','none');
	        	   stopSocketWorker();
	        	   swal("Database/table empty");
	           }
	         }
	       },
	       failure: function (error) {
	    	   stopSocketWorker();
	    	   alert(error);

	       }
	    });
}
