var globalData = {};
$(document).ready(function(){
	serverIPAddress = localStorage.getItem("SERVER_IP");
    getJSONValues();
    console.log(localStorage.getItem("readClients"));
    console.log(localStorage.getItem("writeClients"));
    if(localStorage.getItem("readClients") != null || localStorage.getItem("writeClients") != null){
	    var rc = JSON.parse(localStorage.getItem("readClients"));
	    var wc =JSON.parse(localStorage.getItem("writeClients"));
		//disable read when read not active and same for write
    }else{
    	var rc = [];
    	var wc = [];
    }

    $('.uploadFileCommand').on('click',function(e){
    	$('.queryCommandFile').trigger('click');
    });

    $('.queryCommandFile').on('change',function(e){
    	var id = $(this).parent().parent().attr('id');
    	console.log(id);
    	var fileCmd = '';
    	for (var i = 0; i < e.target.files.length; ++i) {
    	    (function (file) {               // Wrap current file in a closure.
    	      var loader = new FileReader();
    	      loader.onload = function (loadEvent) {
    	        if (loadEvent.target.readyState != 2)
    	          return;
    	        if (loadEvent.target.error) {
    	          alert("Error while reading file " + file.name + ": " + loadEvent.target.error);
    	          return;
    	        }
    	        $(this).parent().find("a").css('display','none');
    	  	  	$(this).parent().find(".queryProgress").css('display','inline-block');
    	        //console.log(loadEvent.target.result.length); // Your text is in loadEvent.target.result
    	        //console.log(loadEvent.target.result);
    	        //fileCmd = loadEvent.target.result;
    	        sendCommandToBackend(loadEvent.target.result,'getid');
    	      };
    	      loader.readAsText(file);
    	    })(e.target.files[i]);
    	  }
    	//console.log(fileCmd);
    	$(this).parent().find("a").css('display','none');
  	  	$(this).parent().find(".queryProgress").css('display','inline-block');
    });


    if(rc.length > 0){
    	var appendClientsString = "";
    	for(var clientId in rc){
    		console.log(rc);

    		appendClientsString = appendClientsString + "<div id='"+rc[clientId]+"' class='clientQuery'>"+
                                "<div class='clientName inline-middle'>"+rc[clientId]+"</div>"+
                                 "<select class='commandInput inline-middle'>"+
					    		     "<option value='GetRowKeys' >GetRowKeys</option>"+
					    		     "<option value='TagTrend' >TagTrend</option>"+
					    		     "<option value='TagTrendAggregate'>TagTrendAggregate</option>"+
					    		     "<option value='TagTrendAggregateWithTimestep' >TagTrendAggregateWithTimestep</option>"+
					    		     "<option value='TagTrendLatest' >TagTrendLatest</option>"+
					    		     "<option value='TagTrendTall' >TagTrendTall</option>"+
					    		     "<option value='TagDataSuperColumn' >TagDataSuperColumn</option>"+
					    		     "<option value='TagDataTall' >TagDataTall</option>"+
					    		     "<option value='GetStudentMarksWide' >GetStudentMarksWide</option>"+
					    		 "</select>"+

					    		 "<div class='showCommandStatus inline-middle'>"+
                                    "<a class='btn execQuery' style='margin-left:10px;margin-right-10px;'  >Execute Query</a>"+
                                    "<a href='#' class='btn uploadFileCommand' style='margin-left:10px;margin-right-10px;' >Upload Query File</a>"+
                                    "<input style='display:none;' class='queryCommandFile' type='file' multiple=''/>"+                                   
                                    "<div class='loading-content inline-middle queryProgress'>"+
                                        "<div class='loading-text inline-middle'>Operation in progress</div>"+
                                        "<div class='loading-dots dark-gray inline-middle'>"+
                                            "<i></i><i></i><i></i><i></i>"+
                                        "</div>"+
                                    "</div>"+
                                "</div>"+
                                    
                                "<div class='parametersBox inline-middle'>"+
                                    "<Input class='inputParamsRowKey' placeholder='Row Key' type='text' />"+
								    "<Input class='inputParamsBegin' placeholder='Begin' type='text' />"+
								    "<Input class='inputParamsEnd' placeholder='End' type='text' />"+
								    "<Input class='inputParamsLimit' placeholder='Limit (optional)' type='text' />"+
                                 "</div>"+
                                    
                              "</div>";
    	}

    	//appendClientsString = appendClientsString + "<div id='qryRes' style='margin-top:150px;margin-right: 20px;'></div>";
    	$("#clientQueryContainer").html(appendClientsString);

    	$('.uploadFileCommand').on('click',function(e){
    		e.preventDefault();
        	$(this).parent().find('.queryCommandFile').trigger('click');
        });

    	$('.commandInput').on('change',function(e){
    		//console.log($(this).val());
    		if($(this).val() != "GetRowKeys"){
    			$(this).parent().find('.parametersBox').css('display','inline-block');
    		}else{
    			$(this).parent().find('.parametersBox').css('display','none');
    		}
    	});

        $('.queryCommandFile').on('change',function(e){

        	$(this).parent().find("a").css('display','none');
	    	$(this).parent().find(".queryProgress").css('display','inline-block');
	      	var id = $(this).parent().parent().attr('id');
	      	console.log(id);

        	for (var i = 0; i < e.target.files.length; ++i) {
        	    (function (file) {               // Wrap current file in a closure.
        	      var loader = new FileReader();
        	      loader.onload = function (loadEvent) {
        	        if (loadEvent.target.readyState != 2)
        	          return;
        	        if (loadEvent.target.error) {
        	          alert("Error while reading file " + file.name + ": " + loadEvent.target.error);
        	          return;
        	        }

        	        //console.log(loadEvent.target.result.length); // Your text is in loadEvent.target.result
        	        console.log(loadEvent.target.result);
        	        sendCommandToBackend(loadEvent.target.result,id);
        	      };
        	      loader.readAsText(file);
        	    })(e.target.files[i]);
        	  }
        });
        
        if(wc.length > 0){
    	    $('#rwTabs a').click(function (e) {
    	       $(this).tab('show');
    	    });
	    }else{
	    	$('#rwTabs a[href="#readSum"]').tab('show');
	    	$("#rwTabs a"). removeAttr("data-toggle");
	    	$('#rwTabs a').addClass('disabled');
	    }        
    }else{
	    if(wc.length > 0){
	    	$('#rwTabs a[href="#writeSum"]').tab('show');
	    	$("#rwTabs a"). removeAttr("data-toggle");
	    	$('#rwTabs a').addClass('disabled');
	    }
    }
    
	$('#summaryTabs a').click(function (e) {
      //$(this).tab('show');
    });

    $(".execQuery").click(function(e) {
      var command = $(this).parent().parent().find('.commandInput').val();
      var id = $(this).parent().parent().attr('id');

      if(command != "GetRowKeys"){
    	  //command = command + " " + $(".inputParams").val();
    	  var commandParams = "";
    	  var rowKey = $(".inputParamsRowKey").val();
    	  var begin = $(".inputParamsBegin").val();
    	  var end = $(".inputParamsEnd").val();
    	  var limit = $(".inputParamsLimit").val();
    	  
    	  commandParams = commandParams + " rowkey " + rowKey + " begin " + begin + " end " + end;
    	  if(limit != ""){
    		  commandParams = commandParams + " limit " + limit;
    	  }
    	  
    	  command = command + commandParams;
    	  
      }else{
    	  console.log(command);
      }
      
      $(this).parent().find("a").css('display','none');
	  $(this).parent().find(".queryProgress").css('display','inline-block');

      if(command == ""){
        alert("Command Invalid!");
      }else{
        sendCommandToBackend(command,id);
      }
    });

    $("#writeQuery").click(function(e) {
      $(this).parent().find("a").css('display','none');
  	  $(this).parent().find(".queryProgress").css('display','inline-block');
      sendCommandToBackend("write","allClients");
    });

});

function sendClientReadCommand(recieverIP,command){
	//console.log(value);

    var id = $(this).parent().parent().attr('id');

    if(command == ""){
      alert("Command Invalid!");
    }else{
      sendCommandToBackend(command,recieverIP);
    }
}

function getJSONValues(){
    try{
    	globalData = JSON.parse(localStorage.getItem('cassandraProperties'));
    }catch(ex){
    	console.log(ex);
    }
}

function sendCommandToBackend(command , client) {
	//console.log(command);
	if(client == 'getid'){
		var ids = localStorage.getItem("writeClients");
		client = ids[0];
	}
	console.log(globalData);
  var postData = {
    "client": client,
    "command": command,
    "data": JSON.stringify(globalData)
  }
  $.ajax({
     type: 'POST',
     dataType: 'jsonp',
     url: 'http://'+serverIPAddress+':8080/benchmarkingServer/runCommand',
     data: postData,
     success: function(data) {
       console.log(data);
       if(data.result == "Client is busy"){
    	   alert(data.result);
    	    if($("#readSum").is(":visible")){
	   			$(".showCommandStatus a").css('display','inline-block');
	   			$(".queryProgress").css('display','none');
	   		}else{
	   			//fetching writeQuery
	   			$("#writeQuery").css('display','inline-block');
	   			$(".queryProgress").css('display','none');
	   		}
       }else if(data.result == "Operation in progress"){
    	   //
    	   console.log(data);
       }else{
    	  startSocketWorker();
       }

     },
     failure: function (error) {
       alert(error);
       $(".showCommandStatus a").css('display','inline-block');
		if($("#readSum").is(":visible")){
			$(".showCommandStatus a").css('display','inline-block');
			$(".progress").css('display','none');
		}else{
			//fetching writeQuery
			$("#writeQuery").css('display','inline-block');
			$(".progress").css('display','none');
		}
         //console.log(error);
     }
  });

}
