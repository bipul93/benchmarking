Configuring STS Web Service
--------------------------------------

1. Install STS (Spring Tool Suite)
2. Open STS and File->Open Projects from File System
3. Open this project -> if open/or any failure fails go to step 7
4. Add dependency of libraries from the lib folder
5. Run the project as Server
6. Right now the client is configured with localhost. In case of changes, please change all 'localhost' text to the ip address of the system on which STS is running.

7. Create new dynamic web project in STS under title benchmarkingServer.
8. replace webContent and src file with the files.
9. follow step 4 to step 6.
