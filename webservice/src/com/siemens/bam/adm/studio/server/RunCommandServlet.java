package com.siemens.bam.adm.studio.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.siemens.bam.adm.studio.server.utils.GlobalData;
import com.siemens.bam.adm.studio.server.utils.MessagePacket;

public class RunCommandServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		doPost(req,resp);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		MessagePacket messagePacket = new MessagePacket();
		resp.setContentType("text/html");
		String callBackJavaScripMethodName = req.getParameter("callback");
		PrintWriter out = resp.getWriter();

		ServletContext context = getServletContext();
		String[] cmd =  req.getParameterValues("command");
		String[] cli =  req.getParameterValues("client");
		String parametersData =  req.getParameterValues("data")[0];

		//GlobalData.command = cmd[0];
		GlobalData.setCommand(cmd[0]);
		GlobalData.commandClient = cli[0];

		System.out.println("running command");
		
		messagePacket.setMessage("run command");
		messagePacket.setRecieverIP(cli[0]);
		messagePacket.setSenderIP(req.getRemoteAddr());
		if(GlobalData.messageQueue.contains(messagePacket)){
			out.print(callBackJavaScripMethodName + "(");
			out.print("{\"result\":\"Operation in progress\"}");
			out.print(")");
		}else{
			Boolean busyState = false;
			Iterator it = GlobalData.messageQueue.iterator();
			while(it.hasNext()){
				MessagePacket packet = (MessagePacket)it.next();

				if(packet.getRecieverIP().equals(cli[0])){
					busyState = true;
					break;
				}
				it.remove();
			}

			if(busyState){
				out.print(callBackJavaScripMethodName + "(");
				out.print("{\"result\":\"Client is busy\"}");
				out.print(")");

			}else{
				GlobalData.messageQueue.add(messagePacket);
				RequestDispatcher dispatcher = context.getRequestDispatcher("/getInfo");
				GlobalData.data = parametersData;
				// change your request and response accordingly
				dispatcher.forward(req, resp);

				//GlobalData.message = "";
			}
		}

		out.close();
	}

}
