package com.siemens.bam.adm.studio.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.siemens.bam.adm.studio.server.utils.GlobalData;

public class TestResultsServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		doPost(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		ServletContext context = getServletContext();
		//out.print(GlobalData.message);
		System.out.println(req.getParameter("jsondata"));
		GlobalData.queryResult = req.getParameter("jsondata");
		
		out.print(GlobalData.queryResult);
		out.close();
	}
}
