package com.siemens.bam.adm.studio.server.utils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

public class ProjectProps {
	
	String filePath = "proj.properties";
	Properties projectProperties = new Properties();
	int savedProjects = 0;
	String projectName = "";
	String dataSet = "";
	String [] readClients = new String[10];
	String [] writeClients = new String[10];
	String [] databaseParams = new String[10];
	String [] readMetrics = new String[10] ;
	String [] writeMetrics = new String[10];
	
	public void setProjectName(String name){
		System.out.println("name = " + name);
		projectName = name;
	}
	
	public void setDataSet(String data){
		dataSet = data;
	}
	
	public void setReadClients(String[] clients){
		if(clients == null){
			
		}else{
			readClients = clients;
		}
	}
	
	public void setWriteClients(String[] clients){
		if(clients == null){
			
		}else{
			writeClients = clients;
		}
	}
	
	public void setDatabaseParams(String[] params){
		databaseParams = params;
	}
	
	public void setReadMetrics(String[] metrics){
		readMetrics = metrics;
	}
	
	public void setWriteMetrics(String[] metrics){
		writeMetrics = metrics;
	}
	
	public String getProjectName(){
		return projectName;
	}
	
	public String [] getReadClients(){
		return readClients;
	}
	
	public String [] getWriteClients(){
		return writeClients;
	}
	
	public String [] getDatabaseParams(){
		return databaseParams;
	}
	
	public String [] getReadMetrics(){
		return readMetrics;
	}
	
	public String [] getWriteMetrics(){
		return writeMetrics;
	}
	
	public void createFile() throws IOException{
		File myFile = new File(System.getProperty("user.home"), filePath);
		if(myFile.isFile()){
			//System.out.println("Exists " + myFile.getAbsolutePath() + " " + myFile.getCanonicalPath() + " " + myFile.getPath());
		}else{
			//System.out.println("Creating");
			myFile.createNewFile();
		}
	}
	
	public void saveFile() throws IOException{
		createFile();
		
		if(projectName == ""){
			projectName = getProjectTitle();
		}else{
			String temp = getProjectTitle();
		}
		File myFile = new File(System.getProperty("user.home"), filePath);
		//System.out.println(printProperties());
		projectProperties.put(projectName, printProperties());
		//System.out.println(projectProperties);
		//System.out.println(filePath);
			projectProperties.store(new FileWriter(myFile),null);
		
	}
	
	public void readFile() throws IOException{
		createFile();
		File myFile = new File(System.getProperty("user.home"), filePath);
		InputStream input = new FileInputStream(myFile);
		projectProperties.load(input);
		
	}
	
	public String printAll(){
		
		String str = "";
		String kStr = "";
		Set keyElms = projectProperties.keySet();
		Iterator itr = keyElms.iterator();
		int count = 0;
		while(itr.hasNext()) {
			if(count++ != 0){
				str = str.concat(",");
			}
			kStr = (String) itr.next();
			str = str.concat( projectProperties.getProperty(kStr));
		} 
		return str;
		
	}
	
	public String getProjectTitle() throws IOException{
		readFile();
		int count = 0;
		String kStr = "";
		Set keyElms = projectProperties.keySet();
		Iterator itr = keyElms.iterator();
		while(itr.hasNext()) {
			count++;
			kStr = (String) itr.next();
		} 
		
		return "project_"+count;
	}
	
	public String printProperties(){
		
		//
		
		String str = "";
		
		str = str.concat( "\""+projectName+"\":{");
		str = str.concat("\"readClients\":\"");
		for(int i=0;i<readClients.length;i++){
			str = str.concat(readClients[i] + "-");
		}

		str = str.concat("\",\"writeClients\":\"");
		
		for(int i=0;i<writeClients.length;i++){
			str = str.concat( writeClients[i]+ "-");
		}
		
		/*str = str.concat("\",\"readMetrics\":\"");
		for(int i=0;i<readMetrics.length;i++){
			str = str.concat(readMetrics[i] + " - ");
		}
		
		str = str.concat("\",\"writeMetrics\":\"");
		for(int i=0;i<writeMetrics.length;i++){
			str = str.concat(writeMetrics[i] + "-");
		}*/
		
		
		String jsonString = jsonStringtoString(dataSet);
		//dataset.replaceAll("","");
		str = str.concat("\",\"dataSet\":"+jsonString+"");
		str = str.concat("}");
		/*String kStr = "";
		Set keyElms = projectProperties.keySet();
		Iterator itr = keyElms.iterator();
		int count = 0;
		while(itr.hasNext()) {
			if(count++ != 0){
				str = str.concat(",");
			}
			kStr = (String) itr.next();
			str = str.concat(projectProperties.getProperty(kStr));
		} 
		return str;*/
		//System.out.print(str);
		return str;
	}
	
	public String printCurrentProperties(){
		String retStr = "";
		String kStr = "";
		Set keyElms = projectProperties.keySet();
		Iterator itr = keyElms.iterator();
		while(itr.hasNext()) {
			kStr = (String) itr.next();
			retStr = retStr.concat( projectProperties.getProperty(kStr));
			break;
		} 
		
		return retStr;
	}
	
	public void deleteEntry(String name) throws IOException{
		//System.out.println(name);
		int count = 0;
		readFile();
		Enumeration e = projectProperties.propertyNames();
		
		while(e.hasMoreElements()){
			count++;
			String key = (String) e.nextElement();
			//System.out.println(projectProperties.getProperty(key));
			if(key.equalsIgnoreCase(name)){
				projectProperties.remove(key);
			}else{
				
			}
		}
		File myFile = new File(System.getProperty("user.home"), filePath);
		projectProperties.store(new FileWriter(myFile),null);
	}
	
	public String jsonStringtoString(String jsonData){
		String returnData = jsonData.replaceAll("\"","\\\"");
		return returnData;
	}
}
