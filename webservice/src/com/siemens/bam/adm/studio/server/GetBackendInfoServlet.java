package com.siemens.bam.adm.studio.server;

import java.io.*;
import java.util.*;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


import com.siemens.bam.adm.studio.server.utils.ClientProps;
import com.siemens.bam.adm.studio.server.utils.ProjectProps;
import com.siemens.bam.adm.studio.server.utils.GlobalData;
import com.siemens.bam.adm.studio.server.utils.MessagePacket;

public class GetBackendInfoServlet extends HttpServlet {

	HashMap<String, String> IPSessionMap;
	HttpServletRequest gRequest;
	HttpServletResponse gResponse;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void sendToSpecificAddress(HttpServletRequest request, HttpServletResponse response){
		MessagePacket messagePacket = new MessagePacket();

		IPSessionMap = new HashMap<String, String>();
		ServletContext context = getServletContext();
		String callBackJavaScripMethodName = request.getParameter("callback");
		if (context.getAttribute("IPSessionObjects") != null) {
			IPSessionMap = (HashMap<String, String>) context.getAttribute("IPSessionObjects");
		}

		// Iterating over keys only
		for (String key : IPSessionMap.keySet()) {

		    messagePacket = GlobalData.messageQueue.peek();
		    if(messagePacket.getRecieverIP() == key){

		    }
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		ClientProps clientProps = new ClientProps();
		ProjectProps projectProps = new ProjectProps();
		//projectProps.readFile();
		MessagePacket messagePacket = new MessagePacket();
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		IPSessionMap = new HashMap<String, String>();
		ServletContext context = getServletContext();
		String callBackJavaScripMethodName = request.getParameter("callback");

		HashMap<String,Integer> localClients = new HashMap<String,Integer>();
		int connectionCounts = 5;
		//String clientKey =  GlobalData.allClients.get(request.getRemoteAddr());
		if(!GlobalData.allClients.containsKey(request.getRemoteAddr())){
			GlobalData.allClients.put(request.getRemoteAddr(),connectionCounts);
		}

		for(String key : GlobalData.allClients.keySet()){
			if(key.equalsIgnoreCase(request.getRemoteAddr().toString())){
				GlobalData.allClients.put(key, connectionCounts);
			}else{
				int decrCount = GlobalData.allClients.get(key);

				decrCount -= 1;
				if(decrCount == 0){

					GlobalData.allClients.remove(key);
					clientProps.deleteEntry(key);
				}else{
					GlobalData.allClients.put(key, decrCount);
				}
			}
		}



		if (context.getAttribute("IPSessionObjects") != null) {
			IPSessionMap = (HashMap<String, String>) context.getAttribute("IPSessionObjects");
		}

		if(callBackJavaScripMethodName == null){
			try{
				GlobalData.message = GlobalData.messageQueue.peek().getMessage();
				GlobalData.sentMessagesQueue.add(GlobalData.messageQueue.peek());
				GlobalData.messageQueue.remove();
			}catch(Exception e){
				GlobalData.message = "";
			}
		}else{
			try{
				GlobalData.message = GlobalData.messageQueue.peek().getMessage();
			}catch(NullPointerException e){
				e.printStackTrace();
			}

			if(GlobalData.message == null){
				GlobalData.message = "";
			}
		}

		if(!GlobalData.message.equalsIgnoreCase("")){
			if(GlobalData.message.equalsIgnoreCase("Test Connection")){

				 if(callBackJavaScripMethodName == null){

					 String tempIP = "";

					 messagePacket = GlobalData.sentMessagesQueue.peek();

					 GlobalData.sentMessagesQueue.remove();
					 //GlobalData.messageQueue.remove();

					// Iterating over keys only
						//for (String key : IPSessionMap.keySet()) {

						   //if(messagePacket.getRecieverIP().equalsIgnoreCase(request.getRemoteAddr())){

						    	out.print(messagePacket.getRecieverIP() + "~" + messagePacket.getMessage() + "~" + GlobalData.data );
						    	System.out.println("testing done");
						    	//break;
						    //}
						//}
						if(GlobalData.messageQueue.size() > 0)
							GlobalData.messageQueue.remove();
				 }else{
					 out.print(callBackJavaScripMethodName + "(");
					 out.print("{");
					 Enumeration propertyNames = request.getParameterNames();

					 System.out.println(request.getParameter("data"));


					 out.print("\"" + GlobalData.message + "\" : {");
					int count = 0;
						//out.print(propertyNames);
					int length = GlobalData.connectedClients.length();
					// read clients info and send connection config to all clients

					if(GlobalData.connectedClients.length() != 0){
						out.print("[" + GlobalData.connectedClients + "]");

					}else{

					}

					  /*while(propertyNames.hasMoreElements()){

						  String propName = (String) propertyNames.nextElement();
						  out.print( propName + "     ");
						  String[] paramValues = request.getParameterValues(propName);

						  for(int i=0;i<paramValues.length;i++){
							  out.print("  " + paramValues[i] + " ");
						  }
					  }*/

					 out.print("}");
					 out.print("}");
					 out.print(")");

					 //GlobalData.messageQueue.remove();
					 GlobalData.connectedClients = "";
				 }
			}else if(GlobalData.message.equalsIgnoreCase("run command")){
				//out.println("data");
				if(callBackJavaScripMethodName != null){
					out.print(callBackJavaScripMethodName + "(");
					 out.print("{");
					 Enumeration propertyNames = request.getParameterNames();
					 out.print("\"" + GlobalData.message + "\" : {");
					int count = 0;
						//out.print(propertyNames);
					// read clients info and send connection config to all clients
					out.print("\"Values\" : " );
					if(GlobalData.connectedClients.length() != 0){
						out.print("[" + GlobalData.connectedClients + "]");

					}else{
						//try {
							//Thread.currentThread().sleep(1000);
							out.print("[" + "\"command sent to the backend , waiting for the result\"" + "]");
						//} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							//e.printStackTrace();
						//}
					}

					  /*while(propertyNames.hasMoreElements()){

						  String propName = (String) propertyNames.nextElement();
						  out.print( propName + "     ");
						  String[] paramValues = request.getParameterValues(propName);

						  for(int i=0;i<paramValues.length;i++){
							  out.print("  " + paramValues[i] + " ");
						  }
					  }*/

					 out.print("}");
					 out.print("}");
					 out.print(")");

				}else{
					
					messagePacket = GlobalData.sentMessagesQueue.peek();
					//GlobalData.sentMessagesQueue.remove();

					projectProps.readFile();
					//String message = messagePacket.getMessage().replaceAll("\"", "'");
					System.out.println("sending command" + messagePacket.getMessage());
					out.print(messagePacket.getRecieverIP() + "~" + GlobalData.getCommand() + "~" + messagePacket.getMessage() +"~"+projectProps.printCurrentProperties() +"~"+GlobalData.data);

					//GlobalData.messageQueue.remove();
				}
			}
		}else{

		}

		// String callBackJavaScripMethodName =
		// request.getParameter("callback");
		// out.print(callBackJavaScripMethodName + "(");
		// out.print("{");
		 Enumeration propertyNames = request.getParameterNames();
		// out.print("\" properties \" : {");
		int count = 0;
		//out.print(propertyNames);
		// read clients info and send connection config to all clients


		  /*while(propertyNames.hasMoreElements()){

			  String propName = (String) propertyNames.nextElement();
			  out.print( propName + "     ");
			  String[] paramValues = request.getParameterValues(propName);

			  for(int i=0;i<paramValues.length;i++){
				  out.print("  " + paramValues[i] + " ");
			  }
		  }*/

		// out.print("}");
		// out.print("}");
		// out.print(")");
		//out.println("");

		String IPObject = (String) request.getAttribute("remoteClientIP");
		//out.print(request.getSession().getId());

		//out.print(request.getSession().getAttribute(arg0));
		if (IPObject == null) {
			IPObject = "".concat(request.getRemoteAddr() + ":" + request.getRemotePort());
		} else {
			IPObject = IPObject.concat("-" + request.getRemoteAddr() + " :" + request.getRemotePort());
		}

		// out.println(request.getRemoteAddr() + ":" + request.getRemotePort() +
		// "/" + request.getRemoteHost());
		IPSessionMap.put(IPObject, request.getSession().getId());
		context.setAttribute("IPSessionObjects", IPSessionMap);
		//out.println(" " + GlobalData.message);

		out.close();
	}

}
