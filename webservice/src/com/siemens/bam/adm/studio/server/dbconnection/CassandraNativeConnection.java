package com.siemens.bam.adm.studio.server.dbconnection;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

//import org.apache.commons.lang3.EnumUtils;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SimpleStatement;
import com.datastax.driver.core.Statement;
import com.datastax.driver.core.Cluster.Builder;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;

//import com.siemens.bam.adm.studio.dbdriver.cassandra.CassandraProperties;
import com.siemens.bam.adm.studio.server.exception.DBException;


/**
 * The Connection Class for Cassandra with Binary Protocol
 *
 * @author varsha.raveendran@siemens.com
 * @author nisha.menon@siemens.com
 *
 */
public class CassandraNativeConnection implements IConnection {

	//final static Logger logger = LoggerFactory.getLogger(CassandraNativeConnection.class.getName());

	private static AtomicInteger atomicCounter = new AtomicInteger(0);
	private Session session;
	private Cluster cluster;
	private PreparedStatement statement;
	//private static CassandraProperties cassandraProperties = new CassandraProperties();

	// The atomic counter variable is used to ensure that the connection is
	// closed for all threads
	public CassandraNativeConnection(){
		//logger.debug(Thread.currentThread().getName() + " : Creating a new CassandraNativeConnection instance");
		//cassandraProperties.validateAndSetCassandraConsistencyLevel();

		//initializeConnection();

	}


	public void initializeConnection(String iplist,int port,String consistencyLevel,String dbName) throws Exception {

		String[] hostIPs = iplist.split(",");

		Builder builder = new Cluster.Builder().addContactPoint(hostIPs[0]);

		if (hostIPs.length > 1) {
			//logger.debug(Thread.currentThread().getName() + " Adding additional Cassandra contact points =>");
			for (int i = 1; i < hostIPs.length; i++) {
				//logger.debug(hostIPs[i]);
				builder.addContactPoint(hostIPs[i]);
			}
		}

		try {

			/*if (!EnumUtils.isValidEnum(ConsistencyLevel.class, cassandraProperties.getCassandraConsistencyLevel())) {

				logger.error("No such Consistency Level " + cassandraProperties.getCassandraConsistencyLevel()
						+ " available in Cassandra. Please check the config file. ");
				throw new Exception("No such Consistency Level " + cassandraProperties.getCassandraConsistencyLevel()
						+ " available in Cassandra. Please check the config file. ");
			}*/

			//cluster = builder.withPort(port)
				//	.withQueryOptions(new QueryOptions().setConsistencyLevel(
					//		ConsistencyLevel.valueOf(consistencyLevel)))
					//.withLoadBalancingPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build())).build();

			Metadata metadata = cluster.getMetadata();
			for (Host host : metadata.getAllHosts()) {
				//logger.debug("Datacenter: {}", host.getDatacenter());
				//logger.debug(" Host: {}", host.getAddress().toString());
				//logger.debug(" Rack: {}",

						host.getRack();
			}

			//logger.debug("Connected to Cluster [" + metadata.getClusterName() + "]");
		} catch (Exception e) {
			throw new DBException(
					"Exception in CassandraNativeConnection::getConnection - Error in connecting to Host " + e);

		}
		try {
			session = cluster.connect(dbName);
		} catch (Exception e) {
			throw new DBException(
					"Exception in CassandraNativeConnection::getConnection - Error while connecting to cluster " + e);
		}

	}


	public boolean testConnection(String iplist,int port,String consistencyLevel,String dbName ) throws Exception{
			initializeConnection(iplist, port, consistencyLevel, dbName);

			if(session != null  && cluster != null){
				closeConnection();
				return true;
			}

			return false;
	}


	public void closeConnection() throws DBException {

		try {
			if (atomicCounter.decrementAndGet() == 0) {
				if (session != null && cluster != null) {
					//logger.debug(Thread.currentThread().getName() + " : Session closed.");
					session.close();
					cluster.close();
				}
			}
		} catch (Exception e) {
			throw new DBException(e);
		}
	}

	public Session getSession() throws DBException {
		if(session == null) {
			throw new DBException("Exception thrown : Session is NULL. ");
		}
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

	public Cluster getCluster() {
		return cluster;
	}

	public void setCluster(Cluster cluster) {
		this.cluster = cluster;
	}
}
