package com.siemens.bam.adm.studio.server.utils;

import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import com.siemens.bam.adm.studio.server.utils.MessagePacket;

public class GlobalData {

	//public static MessagePacket messagePacket = new MessagePacket();
	
	public static ConcurrentHashMap<String,Integer> allClients = new ConcurrentHashMap<String,Integer>();
	
	public static Queue<MessagePacket> messageQueue = new ConcurrentLinkedQueue<MessagePacket>();
	
	public static Queue<MessagePacket> sentMessagesQueue = new ConcurrentLinkedQueue<MessagePacket>();
	
	public static String message = "";
	public static String connectedClients = "";
	public static String queryResult = "";
	public static String data = "";
	public static String command = "";
	public static String commandClient = "";
	
	public static Boolean readInProgress = false;
	public static Boolean writeInProgress = false;
	
	
	public static synchronized void setCommand(String cmd){
		command = cmd;
	}
	public static synchronized String getCommand(){
		return command;
	}
}
