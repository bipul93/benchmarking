package com.siemens.bam.adm.studio.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.siemens.bam.adm.studio.server.utils.ProjectProps;;

public class DeleteProjectServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String callBackJavaScripMethodName = request.getParameter("callback");
		PrintWriter out=response.getWriter();
		String clientName =  request.getParameterValues("clientName")[0];
		ProjectProps projectProps = new ProjectProps();
		projectProps.deleteEntry(clientName);
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		out.print("\"result\":\" " + clientName + " has been deleted \"");
		out.print("}");
		out.print(")");
		out.close();
	}

}
