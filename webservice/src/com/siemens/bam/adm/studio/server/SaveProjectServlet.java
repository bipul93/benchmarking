package com.siemens.bam.adm.studio.server;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.siemens.bam.adm.studio.server.utils.ProjectProps;

public class SaveProjectServlet extends HttpServlet {


protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doPost(request,response);
}

protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		response.setContentType("text/html");
		PrintWriter out=response.getWriter();

		ProjectProps projectProps = new ProjectProps();
		String callBackJavaScripMethodName = request.getParameter("callback");
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		projectProps.setProjectName(request.getParameterValues("name")[0]);
		projectProps.setReadClients(request.getParameterValues("selectedReadClients[]"));
		projectProps.setWriteClients(request.getParameterValues("selectedWriteClients[]"));
		projectProps.setReadMetrics(request.getParameterValues("readMetrics[]"));
		projectProps.setWriteMetrics(request.getParameterValues("writeMetrics[]"));
		projectProps.setDataSet(request.getParameterValues("dataSet")[0]);
		projectProps.saveFile();
		out.print(projectProps.printProperties());

		out.print("}");
		out.print(")");
		out.close();
	}
}
