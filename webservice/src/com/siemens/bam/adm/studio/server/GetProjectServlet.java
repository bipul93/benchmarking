package com.siemens.bam.adm.studio.server;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.siemens.bam.adm.studio.server.utils.ProjectProps;

public class GetProjectServlet extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		ProjectProps props = new ProjectProps();
		props.readFile();
		String callBackJavaScripMethodName = request.getParameter("callback");
		
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		out.print(props.printAll());
		out.print("})");
		out.close();
	}

}
