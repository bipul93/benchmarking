package com.siemens.bam.adm.studio.server.utils;
import java.util.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;

public class ClientProps {
	String filePath = "info.properties";
	Properties cProperties = new Properties();
	
	public void createFile() throws IOException{
		File myFile = new File(System.getProperty("user.home"), filePath);
		if(myFile.isFile()){
			//System.out.println("Exists " + myFile.getAbsolutePath() + " " + myFile.getCanonicalPath() + " " + myFile.getPath());
		}else{
			myFile.createNewFile();
		}
	}
	
	public void addClientInfo(String clientName,String clientIPAddrs){
		//if(cProperties.containsKey(clientName)){
			/*String nameParts[] = clientName.split("_");
			int number = (Integer.parseInt(nameParts[1]) + 100);
			String newStringName = nameParts[0].concat("_");
			String newName = newStringName.concat(String.valueOf(number));
			
			cProperties.setProperty(newName,clientIPAddrs);*/
			
		//}else{
			cProperties.setProperty(clientName,clientIPAddrs);
		//}
	}
	
	public void readPropsFile() throws IOException{
		
		createFile();
		File myFile = new File(System.getProperty("user.home"), filePath);
		InputStream input = new FileInputStream(myFile);
		cProperties.load(input);
		System.out.println(cProperties);
	}
	
	public void writePropsFile() throws IOException{
		createFile();
		//System.out.println("List updated");
		File myFile = new File(System.getProperty("user.home"), filePath);
		cProperties.store(new FileWriter(myFile),null);
	}
	
	public Properties getProps(){
		return cProperties;
	}
	
	public void setProps(Properties props){
		this.cProperties = props;
	}
	
	public String pingClients() throws IOException{
		//check the reachablity of the client ip and set active state
		int count = 0;
		String newURL = "";
		ClientsInfo clientInfo = new ClientsInfo();
		String IPs = "";
		Enumeration e = cProperties.propertyNames();
		while(e.hasMoreElements()){
			count++;
			
			String key = (String) e.nextElement();
			String ip = clientInfo.readIP(cProperties.getProperty(key));
			if(ip == ""){
				//return "Error Fetching Properties";
			}else{
				clientInfo.setHostName(key);
				clientInfo.setIPAddress(ip);
				
				if(InetAddress.getByName(ip).isReachable(500)){
					clientInfo.setActiveState(true);
				}else{
					clientInfo.setActiveState(false);
				}
				
			}
		}
		return printContent();
	}
	
	public String checkIP(String ip) throws IOException{
		int count = 0;
		
		if(GlobalData.allClients.keySet().size() <= 1){
			clearProps();
		}
		
		ClientsInfo clientInfo = new ClientsInfo();
		clientInfo.setActiveState(true);
		clientInfo.setIPAddress(ip);
		
		Enumeration e = cProperties.propertyNames();
		
		while(e.hasMoreElements()){
			count++;
			String key = (String) e.nextElement();
			if(cProperties.getProperty(key).contains(ip)){
				clientInfo.setHostName(key);
				cProperties.setProperty(key, clientInfo.toString());
				writePropsFile();
				return "Client Status has been Successfully Updated";
			}else{
				
			}
		}
		String newHostName = "Client_".concat(Integer.toString(++count));
		clientInfo.setHostName(newHostName);
		addClientInfo(newHostName,clientInfo.toString());
		writePropsFile();
		return "New Client has been Registered";
	}
	
	public void clearProps() throws IOException{
		readPropsFile();
		Enumeration e = cProperties.propertyNames();
		
		while(e.hasMoreElements()){
			String key = (String) e.nextElement();
			cProperties.remove(key);
		}
		writePropsFile();
		
	}
	
	public String printContent(){
		
		String str = "";
		String kStr = "";
		Set keyElms = cProperties.keySet();
		Iterator itr = keyElms.iterator();
		int count = 0;
		while(itr.hasNext()) {
			if(count++ != 0){
				str = str.concat(",");
			}
			kStr = (String) itr.next();
			System.out.println("kstr" + cProperties.getProperty(kStr));
			str = str.concat(cProperties.getProperty(kStr));
		} 
		return str;
	}
	
	public boolean ifFileExists() throws IOException{
		File f = new File(filePath);
		if(f.exists() && !f.isDirectory()) { 
		   return true;
		}else{
			return false;
		}
	}
	
	public void deleteEntry(String ip) throws IOException{
		System.out.println(ip);
		int count = 0;
		readPropsFile();
		Enumeration e = cProperties.propertyNames();
		
		while(e.hasMoreElements()){
			count++;
			String key = (String) e.nextElement();
			if(cProperties.getProperty(key).contains(ip)){
				cProperties.remove(key);
			}else{
				
			}
		}
		writePropsFile();
	}	
}