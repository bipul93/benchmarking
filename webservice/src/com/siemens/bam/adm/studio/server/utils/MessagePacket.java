package com.siemens.bam.adm.studio.server.utils;

public class MessagePacket {
	
	String senderIP, recieverIP, message;
	
	public void setSenderIP(String ip){
		this.senderIP = ip;
	}
	
	public void setRecieverIP(String ip){
		this.recieverIP = ip;
	}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	
	public String getSenderIP(){
		return senderIP;
	}
	
	public String getRecieverIP(){
		return recieverIP;
	}
	
	public String getMessage(){
		return message;
	}

}
