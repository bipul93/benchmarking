package com.siemens.bam.adm.studio.server;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.siemens.bam.adm.studio.server.utils.ClientProps;


public class IPAddressesServlet extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String callBackJavaScripMethodName = request.getParameter("callback");
		ClientProps clientProps = new ClientProps();
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		int count = 0;
		int timeout=200;
		   	clientProps.readPropsFile();
		   	//Properties props = clientProps.getProps();
		 out.print(clientProps.printContent());
		 out.print("}");
		 out.print(")");
		 out.close();
		
	}
}
