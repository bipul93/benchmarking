package com.siemens.bam.adm.studio.server;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringEscapeUtils;

import com.siemens.bam.adm.studio.server.utils.GlobalData;

public class SendQueryResult extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		doPost(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		resp.setContentType("text/html");
		PrintWriter out = resp.getWriter();
		ServletContext context = getServletContext();
		String callBackJavaScripMethodName = req.getParameter("callback");
		//logger.info("Sending results to front end");
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		//logger.info((GlobalData.messageQueue.size() == 0 && GlobalData.sentMessagesQueue.size() == 0));
		if(GlobalData.messageQueue.size() == 0 && GlobalData.sentMessagesQueue.size() == 0){
			GlobalData.queryResult = "\"Queue empty\"";
		}
		System.out.println(GlobalData.queryResult);
		if(!GlobalData.queryResult.equals("")){
			out.print("\"value\":"+GlobalData.queryResult);
			GlobalData.queryResult = "";
			if(GlobalData.sentMessagesQueue.size() != 0)
				GlobalData.sentMessagesQueue.remove();
		}else{
			out.print("\"value\":\"\"");
		}
		out.print("}");
		out.print(")");
		out.close();
	}

}
