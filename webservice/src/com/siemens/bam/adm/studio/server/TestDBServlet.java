package com.siemens.bam.adm.studio.server;

import java.io.*;
import java.util.*;
import java.net.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.siemens.bam.adm.studio.server.dbconnection.CassandraNativeConnection;
import com.siemens.bam.adm.studio.server.listener.DBTestConnectionListener;
import com.siemens.bam.adm.studio.server.utils.GlobalData;
import com.siemens.bam.adm.studio.server.utils.MessagePacket;


public class TestDBServlet extends HttpServlet {

	

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		MessagePacket messagePacket = new MessagePacket();
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		//out.print(sendMessage(null,null));

		ServletContext context = getServletContext();
		HashMap <String, HttpSession> IPSessionMap = new HashMap <String, HttpSession>();

		if(context.getAttribute("IPSessionObjects") != null){
			IPSessionMap = (HashMap<String, HttpSession>) context.getAttribute("IPSessionObjects");
		}else{
			
		}

		String dbType = request.getParameter("dbType");
		String readClients = request.getParameter("selectedReadClients");
		String writeClients = request.getParameter("selectedWriteClients");
		String dbData = request.getParameter("data");
		String deviceIP = request.getParameter("deviceIP");

		String callBackJavaScripMethodName = request.getParameter("callback");
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		out.print("\" output \" : {");
		int count = 0;

		GlobalData.message = "Test Connection";
		GlobalData.data = dbData;
		String senderIP = request.getRemoteAddr();
		messagePacket.setMessage(GlobalData.message);
		messagePacket.setSenderIP(senderIP);
		messagePacket.setRecieverIP(deviceIP);
		//messagePacket.setMessage("Test Connection");
		GlobalData.messageQueue.add(messagePacket);
		
		RequestDispatcher dispatcher = context.getRequestDispatcher("/getInfo");

		// change your request and response accordingly
		dispatcher.forward(request, response);
		//for(int i=0;i<1000;i++);
		//GlobalData.message = "";

		//out.print(" , \"ClientProps\" : [ \"" + context.getAttribute("remoteClientIP") + "\" , " + context.getAttribute("remoteClientPort") + "]" );
		out.print("}");
		out.print("}");
		out.print(")");
		out.close();
	}

	public String sendMessage(String IPString, String sessionId){
		String outputMessage = "Begin Test Operation";

		String msg = "";
	  	URL url = null;
	    HttpURLConnection httpc = null;
	    BufferedReader input = null;

      try{
        url = new URL("http://localhost:8080/benchmarkingServer/getInfo");

        httpc = (HttpURLConnection) url.openConnection();

        httpc.setConnectTimeout(60000);
        httpc.setDoOutput( true );
        httpc.setInstanceFollowRedirects( false );
        httpc.setRequestMethod( "POST" );
        httpc.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
        httpc.setRequestProperty( "charset", "utf-8");
        httpc.setRequestProperty( "Content-Length", Integer.toString( outputMessage.length() ));
        httpc.setUseCaches( false );
        OutputStreamWriter writer = new OutputStreamWriter(httpc.getOutputStream());

        writer.write(outputMessage);

        InputStream in = url.openStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder result = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            result.append(line);
        }
        msg = msg.concat(result.toString());
        writer.flush();
        writer.close();
        httpc.disconnect();
      }catch (IOException ex){
    	  ex.printStackTrace();
      }
      
      return msg;
	}
}
