package com.siemens.bam.adm.studio.server;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletInfoServlet extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		//ClientProps cp = new ClientProps();
		//cp.createFile();
		String name2=request.getLocalName();
		String millis = new Long(System.currentTimeMillis()).toString();
		String name = name2.concat(millis);
		String ipAddr = request.getRemoteAddr();
		HttpSession session = request.getSession();
		if(session != null){
			request.getRequestDispatcher("link1.html").include(request, response);

			//cp.addClientInfo(name, ipAddr);
			//cp.createFile();

			//cp.writePropsFile();
			out.print(ipAddr + "  " + name);

			/*int timeout=500;
			   for (int i=1;i<255;i++){
			       String host="192.168.3" + "." + i;
			       if (InetAddress.getByName(host).isReachable(timeout)){
			           out.print("\n" + host + " is reachable");
			       }
			   }*/

			/*try {
		        Enumeration nis = NetworkInterface.getNetworkInterfaces();
		        while(nis.hasMoreElements())
		        {
		            NetworkInterface ni = (NetworkInterface) nis.nextElement();
		            Enumeration ias = ni.getInetAddresses();
		            while (ias.hasMoreElements())
		            {
		                InetAddress ia = (InetAddress) ias.nextElement();
		                out.print("\n" + ia.getHostAddress());
		            }

		        }
		    } catch (SocketException ex) {

		    }*/


		}else{

		}
		out.close();
	}
}
