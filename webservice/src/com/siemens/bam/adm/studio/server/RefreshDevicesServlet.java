package com.siemens.bam.adm.studio.server;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.siemens.bam.adm.studio.server.utils.ClientProps;

public class RefreshDevicesServlet extends HttpServlet {
	
protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		ClientProps clientProps = new ClientProps();
		clientProps.readPropsFile();
		String callBackJavaScripMethodName = request.getParameter("callback");
		out.print(callBackJavaScripMethodName + "(");
		out.print("{");
		out.print(clientProps.pingClients());
		clientProps.writePropsFile();
		out.print("}");
		out.print(")");
		out.close();
	}

}
