package com.siemens.bam.adm.studio.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import com.siemens.bam.adm.studio.server.utils.GlobalData;

public class GetTestResultServlet extends HttpServlet {
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		String callBackJavaScripMethodName = request.getParameter("callback");
		
		out.print(callBackJavaScripMethodName + "(");
		 //out.print("{");
		 Enumeration propertyNames = request.getParameterNames();
		 //out.print("\"result\" : {");
		int count = 0;
			//out.print(propertyNames);
		int length = GlobalData.connectedClients.length();
		// read clients info and send connection config to all clients
		
		out.println(GlobalData.queryResult);
		
		  /*while(propertyNames.hasMoreElements()){ 
		  
			  String propName = (String) propertyNames.nextElement();
			  out.print( propName + "     ");
			  String[] paramValues = request.getParameterValues(propName);
			  
			  for(int i=0;i<paramValues.length;i++){
				  out.print("  " + paramValues[i] + " ");
			  } 
		  }*/
		 
		 //out.print("}");
		 //out.print("}");
		 out.print(")");
		 GlobalData.queryResult="";
		 GlobalData.connectedClients = "";
		
		out.close();
	}

}
