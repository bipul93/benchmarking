package com.siemens.bam.adm.studio.server.utils;

public class ClientsInfo {
	String hostName;
	String IPAddress;
	boolean activeState;
	
	public void setHostName(String hostname){
		this.hostName = hostname;
	}
	
	public void setIPAddress(String ipAddress){
		this.IPAddress = ipAddress;
	}
	
	public void setActiveState(boolean state){
		this.activeState = state;
	}
	
	public String getHostName(){
		return hostName;
	}
	
	public String getIPAddress(){
		return IPAddress;
	}
	
	public boolean getActiveState(){
		return activeState;
	}
	
	public String readIP(String val){
		String ret = "";
		String [] values = val.split(":");
		String newString = Integer.toString(values.length);
		if(values.length > 2){
			String [] ipVal = values[2].split(",");
			
			ret = ipVal[0].substring(2, ipVal[0].length() - 1);
		}else {
			ret = "";
		}
		
		return ret;
	}
	
	@Override
	public String toString() {
		return "\"" + hostName + "\" : { \"ip\" : \"" + IPAddress + "\", \"active\" : \"" + activeState + "\" }";
	}
	
}
