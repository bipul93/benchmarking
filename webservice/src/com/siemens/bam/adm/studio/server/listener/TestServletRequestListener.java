package com.siemens.bam.adm.studio.server.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;


@WebListener
public class TestServletRequestListener implements ServletRequestListener {

	ServletContext context = null;
	public void requestDestroyed(ServletRequestEvent servletRequestEvent) {
		// TODO Auto-generated method stub

	}

	public void requestInitialized(ServletRequestEvent servletRequestEvent) {
		// TODO Auto-generated method stub
		context = servletRequestEvent.getServletContext();
		context.setAttribute("IPAddrs",servletRequestEvent.getServletRequest().getRemoteAddr());
		//context.setAttribute("IPAddrs",servletRequestEvent.getServletRequest().getParameter(arg0));
	}

}
