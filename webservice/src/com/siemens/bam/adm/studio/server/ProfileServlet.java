package com.siemens.bam.adm.studio.server;


import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
public class ProfileServlet extends HttpServlet {
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		
		HttpSession session=request.getSession(false);
		if(session!=null){
			request.getRequestDispatcher("link1.html").include(request, response);
			String name=(String)session.getAttribute("name");
			out.print("Hello, "+name+" Welcome to Profile");
			request.getRequestDispatcher("send_msg.html").include(request, response);
		}
		else{
			request.getRequestDispatcher("link2.html").include(request, response);
			out.print("Please login first");
			request.getRequestDispatcher("login.html").include(request, response);
		}
		out.close();
	}
}
