package com.siemens.bam.adm.studio.server.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CreateBatchFile {
	private String filePath = "command.txt";
	private String command = "";
	
	public void setCommand(String cmd){
		this.command = cmd;
	}
	
	public String getCommand(){
		return command;
	}
	
	public void createFile() throws IOException{
		File myFile = new File(System.getProperty("user.home"), filePath);
		if(myFile.isFile()){
			System.out.println("File Exists");
		}else{
			myFile.createNewFile();
		}
	}
	
	public void writeCommandFile() throws IOException{
		createFile();
		File myFile = new File(System.getProperty("user.home"), filePath);
		FileWriter fw = new FileWriter(myFile.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		String content = "tagdatasupercolumn rowkey [ff84d0f5-d8bf-4c96-995d-97ed10a98f0b,ff51065d-c297-4602-a2b2-bb6a164dceaa] begin 2016-06-01T00:00:00.000 end 2016-06-01T23:15:00.000";
        // write in file
        bw.write(content);
        // close connection
        bw.close();
	}
																								
}
